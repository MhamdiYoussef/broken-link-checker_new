<?php
/**
 * The base class for link container managers.
 *
 * Sub-classes should override at least the get_containers() and resynch() methods.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Manager;

use BLC\Abstracts\Module as Module;

/**
 * Container manager class.
 *
 * @since  2.0.0
 * @access public
 */
class ContainerManager extends Module {

	/**
	 * Plugin configuration.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $container_type = '';

	/**
	 * Fields.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $fields = array();

	/**
	 * Fields.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $container_class_name = 'blcContainer';

	/**
	 * Fields.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $updating_urls = '';

	/**
	 * Do whatever setup necessary that wasn't already done in the constructor.
	 *
	 * This method was added so that sub-classes would have something "safe" to
	 * over-ride without having to deal with PHP4/5 constructors.
	 *
	 * @return void
	 */
	public function init() {
		parent::init();
		$this->container_type = $this->module_id;
		// Sub-classes might also use it to set up hooks, etc.
	}

	/**
	 * Instantiate a link container.
	 *
	 * @param array $container An associative array of container data.
	 * @return blcContainer
	 */
	public function get_container( $container ) {
		$container['fields'] = $this->get_parseable_fields();
		$container_obj       = new $this->container_class_name( $container );
		return $container_obj;
	}

	/**
	 * Instantiate multiple containers of the container type managed by this class and optionally
	 * pre-load container data used for display/parsing.
	 *
	 * Sub-classes should, if possible, use the $purpose argument to pre-load any extra data required for
	 * the specified task right away, instead of making multiple DB roundtrips later. For example, if
	 * $purpose is set to the BLC_FOR_DISPLAY constant, you might want to preload any DB data that the
	 * container will need in blcContainer::ui_get_source().
	 *
	 * @see blcContainer::make_containers()
	 * @see blcContainer::ui_get_source()
	 * @see blcContainer::ui_get_action_links()
	 *
	 * @param array  $containers Array of assoc. arrays containing container data.
	 * @param string $purpose An optional code indicating how the retrieved containers will be used.
	 * @param bool   $load_wrapped_objects Preload wrapped objects regardless of purpose.
	 *
	 * @return array of blcContainer indexed by "container_type|container_id"
	 */
	public function get_containers( $containers, $purpose = '', $load_wrapped_objects = false ) {
		return $this->make_containers( $containers );
	}

	/**
	 * Instantiate multiple containers of the container type managed by this class
	 *
	 * @param array $containers Array of assoc. arrays containing container data.
	 * @return array of blcContainer indexed by "container_type|container_id"
	 */
	public function make_containers( $containers ) {
		$results = array();
		foreach ( $containers as $container ) {
			$key             = $container['container_type'] . '|' . $container['container_id'];
			$results[ $key ] = $this->get_container( $container );
		}
		return $results;
	}

	/**
	 * Create or update synchronization records for all containers managed by this class.
	 *
	 * Must be over-ridden in subclasses.
	 *
	 * @param bool $forced If true, assume that all synch. records are gone and will need to be recreated from scratch.
	 * @return void
	 */
	public function resynch( $forced = false ) {
		trigger_error( 'Function blcContainerManager::resynch() must be over-ridden in a sub-class', E_USER_ERROR ); //phpcs:ignore
	}

	/**
	 * Resynch when activated.
	 *
	 * @uses blcContainerManager::resynch()
	 *
	 * @return void
	 */
	public function activated() {
		$this->resynch();
		blc_got_unsynched_items();
	}

	/**
	 * Get a list of the parseable fields and their formats common to all containers of this type.
	 *
	 * @return array Associative array of formats indexed by field name.
	 */
	public function get_parseable_fields() {
		return $this->fields;
	}

	/**
	 * Get the message to display after $n containers have been deleted.
	 *
	 * @param int $n Number of deleted containers.
	 * @return string A delete confirmation message, e.g. "5 posts were moved to trash"
	 */
	public function ui_bulk_delete_message( $n ) {
		return sprintf(
			_n(
				"%1\$d '%2\$s' has been deleted",
				"%1\$d '%2\$s' have been deleted",
				$n,
				'broken-link-checker'
			),
			$n,
			$this->container_type
		);
	}

	/**
	 * Get the message to display after $n containers have been moved to the trash.
	 *
	 * @param int $n Number of trashed containers.
	 * @return string A delete confirmation message, e.g. "5 posts were moved to trash"
	 */
	public function ui_bulk_trash_message( $n ) {
		return $this->ui_bulk_delete_message( $n );
	}
}
