<?php
/**
 * Links class.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Plugin Class handling links.
 *
 * @since  2.0.0
 * @access public
 */
class TransacationManager {

	/**
	 * Is transaction started.
	 *
	 * @since  2.0.0
	 * @access private
	 * @var    bool
	 */
	private $is_transaction_started = false;

	/**
	 * Instance of the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    bool
	 */
	private static $instance;

	/**
	 * Start the transcation manager.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function start() {
		global $wpdb;

		if ( ! $this->is_transaction_started ) {
			$wpdb->query( 'BEGIN' );//phpcs:ignore
			$this->is_transaction_started = true;
		}
	}

	/**
	 * Commit the transaction.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function commit() {
		global $wpdb;
		$blclog = \BLC\blc_logger();

		$blclog->debug( 'Starting DB commit.' );

		$this->start();

		try {
			$wpdb->query( 'COMMIT' );//phpcs:ignore
			$blclog->debug( 'Commit executed.' );
			$this->is_transaction_started = false;
		} catch ( Exception $e ) {
			$wpdb->query( 'ROLLBACK' );//phpcs:ignore
			$blclog->debug( 'Commit failed; rollback.' );
			$this->is_transaction_started = false;
		}
	}

	/**
	 * Start the transcation manager.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return object Class instance.
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new TransactionManager();
		}
		return self::$instance;
	}
}
