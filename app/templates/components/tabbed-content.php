<?php
/**
 * Tabbed content template. Render with $this->render_tabbed_content( $tabs ) by passing
 * $tabs as array where key is tab id, and value is array containing keys:
 * title - translatable title that will be displayed on tab.
 * template - template path relative to app/templates.
 *
 * @package templates
 */

?>
<section class="sui-row-with-sidenav">

	<!-- Navigation -->
	<div role="navigation" class="sui-sidenav">

		<ul class="sui-vertical-tabs sui-sidenav-hide-md">
			<?php
			foreach ( $tabs as $index => $blctab ) :
				$c = '';
				if ( 0 === $index ) {
					$c = 'current';
				}
				?>
				<li class="sui-vertical-tab <?php echo esc_attr( $c ); ?> ">
					<a href="#" role="button" data-tab="tab-<?php echo esc_attr( $blctab->get_slug() ); ?>">
						<?php echo esc_html( $blctab->get_title() ); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>

		<div class="sui-sidenav-hide-lg">
			<select class="sui-mobile-nav">
				<?php foreach ( $tabs as $index => $blctab ) : ?>
				<option
					value="<?php echo esc_attr( $blctab->get_slug() ); ?>"
					<?php
					if ( 0 === $index ) {
						echo "selected= 'selected'"; }
					?>
				>
					<?php echo esc_html( $blctab->get_title() ); ?>
				</option>
				<?php endforeach; ?>
			</select>
		</div>

	</div>

<?php foreach ( $tabs as $index => $blctab ) : ?>
	<?php
				$d = '';
	if ( 0 !== $index ) {
		$d = 'display:none';
	}
	?>
	<div class='sui-box blc-tab-content' id='tab-<?php echo esc_attr( $blctab->get_slug() ); ?>' style="<?php echo esc_attr( $d ); ?>">
	<?php $blctab->render(); ?>
</div>
<?php endforeach; ?>
</section>
