<?php
/**
 * Notifications Links page.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

$schedules        = wp_get_schedules();
$schedule_display = $send_emails_schedule;
if ( isset( $schedules[ $send_emails_schedule ] ) ) {
	$schedule_display = strtolower( $schedules[ $send_emails_schedule ]['display'] );
}
?>
<div class="sui-box blc-issue-notification" data-tab="header">

    <div class="sui-box-header">
        <h2 class="sui-box-title"><?php esc_html_e( 'Notifications', 'broken-link-checker' ); ?></h2>
    </div>

    <div class="sui-box-body">
        <p><?php esc_html_e( 'Enable schedule email notifications when a link needs fixing, and send enail reports to as many recipients as you like', 'broken-link-checker' ); ?>
        </p>
        <?php if ( $send_email_notifications && $send_emails_schedule ) : ?>
        <div class="sui-notice sui-notice-success sui-active">
            <div class="sui-notice-content">
                <div class="sui-notice-message">
                    <i class="sui-notice-icon sui-icon-check-tick sui-success" aria-hidden="true"></i>
                    <?php // translators: %s - sending frequency, %d - number of recipients. ?>
                    <p><?php echo esc_html( sprintf( esc_html( _n( 'Automatic notifications are enabled and sending %1$s to %2$d recipient.', 'Automatic notifications are enabled and sending %1$s to %2$d recipients.', $notification_recipient_count, 'broken-link-checker' ) ), $schedule_display, $notification_recipient_count ) ); ?>
                    </p>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <form action="<?php echo esc_attr( $page_url ); ?>" method="POST">
        <div class="sui-box-body sui-upsell-items">
            <div class="sui-box-settings-row">
                <div class="sui-box-settings-col-1">
                    <span
                        class="sui-settings-label-with-tag"><?php esc_html_e( 'Email notifications', 'broken-link-checker' ); ?></span>
                    <span id="super-compression" class="sui-description">
                        <?php esc_html_e( 'Enabling this option will ensure you don\'t need to check in to see that all your links are ok.', 'broken-link-checker' ); ?>
                    </span>
                </div>
                <div class="sui-box-settings-col-2">
                    <div>
                        <label for="email-notification" class="sui-toggle">
                            <?php
								$send_email_check = '';
							if ( $send_email_notifications ) {
								$send_email_check = 'checked=checked';
							}
							?>
                            <input type="checkbox" name="send_email_notifications" id="email-notification"
                                <?php echo esc_html( $send_email_check ); ?> />
                            <span class="sui-toggle-slider" aria-hidden="true"></span>
                        </label>
                        <label
                            class="sui-toggle-label"><?php esc_html_e( 'Enable automatic email notifications', 'broken-link-checker' ); ?></label>
                    </div>
                    <div class="sui-box-body blc-border-box">
                        <div class="blc-recipient-area">
                            <label class="sui-label"><?php esc_html_e( 'Recipients', 'broken-link-checker' ); ?></label>
                            <!-- for the design	checking -->
                            <?php
								// $notification_email_addresses = array (
								// 	"user_one"  => array("Bob", "bob@example.com"),
								// 	"user_two" => array("Arny", "arny@example.com"),
								// 	"user_three"   => array("Alex", "alex@example.com")
								// );
							?>
                            <!-- for the design	End -->
                            <?php foreach ( $notification_email_addresses as $email ) : ?>
                            <div id="demo-notice-inline-dismiss-tooltip-1" class="sui-notice">
                                <div class="sui-notice-content">
                                    <div class="sui-notice-message">
                                        <i class="sui-notice-icon sui-icon-profile-male sui-md" aria-hidden="true"></i>
                                        <p>
                                            <span
                                                class="blc-recipient-title"><?php echo esc_html( $email[0] ); ?></span>
                                            <span
                                                class="blc-recipient-email"><?php echo esc_html( $email[1] ); ?></span>
                                        </p>
                                    </div>
                                    <div class="sui-notice-actions">
                                        <div class="sui-tooltip"
                                            data-tooltip="<?php esc_html_e( 'Delete recipient', 'broken-link-checker' ); ?>">
                                            <button class="sui-button-icon"
                                                data-notice-close="demo-notice-inline-dismiss-tooltip-1">
                                                <i class="sui-icon-trash" aria-hidden="true"></i>
                                                <span
                                                    class="sui-screen-reader-text"><?php esc_html_e( 'Delete recipient', 'broken-link-checker' ); ?></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <button role="button" class="sui-button sui-button-ghost" data-notice-open="save_changes"
                                data-notice-message="<p><?php esc_html_e( 'Andy has been added as a recipient, but you need to save changes below to make this live.', 'broken-link-checker' ); ?></p>"><i
                                    class="sui-icon-plus" aria-hidden="true"></i>
                                <?php esc_html_e( 'ADD RECIPIENT', 'broken-link-checker' ); ?>
                            </button>
                        </div>
                        <div class="blc-schedule-form">
                            <div class="sui-form-field">

                            </div>
                            <div class="sui-form-field">
                                <label for="email-schedule" id="label-email-schedule"
                                    class="sui-label"><?php esc_html_e( 'Schedule', 'broken-link-checker' ); ?></label>
                                <select id="email-schedule" name="send_emails_schedule" class="sui-select"
                                    aria-labelledby="label-email-schedule">
                                    <option value="" <?php if ( ! $send_emails_schedule ) : ?> selected <?php endif; ?>>
                                        <?php esc_html_e( 'None', 'broken-link-checker' ); ?></option>
                                    <?php
								foreach ( $schedules as $key => $schedule ) :
									$selected = '';
									if ( $key === $send_emails_schedule ) {
										$selected = 'selected';
									}
									?>
                                    <option <?php echo esc_html( $selected ); ?>
                                        value="<?php echo esc_attr( $key ); ?>">
                                        <?php echo esc_html( $schedule['display'] ); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="sui-form-field">
                                <label for="email-schedule-time" id="label-email-schedule-time"
                                    class="sui-label"><?php esc_html_e( 'Time of day', 'broken-link-checker' ); ?></label>
                                <select id="email-schedule-time" name="send_emails_schedule_time" class="sui-select"
                                    aria-labelledby="label-email-schedule-time">
                                    <option> <?php echo esc_html( "03:30 AM" ); ?></option>
                                    <option> <?php echo esc_html( "08:30 AM" ); ?></option>
                                    <option> <?php echo esc_html( "03:30 PM" ); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="sui-box-settings-row">
                <div class="sui-box-settings-col-1">
                    <span
                        class="sui-settings-label-with-tag"><?php esc_html_e( 'Authors notifications', 'broken-link-checker' ); ?></span>
                    <span id="super-compression" class="sui-description">
                        <?php esc_html_e( 'Send authors email notifications about broken links in their posts.', 'broken-link-checker' ); ?>
                    </span>
                </div>
                <div class="sui-box-settings-col-2">
                    <div>
                        <label for="author-notification" class="sui-toggle">
                            <?php
								$authors_checked = '';
							if ( $send_authors_email_notifications ) {
								$authors_checked = 'checked=checked';
							}
							?>
                            <input type="checkbox" name="send_authors_email_notifications" id="author-notification"
                                <?php echo esc_html( $authors_checked ); ?> />
                            <span class="sui-toggle-slider" aria-hidden="true"></span>
                        </label>
                        <label
                            class="sui-toggle-label"><?php esc_html_e( 'Enable authors email notifications', 'broken-link-checker' ); ?></label>
                    </div>
                    <span class="sui-description author-notification-desc">
                        <?php esc_html_e( 'Notify authors when a broken link is discovered in one of their posts.', 'broken-link-checker' ); ?>
                    </span>
                </div>
            </div>
        </div>

        <div class="sui-box-footer">
            <div class="sui-actions-right">
                <button role="button" name="save-settings" value="1" class="sui-button sui-button-blue">
                    <i class="sui-icon-save" aria-hidden="true"></i>
                    <?php esc_html_e( 'SAVE CHANGES', 'broken-link-checker' ); ?></button>
            </div>
        </div>
        <input type="hidden" name="links-tab" value="notifications">
        <?php wp_nonce_field( 'wdblc_links_notifications' ); ?>
    </form>

    <div class="sui-floating-notices">
        <div role="alert" id="save_changes" class="sui-notice" aria-live="assertive">
            <!-- Nothing should be placed here -->
        </div>
    </div>
</div>