<?php
/**
 * All Links page.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>
<div class="sui-box">
	<div class="sui-box">
		<div class="sui-box-header">
			<div class="sui-actions-left">
				<span class="sui-box-title"><?php esc_html_e( 'All checked links', 'broken-link-checker' ); ?></span>
			</div>
			<?php if ( $filter['count'] ) : ?>
				<div class="sui-actions-right blc-filter-horizontal">

					<div class="sui-form-field blc-show-inline">

						<div class="blc-inline-elm blc-filter-label">
							<label for="filter-id" id="label-filter-id" class="sui-label"><?php esc_html_e( 'Filter', 'broken-link-checker' ); ?></label>
						</div>

						<div class="blc-inline-elm blc-filter-dropdown">
							<select id="filter-id" aria-labelledby="label-filter-id" aria-describedby="description-filter-id" class="sui-select-sm">
								<option><?php esc_html_e( 'All', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Broken', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Warnings', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Redirects', 'broken-link-checker' ); ?></option>
							</select>
						</div>

						<div class="blc-inline-elm blc-filter-funnel">
							<div class="sui-pagination-wrap blc-pagination-wrap" id="pagination-wrap">
								<button class="sui-button-icon sui-button-outlined sui-pagination-open-filter sui-tooltip" data-tooltip="<?php esc_html_e( 'Filter', 'broken-link-checker' ); ?>" aria-label="Filter pagination" id="blc-advance-all-filter-open"><i class="sui-icon-filter" aria-hidden="true"></i></button>
							</div>
						</div>

					</div>

				</div>
			<?php endif; ?>
		</div>

		<?php if ( $filter['count'] ) : ?>
			<div class="sui-box-body blc-advance-issue-filter" id="blc-advance-all-filter">

				<div class="sui-row">
					<div class="sui-col">
						<div class="sui-form-field">
							<label for="header_issue_filter_link_text" id="label-header_issue_filter_link_text" class="sui-label"><?php esc_html_e( 'Link Text', 'broken-link-checker' ); ?></label>
							<input
									placeholder="<?php esc_attr_e( 'Search', 'broken-link-checker' ); ?>"
									id="header_issue_filter_link_text"
									class="sui-form-control"
									aria-labelledby="label-header_issue_filter_link_text"
									aria-describedby="error_header_issue_filter_link_text description_header_issue_filter_link_text"
							/>
						</div>
					</div>
					<div class="sui-col">
						<div class="sui-form-field">
							<label for="header_issue_filter_url" id="label-header_issue_filter_url" class="sui-label"><?php esc_html_e( 'URL', 'broken-link-checker' ); ?></label>
							<input
									placeholder="<?php esc_attr_e( 'Search', 'broken-link-checker' ); ?>"
									id="header_issue_filter_url"
									class="sui-form-control"
									aria-labelledby="label-header_issue_filter_url"
									aria-describedby="error_header_issue_filter_url description_header_issue_filter_url"
							/>
						</div>
					</div>
					<div class="sui-col">
						<div class="sui-form-field">
							<label for="header_issue_filter_http_code" id="label-header_issue_filter_http_code" class="sui-label"><?php esc_html_e( 'HTTP Code', 'broken-link-checker' ); ?></label>
							<input
									placeholder="<?php esc_attr_e( 'Search', 'broken-link-checker' ); ?>"
									id="header_issue_filter_http_code"
									class="sui-form-control"
									aria-labelledby="label-header_issue_filter_http_code"
									aria-describedby="error_header_issue_filter_http_code description_header_issue_filter_http_code"
							/>
						</div>
					</div>
				</div>

				<div class="sui-row">
					<div class="sui-col">
						<div class="sui-form-field">
							<label for="header_issue_filter_link_status" id="label_header_issue_filter_link_status" class="sui-label"><?php esc_html_e( 'Link status', 'broken-link-checker' ); ?></label>
							<select id="header_issue_filter_link_status"
									aria-labelledby="label_header_issue_filter_link_status"
									aria-describedby="description_header_issue_filter_link_status">
								<option><?php esc_html_e( 'All', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Warnings', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Broken', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Redirects', 'broken-link-checker' ); ?></option>
							</select>
						</div>
					</div>
					<div class="sui-col">
						<div class="sui-form-field">
							<label for="header_issue_filter_link_used_in" id="label_header_issue_filter_link_used_in" class="sui-label"><?php esc_html_e( 'Link used in', 'broken-link-checker' ); ?></label>
							<select id="header_issue_filter_link_used_in"
									aria-labelledby="label_header_issue_filter_link_used_in"
									aria-describedby="description_header_issue_filter_link_used_in">
								<option><?php esc_html_e( 'All', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Pages', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Posts', 'broken-link-checker' ); ?></option>
								<option><?php esc_html_e( 'Comments', 'broken-link-checker' ); ?></option>
							</select>
						</div>
					</div>
					<div class="sui-col">
						<div class="sui-form-field">
							<label for="header_issue_filter_link_type" id="label_header_issue_filter_link_type" class="sui-label"><?php esc_html_e( 'Link type', 'broken-link-checker' ); ?></label>
							<select id="header_issue_filter_link_type"
									aria-labelledby="label_header_issue_filter_link_type"
									aria-describedby="description_header_issue_filter_link_type">
								<option>All</option>
								<option>Option name</option>
								<option>Option name</option>
							</select>
						</div>
					</div>
				</div>
				<hr>
				<div class="sui-row">
					<div class="sui-actions-left">
						<label for="header_issue_filter_save_search" class="sui-checkbox">
							<input type="checkbox" id="header_issue_filter_save_search" aria-labelledby="label_header_issue_filter_save_search"/>
							<span aria-hidden="true"></span>
							<span id="label_header_issue_filter_save_search"><?php esc_html_e( 'Save Search as filter', 'broken-link-checker' ); ?></span>
						</label>
						<input
								placeholder="<?php esc_attr_e( 'Filter name', 'broken-link-checker' ); ?>"
								id="header_issue_filter_save_search_name"
								class="sui-form-control sui-input-sm"
						/>
					</div>
					<div class="sui-actions-right">
						<button role="button" class="sui-button sui-button-ghost"><?php esc_html_e( 'CANCLEr', 'broken-link-checker' ); ?></button>
						<button role="button" class="sui-button sui-button-blue"><?php esc_html_e( 'SEARCH LINKS', 'broken-link-checker' ); ?></button>
					</div>
				</div>
				<hr class="search_filter_ending_hr">
			</div>
		<?php endif; ?>
		<div class="sui-box-body">

			<p><?php esc_html_e( 'Here\'s a list of issues with your website\'s in-page links. We recommend you review and fix each to avoid any SEO issues.', 'broken-link-checker' ); ?></p>
			<?php if ( 0 === $filter['count'] ) : ?>
				<div class="sui-notice sui-notice-success sui-active">
					<div class="sui-notice-content">
						<div class="sui-notice-message">
							<i class="sui-notice-icon sui-icon-check-tick sui-success" aria-hidden="true"></i>
							<p><?php esc_html_e( 'Well, turns out you haven’t any issues yet - keep up the good fight.', 'broken-link-checker' ); ?></p>
						</div>
					</div>
				</div>
			<?php else : ?>
				<div class="sui-row">
					<div class="sui-col">
						<div class="blc-filter-horizontal">
							<div class="sui-form-field blc-show-inline">
								<div class="blc-inline-elm">
									<label for="filter-top-id" class="sui-checkbox">
										<input type="checkbox" id="filter-top-id" aria-labelledby="label-filter-top-id" />
										<span aria-hidden="true"></span>
									</label>
								</div>
								<div class="blc-inline-elm blc-filter-dropdown">
									<select id="blc-all-filter-id" aria-labelledby="label-blc-all-filter-id" aria-describedby="description-blc-all-filter-id" class="sui-select-sm">
										<option value="recheck" data-icon="refresh"><?php esc_html_e( 'Recheck', 'broken-link-checker' ); ?></option>
										<option value="edit_url" data-icon="pencil"><?php esc_html_e( 'Edit URL', 'broken-link-checker' ); ?></option>
										<option value="ignore" data-icon="eye-hide"><?php esc_html_e( 'Ignore', 'broken-link-checker' ); ?></option>
										<option value="unlink" data-icon="unlink"><?php esc_html_e( 'Unlink', 'broken-link-checker' ); ?></option>
										<option value="trash" data-icon="trash"><?php esc_html_e( 'Move to bin', 'broken-link-checker' ); ?></option>
									</select>
								</div>
								<div class="blc-inline-elm">
									<button role="button" class="sui-button" id="filter-apply-button"><?php esc_html_e( 'APPLY', 'broken-link-checker' ); ?></button>
								</div>

							</div>
						</div>
					</div>

					<div class="sui-actions-right">
						<div class="sui-pagination-wrap">
							<!-- ELEMENT: Number of Results. -->
							<span class="sui-pagination-results"><?php echo esc_html( sprintf( '%s %s', $filter['count'], __( 'results', 'broken-link-checker' ) ) ); ?></span>
							<!-- ELEMENT: List of Pages. -->
							<ul class="sui-pagination">
								<!-- BUTTON: Previous page. -->
								<?php
									$prev_page     = $filter['page'] - 1;
									$prev_disabled = '';
								if ( 1 > $prev_page ) {
									$prev_page     = '';
									$prev_disabled = 'disabled';
								} else {
									$prev_page = '?page=brokenlinkchecker-links&tab=all&paged=' . $prev_page;
								}
								?>
								<li>
									<a href="<?php echo esc_attr( $prev_page ); ?>" role="button" <?php echo esc_attr( $prev_disabled ); ?>>
										<i class="sui-icon-chevron-left" aria-hidden="true"></i>
										<span class="sui-screen-reader-text"><?php esc_html_e( 'Go to previous page', 'broken-link-checker' ); ?></span>
									</a>
								</li>

								<?php for ( $i = 1; $i <= $filter['max_pages']; $i++ ) : ?>
									<?php
									$active_class = '';
									if ( $filter['page'] === $i ) {
										$active_class = 'class="sui-active"';
									}
									?>
								<li <?php echo esc_html( $active_class ); ?>>
									<a href="?page=brokenlinkchecker-links&tab=all&paged=<?php echo esc_attr( $i ); ?>" role="button"><?php echo esc_html( $i ); ?></a>
								</li>
								<?php endfor; ?>

								<!-- BUTTON: Next page. -->
								<?php
									$next_page     = $filter['page'] + 1;
									$next_disabled = '';
								if ( $next_page > $filter['max_pages'] ) {
									$next_page     = '';
									$next_disabled = 'disabled';
								} else {
									$next_page = '?page=brokenlinkchecker-links&tab=all&paged=' . $next_page;
								}
								?>
								<li>
									<a href="<?php echo esc_attr( $next_page ); ?>" role="button" <?php echo esc_attr( $next_disabled ); ?>>
										<i class="sui-icon-chevron-right" aria-hidden="true"></i>
										<span class="sui-screen-reader-text"><?php esc_html_e( 'Go to next page', 'broken-link-checker' ); ?></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="sui-row blc-bulk-url-edit-wrap">
					<div class="sui-col">
						<h4><?php esc_html_e( 'Edit URL', 'broken-link-checker' ); ?></h4>
						<div class="blc-bulk-url-edit">
							<div class="sui-form-field">
								<label for="blc-bulk-url-edit-find" id="label-blc-bulk-url-edit-find" class="sui-label">
									<?php esc_html_e( 'Find', 'broken-link-checker' ); ?>
								</label>
								<input
										placeholder="<?php esc_attr_e( 'E.g. wordpress.com', 'broken-link-checker' ); ?>"
										id="blc-bulk-url-edit-find"
										class="sui-form-control"
										aria-labelledby="label-blc-bulk-url-edit-find"
										aria-describedby="description-blc-bulk-url-edit-find"
								/>
								<span id="description-blc-bulk-url-edit-find" class="sui-description">
								<?php esc_html_e( 'Enter the broken link you want to fix', 'broken-link-checker' ); ?>
							</span>
							</div>
							<div class="sui-form-field">
								<label for="blc-bulk-url-edit-find" id="label-blc-bulk-url-edit-find" class="sui-label">
									<?php esc_html_e( 'Replace with', 'broken-link-checker' ); ?>
								</label>
								<input
										placeholder="<?php esc_attr_e( 'E.g. https://www.wordpress.com', 'broken-link-checker' ); ?>"
										id="blc-bulk-url-edit-find"
										class="sui-form-control"
										aria-labelledby="label-blc-bulk-url-edit-find"
										aria-describedby="description-blc-bulk-url-edit-find"
								/>
								<span id="description-blc-bulk-url-edit-find" class="sui-description">
								<?php esc_html_e( 'Write the entire url. For example https://www.wordpress.com', 'broken-link-checker' ); ?>
							</span>
							</div>
						</div>
					</div>
				</div>

			<?php endif; ?>

		</div>

		<?php if ( $filter['count'] ) : ?>

			<table class="sui-table sui-accordion sui-accordion-flushed issue-table">

				<thead>
				<tr>
					<th colspan="2">
						<?php esc_html_e( 'Link text', 'broken-link-checker' ); ?>
					</th>
					<th>
						<?php esc_html_e( 'Status', 'broken-link-checker' ); ?>
					</th>
					<th colspan="2">
						<?php esc_html_e( 'URL', 'broken-link-checker' ); ?>
					</th>
					<th colspan="2">
						<?php esc_html_e( 'Redirect', 'broken-link-checker' ); ?>
					</th>
					<th colspan="2">
						<?php esc_html_e( 'Source', 'broken-link-checker' ); ?>
					</th>
				</tr>
				</thead>

				<tbody>

				<?php foreach ( $links as $link ) : ?>

					<?php
					$issue       = $link->url;
					$status_text = $link->status_text ? $link->status_text : 'OK';
					$status_code = $link->http_code ? $link->http_code : '404';
					if ( $link->broken ) {
						$err_type = 'error';
					} elseif ( $link->warning ) {
						$err_type = 'warning';
					} else {
						$err_type = 'ok';
					}

					// Retrieve link instances to parse link text.
					$instances = $link->get_instances();
					$link_text = '';

					$can_edit_text           = false;
					$can_edit_url            = false;
					$editable_link_texts     = array();
					$non_editable_link_texts = array();

					foreach ( $instances as $instance ) {
						if ( $instance->is_link_text_editable() ) {
							$can_edit_text                               = true;
							$editable_link_texts[ $instance->link_text ] = true;
						} else {
							$non_editable_link_texts[ $instance->link_text ] = true;
						}

						if ( $instance->is_url_editable() ) {
							$can_edit_url = true;
						}
					}

					$link_texts     = $can_edit_text ? $editable_link_texts : $non_editable_link_texts;
					$data_link_text = '';
					if ( 1 === count( $link_texts ) ) {
						// All instances have the same text - use it.
						$link_text = key( $link_texts );
					}
					if ( '' === $link_text ) {
						$link_text = $link->url;
					}
					?>

					<tr class="wdblc-link sui-accordion-item sui-<?php echo esc_attr( $err_type ); ?>">

						<td class="sui-table-item-title" colspan="2">
							<div class="sui-form-field">
								<label for="<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>" class="sui-checkbox">

									<input
											type="checkbox"
											id="<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
											aria-labelledby="label-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
									/>
									<input type="hidden" name="wdblc-link-id" value="<?php echo esc_attr( $link->link_id ); ?>">

									<span aria-hidden="true"></span>

									<span id="label-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>">
										<?php echo esc_html( $issue ); ?>
								</span>
								</label>
							</div>

						</td>

						<td>
						<span class="sui-tag sui-tooltip" data-tooltip="<?php echo esc_html( $status_text ); ?>">
							<?php echo esc_html( $status_code ); ?>
						</span>
						</td>

						<td colspan="2">
						<span class="sui-description sui-tooltip" data-tooltip="<?php echo esc_html( $issue ); ?>">
							<?php echo esc_html( $link->url ); ?>
						</span>
						</td>

						<td colspan="2">

						</td>

						<?php
							$instance = reset( $instances );
							$view_url = $instance->get_view_url();
						?>
						<td>
						<span class="sui-description">
							<?php echo esc_html( $instance->ui_get_source_short() ); ?>
						</span>
						</td>

						<td>
							<div class="sui-dropdown sui-accordion-item-action">
								<a href="#" class="sui-dropdown-anchor sui-icon-widget-settings-config sui-button-icon" aria-label="Open Item Settings"></a>
								<ul>
									<li><a class="wdblc-action-edit" href="#"><i class="sui-icon-pencil" aria-hidden="true"></i> <?php esc_html_e( 'Edit URL', 'broken-link-checker' ); ?></a></li>
									<li><a class="wdblc-action-recheck" href="#"><i class="sui-icon-refresh" aria-hidden="true"></i> <?php esc_html_e( 'Recheck', 'broken-link-checker' ); ?></a></li>
									<li><a class="wdblc-action-unlink" href="#" data-modal-open="are-you-sure-notice-redirect-unlink"><i class="sui-icon-unlink" aria-hidden="true"></i> <?php esc_html_e( 'Unlink', 'broken-link-checker' ); ?></a></li>
									<li><a class="wdblc-action-mark-not-broken" href="#"  data-modal-open="are-you-sure-notice-redirect-not-broken"><i class="sui-icon-check" aria-hidden="true"></i> <?php esc_html_e( 'Mark not broken', 'broken-link-checker' ); ?></a></li>
									<li><a class="wdblc-action-ignore" href="#"><i class="sui-icon-eye-hide" aria-hidden="true"></i> <?php esc_html_e( 'Ignore', 'broken-link-checker' ); ?></a></li>
									<li><a class="wdblc-action-view-source-page" target="_blank" href="<?php echo esc_attr( $view_url ); ?>"><i class="sui-icon-eye" aria-hidden="true"></i> <?php esc_html_e( 'View source page', 'broken-link-checker' ); ?></a></li>
									<li><a class="wdblc-action-delete-source-page" href="#"  data-modal-open="are-you-sure-notice-redirect-remove"><i class="sui-icon-trash" aria-hidden="true"></i> <?php esc_html_e( 'Delete source page', 'broken-link-checker' ); ?></a></li>
								</ul>
							</div>

							<button role="button" class="sui-button-icon sui-accordion-open-indicator" aria-label="Expand" id="chevron-icon">
								<i class="sui-icon-chevron-down" aria-hidden="true"></i>
							</button>
						</td>

					</tr>
					<tr class="sui-accordion-item-content">
						<td colspan="9">
							<div class="sui-box">
								<div class="sui-box-body">
									<div class="sui-tabs">

										<div role="tablist" class="sui-tabs-menu">

											<button
													type="button"
													role="tab"
													id="tab-1-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
													class="sui-tab-item"
													aria-controls="tab-content-1-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
													aria-selected="true"
											>
												<?php esc_html_e( 'EDIT LINK', 'broken-link-checker' ); ?>
											</button>

											<button
													type="button"
													role="tab"
													id="tab-2-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
													class="sui-tab-item active"
													aria-controls="tab-content-2-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
													aria-selected="false"
													tabindex="-1"
											>
												<?php esc_html_e( 'DETAILS', 'broken-link-checker' ); ?>
											</button>
										</div>

										<div class="sui-tabs-content">
											<div
													role="tabpanel"
													tabindex="0"
													id="tab-content-1-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
													class="sui-tab-content blc-issue-details"
													aria-labelledby="tab-1-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
											>
												<div class="sui-box">

													<div class="sui-box-header">
														<h2 class="sui-box-title"><?php esc_html_e( 'Overview', 'broken-link-checker' ); ?></h2>
													</div>
													<div class="sui-box-body">
														<p><?php echo wp_kses_post( 'A 404 means that the page you are trying to open could not be found on the server. This is a client side incident which means either the page has been deleted OR moved, and the ulr has not been modified' ); ?>t</p>
													</div>

													<div class="sui-box-header">
														<h2 class="sui-box-title"><?php esc_html_e( 'Status', 'broken-link-checker' ); ?></h2>
													</div>
													<div class="sui-box-body">

														<div class="sui-notice sui-notice-error">

															<div class="sui-notice-content">
																<div class="sui-notice-message">
																	<i class="sui-notice-icon sui-icon-info sui-md" aria-hidden="true"></i>
																	<p><?php esc_html_e( 'The url could not be found.', 'broken-link-checker' ); ?></p>
																</div>
															</div>

														</div>

													</div>

													<div class="sui-box-header">
														<h2 class="sui-box-title"><?php esc_html_e( 'How to fix', 'broken-link-checker' ); ?></h2>
													</div>
													<div class="sui-box-body">
														<p><?php echo wp_kses_post( 'Check for errorsin the URL. Often times the 404 not found error appears because the url was typed wrong or point to the wrong url. Make sure to write the entire url nd a description text link' ); ?>t</p>
													</div>

													<div class="sui-box-body">
														<div class="blc-url-fixer-panel">
															<div class="blc-url-fixer-form">

																<div class="sui-form-field">
																	<label for="unique-id<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>" id="label-unique-id<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>" class="sui-label">
																		<?php esc_html_e( 'New text link', 'broken-link-checker' ); ?>
																	</label>
																	<input
																			placeholder="E.g. WordPress"
																			id="unique-id<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
																			class="sui-form-control"
																			aria-labelledby="label-unique-id<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
																			aria-describedby="error-unique-id description-unique-id<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
																	/>
																</div>

																<div class="sui-form-field">
																	<label for="unique-id-1<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>" id="label-unique-id-1<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>" class="sui-label">
																		<?php esc_html_e( 'New URL', 'broken-link-checker' ); ?>
																	</label>
																	<input
																			placeholder="E.g. https://www.wordpress.com"
																			id="unique-id-1<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
																			class="sui-form-control"
																			aria-labelledby="label-unique-id-1<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
																			aria-describedby="error-unique-id-1<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?> description-unique-id-1<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
																	/>
																	<span id="description-unique-id" class="sui-description">
																<?php esc_html_e( 'Write the entire URL. For eaxmple https://www.wordpress.com', 'broken-link-checker' ); ?>
															</span>
																</div>


															</div>
														</div>
													</div>

													<div class="sui-box-body">
														<div class="sui-row">
															<div class="sui-actions-left">
																<button role="button" class="sui-button sui-button-ghost blc-issue-ignore-btn"  data-modal-open="are-you-sure-notice-all">
																	<i class="sui-icon-eye-hide" aria-hidden="true"></i> <?php esc_html_e( 'IGNORE', 'broken-link-checker' ); ?>
																</button>
															</div>
															<div class="sui-actions-right btn-right">
																<button role="button" class="sui-button sui-button-ghost blc-issue-re-check-btn">
																	<i class="sui-icon-undo" aria-hidden="true"></i> <?php esc_html_e( 'RE-CHECK LINK', 'broken-link-checker' ); ?>
																</button>
																<button
																		role="button"
																		class="sui-button sui-button-blue blc-issue-update-btn"
																		data-notice-open="confirm_all"
																		data-notice-message="<p><?php esc_attr_e( 'Your link has been redirected.', 'broken-link-checker' ); ?></p>"
																>
																	<?php esc_html_e( 'UPDATE LINK', 'broken-link-checker' ); ?>
																</button>
															</div>
														</div>
													</div>

												</div>
											</div>

											<div
													role="tabpanel"
													tabindex="0"
													id="tab-content-2-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
													class="sui-tab-content active"
													aria-labelledby="tab-2-<?php echo esc_attr( sanitize_title_with_dashes( $issue ) ); ?>"
													hidden
											>
												<table class="sui-table blc-status-details" id="accordion-content">
													<tbody>

													<tr>
														<td class="sui-table-item-title"><?php esc_html_e( 'HTTP Code', 'broken-link-checker' ); ?></td>
														<td class="sui-description"><?php esc_html_e( $status_code, 'broken-link-checker' ); ?></td>
													</tr>
													<tr>
														<td class="sui-table-item-title"><?php esc_html_e( 'Link last checked', 'broken-link-checker' ); ?></td>
														<td class="sui-description"><?php esc_html_e( date_i18n( get_option( 'date_format' ), strtotime( $link->last_check ) ), 'broken-link-checker' ); ?></td>
													</tr>
													<tr>
														<td class="sui-table-item-title"><?php esc_html_e( 'Response time', 'broken-link-checker' ); ?></td>
														<td class="sui-description"><?php esc_html_e( $link->request_duration, 'broken-link-checker' ); ?></td>
													</tr>
													<tr>
														<td class="sui-table-item-title"><?php esc_html_e( 'Find URL', 'broken-link-checker' ); ?></td>
														<td class="sui-description"><?php esc_html_e( $link->url, 'broken-link-checker' ); ?></td>
													</tr>
													<tr>
														<td class="sui-table-item-title"><?php esc_html_e( 'Redirect count', 'broken-link-checker' ); ?></td>
														<td class="sui-description"><?php esc_html_e( $link->redirect_count, 'broken-link-checker' ); ?></td>
													</tr>
													<tr>
														<td class="sui-table-item-title"><?php esc_html_e( 'Instance count', 'broken-link-checker' ); ?></td>
														<td class="sui-description"><?php esc_html_e( count( $link->_instances ), 'broken-link-checker' ); ?></td>
													</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>

				<?php endforeach; ?>

				</tbody>

			</table>

			<div class="sui-box-body">
				<div class="sui-row">
					<div class="sui-col">
						<div class="blc-filter-horizontal">
							<div class="sui-form-field blc-show-inline">
								<div class="blc-inline-elm">
									<label for="filter-bottom-id" class="sui-checkbox">
										<input type="checkbox" id="filter-bottom-id" aria-labelledby="label-filter-bottom-id" />
										<span aria-hidden="true"></span>
									</label>
								</div>

								<div class="blc-inline-elm blc-filter-dropdown">
									<select id="filter-id" aria-labelledby="label-filter-id" aria-describedby="description-filter-id" class="sui-select-sm">
										<option><?php esc_html_e( 'Bulk Actions', 'broken-link-checker' ); ?></option>
										<option><?php esc_html_e( 'Action 1', 'broken-link-checker' ); ?></option>
										<option><?php esc_html_e( 'Action 2', 'broken-link-checker' ); ?></option>
									</select>
								</div>

								<div class="blc-inline-elm">
									<button role="button" class="sui-button sui-button-blue" id="filter-apply-button" disabled><?php esc_html_e( 'APPLY', 'broken-link-checker' ); ?></button>
								</div>

							</div>
						</div>
					</div>

					<div class="sui-actions-right">
						<div class="sui-pagination-wrap">

							<!-- ELEMENT: Number of Results. -->
							<span class="sui-pagination-results"><?php echo esc_html( sprintf( '%s %s', $filter['count'], __( 'results', 'broken-link-checker' ) ) ); ?></span>

							<!-- ELEMENT: List of Pages. -->
							<ul class="sui-pagination">

								<!-- BUTTON: Previous page. -->
								<li>
									<a href="<?php echo esc_attr( $prev_page ); ?>" role="button" <?php echo esc_attr( $prev_disabled ); ?>>
										<i class="sui-icon-chevron-left" aria-hidden="true"></i>
										<span class="sui-screen-reader-text"><?php esc_html_e( 'Go to previous page', 'broken-link-checker' ); ?></span>
									</a>
								</li>

								<?php for ( $i = 1; $i <= $filter['max_pages']; $i++ ) : ?>
									<?php
									$active_class = '';
									if ( $filter['page'] === $i ) {
										$active_class = 'class="sui-active"';
									}
									?>
								<li <?php echo esc_html( $active_class ); ?>><a href="?page=brokenlinkchecker-links&tab=all&paged=<?php echo esc_attr( $i ); ?>" role="button"><?php echo esc_html( $i ); ?></a></li>
								<?php endfor; ?>

								<!-- BUTTON: Next page. -->
								<li>
									<a href="<?php echo esc_attr( $next_page ); ?>" role="button" <?php echo esc_attr( $next_disabled ); ?>>
										<i class="sui-icon-chevron-right" aria-hidden="true"></i>
										<span class="sui-screen-reader-text"><?php esc_html_e( 'Go to next page', 'broken-link-checker' ); ?></span>
									</a>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="sui-modal sui-modal-sm">
				<div
						role="dialog"
						id="are-you-sure-notice-all"
						class="sui-modal-content"
						aria-modal="true"
						aria-labelledby="are-you-sure-notice-all-title"
						aria-describedby="are-you-sure-notice-all-desc"
				>
					<div class="sui-box">
						<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
							<button class="sui-button-icon sui-button-float--right" data-modal-close="">
								<i class="sui-icon-close sui-md" aria-hidden="true"></i>
								<span class="sui-screen-reader-text">Close this dialog.</span>
							</button>
							<h3 id="are-you-sure-notice-all-title" class="sui-box-title sui-lg"><?php esc_html_e( 'Are you sure?', 'broken-link-checker' ); ?></h3>
							<p id="are-you-sure-notice-all-desc" class="sui-description">
								<?php esc_html_e( 'Are you sure you wish to ignore this issue? You can re-scan anytime to check your links and images again.', 'broken-link-checker' ); ?>
							</p>
						</div>
						<div class="sui-box-footer sui-flatten sui-content-center">
							<button class="sui-button sui-button-ghost" data-modal-close=""><?php esc_html_e( 'CANCLE', 'broken-link-checker' ); ?></button>
							<button
									class="sui-button sui-button-blue"
									data-modal-close=""
									data-notice-open="confirm_all"
									data-notice-message="<p><?php esc_attr_e( 'Your issue has been ignored.', 'broken-link-checker' ); ?></p>"
									data-notice-icon="info"
							><?php esc_html_e( 'CONFIRM', 'broken-link-checker' ); ?></button>
						</div>
					</div>
				</div>
			</div>

			<div class="sui-modal sui-modal-sm">
				<div
						role="dialog"
						id="are-you-sure-notice-all-unlink"
						class="sui-modal-content"
						aria-modal="true"
						aria-labelledby="are-you-sure-notice-all-unlink-title"
						aria-describedby="are-you-sure-notice-all-unlink-desc"
				>
					<div class="sui-box">
						<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
							<button class="sui-button-icon sui-button-float--right" data-modal-close="">
								<i class="sui-icon-close sui-md" aria-hidden="true"></i>
								<span class="sui-screen-reader-text">Close this dialog.</span>
							</button>
							<h3 id="are-you-sure-notice-all-unlink-title" class="sui-box-title sui-lg"><?php esc_html_e( 'Are you sure?', 'broken-link-checker' ); ?></h3>
							<p id="are-you-sure-notice-all-unlink-desc" class="sui-description">
								<?php esc_html_e( 'Are you sure you wish to unlink this issue? You can re-scan anytime to check your links and images again.', 'broken-link-checker' ); ?>
							</p>
						</div>
						<div class="sui-box-footer sui-flatten sui-content-center">
							<button class="sui-button sui-button-ghost" data-modal-close=""><?php esc_html_e( 'CANCLE', 'broken-link-checker' ); ?></button>
							<button
									class="sui-button sui-button-blue"
									data-modal-close=""
									data-notice-open="confirm_all"
									data-notice-message="<p><?php esc_attr_e( 'Your issue has been unlinked.', 'broken-link-checker' ); ?></p>"
									data-notice-icon="info"
							><?php esc_html_e( 'CONFIRM', 'broken-link-checker' ); ?></button>
						</div>
					</div>
				</div>
			</div>

			<div class="sui-modal sui-modal-sm">
				<div
						role="dialog"
						id="are-you-sure-notice-all-remove"
						class="sui-modal-content"
						aria-modal="true"
						aria-labelledby="are-you-sure-notice-all-remove-title"
						aria-describedby="are-you-sure-notice-all-remove-desc"
				>
					<div class="sui-box">
						<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
							<button class="sui-button-icon sui-button-float--right" data-modal-close="">
								<i class="sui-icon-close sui-md" aria-hidden="true"></i>
								<span class="sui-screen-reader-text">Close this dialog.</span>
							</button>
							<h3 id="are-you-sure-notice-all-remove-title" class="sui-box-title sui-lg"><?php esc_html_e( 'Are you sure?', 'broken-link-checker' ); ?></h3>
							<p id="are-you-sure-notice-all-remove-desc" class="sui-description">
								<?php esc_html_e( 'Are you sure you wish to remove this Source page? You can edit your broken links and solve the issue without deleting the source.', 'broken-link-checker' ); ?>
							</p>
						</div>
						<div class="sui-box-footer sui-flatten sui-content-center">
							<button class="sui-button sui-button-ghost" data-modal-close=""><?php esc_html_e( 'CANCLE', 'broken-link-checker' ); ?></button>
							<button
									class="sui-button sui-button-blue"
									data-modal-close=""
									data-notice-open="confirm_all"
									data-notice-message="<p><?php esc_attr_e( 'Your issue has been removed.', 'broken-link-checker' ); ?></p>"
									data-notice-icon="info"
							><?php esc_html_e( 'CONFIRM', 'broken-link-checker' ); ?></button>
						</div>
					</div>
				</div>
			</div>

			<div class="sui-modal sui-modal-sm">
				<div
						role="dialog"
						id="are-you-sure-notice-all-not-broken"
						class="sui-modal-content"
						aria-modal="true"
						aria-labelledby="are-you-sure-notice-all-not-broken-title"
						aria-describedby="are-you-sure-notice-all-not-broken-desc"
				>
					<div class="sui-box">
						<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
							<button class="sui-button-icon sui-button-float--right" data-modal-close="">
								<i class="sui-icon-close sui-md" aria-hidden="true"></i>
								<span class="sui-screen-reader-text">Close this dialog.</span>
							</button>
							<h3 id="are-you-sure-notice-all-not-broken-title" class="sui-box-title sui-lg"><?php esc_html_e( 'Are you sure?', 'broken-link-checker' ); ?></h3>
							<p id="are-you-sure-notice-all-not-broken-desc" class="sui-description">
								<?php esc_html_e( 'Are you sure you wish to mark this link not broken? You can re-scan anytime to check your links and images again.', 'broken-link-checker' ); ?>
							</p>
						</div>
						<div class="sui-box-footer sui-flatten sui-content-center">
							<button class="sui-button sui-button-ghost" data-modal-close=""><?php esc_html_e( 'CANCLE', 'broken-link-checker' ); ?></button>
							<button
									class="sui-button sui-button-blue"
									data-modal-close=""
									data-notice-open="confirm_all"
									data-notice-message="<p><?php esc_attr_e( 'Your issue has been confirmed not-broken.', 'broken-link-checker' ); ?></p>"
									data-notice-icon="info"
							><?php esc_html_e( 'CONFIRM', 'broken-link-checker' ); ?></button>
						</div>
					</div>
				</div>
			</div>

			<div class="sui-floating-notices">
				<div
						role="alert"
						id="confirm_all"
						class="sui-notice"
						aria-live="assertive"
				>
					<!-- Nothing should be placed inside. -->
				</div>
			</div>

		<?php endif; ?>
	</div>
</div>
