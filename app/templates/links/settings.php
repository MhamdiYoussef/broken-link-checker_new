<?php
/**
 * Settings Links page.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>
<div class="sui-box blc-issue-settings" data-tab="header">
<form action="<?php echo esc_attr( $page_url ); ?>" method="POST">

	<div class="sui-box-header">
		<h2 class="sui-box-title"><?php esc_html_e( 'Settings', 'broken-link-checker' ); ?></h2>
	</div>

	<div class="sui-box-body">
		<div class="sui-row">
			<div class="sui-col-lg-4">
				<h2 class="sui-box-title"><?php esc_html_e( 'Actions', 'broken-link-checker' ); ?></h2>
				<div>
					<?php esc_html_e( 'Select what action you want to make available for each broken link.', 'broken-link-checker' ); ?>
				</div>
			</div>
			<div class="sui-col-lg-8">
				<div class="sui-form-field">
					<label for="blc-issue-settings-edit-url" class="sui-checkbox">
						<?php
							$edit_checked = '';
						if ( isset( $show_link_actions['edit'] ) && (bool) ( $show_link_actions['edit'] ) ) {
							$edit_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $edit_checked ); ?> name="show_link_actions[edit]" id="blc-issue-settings-edit-url" aria-labelledby="label-blc-issue-settings-edit-url"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-edit-url"><?php esc_html_e( 'Edit URL', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-unlink" class="sui-checkbox">
						<?php
							$unlink_checked = '';
						if ( isset( $show_link_actions['delete'] ) && (bool) ( $show_link_actions['delete'] ) ) {
							$unlink_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $unlink_checked ); ?> name="show_link_actions[delete]" id="blc-issue-settings-unlink" aria-labelledby="label-blc-issue-settings-unlink"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-unlink"><?php esc_html_e( 'Unlink', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-not-broken" class="sui-checkbox">
						<?php
							$discard_checked = '';
						if ( isset( $show_link_actions['blc-discard-action'] ) && (bool) ( $show_link_actions['blc-discard-action'] ) ) {
							$discard_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $discard_checked ); ?> name="show_link_actions[blc-discard-action]" id="blc-issue-settings-not-broken" aria-labelledby="label-blc-issue-settings-not-broken"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-not-broken"><?php esc_html_e( 'Not Broken', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-dismiss" class="sui-checkbox">
						<?php
							$dismiss_checked = '';
						if ( isset( $show_link_actions['blc-dismiss-action'] ) && (bool) ( $show_link_actions['blc-dismiss-action'] ) ) {
							$dismiss_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $dismiss_checked ); ?> name="show_link_actions[blc-dismiss-action]" id="blc-issue-settings-dismiss" aria-labelledby="label-blc-issue-settings-dismiss"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-dismiss"><?php esc_html_e( 'Dismiss', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-recheck" class="sui-checkbox">
						<?php
							$recheck_checked = '';
						if ( isset( $show_link_actions['blc-recheck-action'] ) && (bool) ( $show_link_actions['blc-recheck-action'] ) ) {
							$recheck_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $recheck_checked ); ?> name="show_link_actions[blc-recheck-action]" id="blc-issue-settings-recheck" aria-labelledby="label-blc-issue-settings-recheck"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-edit-url"><?php esc_html_e( 'Recheck', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-fix-redirect" class="sui-checkbox">
						<?php
							$redirect_checked = '';
						if ( isset( $show_link_actions['blc-deredirect-action'] ) && (bool) ( $show_link_actions['blc-deredirect-action'] ) ) {
							$redirect_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $redirect_checked ); ?> name="show_link_actions[blc-deredirect-action]" id="blc-issue-settings-fix-redirect" aria-labelledby="label-blc-issue-settings-fix-redirect"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-fix-redirect"><?php esc_html_e( 'Fix Redirect', 'broken-link-checker' ); ?></span>
					</label>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<div class="sui-box-body">
		<div class="sui-row">
			<div class="sui-col-lg-4">
				<h2 class="sui-box-title"><?php esc_html_e( 'Column visibility', 'broken-link-checker' ); ?></h2>
				<div>
					<?php esc_html_e( 'Control what information you want to see on the broken links list', 'broken-link-checker' ); ?>
				</div>
			</div>
			<div class="sui-col-lg-8">
				<div class="sui-form-field">
					<label for="blc-issue-settings-link-text" class="sui-checkbox">
						<?php
							$link_text_checked = '';
						if ( isset( $visible_columns['link_text'] ) && (bool) ( $visible_columns['link_text'] ) ) {
							$link_text_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $link_text_checked ); ?> name="visible_columns[link_text]" id="blc-issue-settings-link-text" aria-labelledby="label-blc-issue-settings-link-text"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-link-text"><?php esc_html_e( 'Link text', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-status" class="sui-checkbox">
						<?php
							$status_checked = '';
						if ( isset( $visible_columns['status'] ) && (bool) ( $visible_columns['status'] ) ) {
							$status_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $status_checked ); ?> name="visible_columns[status]" id="blc-issue-settings-status" aria-labelledby="label-blc-issue-settings-status"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-unlink"><?php esc_html_e( 'Status', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-url" class="sui-checkbox">
						<?php
							$url_checked = '';
						if ( isset( $visible_columns['url'] ) && (bool) ( $visible_columns['url'] ) ) {
							$url_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $url_checked ); ?> name="visible_columns[url]" id="blc-issue-settings-url" aria-labelledby="label-blc-issue-settings-url"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-url"><?php esc_html_e( 'URL', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-redirect-url" class="sui-checkbox">
						<?php
							$redirect_url_checked = '';
						if ( isset( $visible_columns['redirect_url'] ) && (bool) ( $visible_columns['redirect_url'] ) ) {
							$redirect_url_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $redirect_url_checked ); ?> name="visible_columns[redirect_url]" id="blc-issue-settings-redirect-url" aria-labelledby="label-blc-issue-settings-redirect-url"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-redirect-url"><?php esc_html_e( 'Redirect URL', 'broken-link-checker' ); ?></span>
					</label>
				</div>
				<div class="sui-form-field">
					<label for="blc-issue-settings-source" class="sui-checkbox">
						<?php
							$source_checked = '';
						if ( isset( $visible_columns['source'] ) && (bool) ( $visible_columns['source'] ) ) {
							$source_checked = 'checked=checked';
						}
						?>
						<input type="checkbox" <?php echo esc_html( $source_checked ); ?> name="visible_columns[source]" id="blc-issue-settings-source" aria-labelledby="label-blc-issue-settings-source"/>
						<span aria-hidden="true"></span>
						<span id="label-blc-issue-settings-source"><?php esc_html_e( 'Source', 'broken-link-checker' ); ?></span>
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="sui-box-footer">
		<div class="sui-actions-right">
			<button role="button" name="save-settings" value="1" class="sui-button sui-button-blue">
				<i class="sui-icon-save" aria-hidden="true"></i> <?php esc_html_e( 'SAVE CHANGES', 'broken-link-checker' ); ?>
			</button>
		</div>
	</div>
	<input type="hidden" name="links-tab" value="settings">
	<?php wp_nonce_field( 'wdblc_links_settings' ); ?>
</form>

</div>
