<?php
/**
 * Main plugin page.
 *
 * @package BrokenLinkChecker
 * @since 1.0.0
 */

?>

<script type="text/javascript">
window.BLC = window.BLC || {};
BLC.ajax_url = "<?php echo esc_html( admin_url( 'admin-ajax.php' ) ); ?>";
BLC.skip_onboarding_nonce = "<?php echo esc_html( wp_create_nonce( 'wdblc_skip_onboarding' ) ); ?>";

automatic_engine_on = <?php echo esc_html( $automatic_engine_on ? 'true' : 'false' ); ?>;
if ( automatic_engine_on ) {
	BLC.work_nonce = "<?php echo esc_html( wp_create_nonce( 'blc_work' ) ); ?>";
}

BLC.onboarding_done = <?php echo esc_html( $onboarding_done ? 'true' : 'false' ); ?>;
</script>

<main class="sui-wrap">
	<?php $this->render_header(); ?>
	<?php
	if ( ! $onboarding_done ) {
		$this->render_part( 'dashboard/onboarding' );
	}
	?>

	<div class="sui-row">
		<!-- Broken Links box -->
		<div class="sui-col-md-6">
			<div class="sui-box">

				<div class="sui-box-header">
					<h2 class="sui-box-title">
						<span class="sui-icon-unlink" aria-hidden="true"></span>
						<?php esc_html_e( 'Broken Links', 'broken-link-checker' ); ?>
					</h2>

					<?php if ( 0 !== $filter['count'] && $onboarding_done ) { ?>
					<span class="sui-tag sui-tag-yellow"
						style="margin-left: 10px;"><?php echo esc_html( $filter['count'] ); ?></span>
					<?php } ?>
				</div>

				<div class="sui-box-body">

					<p><?php esc_html_e( 'Analyze your website for broken links in posts and pages to ensure all links are valid.', 'broken-link-checker' ); ?>
					</p>

				<?php if ( ! $onboarding_done ) { ?>

					<a href="#"
						class="sui-button sui-button-blue"></i><?php esc_html_e( 'Activate', 'broken-link-checker' ); ?></a>
				</div>
					<?php } else { ?>

					<?php if ( 0 === $filter['count'] ) { ?>

					<div id="notice-id" class="sui-notice sui-notice-success">

						<div class="sui-notice-content">

							<div class="sui-notice-message">

								<span class="sui-notice-icon sui-icon-check-tick sui-success" aria-hidden="true"></span>

								<p><?php esc_html_e( 'You have no broken links. Awesome work!', 'broken-link-checker' ); ?>
								</p>

							</div>

						</div>

					</div>

				</div>
				<?php } else { ?>
			</div>

			<table class="sui-table sui-accordion sui-accordion-flushed">

				<tbody>

						<?php foreach ( $links as $link ) : ?>
							<?php
							// Retrieve link instances to parse link text.
							$instances = $link->get_instances();
							$link_text = '';

							$can_edit_text           = false;
							$can_edit_url            = false;
							$editable_link_texts     = array();
							$non_editable_link_texts = array();

							foreach ( $instances as $instance ) {
								if ( $instance->is_link_text_editable() ) {
									$can_edit_text                               = true;
									$editable_link_texts[ $instance->link_text ] = true;
								} else {
									$non_editable_link_texts[ $instance->link_text ] = true;
								}

								if ( $instance->is_url_editable() ) {
									$can_edit_url = true;
								}
							}

							$link_texts     = $can_edit_text ? $editable_link_texts : $non_editable_link_texts;
							$data_link_text = '';
							if ( 1 === count( $link_texts ) ) {
								// All instances have the same text - use it.
								$link_text = key( $link_texts );
							}
							if ( '' === $link_text ) {
								$link_text = $link->url;
							}

							$status_text = $link->status_text ? $link->status_text : 'This URL could not be found.';
							$status_code = $link->http_code ? $link->http_code : '404';
							$status_color = '';

							if ( '404' == $status_code ) {
								$status_color = 'warning';
							}

							if ( '401' == $status_code ) {
								$status_color = 'yellow';
							}

							?>

					<tr class="sui-accordion-item sui-<?php echo esc_html( $status_color ); ?>">
						<td class="sui-table-item-title">

							<?php echo esc_html( $link_text ); ?>

						</td>

						<td style="text-align:center">

							<div class="sui-tooltip" data-tooltip="<?php echo esc_html_e( $status_text ); ?>">
								<span class="sui-tag sui-tag-<?php echo esc_html( $status_color ); ?>">
									<?php echo esc_html( $status_code ); ?>
								</span>
							</div>

						</td>

						<td>

							<button role="button" class="sui-button-icon sui-accordion-open-indicator"
								aria-label=<?php echo esc_html_e( 'Expand' ); ?>>

								<span class="sui-icon-chevron-right" aria-hidden="true"
									id="wdblc-open-issues-page"></span>

							</button>

						</td>

					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

					<?php } ?>

			<div class="sui-box-body sui-padding-top--0 ">

				<button role="button" id="wdblc-dash-view-links-btn" class="sui-button sui-button-ghost">
					<span class="sui-icon-eye" aria-hidden="true"></span>
					<?php esc_html_e( 'VIEW LINKS', 'broken-link-checker' ); ?>
				</button>

			</div>
			<?php } ?>
		</div>
	</div>

	<!-- Email Notification box-->
	<div class="sui-col-md-6">
		<div class="sui-box sui-accordion-flushed">

			<div class="sui-box-header">
				<h2 class="sui-box-title">
					<span class="sui-icon-mail" aria-hidden="true"></span>
					<?php esc_html_e( 'Email Notification', 'broken-link-checker' ); ?>
				</h2>
			</div>

			<div class="sui-box-body">
				<p><?php esc_html_e( 'Get notified by email when broken links are found and need fixing.', 'broken-link-checker' ); ?>
				</p>
			</div>

			<table class="sui-table sui-accordion sui-accordion-flushed">

				<tbody>

					<tr class="sui-accordion-item">
						<td class="sui-table-item-title" colspan="2">

							<?php esc_html_e( 'Broken Links Notification', 'broken-link-checker' ); ?>

						</td>

						<?php if ( ! $schedule_display || ! $send_email_notifications ) { ?>

						<td style="text-align:center">
							<span class="sui-tag sui-description">
								<?php esc_html_e( 'Inactive', 'broken-link-checker' ); ?>
							</span>
						</td>

						<td style="text-align:center">

							<form action="<?php echo esc_attr( $page_url ); ?>" method="POST">
								<button role="button"
									name="activate-notifications"
									value="1"
									class="sui-button-icon sui-button-blue sui-accordion-open-indicator sui-tooltip"
									data-tooltip=<?php echo esc_html_e( 'Activate' ); ?>
									aria-label=<?php echo esc_html_e( 'Expand' ); ?>>
									<span class="sui-icon-plus" aria-hidden="true"></span>
								</button>
								<?php wp_nonce_field( 'wdblc_dashboard' ); ?>
							</form>

						</td>

						<?php } else { ?>

						<td style="text-align:center">
							<span class="sui-tag sui-tag-blue">
								<?php echo esc_html( $schedule_display ); ?>
							</span>
						</td>

						<td style="text-align:center">
							<button role="button" id="wdblc-open-email-settings"
								class="sui-button-icon sui-accordion-open-indicator"
								aria-label=<?php echo esc_html_e( 'Expand' ); ?>>
								<span class="sui-icon-widget-settings-config" aria-hidden="true"></span>
							</button>
						</td>

						<?php } ?>
					</tr>

				</tbody>
			</table>

			<span class="last-element"></span>

		</div>
	</div>
	</div>


	<?php $this->render_part( 'footer' ); ?>
	<div class="sui-floating-notices">
		<div role="alert" id="ntc-confirm" class="sui-notice" aria-live="assertive">
		</div>
	</div>
	<div id="notification-texts" style="display:none;">
		<div id="ntc-settings-saved">
			<p><?php esc_attr_e( 'Settings are saved. Reload page to see results.', 'broken-link-checker' ); ?></p>
		</div>
		<div id="ntc-onboarding-skipped">
			<p><?php esc_attr_e( 'Onboarding is skipped. Visit Settings page to adjust your preferences.', 'broken-link-checker' ); ?>
			</p>
		</div>
		<div id="ntc-not-allowed">
			<p><?php esc_attr_e( 'You are not allowed to execute this action. Refresh the page to reload permissions.', 'broken-link-checker' ); ?>
			</p>
		</div>
		<div id="ntc-work-done">
			<p>
				<?php esc_attr_e( 'Your link check has finished. Reload page to see results', 'broken-link-checker' ); ?>
				<button id="wdblc-reload-page-btn" class="sui-button sui-button-ghost float-right sui-mt-5" onclick="reloardPage()">
					<span class="sui-icon-undo" aria-hidden="true"></span>
					<span class="sui-progress-icon sui-hidden" aria-hidden="true" style="position: absolute;top: 7px;left: 31px;">
						<span style="display:inline-block;" class="sui-icon-loader sui-loading"></span>
					</span>
					<span id="wdblc-btn-text" >
						<?php esc_attr_e( 'Reaload Page', 'broken-link-checker' ); ?>
					</span>
				</button>
			</p>
	</div>
