<?php
/**
 * Locations settings page.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>
<div class="sui-box" data-tab="header">

	<div class="sui-box-header">
		<h2 class="sui-box-title"><?php esc_html_e( 'Locations', 'broken-link-checker' ); ?></h2>
	</div>

	<form action="<?php echo esc_attr( $page_url ); ?>" method="POST">
		<div class="sui-box-body">

		<!-- SECTION: Simple Header -->
		<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
				<span class="sui-settings-label"><?php esc_html_e( 'Locations', 'broken-link-checker' ); ?></span>
				<span class="sui-description">
				<?php esc_html_e( 'Select what parts of your website to include and exclude from the checker engine. We recommend excluding any areas of your site that are unlikely to have broken links.', 'broken-link-checker' ); ?>
				</span>
			</div>

			<div class="sui-box-settings-col-2">
					<div class="sui-accordion">
					<?php foreach ( $modules as $module_id => $module ) : ?>
						<div class="sui-accordion-item
						<?php if ( false ) : ?>
						 sui-accordion-item--open<?php endif; ?>">
							<div class="sui-accordion-item-header">
								<div class="sui-accordion-item-title">
									<label for="<?php echo esc_attr( $module_id ); ?>-checkbox" class="sui-toggle sui-accordion-item-action">
										<?php
											$checked = '';
										if ( $module['active'] ) {
											$checked = 'checked=checked';
										}
										?>
										<input type="checkbox" id="<?php echo esc_attr( $module_id ); ?>-checkbox" name="module[<?php echo esc_attr( $module_id ); ?>]" <?php echo esc_html( $checked ); ?>>
										<span aria-hidden="true" class="sui-toggle-slider"></span>
										<span class="sui-screen-reader-text"><?php esc_html_e( 'Toggle', 'broken-link-checker' ); ?></span>
									</label>
									<span><?php echo esc_html( $module['title'] ); ?></span>
								</div>

									<div class="sui-accordion-col-auto">
										<button class="sui-button-icon sui-accordion-open-indicator" aria-label="Open item">
											<i class="sui-icon-chevron-down" aria-hidden="true"></i>
										</button>
									</div>
							</div>

							<div class="sui-accordion-item-body">
									<div class="sui-box">
										<div class="sui-box-body">
										<p> <?php esc_html_e( 'Enter the keys of ACF fields you want to check (one per line). If a field contains HTML code, prefix its name with html:', 'broken-link-checker' ); ?></p>
											<textarea
												placeholder="<?php esc_html_e( 'One field key per line', 'broken-link-checker' ); ?>"
												id="<?php echo esc_attr( 'acf-text-area' ); ?>"
												class="sui-form-control"
												aria-labelledby="label-<?php echo esc_attr( 'acf-text-area' ); ?>"
												aria-describedby="error-<?php echo esc_attr( 'acf-text-area' ); ?> description-<?php echo esc_attr( 'acf-text-area' ); ?>"
											></textarea>
											<!--
											NOTE:
											Notice error message element it is empty. This because content should be printed
											when error happens and not before to avoid screenreaders confusing users.
											-->
											<span id="error-<?php echo esc_attr( 'acf-text-area' ); ?>" class="sui-error-message" style="display: none;"></span>
											<span id="description-<?php echo esc_attr( 'acf-text-area' ); ?>" class="sui-description"><?php esc_html_e( 'Example: html:field_586a3eaa4091b.', 'broken-link-checker' ); ?></span>
										</div>
									</div>
								</div>
						</div>

						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<div class="sui-box-settings-row">
				<div class="sui-box-settings-col-1">
					<span class="sui-settings-label"><?php esc_html_e( 'Post Status', 'broken-link-checker' ); ?></span>
					<span class="sui-description">
						<?php esc_html_e( 'Choose the statuses you want checked for broken links. We recommend including any content that could be indexed by Google, and excluding the rest, for performance reasons.', 'broken-link-checker' ); ?>
					</span>
				</div>

				<div class="sui-box-settings-col-2">

					<?php foreach ( $post_statuses as $status_id => $status_data ) { ?>
						<label for="<?php echo esc_attr( sanitize_title_with_dashes( $status_id ) ); ?>-checkbox" class="sui-checkbox sui-checkbox-stacked">
							<input
								type="checkbox"
								name="post_statuses[<?php echo esc_attr( sanitize_title_with_dashes( $status_id ) ); ?>]"
						<?php if ( $status_data['active'] ) : ?>
								checked="checked"
						<?php endif; ?>
								id="<?php echo esc_attr( sanitize_title_with_dashes( $status_id ) ); ?>-checkbox"
								aria-labelledby="label-<?php echo esc_attr( sanitize_title_with_dashes( $status_id ) ); ?>-checkbox"
							/>

							<span aria-hidden="true"></span>

							<span id="label-<?php echo esc_attr( sanitize_title_with_dashes( $status_id ) ); ?>-checkbox"><?php echo esc_html( $status_data['label'] ); ?></span>

						</label>

					<?php } ?>

				</div>
			</div>
		</div>

		<div class="sui-box-footer">
			<div class="sui-actions-right">
				<button type="submit" name="save-settings" value="1" class="sui-button sui-button-blue" id="wp-blc-save-settings" aria-describedby="blc-submit-description" data-msg="Save Changed">
					<i class="sui-icon-save" aria-hidden="true"></i>
					<?php esc_html_e( 'SAVE CHANGES', 'broken-link-checker' ); ?>
				</button>
				<span class="sui-icon-loader sui-loading sui-hidden"></span>
			</div>
		</div>
		<input type="hidden" name="settings-tab" value="locations">
		<?php wp_nonce_field( 'wdblc_settings_locations' ); ?>
	</form>
</div>
