<?php
/**
 * Settings Link Types template.
 *
 * @package templates
 */

?>
<div class="sui-box-header">
	<h2 class="sui-box-title"><?php esc_html_e( 'Link Types', 'broken-link-checker' ); ?></h2>
</div>

<form action="<?php echo esc_attr( $page_url ); ?>" method="POST">
<div class="sui-box-body">
	<!-- SECTION: Simple Header -->
	<div class="sui-box-settings-row">
		<div class="sui-box-settings-col-1 sui-col-md-2">
			<span class="sui-settings-label"><?php esc_html_e( 'Which Links', 'broken-link-checker' ); ?></span>
			<span class="sui-description">
			<?php
				printf(
					/* translators: %1$s - link, %2$s - closing link tag */
					esc_html__( 'Choose which links to check. Note: You can connect to the available apps via their API on the %1$sProtocols & APIs%2$s page so we can double-check if any media links on your site are broken.', 'broken-link-checker' ),
					'<a href="' . esc_url( network_admin_url( 'admin.php?page=brokenlinkchecker-settings&view=protocols&apis' ) ) . '">',
					'</a>'
				);
				?>
				</span>
		</div>

		<div class="sui-box-settings-col-2">
			<?php
			$i = 1;
			foreach ( $modules as $name => $option ) :
				?>
					<label for="<?php echo esc_attr( $name ); ?>-checkbox" class="sui-checkbox sui-checkbox-stacked">
					<input
						type="checkbox"
						<?php if ( $option['active'] ) : ?>
						checked=checked
						<?php endif; ?>
						id="<?php echo esc_attr( $name ); ?>-checkbox"
						name="module[<?php echo esc_attr( $name ); ?>]"
						aria-labelledby="<?php echo esc_attr( $name ); ?>-checkbox-label"
					/>
					<span aria-hidden="true"></span>
					<span id="<?php echo esc_attr( $name ); ?>-checkbox-label"><?php echo esc_html( $option['title'] ); ?></span>
				</label>

				<?php if ( $i++ === $parsers_halfway ) : ?>
		</div>
		<div class="sui-box-selectors-col-3 sui-col-md-5">
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>

	<!-- SECTION: Complex Header -->
	<div class="sui-box-settings-row">

		<div class="sui-box-settings-col-1">

			<span class="sui-settings-label"><?php esc_html_e( 'Exclusion list', 'broken-link-checker' ); ?></span>
			<span class="sui-description"><?php esc_html_e( 'Exclude links that contains these words.', 'broken-link-checker' ); ?></span>

		</div>
		<div class="sui-box-settings-col-2">
			<span class="sui-builder-text"><?php esc_html_e( 'Don\'t check links where the URL contains any of these words (one per line) :', 'broken-link-checker' ); ?></span>
			<textarea
				placeholder="E.g. /wordpress"
				id="unique-id"
				class="sui-form-control"
				name="exclusion_list"
				aria-labelledby="label-unique-id"
				aria-describedby="error-unique-id description-unique-id"
			><?php echo esc_textarea( $exclusion_list ); ?></textarea>

			<!--
			NOTE:
			Notice error message element it is empty. This because content should be printed
			when error happens and not before to avoid screenreaders confusing users.
			-->
			<span id="error-unique-id" class="sui-error-message" style="display: none;"></span>
		</div>
	</div>
</div>

<div class="sui-box-footer">
	<div class="sui-actions-right">
		<button type="submit" name="save-settings" value="1" class="sui-button sui-button-blue" id="wp-blc-save-settings" aria-describedby="blc-submit-description" data-msg="Save Changed">
			<i class="sui-icon-save" aria-hidden="true"></i>
			<?php esc_html_e( 'Save Changes', 'broken-link-checker' ); ?>
		</button>
		<span class="sui-icon-loader sui-loading sui-hidden"></span>
	</div>
</div>
<input type="hidden" name="settings-tab" value="link-types">
<?php wp_nonce_field( 'wdblc_settings_link_types' ); ?>
</form>
