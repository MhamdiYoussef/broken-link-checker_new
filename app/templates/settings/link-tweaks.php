<?php
/**
 * Link Tweaks settings page.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>
<div class="sui-box link-tweaks" data-tab="header">

	<div class="sui-box-header">
		<h2 class="sui-box-title"><?php esc_html_e( 'Link Tweaks', 'broken-link-checker' ); ?></h2>
	</div>

<form action="<?php echo esc_attr( $page_url ); ?>" method="POST">
	<div class="sui-box-body">
		<p>
			<?php esc_html_e( 'This settings only apply to the content of posts, not comments or custom fields.', 'broken-link-checker' ); ?>
		</p>
	</div>

	<div class="sui-box-body">
		<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Styles', 'broken-link-checker' ); ?>
					</span>
				<span class="sui-description">
						<?php esc_html_e( 'Apply your own custom styling to highlights broken links on the front and backend of your site.', 'broken-link-checker' ); ?>
					</span>
			</div>
			<div class="sui-box-settings-col-2">
				<div class="sui-form-field">
					<label for="blc-enable-custom-format" class="sui-toggle">
						<input
							type="checkbox"
							name="mark_broken_links"
							<?php if ( $mark_broken_links ) : ?>
							checked=checked
							<?php endif; ?>
							id="blc-enable-custom-format"
							aria-labelledby="blc-enable-custom-format-label"
							aria-describedby="blc-enable-custom-format-description"
							aria-controls="blc-enable-custom-format-settings"
						>
						<span class="sui-toggle-slider" aria-hidden="true"></span>
						<span id="blc-enable-custom-format-label" class="sui-toggle-label">
								<?php esc_html_e( 'Apply custom formatting to broken links', 'broken-link-checker' ); ?>
						</span>
					</label>
					<div tabindex="0" class="sui-toggle-content sui-border-frame" aria-label="Edit your pop-up custom border settings.">
						<div class="sui-row">
							<div id="sui-ace-editor"><?php echo esc_textarea( $broken_link_css ); ?></div>
							<span class="sui-description sui-ace-editor-desc">
								Example:Lorem ipsum <span style="text-decoration: line-through">broken link</span> dolor sit amet.
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="sui-box-body">
		<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1"></div>
			<div class="sui-box-settings-col-2">
				<div class="sui-form-field">
					<label for="blc-enable-custom-format-remove-link" class="sui-toggle">
						<input
							type="checkbox"
							name="mark_removed_links"
							<?php if ( $mark_removed_links ) : ?>
							checked=checked
							<?php endif; ?>
							id="blc-enable-custom-format-remove-link"
							aria-labelledby="blc-enable-custom-format-remove-link-label"
							aria-describedby="blc-enable-custom-format-remove-link-description"
							aria-controls="blc-enable-custom-format-remove-link-settings"
						>
						<span class="sui-toggle-slider" aria-hidden="true"></span>
						<span id="blc-enable-custom-format-remove-link-label" class="sui-toggle-label">
								<?php esc_html_e( 'Apply custom formatting to removed links', 'broken-link-checker' ); ?>
						</span>
					</label>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<div class="sui-box-body">
		<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Search Engines', 'broken-link-checker' ); ?>
					</span>
				<span class="sui-description">
						<?php esc_html_e( 'This option allows you to stop search engines from following broken links.', 'broken-link-checker' ); ?>
					</span>
			</div>
			<div class="sui-box-settings-col-2">
				<div class="sui-form-field">
					<label for="blc-enable-search-engines" class="sui-toggle">
						<input
							type="checkbox"
							name="nofollow_broken_links"
							id="blc-enable-search-engines"
							<?php if ( $nofollow_broken_links ) : ?>
							checked=checked
							<?php endif; ?>
							aria-labelledby="blc-enable-search-engines-label"
							aria-describedby="blc-enable-search-engines-description"
							aria-controls="blc-enable-search-engines-settings"
						>
						<span class="sui-toggle-slider" aria-hidden="true"></span>
						<span id="blc-enable-search-engines-label" class="sui-toggle-label">
								<?php esc_html_e( 'Stop search engines from following broken links', 'broken-link-checker' ); ?>
						</span>
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="sui-box-footer">

		<div class="sui-actions-right">

			<button type="submit" name="save-settings" value="1" class="sui-button sui-button-blue" id="wp-blc-save-settings" aria-describedby="blc-submit-description" data-msg="Save Changed">
				<i class="sui-icon-save" aria-hidden="true"></i>
				<?php esc_html_e( 'SAVE CHANGES', 'broken-link-checker' ); ?>
			</button>

			<span class="sui-icon-loader sui-loading sui-hidden"></span>
		</div>
	</div>
	<input type="hidden" name="settings-tab" value="link-tweaks">
	<?php wp_nonce_field( 'wdblc_settings_link_tweaks' ); ?>
</form>
</div>
