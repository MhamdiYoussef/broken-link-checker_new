<?php
/**
 * Protocols & Api page.
 *
 * @package BrokenLinkChecker
 * @since 1.0.0
 */

// Creating a demo array to demonstrate the UI. All UI can be demonstrated by changing the empty() condition below.

$connected_apps = array(
	__( 'YouTube API', 'broken-link-checker' ),
);

$available_apps = array(
	__( 'MediaFire API', 'broken-link-checker' ),
);
?>

<div class="sui-box" data-tab="header">

	<?php $this->render_part( 'settings/connect-app' ); ?>

	<div class="sui-box-header">
		<h2 class="sui-box-title"><?php esc_html_e( 'Protocols & APIs', 'broken-link-checker' ); ?></h2>
	</div>


	<div class="sui-box-body">
		<!-- SECTION: Simple Header -->
		<p>
			<?php esc_html_e( 'Broken Link Checker integrates with multiple third-party apps. You can connect to the available apps via their API here so we can double-check if any media links on your site are broken.', 'broken-link-checker' ); ?>
		</p>

		<?php if ( empty( $connected_apps ) ) { ?>
		<label class="sui-label">
			<?php esc_html_e( 'Connected Apps', 'broken-link-checker' ); ?>
		</label>

		<div class="sui-notice sui-notice-info">

			<div class="sui-notice-content">

				<div class="sui-notice-message">
					<i class="sui-notice-icon sui-icon-info sui-md" aria-hidden="true"></i>
					<p>
						<?php esc_html_e( 'You are not connected to any third party apps. You can connect to the available apps listed below and activate them so we can double-check if any media links on your site are broken.', 'broken-link-checker' ); ?>
					</p>
				</div>

			</div>
		</div>

		<?php } else { ?>
		<table class="sui-table">
			<caption>
				<span class="sui-label" style="margin-bottom: 5px;">
					<?php esc_html_e( 'Connected Apps', 'broken-link-checker' ); ?>
				</span>
			</caption>
			<tbody>
			<?php foreach ( $connected_apps as $app ) : ?>
			<tr>
				<td><i class="sui-icon-social-youtube sui-xl sui-error" aria-hidden="true"></i></td>
				<td colspan="10"><strong><?php echo esc_html( $app ); ?></strong></td>
				<td>
					<div class="sui-tooltip" data-tooltip="<?php esc_html_e( 'Configure App', 'broken-link-checker' ); ?>">
						<button role="button" class="sui-button-icon">
							<i class="sui-icon-widget-settings-config" aria-hidden="true"></i>
							<span class="sui-screen-reader-text"><?php echo esc_html( $app ); ?></span>
						</button>
					</div>
				</td>
			</tr>
			<?php endforeach; ?>

			</tbody>

		</table>

		<?php } ?>


		<?php if ( empty( $available_apps ) ) { ?>
			<label class="sui-label">
				<?php esc_html_e( 'Available Apps', 'broken-link-checker' ); ?>
			</label>

			<div class="sui-notice sui-notice-info">

				<div class="sui-notice-content">

					<div class="sui-notice-message">
						<i class="sui-notice-icon sui-icon-info sui-md" aria-hidden="true"></i>
						<p>
							<?php esc_html_e( 'You are connected to all your available third party apps.', 'broken-link-checker' ); ?>
						</p>
					</div>

				</div>
			</div>

		<?php } else { ?>
		<table class="sui-table">
			<caption>
				<span class="sui-label" style="margin-bottom: 5px;">
					<?php esc_html_e( 'Available Apps', 'broken-link-checker' ); ?>
				</span>
			</caption>
			<tbody>
			<?php foreach ( $available_apps as $app ) : ?>

				<tr>
					<!-- Dev notes:- icon classes has to be printed inside the loop to show correct icon. -->
					<td><i class="sui-icon-social-youtube sui-xl" aria-hidden="true"></i></td>
					<td colspan="10"><strong><?php echo esc_html( $app ); ?></strong></th>
					<td>
						<div class="sui-tooltip" data-tooltip="<?php esc_html_e( 'Connect App', 'broken-link-checker' ); ?>">
							<button role="button" class="sui-button-icon">
								<i class="sui-icon-plus" aria-hidden="true"></i>
								<span class="sui-screen-reader-text"><?php echo esc_html( $app ); ?></span>
							</button>
						</div>
					</td>
				</tr>

			<?php endforeach; } ?>
			</tbody>
		</table>

	</div>

</div>
