<?php
/**
 * Connect app Youtube API & Mediafire API.
 *
 * @package templates
 */

?>

<div class="sui-modal sui-modal-sm">
	<div
		role="dialog"
		id="wdblc-onboarding-modal"
		class="sui-modal-content"
		aria-live="polite"
		aria-modal="true"
		aria-label="<?php esc_html_e( 'Configure how you want your links to be checked.', 'wpmudev' ); ?>"
	>
		<!-- Loading modal slide 1. -->
		<?php $this->render_part( 'settings/connect-app/loading' ); ?>

		<!-- Connect app slide 2. -->
		<?php $this->render_part( 'settings/connect-app/connect' ); ?>

		<!-- Connected modal slide 3. -->
		<?php $this->render_part( 'settings/connect-app/connected' ); ?>
	</div>
</div>
