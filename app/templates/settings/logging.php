<?php
/**
 * Logging settings page.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>
<div class="sui-box" data-tab="header">

	<div class="sui-box-header">
		<h2 class="sui-box-title"><?php esc_html_e( 'Logging', 'broken-link-checker' ); ?></h2>
	</div>

	<div class="sui-box-body">

		<div id="notice-id" class="sui-notice sui-notice-success">
			<div class="sui-notice-content">
				<div class="sui-notice-message">

					<i class="sui-notice-icon sui-icon-check-tick sui-success" aria-hidden="true"></i>

					<p><?php esc_html_e( 'Logging is enabled and a copy of your log file will be stored.', 'broken-link-checker' ); ?></p>

				</div>
			</div>
		</div>

		<div class="sui-box-settings-row">

			<div class="sui-box-settings-col-1">
				<span class="sui-settings-label">
					<?php esc_html_e( 'Enable logging', 'broken-link-checker' ); ?>
				</span>
				<span class="sui-description">
					<?php esc_html_e( 'Activate this option to save a copy of your log file.', 'broken-link-checker' ); ?>
				</span>
			</div>

			<div class="sui-box-settings-col-2">

				<div class="sui-form-field">
					<label for="dashboard" id="dashboard-label" class="sui-toggle">
						<input
							type="checkbox"
							id="dashboard"
							<?php if ( $logging_enabled ) : ?>
							checked=checked
							<?php endif; ?>
							aria-labelledby="dashboard-label"
							aria-describedby="dashboard-description"
							aria-controls="dashboard-settings"
						>

						<span class="sui-toggle-slider" aria-hidden="true"></span>

						<span id="dashboard-heading" class="sui-toggle-label">
							<?php esc_html_e( 'Enable Logging', 'broken-link-checker' ); ?>
						</span>

						<span id="dashboard-description" class="sui-description">
							<?php esc_html_e( 'Save a copy of your log file and choose where you want to store your log file.', 'broken-link-checker' ); ?>
						</span>
					</label>
				</div>

					<?php // Add sui-hidden class to hide the module. ?>
					<div tabindex="0" class="sui-toggle-content sui-border-frame">

						<div class="sui-form-field">

							<span id="description-choose-path" class="sui-description">
								<?php esc_attr_e( 'Choose where you want to store your log file', 'broken-link-checker' ); ?>
							</span>

							<label for="unique-id-1" id="label-unique-id-1" class="sui-radio sui-radio-stacked">

								<input
									type="radio"
									name="custom_log_file_enabled"
									<?php if ( ! $custom_log_file_enabled ) : ?>
									checked=checked
									<?php endif; ?>
									id="unique-id-1"
									aria-labelledby="label-unique-id-1"
									aria-describedby="description-choose-path"
								>
								<span aria-hidden="true"></span>
								<span id="option-unique-id-1"><?php esc_attr_e( 'Default', 'broken-link-checker' ); ?></span>

							</label>

							<span id="description-unique-id-1" class="sui-description" style="margin-left: 25px; margin-bottom: 10px;">
								<?php
								printf(
									/* translators: %1$s - link, %2$s - closing link tag */
									esc_html__( '../wp-content/uploads/broken-link-checker/%1$sblc-log.txt%2$s', 'broken-link-checker' ),
									'<a href="' . esc_url( network_admin_url( '#' ) ) . '">',
									'</a>'
								);
								?>
							</span>
						</div>

						<div class="sui-form-field">

							<label for="unique-id-2" id="label-unique-id-2" class="sui-radio sui-radio-stacked">

								<input
									type="radio"
									name="custom_log_file_enabled"
									<?php if ( $custom_log_file_enabled ) : ?>
									checked=checked
									<?php endif; ?>
									id="unique-id-2"
									aria-labelledby="label-unique-id-2"
								>
								<span aria-hidden="true"></span>
								<span id="option-unique-id-2"><?php esc_attr_e( 'Custom', 'broken-link-checker' ); ?></span>

							</label>

							<label for="unique-id" id="label-unique-id" class="sui-label sui-screen-reader-text"><?php esc_html_e( 'Custom path', 'broken-link-checker' ); ?></label>

							<input
								placeholder="<?php esc_attr_e( '.. /wp-content/uploads/broken-link-checker/custom.txt', 'broken-link-checker' ); ?>"
								value="<?php echo esc_attr( $log_file ); ?>"
								id="unique-id"
								class="sui-form-control"
								aria-labelledby="label-unique-id"
								aria-describedby="error-unique-id description-unique-id"
							/>

							<!--
							NOTE:
							Notice error message element it is empty. This because content should be printed
							when error happens and not before to avoid screenreaders confusing users.
							-->
							<span id="error-unique-id" class="sui-error-message" style="display: none;" role="alert"></span>

							<span id="description-unique-id" class="sui-description"><?php esc_html_e( 'Choose where you want to store your log file.', 'broken-link-checker' ); ?></span>
						</div>

						<div class="sui-form-field">

							<span class="sui-settings-label" style="margin-top: 20px;">
								<strong>
									<?php esc_html_e( 'Clear Schedule', 'broken-link-checker' ); ?>
								</strong>
							</span>

							<label for="unique-id" id="label-unique-id" class="sui-label sui-screen-reader-text"><?php esc_html_e( 'Clear Schedule', 'broken-link-checker' ); ?></label>

							<span id="description-unique-id" class="sui-description">
								<?php esc_html_e( 'Choose how frequently you want your log file to be automatically cleared.', 'broken-link-checker' ); ?>
							</span>

							<select id="unique-id" aria-labelledby="label-unique-id" aria-describedby="description-unique-id" style="margin-top: 20px;"
								<option
									value=""
									<?php if ( ! $clear_log_on ) : ?>
										selected=selected
									<?php endif; ?>
								>
									<?php esc_html_e( 'Never', 'broken-link-checker' ); ?>
								</option>
								<option
									value="10min"
									<?php if ( '10min' === $clear_log_on ) : ?>
										selected=selected
									<?php endif; ?>
								>
									<?php esc_html_e( 'Every 10 minutes', 'broken-link-checker' ); ?>
								</option>
								<option
									value="hourly"
									<?php if ( 'hourly' === $clear_log_on ) : ?>
										selected=selected
									<?php endif; ?>
								>
									<?php esc_html_e( 'Once Hourly', 'broken-link-checker' ); ?>
								</option>
								<option
									value="daily"
									<?php if ( 'daily' === $clear_log_on ) : ?>
										selected=selected
									<?php endif; ?>
								>
									<?php esc_html_e( 'Once Daily', 'broken-link-checker' ); ?>
								</option>
								<option
									value="twicedaily"
									<?php if ( 'twicedaily' === $clear_log_on ) : ?>
										selected=selected
									<?php endif; ?>
								>
									<?php esc_html_e( 'Twice Daily', 'broken-link-checker' ); ?>
								</option>
								<option
									value="weekly"
									<?php if ( 'weekly' === $clear_log_on ) : ?>
										selected=selected
									<?php endif; ?>
								>
									<?php esc_html_e( 'Once Weekly', 'broken-link-checker' ); ?>
								</option>
								<option
									value="bimonthly"
									<?php if ( 'bimonthly' === $clear_log_on ) : ?>
										selected=selected
									<?php endif; ?>
								>
									<?php esc_html_e( 'Twice a Month', 'broken-link-checker' ); ?>
								</option>
							</select>


						</div>

					</div>
			</div>
		</div>
	</div>

	<div class="sui-box-footer">
		<div class="sui-actions-right">

			<button type="submit" class="sui-button sui-button-blue" id="wp-blc-save-settings" aria-describedby="blc-submit-description" data-msg="Save Changed">
				<i class="sui-icon-save" aria-hidden="true"></i>
				<?php esc_html_e( 'SAVE CHANGES', 'broken-link-checker' ); ?>
			</button>

			<span class="sui-icon-loader sui-loading sui-hidden"></span>
		</div>
	</div>
</div>

