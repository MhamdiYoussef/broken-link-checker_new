<?php
/**
 * Permissions settings page.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>

<div class="sui-box" data-tab="header">

	<div class="sui-box-header">
		<h2 class="sui-box-title"><?php esc_html_e( 'Permissions', 'broken-link-checker' ); ?></h2>
	</div>

<form action="<?php echo esc_attr( $page_url ); ?>" method="POST">
	<div class="sui-box-body">
		<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
				<span class="sui-settings-label">
					<?php esc_html_e( 'Dashboard widget', 'broken-link-checker' ); ?>
				</span>
				<span class="sui-description">
					<?php esc_html_e( 'Select which user roles are able to see a summary of this website\'s link issues in their WordPress Admin Dashboard area.', 'broken-link-checker' ); ?>
				</span>
			</div>

			<div class="sui-box-settings-col-2">
				<div class="sui-form-field">
					<label for="dashboard" class="sui-toggle">
						<input
							type="checkbox"
							name="widget_enabled"
							<?php if ( $widget_enabled ) : ?>
							checked=checked
							<?php endif; ?>
							id="dashboard"
							aria-labelledby="dashboard"
							aria-describedby="dashboard-description"
							aria-controls="dashboard-settings"
						>
						<span class="sui-toggle-slider" aria-hidden="true"></span>
						<span id="dashboard-label" class="sui-toggle-label">
							<?php esc_html_e( 'Enable Dashboard widget', 'broken-link-checker' ); ?>
						</span>
					</label>

					<div tabindex="0" class="sui-toggle-content sui-border-frame" aria-label="Edit your pop-up custom border settings.">

						<span class="sui-description">
							<?php esc_attr_e( 'Choose which user roles can see the dashboard widget.', 'broken-link-checker' ); ?>
						</span>
						<div class="sui-form-field">

						<?php foreach ( $roles as $role_id => $data ) : ?>

							<label for="<?php echo esc_html( sanitize_title_with_dashes( $role_id ) ); ?>" class="sui-checkbox sui-checkbox-stacked">

								<input
									type="checkbox"
									<?php
									if ( $data['enabled'] ) :
										?>
										checked=checked<?php endif; ?>
									name="<?php echo esc_attr( sanitize_title_with_dashes( $role_id ) ); ?>"
									id="<?php echo esc_attr( sanitize_title_with_dashes( $role_id ) ); ?>"
									aria-labelledby="label-<?php echo esc_html( sanitize_title_with_dashes( $role_id ) ); ?>"
								/>

								<span aria-hidden="true"></span>

								<span id="label-<?php echo esc_html( sanitize_title_with_dashes( $role_id ) ); ?>"><?php echo esc_html( $data['name'] ); ?></span>

							</label>

						<?php endforeach; ?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="sui-box-footer">
		<div class="sui-actions-right">

			<button type="submit" name="save-settings" value="1" class="sui-button sui-button-blue" id="wp-blc-save-settings" aria-describedby="blc-submit-description" data-msg="Save Changed">
				<i class="sui-icon-save" aria-hidden="true"></i>
				<?php esc_html_e( 'SAVE CHANGES', 'broken-link-checker' ); ?>
			</button>

			<span class="sui-icon-loader sui-loading sui-hidden"></span>
		</div>
	</div>
	<input type="hidden" name="settings-tab" value="permissions">
	<?php wp_nonce_field( 'wdblc_settings_permissions' ); ?>
</form>
</div>
