<?php
/**
 * Loading modal slide 1.
 *
 * @package templates
 */

?>

<div class="sui-box" style="margin-bottom: calc(100% - 90%);">

	<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">

		<figure class="sui-box-logo" aria-hidden="true">
			<img src="<?php echo esc_html( plugins_url() . '/broken-link-checker/assets/images/blc-summary@2x.png' ); ?>" alt="<?php esc_html_e( 'Broken Link Checker Logo', 'broken-link-checker' ); ?>">
			<figcaption><?php esc_html_e( 'Broken Link Checker Logo', 'broken-link-checker' ); ?></figcaption>
		</figure>

		<button class="sui-button-icon sui-button-float--right" data-modal-close="">
			<i class="sui-icon-close sui-md" aria-hidden="true"></i>
			<span class="sui-screen-reader-text"><?php esc_html_e( 'Close this modal', 'broken-link-checker' ); ?></span>
		</button>

		<h3 class="sui-box-title sui-lg"><?php esc_html_e( 'Connect YouTube', 'broken-link-checker' ); ?></h3>

	</div>

	<div class="sui-box-body sui-content-center">
		<i class="sui-icon-loader sui-loading" aria-hidden="true"></i>
	</div>

</div>

