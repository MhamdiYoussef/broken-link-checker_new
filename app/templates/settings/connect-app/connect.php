<?php
/**
 * Connect app slide 2.
 *
 * @package templates
 */

// link to google console.
$google_console_link     = 'https://console.developers.google.com/';
$google_credentials_link = 'https://console.developers.google.com/apis/credentials';
?>

<div class="sui-box" style="margin-bottom: calc(100% - 90%);">

	<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">

		<figure class="sui-box-logo" aria-hidden="true">
			<img src="<?php echo esc_html( plugins_url() . '/broken-link-checker/assets/images/blc-summary@2x.png' ); ?>" alt="<?php esc_html_e( 'Broken Link Checker Logo', 'broken-link-checker' ); ?>">
			<figcaption><?php esc_html_e( 'Broken Link Checker Logo', 'broken-link-checker' ); ?></figcaption>
		</figure>

		<button class="sui-button-icon sui-button-float--right" data-modal-close="">
			<i class="sui-icon-close sui-md" aria-hidden="true"></i>
			<span class="sui-screen-reader-text"><?php esc_html_e( 'Close this modal', 'broken-link-checker' ); ?></span>
		</button>

		<h3 class="sui-box-title sui-lg"><?php esc_html_e( 'Connect YouTube', 'broken-link-checker' ); ?></h3>

		<p class="sui-description">
			<?php
			printf(
				/* translators: %1$s - <a> link, %2$s - </a> */
				esc_html__( 'Enter your YouTube API credentials below to connect your account. You can find your credentials %1$shere%2$s.', 'broken-link-checker' ),
				'<a href="' . esc_html( $google_credentials_link ) . '" target="_blank">',
				'</a>'
			);
			?>
		</p>

		<div class="sui-form-field">

			<div class="sui-form-field">
				<label for="connect-api" class="sui-label"><?php esc_html_e( 'API URL', 'broken-link-checker' ); ?></label>

				<div class="sui-control-with-icon sui-left-icon">
					<span class="sui-icon-key" aria-hidden="true"></span>
					<input type="text" placeholder="Enter API Credentials" id="connect-api" class="sui-form-control" style="margin-bottom: 20px;" aria-labelledby="" aria-describedby="">
				</div>
			</div>

		</div>

	</div>

		<div class="sui-accordion sui-accordion-flushed">

			<div class="sui-accordion-item sui-accordion-item--open">

				<div class="sui-accordion-item-header">
					<div class="sui-accordion-item-title"><?php esc_html_e( 'Instructions to retrieve your YouTube API', 'broken-link-checker' ); ?>
						<button class="sui-button-icon sui-accordion-open-indicator" aria-label="Open item"><i class="sui-icon-chevron-down" aria-hidden="true"></i></button>
					</div>
				</div>

				<div class="sui-accordion-item-body">
					<div class="sui-box">
						<div class="sui-box-body">
							<p><?php esc_html_e( 'Follow these instructions to retrieve your API URL.', 'broken-link-checker' ); ?></p>
							<ul>
								<li>
									<?php
									printf(
										/* translators: %1$s - <a> link, %2$s - </a> */
										esc_html__( '1. Go to the %1$sGoogle Developers Console%2$s.', 'broken-link-checker' ),
										'<a href="' . esc_html( $google_console_link ) . '" target="_blank">',
										'</a>'
									);
									?>
								</li>
								<li>
									<?php esc_html_e( '2. Go to Enable APIs and Services and search for YouTube Data API v3.', 'broken-link-checker' ); ?>
								</li>
								<li>
									<?php esc_html_e( '3. Enable YouTube Data API v3.', 'broken-link-checker' ); ?>
								</li>
								<li>
									<?php esc_html_e( '4. Next, go to Credentials > Create Credentials and click in the API Key label.', 'broken-link-checker' ); ?>
								</li>
								<li>
									<?php esc_html_e( '5. Once your API is created, copy and paste it into the form field.', 'broken-link-checker' ); ?>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
	</div>

	<div class="sui-box-footer">

		<div class="sui-actions-right">

			<button type="submit" class="sui-button sui-button-onload" aria-describedby="blc-submit-description" data-msg="Save Changes">

				<!-- Content Wrapper -->
				<span class="sui-loading-text"><?php esc_html_e( 'Save', 'broken-link-checker' ); ?></span>

				<!-- Spinning loading icon -->
				<span class="sui-icon-loader sui-loading sui-md" aria-hidden="true"></span>

			</button>

		</div>
	</div>

</div>


