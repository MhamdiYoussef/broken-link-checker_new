<?php
/**
 * Engine setting page.
 *
 * @package BrokenLinkChecker
 * @since 1.0.0
 */

?>


<div class="sui-box" data-tab="header">

	<div class="sui-box-header">
		<h2 class="sui-box-title"><?php esc_html_e( 'Engine Mode', 'broken-link-checker' ); ?></h2>
	</div>

	<form action="<?php echo esc_attr( $page_url ); ?>" method="POST">
	<div class="sui-box-body">
		<!-- SECTION: Simple Header -->
			<div class="sui-box-settings-row">
				<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Engine Mode', 'broken-link-checker' ); ?>
					</span>
					<span class="sui-description">
						<?php esc_html_e( 'Enable the Automatic Engine Mode to automatically analyze your website to ensure all links are valid.', 'broken-link-checker' ); ?>
					</span>
				</div>

				<div class="sui-box-settings-col-2">

					<div class="sui-form-field">
						<label for="toggle-automatic-engine-on" class="sui-toggle">
							<input
								type="checkbox"
								id="toggle-automatic-engine-on"
								name="automatic_engine_on"
								aria-labelledby="toggle-automatic-engine-on-label"
								aria-describedby="toggle-automatic-engine-on-description"
								aria-controls="toggle-automatic-engine-on-settings"
								<?php checked( $automatic_engine_on ); ?>
							>
							<span class="sui-toggle-slider" aria-hidden="true"></span>
							<span id="toggle-automatic-engine-on-label" class="sui-toggle-label">
								<?php esc_html_e( 'Enable Automatic Engine Mode', 'broken-link-checker' ); ?>
							</span>
							<span id="toggle-automatic-engine-on-description" class="sui-description">
								<?php esc_html_e( 'Enable this to check your website automatically.', 'broken-link-checker' ); ?>
							</span>
						</label>

						<div tabindex="0"
							id="toggle-automatic-engine-on-content" 
							class="sui-toggle-content sui-border-frame" 
							aria-label="Edit your pop-up custom border settings."
							<?php echo $automatic_engine_on ? '' : 'hidden'; ?>
						>

							<div class="sui-row sui-col">
								<span class="sui-settings-label">
									<?php esc_html_e( 'Link Monitor', 'broken-link-checker' ); ?>
								</span>

								<span class="sui-description">
									<?php esc_html_e( 'Choose how frequently the link monitor will run in background and whether to run the monitor continuously while the Dashboard is open.', 'broken-link-checker' ); ?>
								</span>


										<div class="sui-form-field sui-margin-top">

											<label for="custom-width" id="custom-width-label" class="sui-label"><?php esc_html_e( 'Run in Background', 'broken-link-checker' ); ?></label>
											<span class="sui-field-prefix">Every</span>

											<label for="link-monitor-every" id="label-link-monitor-every" class="sui-label sui-screen-reader-text">
												<?php esc_html_e( 'Every', 'broken-link-checker' ); ?>
											</label>

												<input name="run_via_cron_frequency" id="link-monitor-every" value="<?php echo esc_attr( $run_via_cron_frequency ); ?>" class="sui-form-control sui-field-has-prefix sui-field-has-suffix sui-input-sm" aria-labelledby="custom-width-label">


											<label for="link-monitor-freq" id="label-link-monitor-freq" class="sui-label sui-screen-reader-text">
												<?php esc_html_e( 'Every', 'broken-link-checker' ); ?>
											</label>

											<select id="link-monitor-freq" name="run_via_cron_period" aria-labelledby="label-link-monitor-freq">
												<option
												value="hours"
												<?php if ( 'hours' === $run_via_cron_period ) : ?>
													selected=selected
												<?php endif; ?>
												>
													<?php esc_html_e( 'Hours', 'broken-link-checker' ); ?>
												</option>
												<option
												value="days"
												<?php if ( 'days' === $run_via_cron_period ) : ?>
													selected=selected
												<?php endif; ?>
												>
													<?php esc_html_e( 'Days', 'broken-link-checker' ); ?>
												</option>
												<option
												value="weeks"
												<?php if ( 'weeks' === $run_via_cron_period ) : ?>
													selected=selected
												<?php endif; ?>
												>
													<?php esc_html_e( 'Weeks', 'broken-link-checker' ); ?>
												</option>

											</select>


											<label for="link-monitor-run" id="label-link-monitor-run" class="sui-checkbox sui-checkbox-sm">

												<input
													type="checkbox"
													id="link-monitor-run"
													name="run_in_dashboard"
													<?php if ( $run_in_dashboard ) : ?>
													checked=checked
													<?php endif; ?>
													aria-labelledby="label-link-monitor-run sui-checkbox-stacked"
												/>

												<span aria-hidden="true"></span>

												<span id="label-link-monitor-run">
													<?php esc_html_e( 'Run continuously while the Dashboard is open', 'broken-link-checker' ); ?>
												</span>

											</label>
										</div>
							</div>

							<div class="sui-row sui-col">
								<span class="sui-settings-label">
									<?php esc_html_e( 'Re-check Frequency', 'broken-link-checker' ); ?>
								</span>

								<span class="sui-description">
									<?php esc_html_e( 'Choose how frequently you want to re-check existing links. Note: Checking links too often could put excess load on your server. We recommend keeping this to weekly.', 'broken-link-checker' ); ?>
								</span>
							</div>

							<div class="sui-form-field">

								<label
									for="re-check-freq"
									id="label-re-check-freq"
									class="sui-label"
								>
								<?php esc_html_e( 'Re-check existing links', 'broken-link-checker' ); ?>
								</label>

								<span class="sui-field-prefix">
									<?php esc_html_e( 'Every', 'broken-link-checker' ); ?>
								</span>

								<input
									placeholder="0"
									name="check_threshold"
									value="<?php echo esc_attr( $check_threshold ); ?>"
									id="re-check-freq"
									class="sui-form-control sui-field-has-prefix sui-field-has-suffix sui-input-sm"
									aria-labelledby="label-re-check-freq"
								/>

								<span class="sui-field-suffix"><?php esc_html_e( 'Hours', 'broken-link-checker' ); ?></span>

							</div>

						</div>

					</div>
				</div>
			</div>

			<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Timeout', 'broken-link-checker' ); ?>
					</span>
				<span class="sui-description">
						<?php esc_html_e( 'Links that take longer than this to load will be marked as broken.', 'broken-link-checker' ); ?>
					</span>
			</div>

			<div class="sui-box-settings-col-2">

				<div class="sui-form-field">

						<div class="sui-form-field">

							<label
								for="timeout"
								id="label-timeout"
								class="sui-label sui-screen-reader-text"
							>
							</label>

							<input
								placeholder="<?php echo esc_attr( '0' ); ?>"
								id="timeout"
								name="timeout"
								value="<?php echo esc_attr( $timeout ); ?>"
								class="sui-form-control sui-field-has-prefix sui-field-has-suffix sui-input-sm"
								aria-labelledby="label-timeout"
							/>

							<span class="sui-field-suffix"><?php esc_html_e( 'seconds', 'broken-link-checker' ); ?></span>
							<span id="description-timeout" class="sui-description"><?php esc_html_e( 'Note: The default timeout is set to 30 seconds.', 'broken-link-checker' ); ?></span>
						</div>
				</div>
			</div>
		</div>

			<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Max. Execution Time', 'broken-link-checker' ); ?>
					</span>
				<span class="sui-description">
						<?php esc_html_e( 'Choose the maximum time the link monitor can run before stopping automatically.', 'broken-link-checker' ); ?>
					</span>
			</div>

			<div class="sui-box-settings-col-2">

				<div class="sui-form-field">

					<div class="sui-form-field">

						<div class="sui-row">
								<span class="sui-settings-label">
									<?php esc_html_e( 'Set execution time', 'broken-link-checker' ); ?>
								</span>

							<span class="sui-description">
									<?php esc_html_e( 'The plugin works by periodically launching a background job that parses your posts for links, checks the discovered URLs, and performs other time-consuming tasks.', 'broken-link-checker' ); ?>
								</span>
						</div>

						<label
							for="max-execution"
							id="label-max-execution"
							class="sui-label sui-screen-reader-text"
						>
							<?php esc_html_e( 'Re-check existing links', 'broken-link-checker' ); ?>
						</label>

						<input
							placeholder="0"
							name="max_execution_time"
							value="<?php echo esc_attr( $max_execution_time ); ?>"
							id="max-execution"
							type="text"
							class="sui-form-control sui-field-has-prefix sui-field-has-suffix sui-input-sm"
							aria-labelledby="label-max-execution"
						/>

						<span class="sui-field-suffix">
							<?php esc_html_e( 'seconds', 'broken-link-checker' ); ?>
						</span>
						<span id="description-max-execution" class="sui-description"><?php esc_html_e( 'Note: The default execution time is set to 420 seconds.', 'broken-link-checker' ); ?></span>
					</div>
				</div>
			</div>
		</div>

			<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Server Load Limit', 'broken-link-checker' ); ?>
					</span>
				<span class="sui-description">
						<?php esc_html_e( 'Link checking will be suspended if the average server load rises above this number.', 'broken-link-checker' ); ?>
					</span>
			</div>

			<div class="sui-box-settings-col-2">

				<div class="sui-form-field">
					<label for="toggle-52" class="sui-toggle">
						<input type="checkbox"
							id="toggle-52"
							name="user_enabled_load_limit"
							aria-labelledby="toggle-52-label"
							aria-describedby="toggle-52-description"
							aria-controls="toggle-52-settings"
							<?php if ( $user_enabled_load_limit ) : ?>
							checked=checked
							<?php endif; ?>
						>
						<span class="sui-toggle-slider" aria-hidden="true"></span>
						<span id="toggle-52-label" class="sui-toggle-label">
								<?php esc_html_e( 'Enable Server load limit', 'broken-link-checker' ); ?>
							</span>
					</label>

					<div tabindex="0" class="sui-toggle-content sui-border-frame" aria-label="Edit your pop-up custom border settings.">

						<div class="sui-form-field">

							<label
								for="server-limit"
								id="label-server-limit"
								class="sui-label sui-screen-reader-text"
							>
								<?php esc_html_e( 'Re-check existing links', 'broken-link-checker' ); ?>
							</label>

							<input
								placeholder="4.00"
								name="server_load_limit"
								value="<?php echo esc_attr( $server_load_limit ); ?>"
								type="number"
								step="0.1"
								id="server-limit"
								class="sui-form-control sui-field-has-prefix sui-field-has-suffix sui-input-sm"
								aria-labelledby="label-server-limit"
							/>

							<span class="sui-field-suffix">
								<?php esc_html_e( 'Current load: 1.50', 'broken-link-checker' ); ?>
							</span>

						</div>

					</div>

				</div>
			</div>
		</div>

			<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Active Time Ratio', 'broken-link-checker' ); ?>
					</span>
				<span class="sui-description">
						<?php esc_html_e( 'Link checking will try to maintain given ratio of actively running and being idle.', 'broken-link-checker' ); ?>
					</span>
			</div>

			<div class="sui-box-settings-col-2">

				<div class="sui-form-field">

					<div class="sui-progress-block">

						<div class="sui-progress">

							<div class="sui-progress-bar" aria-hidden="true">
							<span style="width: <?php echo esc_attr( $target_resource_usage ); ?>%"></span>
							</div>
							<span class="sui-progress-text">
								<?php echo esc_html( $target_resource_usage ); ?>%
							</span>
							<!-- Place progress elements here -->
						</div>

					</div>
					<span class="sui-description">
						<?php esc_html_e( 'Set by default at 40%.', 'broken-link-checker' ); ?>
					</span>

				</div>
			</div>
		</div>

			<div class="sui-box-settings-row">
			<div class="sui-box-settings-col-1">
					<span class="sui-settings-label">
						<?php esc_html_e( 'Forced recheck', 'broken-link-checker' ); ?>
					</span>
				<span class="sui-description">
						<?php esc_html_e( 'Needing to start fresh? Use this button to empty this site\'s link database.', 'broken-link-checker' ); ?>
					</span>
			</div>

			<div class="sui-box-settings-col-2">

				<div class="sui-form-field">

					<button role="button" class="sui-button sui-button-ghost">
						<i class="sui-icon-refresh" aria-hidden="true"></i>
						<?php esc_html_e( 'RE-CHECK ALL PAGES', 'broken-link-checker' ); ?>
					</button>
					<span class="sui-description">
						<?php esc_html_e( 'This option will reset the links database.', 'broken-link-checker' ); ?>
					</span>

				</div>
			</div>
		</div>

	</div>

	<div class="sui-box-footer">

		<div class="sui-actions-right">

			<button type="submit" value="1" name="save-settings" class="sui-button sui-button-blue" id="wp-blc-save-settings" aria-describedby="blc-submit-description" data-msg="Save Changed">
				<i class="sui-icon-save" aria-hidden="true"></i>
				<?php esc_html_e( 'SAVE CHANGES', 'broken-link-checker' ); ?>
			</button>

			<span class="sui-icon-loader sui-loading sui-hidden"></span>
		</div>
	</div>
	<input type="hidden" name="settings-tab" value="engine">
	<?php wp_nonce_field( 'wdblc_settings_engine' ); ?>
</form>
</div>
