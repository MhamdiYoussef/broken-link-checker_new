<?php
/**
 * Setting page.
 * Links page
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>
<main class="sui-wrap">
<script type="text/javascript">
window.BLC = window.BLC || {};
BLC.ajax_url = "<?php echo esc_html( admin_url( 'admin-ajax.php' ) ); ?>";

automatic_engine_on = <?php echo esc_html( $automatic_engine_on ? 'true' : 'false' ); ?>;
if ( automatic_engine_on ) {
	BLC.work_nonce = "<?php echo esc_html( wp_create_nonce( 'blc_work' ) ); ?>";
}
</script>
	<?php
	$this->render_header();
	$tabbed_content->render();
	$this->render_part( 'footer' );
	?>
	<div class="sui-floating-notices">
		<div
				role="alert"
				id="ntc-confirm"
				class="sui-notice"
				aria-live="assertive"
		>
		</div>
		</div>
	<div id="notification-texts" style="display:none;">
		<div id="ntc-work-done"><p><?php esc_attr_e( 'Your link check has finished. Reload page to see results.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-not-allowed"><p><?php esc_attr_e( 'You are not allowed to execute this action. Refresh the page to reload permissions.', 'broken-link-checker' ); ?></p></div>
	</div>
</main>
