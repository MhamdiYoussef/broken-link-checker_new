<?php
/**
 * Header page.
 *
 * @package BrokenLinkChecker
 * @since 1.0.0
 */

$broken_count = absint( $broken );
$tooltip_broken = sprintf(
	/* translators: %d - number of broken links */
	_n( 'You have %d link issue, fix them up!', 'You have %d link issues, fix them up!', $broken_count, 'wp-smushit' ),
	$broken_count
);
$tooltip_no_broken = sprintf(
	/* translators: %d - number of broken links, it should be 0 in this case */
	_n( 'You have %d link issue, well done!', 'You have %d link issues, well done!', $broken_count, 'wp-smushit' ),
	$broken_count
);
/**
 * Add Branded Image filter.
 * Return the image URL
 */
$branded_image = apply_filters( 'wpmudev_branding_hero_image', '' );
?>

<script>
	BLC.work_nonce = "<?php echo esc_html( wp_create_nonce( 'blc_work' ) ); ?>";
</script>
<div class="sui-header">

	<h1 class="sui-header-title">
		<?php echo esc_html( $this->get_page_title() ); ?>
	</h1>

	<!-- Float element to Right -->
	<div class="sui-actions-right">
		<?php if ( ! $automatic_engine_on ) : ?>
		<button
			role="button"
			id="wdblc-run-check-btn"
			class="sui-button sui-button-blue "
			style="height: 31px;"
			data_nonce="<?php echo esc_html( wp_create_nonce( 'nonce_run_check_button' ) ); ?>"
		>

			<span class="wdblc-text"><?php esc_html_e( 'Run check', 'broken-link-checker' ); ?></span>
			<span class="sui-progress-icon sui-hidden" aria-hidden="true" style="position: absolute;top: 7px;left: 31px;">
				<span style="display:inline-block;" class="sui-icon-loader sui-loading"></span>
			</span>
		</button>
		<?php endif; ?>
		<button class="sui-button sui-button-ghost">
			<i class="sui-icon-academy" aria-hidden="true"></i>
			<?php esc_html_e( 'Documentation', 'broken-link-checker' ); ?>
		</button>
	</div>

</div>

<!-- Summary  -->
<div class="sui-box sui-summary">
<?php if ( $branded_image ) : ?>
	<div class="sui-summary-image-space sui-extra-CSS-image" aria-hidden="true" style="background-image: url('<?php echo esc_url( $branded_image ); ?>')"></div>
<?php else : ?>
	<div class="sui-summary-image-space" aria-hidden="true"></div>
<?php endif; ?>

	<div class="sui-summary-segment sui-padding-15-40">

		<div class="sui-summary-details">
			<span id="sui-summary-large-primary" class="sui-summary-large"><?php echo esc_html( $broken_count ); ?></span>
			<?php if ( $broken_count > 0 ) { ?>
				<span class="sui-tooltip" data-tooltip='<?php echo esc_html( $tooltip_broken ); ?>'>
					<span class="sui-icon-check-tick sui-warning" aria-hidden="true"></span>
				</span>
			<?php } else { ?>
				<span class="sui-tooltip" data-tooltip="<?php echo esc_html( $tooltip_no_broken ); ?>">
					<span class="sui-icon-check-tick sui-success" aria-hidden="true"></span>
				</span>
			<?php } ?>
			<span class="sui-summary-sub"><?php esc_html_e( 'Broken Links', 'broken-link-checker' ); ?></span>
			<span id="sui-summary-detail-primary" class="sui-summary-detail"><?php echo absint( $total ); ?></span>
			<span class="sui-summary-sub">	<span class="sui-summary-sub"><?php esc_html_e( 'Links Checked', 'broken-link-checker' ); ?></span></span>
		</div>

	</div>

	<div class="sui-summary-segment">

		<ul class="sui-list">

			<li>
				<span class="sui-list-label"><?php esc_html_e( 'Engine Mode', 'broken-link-checker' ); ?></span>
				<?php if ( $automatic_engine_on ) : ?>
				<span class="sui-list-detail sui-tag"><?php esc_html_e( 'Automatic', 'broken-link-checker' ); ?></span>
				<?php else : ?>
				<span class="sui-list-detail sui-tag"><?php esc_html_e( 'Manual', 'broken-link-checker' ); ?></span>
				<?php endif; ?>
				<span class="last-element"></span>
			</li>

			<li>
				<span class="sui-list-label"><?php esc_html_e( 'Last Check', 'broken-link-checker' ); ?></span>
				<?php if ( $last_checked ) : ?>
				<span id="last-checked-date" class="sui-list-detail wdblc-last-checked-date"><?php echo esc_html( wp_date( 'F j, Y ', $last_checked ) . __( 'at', 'broken-link-checker' ) . wp_date( ' H:i', $last_checked ) ); ?></span>
				<?php else : ?>
				<span class="sui-list-detail wdblc-last-checked-date"><?php esc_html_e( 'None', 'broken-link-checker' ); ?></span>
				<?php endif; ?>
				<span class="sui-list-detail wdblc-last-checked-progress sui-hidden">
					<span class="sui-progress-icon" aria-hidden="true">
						<span style="display:inline-block;" class="sui-icon-loader sui-loading"></span>
					</span>
					<?php esc_html_e( 'In progress...', 'broken-link-checker' ); ?>
				</span>
				<span class="last-element"></span>
			</li>

		</ul>

	</div>

</div>
