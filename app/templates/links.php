<?php
/**
 * Links page.
 *
 * @package BrokenLinkChecker
 * @since 1.0.0
 */

?>
<main class="sui-wrap">
<script type="text/javascript">
window.BLC = window.BLC || {};
BLC.ajax_url = "<?php echo esc_html( admin_url( 'admin-ajax.php' ) ); ?>";
BLC.links_nonce = "<?php echo esc_html( wp_create_nonce( 'wdblc_links' ) ); ?>";

automatic_engine_on = <?php echo esc_html( $automatic_engine_on ? 'true' : 'false' ); ?>;
if ( automatic_engine_on ) {
	BLC.work_nonce = "<?php echo esc_html( wp_create_nonce( 'blc_work' ) ); ?>";
}
</script>
<?php
$this->render_header();

$tabbed_content->render();

$this->render_part( 'footer' );
?>

	<div class="sui-floating-notices">
		<div
				role="alert"
				id="ntc-confirm"
				class="sui-notice"
				aria-live="assertive"
		>
		</div>
		</div>
	<div id="notification-texts" style="display:none;">
		<div id="ntc-link-redirected"><p><?php esc_attr_e( 'The link has been redirected.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-link-rechecked"><p><?php esc_attr_e( 'The link has been re-checked.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-link-ignored"><p><?php esc_attr_e( 'The link has been ignored.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-link-unlinked"><p><?php esc_attr_e( 'The link has been unlinked.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-link-dismissed"><p><?php esc_attr_e( 'The link has been restored.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-link-marked-not-broken"><p><?php esc_attr_e( 'The link has been marked as unbroken.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-cant-modify-link"><p><?php esc_attr_e( 'The link couldn\'t be modified.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-not-allowed"><p><?php esc_attr_e( 'You are not allowed to execute this action. Refresh the page to reload permissions.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-missing-link-data"><p><?php esc_html_e( 'Something went wrong. Error : link data not specified.', 'broken-link-checker' ); ?></p></div>
		<div id="ntc-link-not-found"><p> <?php esc_html_e( 'Something went wrong. Error: link not found.', 'broken-link-checker' ); ?></p></div >
		<div id="ntc-url-not-valid"><p> <?php esc_html_e( 'The new url is not in a valid format. Please check the url format.', 'broken-link-checker' ); ?></p></div >
		<div id="ntc-unexpected-error"><p> <?php esc_html_e( 'An unexpected error occurred. Refresh the page and try again.', 'broken-link-checker' ); ?></p></div >
		<div id="ntc-work-done"><p><?php esc_attr_e( 'Your link check has finished. Reload page to see results.', 'broken-link-checker' ); ?></p></div>
	</div>
</main>
