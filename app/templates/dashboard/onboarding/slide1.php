<?php
/**
 * Onboarding modal slide 1 template.
 *
 * @package templates
 */

?>
<div id="broken-link-checker--onboarding-modal--slide-1" class="sui-modal-slide sui-active sui-loaded" data-modal-size="md">
	<div class="sui-box">
		<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
			<figure class="sui-box-banner" aria-hidden="true">
				<img
				src="<?php echo esc_html( plugins_url() . '/broken-link-checker/assets/images/onboarding-welcome-header.png' ); ?>"
					srcset=""
				/>
			</figure>
			<button class="sui-button-icon wdblc-skip-onboarding sui-button-float--right">
				<span class="sui-icon-close sui-md" aria-hidden="true"></span>
				<span class="sui-screen-reader-text">Close this dialog.</span>
			</button>
			<?php // translators: %s users' display name. ?>
			<h3 class="sui-box-title sui-lg"><?php echo esc_html( sprintf( __( 'Welcome %s!', 'broken-link-checker' ), $user_name ) ); ?></h3>
			<?php $welcome_message = __( 'Nice work installing Broken Link Checker! Let\'s get started by configuring how you want your links to be checked and then let the plugin do all the heavy lifting for you.', 'broken-link-checker' ); ?>
			<p class="sui-description" style="margin-bottom: 0;"><?php echo esc_html( $welcome_message ); ?></p>
		</div>
		<div class="sui-box-body sui-spacing-bottom--50 sui-content-center">
			<button
				role="button" class="sui-button sui-button-icon-right sui-button-blue"
				data-modal-slide="broken-link-checker--onboarding--slide-2"
			>
				<?php esc_html_e( 'Begin Setup', 'broken-link-checker' ); ?> <i class="sui-icon-chevron-right" aria-hidden="true"></i>
			</button>
			<div class="sui-box-steps sui-sm sui-steps-float" aria-hidden="true">
				<span class="sui-current">First step (current).</span>
				<span>Second step.</span>
				<span>Third step.</span>
			</div>
		</div>
	</div>
	<button class="sui-modal-skip wdblc-skip-onboarding"><?php esc_html_e( 'Skip this, I\'ll set it up later', 'broken-link-checker' ); ?></button>
</div>