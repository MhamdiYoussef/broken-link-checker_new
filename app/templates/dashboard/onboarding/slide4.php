<?php
/**
 * Onboarding modal slide 4 template.
 *
 * @package templates
 */

?>
<div id="broken-link-checker--onboarding--slide-4" class="sui-modal-slide sui-loaded" data-modal-size="md">
	<div class="sui-box">
		<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
			<figure class="sui-box-banner" aria-hidden="true">
				<img
				src="<?php echo esc_html( plugins_url() . '/broken-link-checker/assets/images/onboarding-welcome-header.png' ); ?>"
					srcset=""
				/>
			</figure>
			<button class="sui-button-icon sui-button-float--right wdblc-skip-onboarding">
				<i class="sui-icon-close sui-md" aria-hidden="true"></i>
				<span class="sui-screen-reader-text">Close this dialog.</span>
			</button>
			<h3 class="sui-box-title sui-lg"><?php esc_html_e( 'Setup complete!', 'broken-link-checker' ); ?></h3>
			<?php $final_message = __( 'Great, we\'ve activated and pre-configured our recommended settings.<br>Now you can fine-tune it if you need to.', 'broken-link-checker' ); ?>
			<p class="sui-description" style="margin-bottom: 0;"><?php echo esc_html( $final_message ); ?></p>
		</div>
		<div class="sui-box-body sui-spacing-bottom--50 sui-content-center">
			<button
				role="button" id="wdblc-close-onboarding" class="sui-button sui-button-icon-right sui-button-blue"
			>
				<?php esc_html_e( 'Finish', 'broken-link-checker' ); ?> <i class="sui-icon-chevron-right" aria-hidden="true"></i>
			</button>
			<div class="sui-box-steps sui-sm sui-steps-float" aria-hidden="true">
				<span>First step.</span>
				<span>Second step.</span>
				<span class="sui-current">Third step (current).</span>
			</div>
		</div>
	</div>
	<button class="sui-modal-skip wdblc-skip-onboarding"><?php esc_html_e( 'Skip this, I\'ll set it up later', 'broken-link-checker' ); ?></button>
</div>
