<?php
/**
 * Onboarding modal slide 3 template.
 *
 * @package templates
 */

?>
<div id="broken-link-checker--onboarding--slide-3" class="sui-modal-slide sui-loaded" data-modal-size="md">
	<div class="sui-box">
		<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
			<figure class="sui-box-banner" aria-hidden="true">
				<img
				src="<?php echo esc_html( plugins_url() . '/broken-link-checker/assets/images/onboarding-welcome-header.png' ); ?>"
					srcset=""
				/>
			</figure>
			<button class="sui-button-icon sui-button-float--right wdblc-skip-onboarding">
				<span class="sui-icon-close sui-md" aria-hidden="true"></span>
				<span class="sui-screen-reader-text">Close this dialog.</span>
			</button>
			<h3 class="sui-box-title sui-lg"><?php esc_html_e( 'Settings', 'broken-link-checker' ); ?></h3>
			<?php $settings_message = __( 'Choose which types of links you want checker to verify.', 'broken-link-checker' ); ?>
			<p class="sui-description" style="margin-bottom: 0;"><?php echo esc_html( $settings_message ); ?></p>
		</div>
		<div class="sui-box-body sui-spacing-bottom--50 sui-content-center">
			<div class="sui-box-settings-row">
				<div class="sui-box-settings-col-1">
					<?php
					$i = 1;
					foreach ( $modules as $name => $option ) :
						?>
							<label for="<?php echo esc_attr( $name ); ?>-checkbox" class="sui-checkbox sui-checkbox-stacked">
								<input
									type="checkbox"
									<?php if ( $option['active'] ) : ?>
									checked=checked
									<?php endif; ?>
									id="<?php echo esc_attr( $name ); ?>-checkbox"
									name="module[<?php echo esc_attr( $name ); ?>]"
									aria-labelledby="<?php echo esc_attr( $name ); ?>-checkbox-label"
									data-tooltip=<?php echo esc_html_e( 'Activate' ); ?>
								/>
								<span aria-hidden="true"></span>
								<span id="<?php echo esc_attr( $name ); ?>-checkbox-label"
								<?php if( $name == 'youtube-iframe' ){ ?>
									class="sui-tooltip sui-tooltip-constrained"
									data-tooltip="<?php echo esc_html_e( 'Connect the Youtube API so we can check for broken links that prevent videos from displaying.' ); ?>"
								<?php } ?> ><?php echo esc_html( $option['title'] ); ?></span>
							</label>

						<?php if ( $i++ === $parsers_halfway ) : ?>
				</div>
				<div class="sui-box-selectors-col-2 sui-col-md-5">
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<button
				id="wdblc-onboarding-save"
				name="save-onboarding"
				value="1"
				role="button" class="sui-button sui-button-icon-right sui-button-blue"
				aria-live="polite"
				data-tooltip=<?php echo esc_html_e( 'Activate' ); ?>
			>
				<span class="sui-button-text-default">
					<span class="sui-icon-check" aria-hidden="true"></span>
					<span><?php esc_html_e( 'Save &amp; Finish Setup', 'broken-link-checker' ); ?></span>
				</span>
				<span class="sui-button-text-onload">
					<span class="sui-icon-loader sui-loading" aria-hidden="true"></span>
					<span><?php esc_html_e( 'Saving settings...', 'broken-link-checker' ); ?></span>
				</span>
			</button>	
			<div class="sui-box-steps sui-sm sui-steps-float" aria-hidden="true">
				<span>First step.</span>
				<span>Second step.</span>
				<span class="sui-current">Third step (current).</span>
			</div>
		</div>
	</div>
	<button class="sui-modal-skip wdblc-skip-onboarding"><?php esc_html_e( 'Skip this, I\'ll set it up later', 'broken-link-checker' ); ?></button>
</div>
