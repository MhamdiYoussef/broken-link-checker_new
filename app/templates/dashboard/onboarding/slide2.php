<?php
/**
 * Onboarding modal slide 2 template.
 *
 * @package templates
 */

?>
<div id="broken-link-checker--onboarding--slide-2" class="sui-modal-slide" data-modal-size="md">
	<div class="sui-box">
		<div class="sui-box-header sui-flatten sui-content-center sui-spacing-top--60">
			<figure class="sui-box-banner" aria-hidden="true">
				<img
				src="<?php echo esc_html( plugins_url() . '/broken-link-checker/assets/images/onboarding-welcome-header.png' ); ?>"
					srcset=""
				/>
			</figure>
			<button class="sui-button-icon sui-button-float--right wdblc-skip-onboarding">
				<span class="sui-icon-close sui-md" aria-hidden="true"></span>
				<span class="sui-screen-reader-text">Close this dialog.</span>
			</button>
			<h3 class="sui-box-title sui-lg"><?php esc_html_e( 'Automatic Link Check', 'broken-link-checker' ); ?></h3>
			<?php $automatic_check_message = __( 'When you add links to your site, Broken Link Checker can check them for you automatically.', 'broken-link-checker' ); ?>
			<p class="sui-description" style="margin-bottom: 0;"><?php echo esc_html( $automatic_check_message ); ?></p>
		</div>
		<div class="sui-box-body sui-spacing-bottom--10 sui-content-center">
			<div class="sui-form-field">
				<label for="wdblc-automatically-check-new-links" class="sui-toggle">
					<input
						type="checkbox"
						id="wdblc-automatically-check-new-links"
						name="automatic_engine_on"
						<?php if ( $automatic_engine_on ) : ?>
						checked=checked
						<?php endif; ?>
						aria-labelledby="wdblc-automatically-check-new-links-label"
					/>

					<!-- ELEMENT: Toggle slider. -->
					<span class="sui-toggle-slider" aria-hidden="true"></span>

					<!-- ELEMENT: Toggle label. -->
					<span id="wdblc-automatically-check-new-links-label" class="sui-toggle-label"><?php esc_html_e( 'Enable Automatic Engine mode', 'broken-link-checker' ); ?></span>
				</label>

			</div>
		</div>
		<div class="sui-box-body sui-spacing-bottom--10 sui-content-center">
			<button
				role="button"
				class="sui-button sui-button-gray"
				data-modal-slide="broken-link-checker--onboarding--slide-3"
			>
				<?php esc_html_e( 'Next', 'broken-link-checker' ); ?>
			</button>
		</div>
		<div class="sui-box-body sui-spacing-top--10 sui-spacing-bottom--30 sui-content-center">
			<div class="sui-box-steps sui-sm sui-steps-float" aria-hidden="true">
				<span>First step</span>
				<span class="sui-current">Second step.</span>
				<span>Third step.</span>
			</div>
		</div>
	</div>
	<button class="sui-modal-skip wdblc-skip-onboarding"><?php esc_html_e( 'Skip this, I\'ll set it up later', 'broken-link-checker' ); ?></button>
</div>
