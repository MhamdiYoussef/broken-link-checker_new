<?php
/**
 * Onboarding modal template.
 *
 * @package templates
 */

?>
<script type="text/javascript">
window.BLC = window.BLC || {};
window.BLC.wdblcOpenOnboardingModal = true;
</script>
<div class="sui-modal sui-modal-md">
	<div
		role="dialog"
		id="wdblc-onboarding-modal"
		class="sui-modal-content"
		aria-live="polite"
		aria-modal="true"
		aria-label="<?php esc_html_e( 'Configure how you want your links to be checked.', 'wpmudev' ); ?>"
	>
		<form action="<?php echo esc_attr( $page_url ); ?>" method="POST" id="wdblc-onboarding-form">
				<!-- SLIDE: Welcome. -->
		<?php $this->render_part( 'dashboard/onboarding/slide1' ); ?>

				<!-- SLIDE: Setup. -->
		<?php $this->render_part( 'dashboard/onboarding/slide2' ); ?>

				<!-- SLIDE: Settings. -->
		<?php $this->render_part( 'dashboard/onboarding/slide3' ); ?>

				<!-- SLIDE: Finish. -->
		<?php $this->render_part( 'dashboard/onboarding/slide4' ); ?>
		<?php wp_nonce_field( 'wdblc_onboarding' ); ?>
		</form>
	</div>
</div>
