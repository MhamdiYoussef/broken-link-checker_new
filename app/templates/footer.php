<?php
/**
 * Footer page part.
 *
 * @package BrokenLinkChecker
 * @since 2.0.0
 */

?>

<div class="sui-footer">
	<?php echo sprintf(esc_html__( 'Made with %s by WPMU DEV', 'broken-link-checker' ),'<span class="sui-icon-heart" aria-hidden="true"></span>'); ?>
</div>

<?php if ( blcUtility::is_member() ) : ?>

	<ul class="sui-footer-nav">
		<li><a href="https://wpmudev.com/hub2/"
				target="_blank"><?php esc_html_e( 'The Hub', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/projects/category/plugins/"
				target="_blank"><?php esc_html_e( 'Plugins', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/roadmap/"
				target="_blank"><?php esc_html_e( 'Roadmap', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/hub2/support/"
				target="_blank"><?php esc_html_e( 'Support', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/docs/"
				target="_blank"><?php esc_html_e( 'Docs', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/hub2/community/"
				target="_blank"><?php esc_html_e( 'Community', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/academy/"
				target="_blank"><?php esc_html_e( 'Academy', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/terms-of-service/"
				target="_blank"><?php esc_html_e( 'Terms of Service', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://incsub.com/privacy-policy/"
				target="_blank"><?php esc_html_e( 'Privacy Policy', 'broken-link-checker' ); ?></a></li>
	</ul>

<?php else : ?>

	<ul class="sui-footer-nav">
		<li><a href="https://profiles.wordpress.org/wpmudev#content-plugins"
				target="_blank"><?php esc_html_e( 'Free Plugins', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/features/"
				target="_blank"><?php esc_html_e( 'Membership', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/roadmap/"
				target="_blank"><?php esc_html_e( 'Roadmap', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wordpress.org/support/plugin/hummingbird-performance"
				target="_blank"><?php esc_html_e( 'Support', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/docs/"
				target="_blank"><?php esc_html_e( 'Docs', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/hub-welcome/"
				target="_blank"><?php esc_html_e( 'The Hub', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://wpmudev.com/terms-of-service/"
				target="_blank"><?php esc_html_e( 'Terms of Service', 'broken-link-checker' ); ?></a></li>
		<li><a href="https://incsub.com/privacy-policy/"
				target="_blank"><?php esc_html_e( 'Privacy Policy', 'broken-link-checker' ); ?></a></li>
	</ul>

<?php endif; ?>

<!-- ELEMENT: Social Media -->
<ul class="sui-footer-social">
    <li><a href="https://www.facebook.com/wpmudev" target="_blank">
            <span class="sui-icon-social-facebook" aria-hidden="true"></span>
            <span class="sui-screen-reader-text">Facebook</span>
        </a></li>
    <li><a href="https://twitter.com/wpmudev" target="_blank">
            <span class="sui-icon-social-twitter" aria-hidden="true"></span>
            <span class="sui-screen-reader-text">Twitter</span>
        </a></li>
    <li><a href="https://www.instagram.com/wpmu_dev/" target="_blank">
            <span class="sui-icon-instagram" aria-hidden="true"></span>
            <span class="sui-screen-reader-text">Instagram</span>
        </a></li>
</ul>