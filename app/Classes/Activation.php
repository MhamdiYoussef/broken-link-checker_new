<?php
/**
 * Activation class.
 *
 * This is the place where we handle activation/deactivation of the plugin
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Plugin Class handling activation/deactivation.
 *
 * @since  2.0.0
 * @access public
 */
class Activation {

	/**
	 * Initiates the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		// Activation hook.
		register_deactivation_hook( $loader, array( $this, 'deactivation' ) );
	}

	/**
	 * Deactivation method.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function deactivation() {
	}

	/**
	 * Activation method.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function activation() {
	}

	/**
	 * Uninstall method.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function uninstall() {
	}
}
