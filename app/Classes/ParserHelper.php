<?php
/**
 * A helper class for working with parsers. All its methods should be called statically.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Container Helper class.
 *
 * @since  2.0.0
 * @access public
 */
class ParserHelper {

	/**
	 * Get the parser matching a parser type ID.
	 *
	 * @uses blcModuleManager::get_module()
	 *
	 * @param string $parser_type Parser type.
	 * @return blcParser|null
	 */
	public static function get_parser( $parser_type ) {
		$manager = blcModuleManager::getInstance();
		return $manager->get_module( $parser_type, true, 'parser' );
	}

	/**
	 * Get all parsers that support either the specified format or the container type.
	 * If a parser supports both, it will still be included only once.
	 *
	 * @param string $format         Parser Format.
	 * @param string $container_type Container type.
	 * @return blcParser[]
	 */
	public static function get_parsers( $format, $container_type ) {
		$found = array();

		// Retrieve a list of active parsers.
		$manager        = blcModuleManager::getInstance();
		$active_parsers = $manager->get_modules_by_category( 'parser' );

		// Try each one.
		foreach ( $active_parsers as $module_id => $module_data ) {
			$parser = $manager->get_module( $module_id ); // Will autoload if necessary.
			if ( ! $parser ) {
				continue;
			}

			if ( in_array( $format, $parser->supported_formats, true ) || in_array( $container_type, $parser->supported_containers, true ) ) {
				array_push( $found, $parser );
			}
		}

		return $found;
	}
}
