<?php
/**
 * Database class.
 *
 * This is the place where we handle database operations
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Plugin Class handling database.
 *
 * @since  2.0.0
 * @access public
 */
class Database {

	/**
	 * (Re)create synchronization records for all containers
	 * and mark them all as unparsed.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  bool $forced To recreate all synch records.
	 * @return void
	 */
	public function resynch( $forced = false ) {
		global $wpdb;

		if ( $forced ) {
			$blclog->info( '... Forced resynchronization initiated' );

			// Drop all synchronization records.
			$wpdb->query( "TRUNCATE {$wpdb->prefix}blc_synch" );

		} else {
			$blclog->info( '... Resynchronization initiated' );
		}

		//Remove invalid DB entries
		blc_cleanup_database();

		//(Re)create and update synch. records for all container types.
		$blclog->info( '... (Re)creating container records' );
		blcContainerHelper::resynch( $forced );

		$blclog->info( '... Setting resync. flags' );
		blc_got_unsynched_items();

		//All done.
		$blclog->info( 'Database resynchronization complete.' );

	}

}
