<?php
/**
 * Ajax class.
 *
 * Handles all the BLC ajax
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

use BLC\Core\App;

/**
 * Ajax handling.
 *
 * @since  2.0.0
 * @access public
 */
class Ajax {

	/**
	 * Plugin configuration.
	 *
	 * @var object
	 */
	public $conf;

	/**
	 * Loader script path.
	 *
	 * @var string
	 */
	public $loader;

	/**
	 * Loader basename.
	 *
	 * @var string
	 */
	public $my_basename = '';

	/**
	 * DB version.
	 *
	 * @var string
	 */
	public $db_version; // The required version of the plugin's DB schema.

	/**
	 * Execution start time.
	 *
	 * @var string
	 */
	public $execution_start_time;

	/**
	 * Text domain status.
	 *
	 * @var string
	 */
	public $is_textdomain_loaded = false;

	/**
	 * Initiates the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this->db_version  = BLC_DATABASE_VERSION;
		$this->conf        = App::$options;
		$this->loader      = $loader;
		$this->my_basename = plugin_basename( $this->loader );
		// TODO $this->load_language();

		// AJAX hooks.
		add_action( 'wp_ajax_blc_full_status', array( $this, 'ajax_full_status' ) );
		add_action( 'wp_ajax_blc_dashboard_status', array( $this, 'ajax_dashboard_status' ) );
		add_action( 'wp_ajax_blc_work', array( $this, 'ajax_work' ) );
		add_action( 'wp_ajax_blc_discard', array( $this, 'ajax_discard' ) );
		add_action( 'wp_ajax_blc_edit', array( $this, 'ajax_edit' ) );
		add_action( 'wp_ajax_blc_link_details', array( $this, 'ajax_link_details' ) );
		add_action( 'wp_ajax_blc_unlink', array( $this, 'ajax_unlink' ) );
		add_action( 'wp_ajax_blc_recheck', array( $this, 'ajax_recheck' ) );
		add_action( 'wp_ajax_blc_deredirect', array( $this, 'ajax_deredirect' ) );
		add_action( 'wp_ajax_blc_current_load', array( $this, 'ajax_current_load' ) );
		add_action( 'wp_ajax_blc_dismiss', array( $this, 'ajax_dismiss' ) );
		add_action( 'wp_ajax_blc_undismiss', array( $this, 'ajax_undismiss' ) );

		// Set the footer hook that will call the worker function via AJAX.
		add_action( 'admin_footer', array( $this, 'admin_footer' ) );
	}

	/**
	 * Output the script that runs the link monitor while the Dashboard is open.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function admin_footer() {
		if ( ! $this->conf->options['run_in_dashboard'] ) {
			return;
		}
		$nonce = wp_create_nonce( 'blc_work' );
		?>
		<!-- wsblc admin footer -->
		<script type='text/javascript'>
		(function($){

			//(Re)starts the background worker thread
			function blcDoWork(){
				$.post(
					"<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>",
					{
						'action' : 'blc_work',
						'_ajax_nonce' : '<?php echo esc_js( $nonce ); ?>'
					}
				);
			}
			//Call it the first time
			blcDoWork();

			//Then call it periodically every X seconds
			setInterval(blcDoWork, <?php echo ( intval( $this->conf->options['max_execution_time'] ) + 1 ) * 1000; ?>);

		})(jQuery);
		</script>
		<!-- /wsblc admin footer -->
		<?php
	}

	/**
	 * Wrapper for link dismiss.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_dismiss() {
		$this->ajax_set_link_dismissed( true );
	}

	/**
	 * Wrapper for link undismiss.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_undismiss() {
		$this->ajax_set_link_dismissed( false );
	}

	/**
	 * Output the current link checker status in JSON format.
	 * Ajax hook for the 'blc_full_status' action.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_full_status() {
		$status = $this->get_status();
		$text   = $this->status_text( $status );

		echo json_encode(
			array(
				'text'   => $text,
				'status' => $status,
			)
		);

		die();
	}

	/**
	 * Generates a status message based on the status info in $status
	 *
	 * @since  2.0.0
	 * @access public
	 * @param array $status Link status.
	 * @return string
	 */
	public function status_text( $status ) {
		$text = '';

		if ( $status['broken_links'] > 0 ) {
			$text .= sprintf(
				"<a href='%s' title='" . __( 'View broken links', 'broken-link-checker' ) . "'><strong>" .
					_n( 'Found %d broken link', 'Found %d broken links', $status['broken_links'], 'broken-link-checker' ) .
				'</strong></a>',
				esc_attr( admin_url( 'admin.php?page=wdblc' ) ),
				$status['broken_links']
			);
		} else {
			$text .= __( 'No broken links found.', 'broken-link-checker' );
		}

		$text .= '<br/>';

		if ( $status['unchecked_links'] > 0 ) {
			$text .= sprintf(
				_n( '%d URL in the work queue', '%d URLs in the work queue', $status['unchecked_links'], 'broken-link-checker' ),
				$status['unchecked_links']
			);
		} else {
			$text .= __( 'No URLs in the work queue.', 'broken-link-checker' );
		}

		$text .= '<br/>';
		if ( $status['known_links'] > 0 ) {
			$url_count  = sprintf(
				_nx( '%d unique URL', '%d unique URLs', $status['known_links'], 'for the "Detected X unique URLs in Y links" message', 'broken-link-checker' ),
				$status['known_links']
			);
			$link_count = sprintf(
				_nx( '%d link', '%d links', $status['known_instances'], 'for the "Detected X unique URLs in Y links" message', 'broken-link-checker' ),
				$status['known_instances']
			);

			if ( $this->conf->options['need_resynch'] ) {
				$text .= sprintf(
					__( 'Detected %1$s in %2$s and still searching...', 'broken-link-checker' ),
					$url_count,
					$link_count
				);
			} else {
				$text .= sprintf(
					__( 'Detected %1$s in %2$s.', 'broken-link-checker' ),
					$url_count,
					$link_count
				);
			}
		} else {
			if ( $this->conf->options['need_resynch'] ) {
				$text .= __( 'Searching your blog for links...', 'broken-link-checker' );
			} else {
				$text .= __( 'No links detected.', 'broken-link-checker' );
			}
		}

		return $text;
	}

	/**
	 * Returns an array with various status information about the plugin. Array key reference:
	 *  check_threshold     - date/time; links checked before this threshold should be checked again.
	 * srecheck_threshold   - date/time; broken links checked before this threshold should be re-checked.
	 * sknown_links         - the number of detected unique URLs (a misleading name, yes).
	 * sknown_instances     - the number of detected link instances, i.e. actual link elements in posts and other places.
	 * sbroken_links        - the number of detected broken links.
	 * sunchecked_links     - the number of URLs that need to be checked ASAP; based on check_threshold and recheck_threshold.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array
	 */
	public function get_status() {
		$blc_link_query = blcLinkQuery::getInstance();

		$check_threshold   = date( 'Y-m-d H:i:s', strtotime( '-' . $this->conf->options['check_threshold'] . ' hours' ) );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
		$recheck_threshold = date( 'Y-m-d H:i:s', time() - $this->conf->options['recheck_threshold'] );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date

		$known_links     = blc_get_links( array( 'count_only' => true ) );
		$known_instances = blc_get_usable_instance_count();

		$broken_links = $blc_link_query->get_filter_links( 'broken', array( 'count_only' => true ) );

		$unchecked_links = $this->get_links_to_check( 0, true );

		return array(
			'check_threshold'   => $check_threshold,
			'recheck_threshold' => $recheck_threshold,
			'known_links'       => $known_links,
			'known_instances'   => $known_instances,
			'broken_links'      => $broken_links,
			'unchecked_links'   => $unchecked_links,
		);
	}

	/**
	 * Get ajax dashboard status.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_dashboard_status() {
		// Just display the full status.
		$this->ajax_full_status();
	}

	/**
	 * Output the current average server load (over the last one-minute period).
	 * Called via AJAX.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_current_load() {
		$load = \BLC\Classes\Utility::get_server_load();
		if ( empty( $load ) ) {
			die( esc_html( _x( 'Unknown', 'current load', 'broken-link-checker' ) ) );
		}

		$one_minute = reset( $load );
		printf( '%.2f', esc_html( $one_minute ) );
		die();
	}

	/**
	 * Wrapper for work().
	 * Called via AJAX.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_work() {
		check_ajax_referer( 'blc_work' );

		// Run the worker function.
		$this->work();
		die();
	}

	/**
	 * Acquire an exclusive lock.
	 * If we already hold a lock, it will be released and a new one will be acquired.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return bool
	 */
	public function acquire_lock() {
		return WPMutex::acquire( 'blc_lock' );
	}

	/**
	 * Relese our exclusive lock.
	 * Does nothing if the lock has already been released.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return bool
	 */
	public function release_lock() {
		return WPMutex::release( 'blc_lock' );
	}

	/**
	 * Check if server is currently too overloaded to run the link checker.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return bool
	 */
	public function server_too_busy() {
		if ( ! $this->conf->options['enable_load_limit'] || ! isset( $this->conf->options['server_load_limit'] ) ) {
			return false;
		}

		$loads = blcUtility::get_server_load();
		if ( empty( $loads ) ) {
			return false;
		}
		$one_minute = floatval( reset( $loads ) );

		return $one_minute > $this->conf->options['server_load_limit'];
	}

	/**
	 * Execution timer start.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function start_timer() {
		$this->execution_start_time = microtime_float();
	}

	/**
	 * Sleep long enough to maintain the required $ratio between $elapsed_time and total runtime.
	 *
	 * For example, if $ratio is 0.25 and $elapsed_time is 1 second, this method will sleep for 3 seconds.
	 * Total runtime = 1 + 3 = 4, ratio = 1 / 4 = 0.25.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param float $elapsed_time Elapsed time.
	 * @param float $ratio Time ratio.
	 */
	private function sleep_to_maintain_ratio( $elapsed_time, $ratio ) {
		if ( ( $ratio <= 0 ) || ( $ratio > 1 ) ) {
			return;
		}
		$sleep_time = $elapsed_time * ( ( 1 / $ratio ) - 1 );
		if ( $sleep_time > 0.0001 ) {
			usleep( $sleep_time * 1000000 );
		}
	}

	/**
	 * The main worker function that does all kinds of things.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function work() {
		$blclog = App::$logger;

		// Close the session to prevent lock-ups.
		// PHP sessions are blocking. session_start() will wait until all other scripts that are using the same session
		// are finished. As a result, a long-running script that unintentionally keeps the session open can cause
		// the entire site to "lock up" for the current user/browser. WordPress itself doesn't use sessions, but some
		// plugins do, so we should explicitly close the session (if any) before starting the worker.
		if ( ! session_id() ) {
			session_write_close();
		}

		if ( ! $this->acquire_lock() ) {
			$blclog->info( 'Another instance of BLC is already working. Stop.' );
			return;
		}

		if ( $this->server_too_busy() ) {
			$blclog->warn( 'Server load is too high, stopping.' );
			return;
		}

		$this->start_timer();
		$blclog->info( 'work() starts' );

		$max_execution_time = $this->conf->options['max_execution_time'];

		// Preperation.
		if ( Utility::is_safe_mode() ) {
			// Do it the safe mode way - obey the existing max_execution_time setting.
			$t = ini_get( 'max_execution_time' );
			if ( $t && ( $t < $max_execution_time ) ) {
				$max_execution_time = $t - 1;
			}
		} else {
			// Do it the regular way.
			@set_time_limit( $max_execution_time * 2 ); // x2 should be plenty, running any longer would mean a glitch.
		}

		// Don't stop the script when the connection is closed.
		ignore_user_abort( true );

		// Close the connection as per http://www.php.net/manual/en/features.connection-handling.php#71172
		// This reduces resource usage.
		// Disable when debugging or you won't get the FirePHP output.
		if (
			! headers_sent()
			&& ( defined( 'DOING_AJAX' ) && constant( 'DOING_AJAX' ) )
			&& ( ! defined( 'BLC_DEBUG' ) || ! constant( 'BLC_DEBUG' ) )
		) {
			@ob_end_clean(); // Discard the existing buffer, if any.
			header( 'Connection: close' );
			ob_start();
			echo ( 'Connection closed' ); // This could be anything.
			$size = ob_get_length();
			header( "Content-Length: $size" );
			ob_end_flush(); // Strange behaviour, will not work.
			flush();        // Unless both are called !
		}

		// Load modules for this context.
		$moduleManager = ModuleManager::get_instance();
		$moduleManager->load_modules( 'work' );

		$target_usage_fraction = $this->conf->get( 'target_resource_usage', 0.25 );
		// Target usage must be between 1% and 100%.
		$target_usage_fraction = max( min( $target_usage_fraction, 1 ), 0.01 );

		// Parse posts and bookmarks.

		$orphans_possible   = false;
		$still_need_resynch = $this->conf->options['need_resynch'];

		if ( $still_need_resynch ) {

			$max_containers_per_query = 50;

			$start               = microtime( true );
			$containers          = ContainerHelper::get_unsynched_containers( $max_containers_per_query );
			$get_containers_time = microtime( true ) - $start;

			while ( ! empty( $containers ) ) {
				$this->sleep_to_maintain_ratio( $get_containers_time, $target_usage_fraction );

				foreach ( $containers as $container ) {
					$synch_start_time = microtime( true );

					$container->synch();

					$synch_elapsed_time = microtime( true ) - $synch_start_time;
					$blclog->info(
						sprintf(
							'Parsed container %s[%s] in %.2f ms',
							$container->container_type,
							$container->container_id,
							$synch_elapsed_time * 1000
						)
					);

					// Check if we still have some execution time left.
					if ( $this->execution_time() > $max_execution_time ) {

						\BLC\blc_cleanup_links();
						$this->release_lock();
						return;
					}

					// Check if the server isn't overloaded.
					if ( $this->server_too_busy() ) {
						\BLC\blc_cleanup_links();
						$this->release_lock();
						return;
					}

					// Intentionally slow down parsing to reduce the load on the server. Basically,
					// we work $target_usage_fraction of the time and sleep the rest of the time.
					$this->sleep_to_maintain_ratio( $synch_elapsed_time, $target_usage_fraction );
				}
				$orphans_possible = true;

				$start               = microtime( true );
				$containers          = ContainerHelper::get_unsynched_containers( $max_containers_per_query );
				$get_containers_time = microtime( true ) - $start;
			}

			$still_need_resynch = false;

		}

		// Resynch done?
		if ( $this->conf->options['need_resynch'] && ! $still_need_resynch ) {
			$this->conf->options['need_resynch'] = $still_need_resynch;
			$this->conf->save_options();
		}

		// Remove orphaned links.
		if ( $orphans_possible ) {
			$start = microtime( true );

			$blclog->info( 'Removing orphaned links.' );
			\BLC\blc_cleanup_links();

			$get_links_time = microtime( true ) - $start;
			$this->sleep_to_maintain_ratio( $get_links_time, $target_usage_fraction );
		}

		// Check if we still have some execution time left.
		if ( $this->execution_time() > $max_execution_time ) {
			$blclog->info( 'The allotted execution time has run out.' );
			$this->release_lock();
			return;
		}

		if ( $this->server_too_busy() ) {
			$blclog->info( 'Server load too high, stopping.' );
			$this->release_lock();
			return;
		}

		// Check links.
		$max_links_per_query = 30;

		$start          = microtime( true );
		$links          = $this->get_links_to_check( $max_links_per_query );
		$get_links_time = microtime( true ) - $start;

		while ( $links ) {
			$this->sleep_to_maintain_ratio( $get_links_time, $target_usage_fraction );

			// Some unchecked links found.
			$blclog->info( 'Checking ' . count( $links ) . ' link(s)' );

			// Randomizing the array reduces the chances that we'll get several links to the same domain in a row.
			shuffle( $links );

			$transactionManager = \BLC\Manager\TransactionManager::get_instance();
			$transactionManager->start();

			foreach ( $links as $link ) {
				// Does this link need to be checked? Excluded links aren't checked, but their URLs are still
				// tested periodically to see if they're still on the exclusion list.
				if ( ! $this->is_excluded( $link->url ) ) {
					// Check the link.
					$link->check( true );
				} else {
					$link->last_check_attempt = time();
					$link->save();
				}

				// Check if we still have some execution time left.
				if ( $this->execution_time() > $max_execution_time ) {
					$transactionManager->commit();
					$blclog->info( 'The allotted execution time has run out.' );
					$this->release_lock();
					return;
				}

				// Check if the server isn't overloaded.
				if ( $this->server_too_busy() ) {
					$transactionManager->commit();
					$blclog->info( 'Server load too high, stopping.' );
					$this->release_lock();
					return;
				}
			}
			$transactionManager->commit();

			$start          = microtime( true );
			$links          = $this->get_links_to_check( $max_links_per_query );
			$get_links_time = microtime( true ) - $start;
		}

		$this->release_lock();
		$blclog->info( 'work(): All done.' );
	}

	/**
	 * Retrieve links that need to be checked or re-checked.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param integer $max_results The maximum number of links to return. Defaults to 0 = no limit.
	 * @param bool    $count_only If true, only the number of found links will be returned, not the links themselves.
	 * @return int|blcLink[]
	 */
	public function get_links_to_check( $max_results = 0, $count_only = false ) {
		global $wpdb;

		$check_threshold   = date( 'Y-m-d H:i:s', strtotime( '-' . $this->conf->options['check_threshold'] . ' hours' ) );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
		$recheck_threshold = date( 'Y-m-d H:i:s', time() - $this->conf->options['recheck_threshold'] );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date

		// Select some links that haven't been checked for a long time or
		// that are broken and need to be re-checked again. Links that are
		// marked as "being checked" and have been that way for several minutes
		// can also be considered broken/buggy, so those will be selected
		// as well.

		// Only check links that have at least one valid instance (i.e. an instance exists and
		// it corresponds to one of the currently loaded container/parser types).
		$manager           = blcModuleManager::getInstance();
		$loaded_containers = $manager->get_escaped_ids( 'container' );
		$loaded_parsers    = $manager->get_escaped_ids( 'parser' );

		// Note : This is a slow query, but AFAIK there is no way to speed it up.
		// I could put an index on last_check_attempt, but that value is almost
		// certainly unique for each row so it wouldn't be much better than a full table scan.
		if ( $count_only ) {
			$q = "SELECT COUNT(DISTINCT links.link_id)\n";
		} else {
			$q = "SELECT DISTINCT links.*\n";
		}
		$q .= "FROM {$wpdb->prefix}blc_links AS links
			INNER JOIN {$wpdb->prefix}blc_instances AS instances USING(link_id)
			WHERE
				(
					( last_check_attempt < %s )
					OR
					(
						(broken = 1 OR being_checked = 1)
						AND may_recheck = 1
						AND check_count < %d
						AND last_check_attempt < %s
					)
				)

			AND
				( instances.container_type IN ({$loaded_containers}) )
				AND ( instances.parser_type IN ({$loaded_parsers}) )
			";

		if ( ! $count_only ) {
			$q .= "\nORDER BY last_check_attempt ASC\n";
			if ( ! empty( $max_results ) ) {
				$q .= 'LIMIT ' . intval( $max_results );
			}
		}

		$link_q = $wpdb->prepare(
			$q, //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
			$check_threshold,
			$this->conf->options['recheck_count'],
			$recheck_threshold
		);

		// If we just need the number of links, retrieve it and return.
		if ( $count_only ) {
			return $wpdb->get_var( $link_q );//phpcs:ignore
		}

		// Fetch the link data.
		$link_data = $wpdb->get_results( $link_q, ARRAY_A ); //phpcs:ignore
		if ( empty( $link_data ) ) {
			return array();
		}

		// Instantiate blcLink objects for all fetched links.
		$links = array();
		foreach ( $link_data as $data ) {
			$links[] = new blcLink( $data );
		}

		return $links;
	}

	/**
	 * AJAX hook for the "Not broken" button. Marks a link as broken and as a likely false positive.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_discard() {
		if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'blc_discard', false, false ) ) {
			die( esc_html__( "You're not allowed to do that!", 'broken-link-checker' ) );
		}

		if ( isset( $_POST['link_id'] ) ) {
			// Load the link.
			$link = new Link( intval( $_POST['link_id'] ) );

			if ( ! $link->valid() ) {
				printf( esc_html__( "Oops, I can't find the link %d", 'broken-link-checker' ), intval( $_POST['link_id'] ) );
				die();
			}
			// Make it appear "not broken".
			$link->broken             = false;
			$link->warning            = false;
			$link->false_positive     = true;
			$link->last_check_attempt = time();
			$link->log                = __( 'This link was manually marked as working by the user.', 'broken-link-checker' );

			$link->isOptionLinkChanged = true;

			$transactionManager = TransactionManager::get_instance();
			$transactionManager->start();

			// Save the changes.
			if ( $link->save() ) {
				$transactionManager->commit();
				die( 'OK' );
			} else {
				die( esc_html__( "Oops, couldn't modify the link!", 'broken-link-checker' ) );
			}
		} else {
			die( esc_html__( 'Error : link_id not specified', 'broken-link-checker' ) );
		}
	}

	/**
	 * AJAX hook for the inline link editor on Tools -> Broken Links.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_edit() {
		if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'blc_edit', false, false ) ) {
			die(
				json_encode(
					array(
						'error' => __( "You're not allowed to do that!", 'broken-link-checker' ),
					)
				)
			);
		}

		if ( empty( $_POST['link_id'] ) || empty( $_POST['new_url'] ) || ! is_numeric( $_POST['link_id'] ) ) {
			die(
				json_encode(
					array(
						'error' => __( 'Error : link_id or new_url not specified', 'broken-link-checker' ),
					)
				)
			);
		}

		//Load the link
		$link = new blcLink( intval( $_POST['link_id'] ) );

		if ( ! $link->valid() ) {
			die(
				json_encode(
					array(
						'error' => sprintf( __( "Oops, I can't find the link %d", 'broken-link-checker' ), intval( $_POST['link_id'] ) ),
					)
				)
			);
		}

		// Validate the new URL.
		$new_url = stripslashes( $_POST['new_url'] );
		$parsed  = @parse_url( $new_url );
		if ( ! $parsed ) {
			die(
				json_encode(
					array(
						'error' => __( 'Oops, the new URL is invalid!', 'broken-link-checker' ),
					)
				)
			);
		}

		if ( ! current_user_can( 'unfiltered_html' ) ) {
			// Disallow potentially dangerous URLs like "javascript:...".
			$protocols         = wp_allowed_protocols();
			$good_protocol_url = wp_kses_bad_protocol( $new_url, $protocols );
			if ( $new_url !== $good_protocol_url ) {
				die(
					json_encode(
						array(
							'error' => __( 'Oops, the new URL is invalid!', 'broken-link-checker' ),
						)
					)
				);
			}
		}

		$new_text = ( isset( $_POST['new_text'] ) && is_string( $_POST['new_text'] ) ) ? stripslashes( $_POST['new_text'] ) : null;
		if ( '' === $new_text ) {
			$new_text = null;
		}
		if ( ! empty( $new_text ) && ! current_user_can( 'unfiltered_html' ) ) {
			$new_text = stripslashes( wp_filter_post_kses( addslashes( $new_text ) ) ); // wp_filter_post_kses expects slashed data.
		}

		$rez = $link->edit( $new_url, $new_text );
		if ( false === $rez ) {
			die(
				json_encode(
					array(
						'error' => __( 'An unexpected error occurred!', 'broken-link-checker' ),
					)
				)
			);
		} else {
			$new_link     = $rez['new_link']; /** @var blcLink $new_link */
			$new_status   = $new_link->analyse_status();
			$ui_link_text = null;
			if ( isset( $new_text ) ) {
				$instances = $new_link->get_instances();
				if ( ! empty( $instances ) ) {
					$first_instance = reset( $instances );
					$ui_link_text   = $first_instance->ui_get_link_text();
				}
			}

			$response = array(
				'new_link_id'    => $rez['new_link_id'],
				'cnt_okay'       => $rez['cnt_okay'],
				'cnt_error'      => $rez['cnt_error'],

				'status_text'    => $new_status['text'],
				'status_code'    => $new_status['code'],
				'http_code'      => empty( $new_link->http_code ) ? '' : $new_link->http_code,
				'redirect_count' => $new_link->redirect_count,

				'url'            => $new_link->url,
				'escaped_url'    => esc_url_raw( $new_link->url ),
				'final_url'      => $new_link->final_url,
				'link_text'      => isset( $new_text ) ? $new_text : null,
				'ui_link_text'   => isset( $new_text ) ? $ui_link_text : null,

				'errors'         => array(),
			);
			// url, status text, status code, link text, editable link text.

			foreach ( $rez['errors'] as $error ) {
				array_push( $response['errors'], implode( ', ', $error->get_error_messages() ) );
			}

			die( json_encode( $response ) );
		}
	}

	/**
	 * Get link details.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_link_details() {
		global $wpdb; /* @var wpdb $wpdb */

		if ( ! current_user_can( 'edit_others_posts' ) ) {
			die( esc_html__( "You don't have sufficient privileges to access this information!", 'broken-link-checker' ) );
		}

		if ( isset( $_GET['link_id'] ) ) {
			$link_id = intval( $_GET['link_id'] );
		} elseif ( isset( $_POST['link_id'] ) ) {
			$link_id = intval( $_POST['link_id'] );
		} else {
			die( esc_html__( 'Error : link ID not specified', 'broken-link-checker' ) );
		}

		// Load the link.
		$link = new Link( $link_id );

		if ( ! $link->is_new ) {
			if ( ! class_exists( 'blcTablePrinter' ) ) {
				require dirname( $this->loader ) . '/includes/admin/table-printer.php';
			}
			blcTablePrinter::details_row_contents( $link );
			die();
		} else {
			printf( esc_html__( 'Failed to load link details (%s)', 'broken-link-checker' ), $wpdb->last_error );
			die();
		}
	}

	/**
	 * AJAX hook for the "Unlink" action links in Tools -> Broken Links.
	 * Removes the specified link from all posts and other supported items.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_unlink() {
		if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'blc_unlink', false, false ) ) {
			die(
				json_encode(
					array(
						'error' => __( "You're not allowed to do that!", 'broken-link-checker' ),
					)
				)
			);
		}

		if ( isset( $_POST['link_id'] ) ) {
			// Load the link.
			$link = new Links( intval( $_POST['link_id'] ) );

			if ( ! $link->valid() ) {
				die(
					json_encode(
						array(
							'error' => sprintf( __( "Oops, I can't find the link %d", 'broken-link-checker' ), intval( $_POST['link_id'] ) ),
						)
					)
				);
			}

			// Try and unlink it.
			$rez = $link->unlink();

			if ( false === $rez ) {
				die(
					json_encode(
						array(
							'error' => __( 'An unexpected error occured!', 'broken-link-checker' ),
						)
					)
				);
			} else {
				$response = array(
					'cnt_okay'  => $rez['cnt_okay'],
					'cnt_error' => $rez['cnt_error'],
					'errors'    => array(),
				);
				foreach ( $rez['errors'] as $error ) {
					array_push( $response['errors'], implode( ', ', $error->get_error_messages() ) );
				}

				die( json_encode( $response ) );
			}
		} else {
			die(
				json_encode(
					array(
						'error' => __( 'Error : link_id not specified', 'broken-link-checker' ),
					)
				)
			);
		}
	}

	/**
	 * AJAX hook for the "Recheck" action.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_recheck() {
		if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'blc_recheck', false, false ) ) {
			die(
				json_encode(
					array(
						'error' => __( "You're not allowed to do that!", 'broken-link-checker' ),
					)
				)
			);
		}

		if ( ! isset( $_POST['link_id'] ) || ! is_numeric( $_POST['link_id'] ) ) {
			die(
				json_encode(
					array(
						'error' => __( 'Error : link_id not specified', 'broken-link-checker' ),
					)
				)
			);
		}

		$id   = intval( $_POST['link_id'] );
		$link = new blcLink( $id );

		if ( ! $link->valid() ) {
			die(
				json_encode(
					array(
						'error' => sprintf( __( "Oops, I can't find the link %d", 'broken-link-checker' ), $id ),
					)
				)
			);
		}

		$transactionManager = TransactionManager::getInstance();
		$transactionManager->start();

		// In case the immediate check fails, this will ensure the link is checked during the next work() run.
		$link->last_check_attempt  = 0;
		$link->isOptionLinkChanged = true;
		$link->save();

		// Check the link and save the results.
		$link->check( true );

		$transactionManager->commit();

		$status   = $link->analyse_status();
		$response = array(
			'status_text'    => $status['text'],
			'status_code'    => $status['code'],
			'http_code'      => empty( $link->http_code ) ? '' : $link->http_code,
			'redirect_count' => $link->redirect_count,
			'final_url'      => $link->final_url,
		);

		die( json_encode( $response ) );
	}

	/**
	 * AJAX hook for the "Redirect" action.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function ajax_deredirect() {
		if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'blc_deredirect', false, false ) ) {
			die(
				json_encode(
					array(
						'error' => __( "You're not allowed to do that!", 'broken-link-checker' ),
					)
				)
			);
		}

		if ( ! isset( $_POST['link_id'] ) || ! is_numeric( $_POST['link_id'] ) ) {
			die(
				json_encode(
					array(
						'error' => __( 'Error : link_id not specified', 'broken-link-checker' ),
					)
				)
			);
		}

		$id   = intval( $_POST['link_id'] );
		$link = new Link( $id );

		if ( ! $link->valid() ) {
			die(
				json_encode(
					array(
						'error' => sprintf( __( "Oops, I can't find the link %d", 'broken-link-checker' ), $id ),
					)
				)
			);
		}

		// The actual task is simple; it's error handling that's complicated.
		$result = $link->deredirect();
		if ( is_wp_error( $result ) ) {
			die(
				json_encode(
					array(
						'error' => sprintf( '%s [%s]', $result->get_error_message(), $result->get_error_code() ),
					)
				)
			);
		}

		$link = $result['new_link']; /** @var blcLink $link */

		$status   = $link->analyse_status();
		$response = array(
			'url'            => $link->url,
			'escaped_url'    => esc_url_raw( $link->url ),
			'new_link_id'    => $result['new_link_id'],

			'status_text'    => $status['text'],
			'status_code'    => $status['code'],
			'http_code'      => empty( $link->http_code ) ? '' : $link->http_code,
			'redirect_count' => $link->redirect_count,
			'final_url'      => $link->final_url,

			'cnt_okay'       => $result['cnt_okay'],
			'cnt_error'      => $result['cnt_error'],
			'errors'         => array(),
		);

		// Convert WP_Error's to simple strings.
		if ( ! empty( $result['errors'] ) ) {
			foreach ( $result['errors'] as $error ) { /** @var WP_Error $error */
				$response['errors'][] = $error->get_error_message();
			}
		}

		die( json_encode( $response ) );
	}

}
