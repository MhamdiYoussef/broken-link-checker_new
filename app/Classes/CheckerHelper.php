<?php
/**
 * Checker helper class.
 *
 * Helper class for checkers.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Plugin Class handling arbitary data.
 *
 * @since  2.0.0
 * @access public
 */
class CheckerHelper {

	/**
	 * Get a reference to a specific checker.
	 *
	 * @param string $checker_id Checker ID.
	 * @return ModuleManager
	 */
	public static function get_checker( $checker_id ) {
		$manager = blcModuleManager::getInstance();
		return $manager->get_module( $checker_id, true, 'checker' );
	}

	/**
	 * Get a checker object that can check the specified URL.
	 *
	 * @param string $url The url to get the checker for.
	 * @return blcChecker|null
	 */
	public static function get_checker_for( $url ) {
		$parsed = wp_parse_url( $url );

		$manager         = blcModuleManager::getInstance();
		$active_checkers = $manager->get_active_by_category( 'checker' );
		foreach ( $active_checkers as $module_id => $module_data ) {
			// Try the URL pattern in the header first. If it doesn't match, we can avoid loading the module altogether.
			if ( ! empty( $module_data['ModuleCheckerUrlPattern'] ) ) {
				if ( ! preg_match( $module_data['ModuleCheckerUrlPattern'], $url ) ) {
					continue;
				}
			}

			$checker = $manager->get_module( $module_id );

			if ( ! $checker ) {
				continue;
			}

			// The `can_check()` method can perform more sophisticated filtering, or just return true if the checker thinks matching the URL regex is sufficient.
			if ( $checker->can_check( $url, $parsed ) ) {
				return $checker;
			}
		}

		$checker = null;
		return $checker;
	}

}
