<?php
/**
 * The base class for link containers. All containers should extend this class.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Container class.
 *
 * @since  2.0.0
 * @access public
 */
class Container {

	/**
	 * Fields.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    array
	 */
	public $fields = array();

	/**
	 * Default Field.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $default_field = '';

	/**
	 * Container type.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $container_type;

	/**
	 * Container ID
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    int
	 */
	public $container_id = 0;

	/**
	 * Is Synched.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    bool
	 */
	public $synched = false;

	/**
	 * Last Synched.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $last_synch = '0000-00-00 00:00:00';

	/**
	 * Last Synched.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	public $wrapped_object = null;

	/**
	 * Constructor
	 *
	 * @param array  $data Container data.
	 * @param object $wrapped_object Wrapped object.
	 * @return void
	 */
	public function __construct( $data = null, $wrapped_object = null ) {
		$this->wrapped_object = $wrapped_object;
		if ( ! empty( $data ) && is_array( $data ) ) {
			foreach ( $data as $name => $value ) {
				$this->$name = $value;
			}
		}
	}

	/**
	 * Get the value of the specified field of the object wrapped by this container.
	 *
	 * @access protected
	 *
	 * @param string $field Field name. If omitted, the value of the default field will be returned.
	 * @return string
	 */
	public function get_field( $field = '' ) {
		if ( empty( $field ) ) {
			$field = $this->default_field;
		}

		$w = $this->get_wrapped_object();
		return $w->$field;
	}

	/**
	 * Update the value of the specified field in the wrapped object.
	 * This method will also immediately save the changed value by calling update_wrapped_object().
	 *
	 * @access protected
	 *
	 * @param string $field Field name.
	 * @param string $new_value Set the field to this value.
	 * @param string $old_value The previous value of the field. Optional, but can be useful for container that need the old value to distinguish between several instances of the same field (e.g. post metadata).
	 * @return bool|WP_Error True on success, an error object if something went wrong.
	 */
	public function update_field( $field, $new_value, $old_value = '' ) {
		$w         = $this->get_wrapped_object();
		$w->$field = $new_value;
		return $this->update_wrapped_object();
	}

	/**
	 * Retrieve the entity wrapped by this container.
	 * The fetched object will also be cached in the $wrapped_object variable.
	 *
	 * @access protected
	 *
	 * @param bool $ensure_consistency Set this to true to ignore the cached $wrapped_object value and retrieve an up-to-date copy of the wrapped object from the DB (or WP's internal cache).
	 * @return void
	 */
	public function get_wrapped_object( $ensure_consistency = false ) {
		trigger_error( 'Function blcContainer::get_wrapped_object() must be over-ridden in a sub-class', E_USER_ERROR );//phpcs:ignore
	}

	/**
	 * Update the entity wrapped by the container with values currently in the $wrapped_object.
	 *
	 * @access protected
	 *
	 * @return void
	 */
	public function update_wrapped_object() {
		trigger_error( 'Function blcContainer::update_wrapped_object() must be over-ridden in a sub-class', E_USER_ERROR );//phpcs:ignore
	}

	/**
	 * Parse the container for links and save the results to the DB.
	 *
	 * @return void
	 */
	public function synch() {

		// Remove any existing link instance records associated with the container.
		$this->delete_instances();

		// Load the wrapped object, if not done already.
		$this->get_wrapped_object();

		// Iterate over all parse-able fields.
		foreach ( $this->fields as $name => $format ) {
			// Get the field value.
			$value = $this->get_field( $name );
			if ( empty( $value ) ) {
				continue;
			}

			// Get all parsers applicable to this field.
			$parsers = blcParserHelper::get_parsers( $format, $this->container_type );

			if ( empty( $parsers ) ) {
				continue;
			}

			$base_url          = $this->base_url();
			$default_link_text = $this->default_link_text( $name );

			// Parse the field with each parser.
			foreach ( $parsers as $parser ) {
				$found_instances = $parser->parse( $value, $base_url, $default_link_text );

				$transactionManager = TransactionManager::getInstance();
				$transactionManager->start();

				// Complete the link instances by adding container info, then save them to the DB.
				foreach ( $found_instances as $instance ) {
					$instance->set_container( $this, $name );
					$instance->save();
				}

				$transactionManager->commit();

			}
		}

		$this->mark_as_synched();
	}

	/**
	 * Mark the container as successfully synchronized (parsed for links).
	 *
	 * @return bool
	 */
	public function mark_as_synched() {
		global $wpdb;

		$this->last_synch = time();

		$rez = $wpdb->query( //phpcs:ignore
			$wpdb->prepare(
				"INSERT INTO {$wpdb->prefix}blc_synch( container_id, container_type, synched, last_synch)
				VALUES( %d, %s, %d, NOW() )
				ON DUPLICATE KEY UPDATE synched = VALUES(synched), last_synch = VALUES(last_synch)",
				$this->container_id,
				$this->container_type,
				1
			)
		);

		return ( false !== $rez );
	}

	/**
	 * Mark the container as not synchronized (not parsed, or modified since the last parse).
	 * The plugin will attempt to (re)parse the container at the earliest opportunity.
	 *
	 * @return bool
	 */
	public function mark_as_unsynched() {
		global $wpdb;

		$rez = $wpdb->query(//phpcs:ignore
			$wpdb->prepare(
				"INSERT INTO {$wpdb->prefix}blc_synch( container_id, container_type, synched, last_synch)
			  	VALUES( %d, %s, %d, '0000-00-00 00:00:00' )
			  	ON DUPLICATE KEY UPDATE synched = VALUES(synched)",
				$this->container_id,
				$this->container_type,
				0
			)
		);

		blc_got_unsynched_items();

		return ( false !== $rez );
	}

	/**
	 * Get the base URL of the container. Used to normalize relative URLs found
	 * in the container. For example, for posts this would be the post permalink.
	 *
	 * @return string
	 */
	public function base_url() {
		return home_url();
	}

	/**
	 * Get the default link text to use for links found in a specific container field.
	 *
	 * This is generally only meaningful for non-HTML container fields.
	 * For example, if the container is post metadata, the default
	 * link text might be equal to the name of the custom field.
	 *
	 * @param string $field Container field.
	 * @return string
	 */
	public function default_link_text( $field = '' ) {
		return '';
	}



	/**
	 * Delete the DB record of this container.
	 * Also deletes the DB records of all link instances associated with it.
	 * Calling this method will not affect the WP entity (e.g. a post) corresponding to this container.
	 *
	 * @return bool
	 */
	public function delete() {
		global $wpdb;

		// Delete instances first.
		$rez = $this->delete_instances();
		if ( ! $rez ) {
			return false;
		}

		// Now delete the container record.
		$q = $wpdb->query(
			$wpdb->prepare(
				"DELETE FROM {$wpdb->prefix}blc_synch
				WHERE container_id = %d AND container_type = %s",
				$this->container_id,
				$this->container_type
			)
		);

		if ( false === $q ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Delete all link instance records associated with this container.
	 * NB: Calling this method will not affect the WP entity (e.g. a post) corresponding to this container.
	 *
	 * @return bool
	 */
	public function delete_instances() {
		global $wpdb;

		$q = $wpdb->query(//phpcs:ignore
			$wpdb->prepare(
				"DELETE FROM {$wpdb->prefix}blc_instances
			  	WHERE container_id = %d AND container_type = %s",
				$this->container_id,
				$this->container_type
			)
		);

		if ( false === $q ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Delete or trash the WP entity corresponding to this container. Should prefer moving to trash, if possible.
	 * Also remove the synch. record of the container and all associated instances.
	 *
	 * Must be over-ridden in a sub-class.
	 *
	 * @return void
	 */
	public function delete_wrapped_object() {
		trigger_error( 'Function blcContainer::delete_wrapped_object() must be over-ridden in a sub-class', E_USER_ERROR );//phpcs:ignore
	}

	/**
	 * Move the WP entity corresponding to this container to the Trash.
	 *
	 * Must be over-riden in a subclass.
	 *
	 * @return void
	 */
	public function trash_wrapped_object() {
		trigger_error( 'Function blcContainer::trash_wrapped_object() must be over-ridden in a sub-class', E_USER_ERROR );//phpcs:ignore
	}

	/**
	 * Check if the current user can delete/trash this container.
	 *
	 * Should be over-ridden in a subclass.
	 *
	 * @return bool
	 */
	public function current_user_can_delete() {
		return false;
	}

	/**
	 * Determine if this container can be moved to the trash.
	 *
	 * Should be over-ridden in a subclass.
	 *
	 * @return bool
	 */
	public function can_be_trashed() {
		return false;
	}


	/**
	 * Change all links with the specified URL to a new URL.
	 *
	 * @param string $field_name URL field name.
	 * @param object $parser BLC parser Object.
	 * @param string $new_url New url to replace.
	 * @param string $old_url Old url to replace.
	 * @param string $old_raw_url Old raw url.
	 * @param string $new_text Optional. New text.
	 *
	 * @return array|WP_Error The new value of raw_url on success, or an error object if something went wrong.
	 */
	public function edit_link( $field_name, $parser, $new_url, $old_url = '', $old_raw_url = '', $new_text = null ) {
		// Ensure we're operating on a consistent copy of the wrapped object.
		/*
		Explanation

		Consider this scenario where the container object wraps a blog post :
			1) The container object gets created and loads the post data.
			2) Someone modifies the DB data corresponding to the post.
			3) The container tries to edit a link present in the post. However, the post
			has changed since the time it was first cached, so when the container updates
			the post with it's changes, it will overwrite whatever modifications were made
			in step 2.

		This would not be a problem if WP entities like posts and comments were
		actually real objects, not just bags of key=>value pairs, but oh well.

		Therefore, it is necessary to re-load the wrapped object before editing it.
		*/
		$this->get_wrapped_object( true );

		// Get the current value of the field that needs to be edited.
		$old_value = $this->get_field( $field_name );

		// store the new url.
		$this->updating_urls = array(
			'old_url' => $old_url,
			'new_url' => $new_url,
		);

		// Have the parser modify the specified link. If successful, the parser will
		// return an associative array with two keys - 'content' and 'raw_url'.
		// Otherwise we'll get an instance of WP_Error.
		if ( $parser->is_link_text_editable() ) {
			$edit_result = $parser->edit( $old_value, $new_url, $old_url, $old_raw_url, $new_text );
		} else {
			$edit_result = $parser->edit( $old_value, $new_url, $old_url, $old_raw_url );
		}
		if ( is_wp_error( $edit_result ) ) {
			return $edit_result;
		}

		// Update the field with the new value returned by the parser.
		$update_result = $this->update_field( $field_name, $edit_result['content'], $old_value );
		if ( is_wp_error( $update_result ) ) {
			return $update_result;
		}

		// Return the new values to the instance.
		unset( $edit_result['content'] ); // (Except content, which it doesn't need.)
		return $edit_result;
	}

	/**
	 * Remove all links with the specified URL, leaving their anchor text intact.
	 *
	 * @param string $field_name URL field name.
	 * @param object $parser BLC parser Object.
	 * @param string $url URL to unlink.
	 * @param string $raw_url Raw url representation.
	 * @return bool|WP_Error True on success, or an error object if something went wrong.
	 */
	public function unlink( $field_name, $parser, $url, $raw_url = '' ) {
		// Ensure we're operating on a consistent copy of the wrapped object.
		$this->get_wrapped_object( true );

		$old_value = $this->get_field( $field_name );

		$new_value = $parser->unlink( $old_value, $url, $raw_url );
		if ( is_wp_error( $new_value ) ) {
			return $new_value;
		}

		return $this->update_field( $field_name, $new_value, $old_value );
	}

	/**
	 * Retrieve a list of links found in this container.
	 *
	 * @access public
	 *
	 * @return array of blcLink
	 */
	public function get_links() {
		$params = array(
			's_container_type' => $this->container_type,
			's_container_id'   => $this->container_id,
		);
		return blc_get_links( $params );
	}


	/**
	 * Get action links to display in the "Source" column of the Tools -> Broken Links link table.
	 *
	 * @param string $container_field Container field.
	 * @return array
	 */
	function ui_get_action_links( $container_field ) {
		return array();
	}

	/**
	 * Get the container name to display in the "Source" column of the Tools -> Broken Links link table.
	 *
	 * @param string $container_field Container field.
	 * @param string $context Context field.
	 * @return string
	 */
	public function ui_get_source( $container_field, $context = 'display' ) {
		return sprintf( '%s[%d] : %s', $this->container_type, $this->container_id, $container_field );
	}

	public function ui_get_source_short( $container_field, $context = 'display' ) {
		return $this->ui_get_source( $container_field, $context );
	}

	/**
	 * Get edit URL. Returns the URL of the Dashboard page where the item associated with this
	 * container can be edited.
	 *
	 * HTML entities like '&' will be properly escaped for display.
	 *
	 * @access protected
	 *
	 * @return string
	 */
	public function get_edit_url() {
		// Should be over-ridden in a sub-class.
		return '';
	}

}
