<?php
/**
 * Collection class.
 *
 * Makes arbitrary data available.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Plugin Class handling arbitary data.
 *
 * @since  2.0.0
 * @access public
 */
class Collections {

	/**
	 * Http status codes.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array
	 */
	public static function get_http_status_codes() {
		$defaults = array(
			// [Informational 1xx]
			100 => 'Continue',
			101 => 'Switching Protocols',
			// [Successful 2xx]
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			// [Redirection 3xx]
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Moved Temporarily',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			307 => 'Temporary Redirect',
			// [Client Error 4xx]
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			// [Server Error 5xx]
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			509 => 'Bandwidth Limit Exceeded',
			510 => 'Not Extended',
		);

		return apply_filters( 'blc_http_status_codes', $defaults );
	}

	/**
	 * Http status codes.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array
	 */
	public static function get_field_format() {
		$defaults = array(
			'url'                => '%s',
			'first_failure'      => 'datetime',
			'last_check'         => 'datetime',
			'last_success'       => 'datetime',
			'last_check_attempt' => 'datetime',
			'check_count'        => '%d',
			'final_url'          => '%s',
			'redirect_count'     => '%d',
			'log'                => '%s',
			'http_code'          => '%d',
			'request_duration'   => '%F',
			'timeout'            => 'bool',
			'result_hash'        => '%s',
			'broken'             => 'bool',
			'warning'            => 'bool',
			'false_positive'     => 'bool',
			'may_recheck'        => 'bool',
			'being_checked'      => 'bool',
			'status_text'        => '%s',
			'status_code'        => '%s',
			'dismissed'          => 'bool',
		);

		return apply_filters( 'blc_field_formats', $defaults );
	}

	/**
	 * Native filters.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array
	 */
	public static function get_native_filters() {
		$defaults = array(
			'all'       => array(
				'params'       => array(
					'where_expr' => '1',
				),
				'name'         => __( 'All', 'broken-link-checker' ),
				'heading'      => __( 'Detected Links', 'broken-link-checker' ),
				'heading_zero' => __( 'No links found (yet)', 'broken-link-checker' ),
				'native'       => true,
			),

			'broken'    => array(
				'params'       => array(
					'where_expr'          => '( broken = 1 )',
					's_include_dismissed' => false,
				),
				'name'         => __( 'Broken', 'broken-link-checker' ),
				'heading'      => __( 'Broken Links', 'broken-link-checker' ),
				'heading_zero' => __( 'No broken links found', 'broken-link-checker' ),
				'native'       => true,
			),
			'warnings'  => array(
				'params'       => array(
					'where_expr'            => '( warning = 1 )',
					's_include_disKLmissed' => false,
				),
				'name'         => _x( 'Warnings', 'filter name', 'broken-link-checker' ),
				'heading'      => _x( 'Warnings', 'filter heading', 'broken-link-checker' ),
				'heading_zero' => __( 'No warnings found', 'broken-link-checker' ),
				'native'       => true,
			),
			'redirects' => array(
				'params'       => array(
					'where_expr'          => '( redirect_count > 0 )',
					's_include_dismissed' => false,
				),
				'name'         => __( 'Redirects', 'broken-link-checker' ),
				'heading'      => __( 'Redirected Links', 'broken-link-checker' ),
				'heading_zero' => __( 'No redirects found', 'broken-link-checker' ),
				'native'       => true,
			),

			'dismissed' => array(
				'params'       => array(
					'where_expr' => '( dismissed = 1 )',
				),
				'name'         => __( 'Dismissed', 'broken-link-checker' ),
				'heading'      => __( 'Dismissed Links', 'broken-link-checker' ),
				'heading_zero' => __( 'No dismissed links found', 'broken-link-checker' ),
				'native'       => true,
			),
		);

		return apply_filters( 'blc_get_native_filters', $defaults );
	}
}
