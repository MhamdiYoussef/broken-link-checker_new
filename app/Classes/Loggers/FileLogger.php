<?php
/**
 * A basic logger that logs messages to a file.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes\Loggers;

use BLC\Abstracts\Logger;

/**
 * Options Logger class.
 *
 * @since  2.0.0
 * @access public
 */
class FileLogger extends Logger {

	/**
	 * Log file name.
	 *
	 * @since  2.0.0
	 * @access protected
	 * @var    string
	 */
	protected $file_name;

	/**
	 * Initiates the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function __construct( $log_filename ) {
		$this->fileName = $log_filename;
	}

	/**
	 * Log function.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $message Message to log.
	 * @param  object $object  Data object for the logger.
	 * @param  int    $level   Level of the log message.
	 * @return void
	 */
	public function log( $message, $object = null, $level = BLC_LEVEL_INFO ) {

		if ( $level < $this->log_level ) {
			return;
		}

		$line = sprintf(
			'[%1$s] %2$s %3$s',
			date( 'Y-m-d H:i:s P' ), //phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
			$this->get_level_string( $level ),
			$message
		);

		if ( isset( $object ) ) {
			$line .= ' ' . var_export( $object, true ); //phpcs:ignore
		}

		$line .= "\n";

		error_log( $line, 3, $this->fileName ); //phpcs:ignore
	}

	/**
	 * Get the whole log.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  int $min_level Level of the log message.
	 * @return array
	 */
	public function get_log( $min_level = Logger::BLC_LEVEL_DEBUG ) {
		return array( __CLASS__ . ':get_log() is not implemented' );
	}

	/**
	 * Get log message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  int $min_level Level of the log message.
	 * @return array
	 */
	public function get_messages( $min_level = Logger::BLC_LEVEL_DEBUG ) {
		return array( __CLASS__ . ':get_messages() is not implemented' );
	}

	/**
	 * Clear log.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function clear() {
		if ( is_file( $this->fileName ) && is_writable( $this->fileName ) ) {
			global $wp_filesystem;
			require_once ABSPATH . '/wp-admin/includes/file.php';
			WP_Filesystem();
			$wp_filesystem->put_contents(
				$this->fileName,
				'',
				FS_CHMOD_FILE // predefined mode settings for WP files.
			);
		}
	}

}
