<?php
/**
 * A Dummy logger.
 *
 * It isn't supposed to log anything.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes\Loggers;

use BLC\Abstracts\Logger;

/**
 * Dummy Logger class.
 *
 * @since  2.0.0
 * @access public
 */
class DummyLogger extends Logger {}
