<?php
/**
 * A basic logger that uses WP options for permanent storage.
 *
 * Log entries are initially stored in memory and need to explicitly
 * flushed to the database by calling blcCachedOptionLogger::save().
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes\Loggers;

use BLC\Abstracts\Logger;
use BLC\Core\App;

/**
 * Options Logger class.
 *
 * @since  2.0.0
 * @access public
 */
class OptionLogger extends Logger {

	/**
	 * Log array.
	 *
	 * @since  2.0.0
	 * @access protected
	 * @var    array
	 */
	public $log = array();

	/**
	 * Log array.
	 *
	 * @since  2.0.0
	 * @access protected
	 * @var    string
	 */
	public $option_name = 'blc_installation_log';

	/**
	 * Initiates the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$options = App::$options;
		$old_log = $options->get( $this->option_name );
		if ( is_array( $old_log ) && ! empty( $old_log ) ) {
			$this->log = $old_log;
		}
	}
	/**
	 * Log function.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $message Message to log.
	 * @param  object $object  Data object for the logger.
	 * @param  int    $level   Level of the log message.
	 * @return void
	 */
	public function log( $message, $object = null, $level = BLC_LEVEL_INFO ) {
		$new_entry = array( $level, $message, $object );
		array_push( $this->log, $new_entry );
	}

	/**
	 * Get the whole log.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  int $min_level Level of the log message.
	 * @return array
	 */
	public function get_log( $min_level = Logger::BLC_LEVEL_DEBUG ) {
		$this->log_level = $min_level;
		return array_filter( $this->log, array( $this, 'filter_log' ) );
	}

	/**
	 * Log debug message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  array $entry Logs array.
	 * @return array
	 */
	private function filter_log( $entry ) {
		return ( $entry[0] >= $this->log_level );
	}

	/**
	 * Get log message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  int $min_level Level of the log message.
	 * @return array
	 */
	public function get_messages( $min_level = Logger::BLC_LEVEL_DEBUG ) {
		$messages = $this->get_log( $min_level );
		return array_map( array( $this, 'get_log_message' ), $messages );
	}

	/**
	 * Log message from log entry.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  array $entry Logs array.
	 * @return array
	 */
	private function get_log_message( $entry ) {
		return $entry[1];
	}
	/**
	 * Clear log.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function clear() {
		$this->log = array();
		delete_option( $this->option_name );
	}

	/**
	 * Save log.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function save() {
		update_option( $this->option_name, $this->log );
	}
}
