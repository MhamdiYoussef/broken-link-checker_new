<?php

/**
 * Mutex class.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * WP Mutex class.
 *
 * @since  2.0.0
 */
class WPMutex {

	/**
	 * Get an exclusive named lock.
	 *
	 * @param  string  $name Site name.
	 * @param  integer $timeout Timeout time.
	 * @param  bool    $network_wide Network wide.
	 * @return bool
	 */
	public static function acquire( $name, $timeout = 0, $network_wide = false ) {
		global $wpdb;
		if ( ! $network_wide ) {
			$name = self::get_private_name( $name );
		}
		$state = $wpdb->get_var(  //phpcs:ignore
			$wpdb->prepare(
				'SELECT GET_LOCK(%s, %d)',
				$name,
				$timeout
			)
		);
		return 1 === $state;
	}

	/**
	 * Release a named lock.
	 *
	 * @param  string $name Site name.
	 * @param  bool   $network_wide Network wide.
	 * @return bool
	 */
	public static function release( $name, $network_wide = false ) {
		global $wpdb;
		if ( ! $network_wide ) {
			$name = self::get_private_name( $name );
		}
		$released = $wpdb->get_var( //phpcs:ignore
			$wpdb->prepare(
				'SELECT RELEASE_LOCK(%s)',
				$name
			)
		);
		return 1 === $released;
	}

	/**
	 * Given a generic lock name, create a new one that's unique to the current blog.
	 *
	 * @access private
	 *
	 * @param  string $name Site name.
	 * @return string
	 */
	private static function get_private_name( $name ) {
		global $current_blog;
		if ( function_exists( 'is_multisite' ) && is_multisite() && isset( $current_blog->blog_id ) ) {
			$name .= '-blog-' . $current_blog->blog_id;
		}
		return $name;
	}
}
