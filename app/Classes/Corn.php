<?php
/**
 * Cron class.
 *
 * Handles all the BLC Corn functions.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Classes;

/**
 * Corn handling.
 *
 * @since  2.0.0
 * @access public
 */
class Corn {

	/**
	 * Plugin configuration.
	 *
	 * @var object
	 */
	public $conf;

	/**
	 * Initiates the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		// Get options.
		$this->conf = \BLC\blc_get_options();

		// Add/remove Cron events.
		$this->setup_cron_events();

		// Set hooks that listen for our Cron actions.
		add_action( 'blc_cron_email_notifications', array( $this, 'maybe_send_email_notifications' ) );

		add_action( 'blc_cron_check_links', array( $this, 'cron_check_links' ) );

		add_action( 'blc_cron_database_maintenance', array( $this, 'database_maintenance' ) );

		add_action( 'blc_corn_clear_log_file', array( $this, 'clear_log_file' ) );
	}

	/**
	 * Perform various database maintenance tasks on the plugin's tables.
	 *
	 * Removes records that reference disabled containers and parsers,
	 * deletes invalid instances and links, optimizes tables, etc.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function database_maintenance() {
		\BLC\ContainerHelper::cleanup_containers();
		blc_cleanup_instances();
		blc_cleanup_links();
		\BLC\Utility::optimize_database();
	}


	/**
	 * Clear the log file on schedule.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function clear_log_file() {
		$log_file = $this->conf->options['log_file'];

		// clear log file.
		if ( is_writable( $log_file ) && is_file( $log_file ) ) {
			$handle = fopen( $log_file, 'w' );
			fclose( $handle );
		}
	}

	/**
	 * This function is called when the plugin's cron hook executes.
	 * Its only purpose is to invoke the worker function.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function cron_check_links() {
		// TODO this needs to be removed and replaced with call to App::get_instance()->ajax
		\BLC\Manager\OperationsManager::$classes['Ajax']->work();
	}

	/**
	 * Install or uninstall the plugin's Cron events based on current settings.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function setup_cron_events() {

		// Link monitor.
		if ( $this->conf->options['run_via_cron'] ) {
			if ( ! wp_next_scheduled( 'blc_cron_check_links' ) ) {
				wp_schedule_event( time(), '10min', 'blc_cron_check_links' );
			}
		} else {
			wp_clear_scheduled_hook( 'blc_cron_check_links' );
		}

		// Email notifications about broken links.
		if ( $this->conf->options['send_email_notifications'] || $this->conf->options['send_authors_email_notifications'] ) {
			if ( ! wp_next_scheduled( 'blc_cron_email_notifications' ) ) {
				wp_schedule_event( time(), $this->conf->options['notification_schedule'], 'blc_cron_email_notifications' );
			}
		} else {
			wp_clear_scheduled_hook( 'blc_cron_email_notifications' );
		}

		// Run database maintenance every two weeks or so.
		if ( ! wp_next_scheduled( 'blc_cron_database_maintenance' ) ) {
			wp_schedule_event( time(), 'daily', 'blc_cron_database_maintenance' );
		}

		$clear_log = $this->conf->options['clear_log_on'];
		if ( ! wp_next_scheduled( 'blc_corn_clear_log_file' ) && ! empty( $clear_log ) ) {
			wp_schedule_event( time(), $clear_log, 'blc_corn_clear_log_file' );
		}

		if ( empty( $clear_log ) ) {
			wp_clear_scheduled_hook( 'blc_corn_clear_log_file' );
		}
	}

	/**
	 * Check if email should be sent.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function maybe_send_email_notifications() {
		global $wpdb;

		// email notificaiton.
		$send_notification = apply_filters( 'blc_allow_send_email_notification', $this->conf->options['send_email_notifications'] );

		$send_authors_notifications = apply_filters( 'blc_allow_send_author_email_notification', $this->conf->options['send_authors_email_notifications'] );

		if ( ! ( $send_notification || $send_authors_notifications ) ) {
			return;
		}

		// Find links that have been detected as broken since the last sent notification.
		$last_notification = date( 'Y-m-d H:i:s', $this->conf->options['last_notification_sent'] );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
		$where             = $wpdb->prepare( '( first_failure >= %s )', $last_notification );

		$links = blc_get_links(
			array(
				's_filter'             => 'broken',
				'where_expr'           => $where,
				'load_instances'       => true,
				'load_containers'      => true,
				'load_wrapped_objects' => $this->conf->options['send_authors_email_notifications'],
				'max_results'          => 0,
			)
		);

		if ( empty( $links ) ) {
			return;
		}

		// Send the admin/maintainer an email notification.
		$email = $this->conf->get( 'notification_email_address' );
		if ( empty( $email ) ) {
			// Default to the admin email.
			$email = get_option( 'admin_email' );
		}
		if ( $this->conf->options['send_email_notifications'] && ! empty( $email ) ) {
			$this->send_admin_notification( $links, $email );
		}

		// Send notifications to post authors.
		if ( $this->conf->options['send_authors_email_notifications'] ) {
			$this->send_authors_notifications( $links );
		}

		$this->conf->options['last_notification_sent'] = time();
		$this->conf->save_options();
	}

	/**
	 * Prepare admin email contents.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  array  $links Broken links.
	 * @param  string $email Admin email.
	 * @return void
	 */
	public function send_admin_notification( $links, $email ) {
		// Prepare email message.
		$subject = sprintf(
			__( '[%s] Broken links detected', 'broken-link-checker' ),
			html_entity_decode( get_option( 'blogname' ), ENT_QUOTES )
		);

		$body  = sprintf(
			_n(
				'Broken Link Checker has detected %d new broken link on your site.',
				'Broken Link Checker has detected %d new broken links on your site.',
				count( $links ),
				'broken-link-checker'
			),
			count( $links )
		);
		$body .= '<br>';

		$instances = array();
		foreach ( $links as $link ) { /* @var blcLink $link */
			$instances = array_merge( $instances, $link->get_instances() );
		}
		$body .= $this->build_instance_list_for_email( $instances );

		if ( $this->is_textdomain_loaded && is_rtl() ) {
			$body = '<div dir="rtl">' . $body . '</div>';
		}

		$this->send_html_email( $email, $subject, $body );
	}

	/**
	 * Prepare admin email contents.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  array  $instances Broken link instances.
	 * @param  string $max_displayed_links Max links to be displayed.
	 * @param  bool   $add_admin_link Add admin panel link.
	 * @return string
	 */
	public function build_instance_list_for_email( $instances, $max_displayed_links = 5, $add_admin_link = true ) {
		if ( null === $max_displayed_links ) {
			$max_displayed_links = 5;
		}

		$result = '';
		if ( count( $instances ) > $max_displayed_links ) {
			$line = sprintf(
				_n(
					"Here's a list of the first %d broken links:",
					"Here's a list of the first %d broken links:",
					$max_displayed_links,
					'broken-link-checker'
				),
				$max_displayed_links
			);
		} else {
			$line = __( "Here's a list of the new broken links: ", 'broken-link-checker' );
		}

		$result .= "<p>$line</p>";

		// Show up to $max_displayed_links broken link instances right in the email.
		$displayed = 0;
		foreach ( $instances as $instance ) {
			$pieces = array(
				sprintf( __( 'Link text : %s', 'broken-link-checker' ), $instance->ui_get_link_text( 'email' ) ),
				sprintf( __( 'Link URL : <a href="%1$s">%2$s</a>', 'broken-link-checker' ), htmlentities( $instance->get_url() ), blcUtility::truncate( $instance->get_url(), 70, '' ) ),
				sprintf( __( 'Source : %s', 'broken-link-checker' ), $instance->ui_get_source( 'email' ) ),
			);

			$link_entry = implode( '<br>', $pieces );
			$result    .= "$link_entry<br><br>";

			$displayed++;
			if ( $displayed >= $max_displayed_links ) {
				break;
			}
		}

		// Add a link to the "Broken Links" tab.
		if ( $add_admin_link ) {
			$result .= __( 'You can see all broken links here:', 'broken-link-checker' ) . '<br>';
			$result .= sprintf( '<a href="%1$s">%1$s</a>', admin_url( 'admin.php?page=wdblc' ) );
		}

		return $result;
	}

	/**
	 * Prepare admin email contents.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $email_address Email address.
	 * @param  string $subject Email subject.
	 * @param  string $body Email body.
	 * @return string
	 */
	public function send_html_email( $email_address, $subject, $body ) {
		// Need to override the default 'text/plain' content type to send a HTML email.
		add_filter( 'wp_mail_content_type', array( $this, 'override_mail_content_type' ) );

		// Let auto-responders and similar software know this is an auto-generated email
		// that they shouldn't respond to.
		$headers = array( 'Auto-Submitted: auto-generated' );

		$success = wp_mail( $email_address, $subject, $body, $headers );

		// Remove the override so that it doesn't interfere with other plugins that might
		// want to send normal plaintext emails.
		remove_filter( 'wp_mail_content_type', array( $this, 'override_mail_content_type' ) );

		$this->conf->options['last_email'] = array(
			'subject'   => $subject,
			'timestamp' => time(),
			'success'   => $success,
		);
		$this->conf->save_options();

		return $success;
	}

}
