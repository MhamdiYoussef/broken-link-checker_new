<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Links;

use BLC\Component\Tab;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class Issues extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title    = __( 'Issues', 'broken-link-checker' );
		$this->template = 'links/issues';

		$blc_link_query = \blcLinkQuery::getInstance();
		$blc_link_query->load_custom_filters();
		$blc_link_query->count_filter_results();

		$paged = isset( $_GET['paged'] ) ? intval( $_GET['paged'] ) : 1;
		if ( isset( $_GET['tab'] ) ) {
			$paged = 1;
		}

		$current_filter = $blc_link_query->exec_filter(
			'broken',
			$paged,
			10,
			'broken',
			isset( $_GET['orderby'] ) ? $_GET['orderby'] : '',
			isset( $_GET['order'] ) ? $_GET['order'] : ''
		);
		/**
		 * $current_filter - array =>
		 * filter_id, page, per_page, max_pages, links,
		 * search_params, is_broken_filter, base_filter, params,
		 * name, heading, heading_zero, native, count
		 */
		$current_filter['count'] = intval( $current_filter['count'] );
		$this->vars['filter']    = $current_filter;
		/**
		 * $current_filter->links - array<Link> =>
		 * is_new, link_id, url, being_checked, last_check, last_check_attempt,
		 * check_count, http_code, request_duration, timeout, redirect_count,
		 * final_url, broken, warning, first_failure, last_success, may_recheck,
		 * false_positive, result_hash, dismissed, status_text, status_code, log,
		 * field_format, _instances, http_status_codes, isOptionLinkChanged
		 */
		$this->vars['links'] = $current_filter['links'];
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'issues';
	}
}
