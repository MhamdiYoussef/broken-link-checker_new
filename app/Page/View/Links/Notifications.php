<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Links;

use BLC\Component\Tab;
use BLC\Core\App;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class Notifications extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title                                    = __( 'Notifications', 'broken-link-checker' );
		$this->template                                 = 'links/notifications';
		$this->vars['send_emails_schedule']             = App::$options->get( 'send_emails_schedule' );
		$this->vars['send_email_notifications']         = App::$options->get( 'send_email_notifications' );
		$this->vars['send_authors_email_notifications'] = App::$options->get( 'send_authors_email_notifications' );
		$this->setup_emails();
	}

	/**
	 * Convert old email recipeint to new emails if needed.
	 */
	private function setup_emails() {
		$notification_email_address   = App::$options->get( 'notification_email_address' );
		$notification_email_addresses = App::$options->get( 'notification_email_addresses' );

		if ( '' !== $notification_email_address && ! $notification_email_addresses ) {
			$notification_email_addresses = array(
				array( $notification_email_address, $notification_email_address ),
			);
			App::$options->set( 'notification_email_address', '' );
			App::$options->set( 'notification_email_addresses', $notification_email_addresses );
			App::$options->save_options();
		}
		if ( ! $notification_email_addresses ) {
			$notification_email_addresses = array();
		}

		$this->vars['notification_email_addresses'] = $notification_email_addresses;
		$this->vars['notification_recipient_count'] = count( $notification_email_addresses );
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'notifications';
	}
}
