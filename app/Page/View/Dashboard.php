<?php
/**
 * Page class used to set up and render Dashboard page in WP Admin.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View;

use BLC\Core\Page;
use BLC\Core\App;

/**
 * Page class used to set up and render Dashboard page in WP Admin.
 */
class Dashboard extends Page {

	/**
	 * Template file path relative to app/template and without .php file extension.
	 *
	 * @var string
	 */
	protected $template = 'dashboard';

	/**
	 * Returns page title.
	 */
	public function get_page_title() {
		return __( 'Dashboard', 'broken-link-checker' );
	}

	/**
	 * Constructor.
	 *
	 * @param array $vars Variables that will be used in rendering template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );

		$blc_link_query = \blcLinkQuery::getInstance();
		$blc_link_query->load_custom_filters();
		$blc_link_query->count_filter_results();

		$current_filter = $blc_link_query->exec_filter(
			'broken',
			1,
			10,
			'broken'
		);
		/**
		 * $current_filter - array =>
		 * filter_id, page, per_page, max_pages, links,
		 * search_params, is_broken_filter, base_filter, params,
		 * name, heading, heading_zero, native, count
		 */
		$current_filter['count'] = intval( $current_filter['count'] );
		$this->vars['filter']    = $current_filter;
		/**
		 * $current_filter->links - array<Link> =>
		 * is_new, link_id, url, being_checked, last_check, last_check_attempt,
		 * check_count, http_code, request_duration, timeout, redirect_count,
		 * final_url, broken, warning, first_failure, last_success, may_recheck,
		 * false_positive, result_hash, dismissed, status_text, status_code, log,
		 * field_format, _instances, http_status_codes, isOptionLinkChanged
		 */
		$this->vars['links'] = $current_filter['links'];

		$this->vars['send_email_notifications'] = App::$options->get( 'send_email_notifications' );
		$send_emails_schedule                   = App::$options->get( 'send_emails_schedule' );
		$schedules                              = wp_get_schedules();
		$schedule_display                       = $send_emails_schedule;
		if ( isset( $schedules[ $send_emails_schedule ] ) ) {
			$schedule_display = $schedules[ $send_emails_schedule ]['display'];
		}
		$this->vars['schedule_display'] = $schedule_display;

		// Onboarding.
		$user                              = wp_get_current_user();
		$this->vars['user_name']           = $user->get( 'display_name' );
		$this->vars['automatic_engine_on'] = App::$options->get( 'automatic_engine_on', false );

		$onboarding_done = App::$options->get( 'onboarding_done', false );
		if ( defined( 'WDBLC_FORCE_ONBOARDING' ) ) {
			$onboarding_done = false;
		}
		$this->vars['onboarding_done'] = $onboarding_done;

		$modules                       = $this->list_modules();
		$this->vars['modules']         = $modules;
		$this->vars['parsers_halfway'] = $this->calculate_halfway_point( $modules );
	}

	/**
	 * List all modules with their titles and active status.
	 */
	private function list_modules() {
		$parsers        = array();
		$module_manager = \blcModuleManager::getInstance();
		$all_modules    = $module_manager->get_modules_by_category( '', true, true );

		foreach ( $all_modules['parser'] as $module_id => $module_data ) {
			if ( $module_data['ModuleHidden'] ) {
				continue;
			}

			$parsers[ $module_id ] = array(
				'title'  => $module_data['Name'],
				'active' => $module_manager->is_active( $module_id ),
			);
		}

		return $parsers;
	}

	/**
	 * Calculate column halfway split point.
	 *
	 * @param array $modules All modules to display in columns.
	 */
	private function calculate_halfway_point( $modules = array() ) {
		return intval( ceil( count( $modules ) / 2 ) );
	}
}
