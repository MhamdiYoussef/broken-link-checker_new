<?php
/**
 * Page class used to set up and render Settings page in WP Admin.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View;

use BLC\Core\Page;
use BLC\Component\TabbedContent;
use BLC\Core\App;

/**
 * Page class used to set up and render Settings page in WP Admin.
 */
class Settings extends Page {
	/**
	 * Template file path relative to app/template and without .php file extension.
	 *
	 * @var string
	 */
	protected $template = 'settings';


	/**
	 * Constructor, set up variables and all that shiz.
	 *
	 * @param array $vars Variables that will be used in rendering template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );

		$this->vars['tabbed_content'] = new TabbedContent(
			array(
				new Settings\LinkTypes( $vars ),
				new Settings\Locations( $vars ),
				new Settings\Engine( $vars ),
				// new Settings\ProtocolsApi( $vars ), // Protocols & API is a planned feature.
				new Settings\LinkTweaks( $vars ),
				new Settings\Permissions( $vars ),
				new Settings\Logging( $vars ),
			)
		);

		$this->vars['automatic_engine_on'] = App::$options->get( 'automatic_engine_on', false );
	}

	/**
	 * Returns page title.
	 */
	public function get_page_title() {
		return __( 'Settings', 'broken-link-checker' );
	}
}
