<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Settings;

use BLC\Component\Tab;
use BLC\Core\App;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class LinkTypes extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title    = __( 'Link Types', 'broken-link-checker' );
		$this->template = 'settings/link-types';

		$this->vars['exclusion_list'] = $this->compile_exclusion_list();

		$modules                       = $this->list_modules();
		$this->vars['modules']         = $modules;
		$this->vars['parsers_halfway'] = $this->calculate_halfway_point( $modules );
	}

	/**
	 * List all modules with their titles and active status.
	 */
	private function list_modules() {
		$parsers        = array();
		$module_manager = \blcModuleManager::getInstance();
		$all_modules    = $module_manager->get_modules_by_category( '', true, true );

		foreach ( $all_modules['parser'] as $module_id => $module_data ) {
			if ( $module_data['ModuleHidden'] ) {
				continue;
			}

			$parsers[ $module_id ] = array(
				'title'  => $module_data['Name'],
				'active' => $module_manager->is_active( $module_id ),
			);
		}

		return $parsers;
	}

	/**
	 * Compile exclusion list.
	 */
	private function compile_exclusion_list() {
		$exclusion_list = App::$options->get( 'exclusion_list' );
		if ( false === is_array( $exclusion_list ) ) {
			$exclusion_list = array();
		}
		return implode( "\n", $exclusion_list );
	}

	/**
	 * Calculate column halfway split point.
	 *
	 * @param array $modules All modules to display in columns.
	 */
	private function calculate_halfway_point( $modules = array() ) {
		return intval( ceil( count( $modules ) / 2 ) );
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'link-types';
	}
}
