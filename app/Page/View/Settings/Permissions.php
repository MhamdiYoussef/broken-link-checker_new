<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Settings;

use BLC\Component\Tab;
use BLC\Core\App;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class Permissions extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title    = __( 'Permissions', 'broken-link-checker' );
		$this->template = 'settings/permissions';

		// Old roles were stored as capabilities: manage_options, edit_others_posts or do_not_allow.
		$roles_inst = wp_roles();
		$roles      = array();
		foreach ( $roles_inst->roles as $role => $data ) {
			if ( 'subscriber' === $role ) {
				continue;
			}
			$roles[ $role ] = array(
				'name'    => $data['name'],
				'enabled' => false,
			);
		}

		$widget_enabled = App::$options->get( 'dashboard_widget_enabled' );
		if ( App::$options->get( 'dashboard_widget_roles_converted' ) ) {
			// Load new roles directly.
			$enabled_roles = App::$options->get( 'dashboard_widget_roles' );
			foreach ( $roles as $role => $data ) {
				if ( in_array( $role, $enabled_roles, true ) ) {
					$roles[ $role ]['enabled'] = true;
				}
			}
		} else {
			// Recalc from old roles.
			$capability = App::$options->get( 'dashboard_widget_capability' );
			switch ( $capability ) {
				case 'manage_options':
					$roles['administrator']['enabled'] = true;
					$widget_enabled                    = true;
					break;
				case 'edit_others_posts':
					$roles['administrator']['enabled'] = true;
					$roles['editor']['enabled']        = true;
					$widget_enabled                    = true;
					break;
				case 'do_not_allow':
					$widget_enabled = false;
					break;
			}
		}

		$this->vars['roles']          = $roles;
		$this->vars['widget_enabled'] = $widget_enabled;
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'permissions';
	}
}
