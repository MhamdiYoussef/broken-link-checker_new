<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Settings;

use BLC\Component\Tab;
use BLC\Core\App;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class Logging extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title    = __( 'Logging', 'broken-link-checker' );
		$this->template = 'settings/logging';

		$this->vars['logging_enabled']         = App::$options->get( 'logging_enabled' );
		$this->vars['custom_log_file_enabled'] = App::$options->get( 'custom_log_file_enabled' );
		$this->vars['log_file']                = App::$options->get( 'log_file' );
		$this->vars['clear_log_on']            = App::$options->get( 'clear_log_on' );
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'logging';
	}
}
