<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Settings;

use BLC\Component\Tab;
use BLC\Core\App;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class Engine extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title                           = __( 'Engine', 'broken-link-checker' );
		$this->template                        = 'settings/engine';
		$this->vars['run_in_dashboard']        = App::$options->get( 'run_in_dashboard' );
		$this->vars['run_via_cron_frequency']  = App::$options->get( 'run_via_cron_frequency' );
		$this->vars['run_via_cron_period']     = App::$options->get( 'run_via_cron_period' );
		$this->vars['max_execution_time']      = App::$options->get( 'max_execution_time' );
		$this->vars['timeout']                 = App::$options->get( 'timeout' );
		$this->vars['target_resource_usage']   = App::$options->get( 'target_resource_usage' ) * 100;
		$this->vars['server_load_limit']       = sprintf( '%.2f', App::$options->get( 'server_load_limit' ) );
		$this->vars['check_threshold']         = App::$options->get( 'check_threshold' );
		$this->vars['automatic_engine_on']     = App::$options->get( 'automatic_engine_on' );
		$this->vars['user_enabled_load_limit'] = App::$options->get( 'user_enabled_load_limit' );
		$this->vars['run_via_cron']            = App::$options->get( 'run_via_cron' );
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'engine';
	}
}
