<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Settings;

use BLC\Component\Tab;
use BLC\Core\App;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class Locations extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title    = __( 'Locations', 'broken-link-checker' );
		$this->template = 'settings/locations';

		$modules               = $this->list_modules();
		$this->vars['modules'] = $modules;

		$available_statuses = get_post_stati( array( 'internal' => false ), 'objects' );
		$enabled_statuses   = App::$options->get( 'enabled_post_statuses' );
		$post_statuses      = array();

		foreach ( $available_statuses as $status_id => $status_data ) {
			$post_statuses[ $status_id ] = array(
				'label'  => $status_data->label,
				'active' => in_array( $status_id, $enabled_statuses, true ),
			);
		}
		$this->vars['post_statuses'] = $post_statuses;
	}

	/**
	 * List all modules with their titles and active status.
	 */
	private function list_modules() {
		$containers     = array();
		$module_manager = \blcModuleManager::getInstance();
		$all_modules    = $module_manager->get_modules_by_category( '', true, true );

		foreach ( $all_modules['container'] as $module_id => $module_data ) {
			if ( $module_data['ModuleHidden'] ) {
				continue;
			}

			$containers[ $module_id ] = array(
				'title'  => $module_data['Name'],
				'active' => $module_manager->is_active( $module_id ),
			);
		}

		return $containers;
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'locations';
	}
}
