<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View\Settings;

use BLC\Component\Tab;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
class ProtocolsApi extends Tab {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );
		$this->title    = __( 'Protocols & API', 'broken-link-checker' );
		$this->template = 'settings/protocols-api';
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	public function get_title() {
		return $this->title;
	}
	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	public function get_slug() {
		return 'protocols-api';
	}
}
