<?php
/**
 * Page class used to set up and render Links page in WP Admin.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page\View;

use BLC\Core\Page;
use BLC\Component\TabbedContent;
use BLC\Core\App;

/**
 * Page class used to set up and render Links page in WP Admin.
 */
class Links extends Page {
	/**
	 * Template file path relative to app/template and without .php file extension.
	 *
	 * @var string
	 */
	protected $template = 'links';

	/**
	 * Constructor.
	 *
	 * @param array $vars Variables that will be used in rendering template.
	 */
	public function __construct( $vars = array() ) {
		parent::__construct( $vars );

		$this->vars['tabbed_content'] = new TabbedContent(
			array(
				new Links\Issues( $vars ),
				new Links\Redirects( $vars ),
				new Links\Ignored( $vars ),
				new Links\All( $vars ),
				new Links\Notifications( $vars ),
				new Links\Settings( $vars ),
			)
		);

		$this->vars['automatic_engine_on'] = App::$options->get( 'automatic_engine_on', false );
	}

	/**
	 * Returns page title.
	 */
	public function get_page_title() {
		return __( 'Broken Links', 'broken-link-checker' );
	}
}
