<?php
/**
 * Controller class used to process request, set up state and render Settings page in WP Admin.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page;

use BLC\Core\Controller;
use BLC\Core\App;

/**
 * Controller class used to process request, set up state and render Settings page in WP Admin.
 */
class Settings extends Controller {
	/**
	 * Menu slug.
	 *
	 * @var string
	 */
	protected $menu_slug = 'brokenlinkchecker-settings';

	/**
	 * Returns Menu title.
	 *
	 * @return string Menu title.
	 */
	public function get_menu_title() {
		return __( 'Settings', 'broken-link-checker' );
	}

	/**
	 * Returns Page title.
	 *
	 * @return string Page title.
	 */
	public function get_page_title() {
		return __( 'Settings', 'broken-link-checker' );
	}

	/**
	 * Decide what to do with request.
	 */
	public function handle_request() {
		//phpcs:disable
		if ( isset( $_REQUEST['save-settings'] ) ) {
			//phpcs:enable
			$this->save_settings();
		}

		$page = new \BLC\Page\View\Settings(
			array(
				'page_url' => $this->url,
			)
		);
		$page->render();
	}

	/**
	 * Saves settings set by user.
	 */
	private function save_settings() {
		//phpcs:disable
		if ( false === isset( $_REQUEST['settings-tab'] ) ) {
			//phpcs:enable
			return;
		}
		//phpcs:disable
		$tab = sanitize_text_field( wp_unslash( $_REQUEST['settings-tab'] ) );
		//phpcs:enable
		switch ( $tab ) {
			case 'link-types':
				$this->save_link_types_tab();
				break;
			case 'link-tweaks':
				$this->save_link_tweaks_tab();
				break;
			case 'locations':
				$this->save_locations_tab();
				break;
			case 'engine':
				$this->save_engine_tab();
				break;
			case 'permissions':
				$this->save_permissions_tab();
				break;
		}
	}

	/**
	 * Save link types tab.
	 */
	private function save_link_types_tab() {
		check_admin_referer( 'wdblc_settings_link_types' );
		$module_manager = \blcModuleManager::getInstance();
		$parsers        = array();
		$slugs          = $this->list_module_slugs( 'parser' );
		$form_modules   = array();
		if ( isset( $_POST['module'] ) ) {
			//phpcs:disable
			// disabling CS cause arrays can't be properly sanitized with wp functions.
			$form_modules = wp_unslash( $_POST['module'] );
			//phpcs:enable
		}
		if ( false === empty( $form_modules ) ) {
			foreach ( $form_modules as $key => $value ) {
				if ( in_array( $key, $slugs, true ) ) { // only allow registered module names.
					$parsers[] = $key;
				}
			}
		}

		$active = $this->merge_active_modules( 'parser', $parsers );
		$module_manager->set_active_modules( $active );

		if ( false === isset( $_REQUEST['exclusion_list'] ) ) {
			return;
		}
		$exclusion_list = sanitize_text_field( wp_unslash( $_REQUEST['exclusion_list'] ) );
		$exclusion_list = array_filter(
			preg_split(
				'/[\s\r\n]+/', // split on newlines and whitespace.
				$exclusion_list,
				-1,
				PREG_SPLIT_NO_EMPTY // skip empty values.
			)
		);
		App::$options->set( 'exclusion_list', $exclusion_list );
		App::$options->save_options();
	}


	/**
	 * Save locations tab.
	 */
	private function save_locations_tab() {
		$this->save_containers();
		$this->save_acf_fields();
		$this->save_post_statuses();
	}

	/**
	 * Save post statuses.
	 */
	private function save_post_statuses() {
		check_admin_referer( 'wdblc_settings_locations' );
		// At least one post status must be enabled.
		$post_statuses = array( 'publish' => 'on' );

		if ( isset( $_POST['post_statuses'] ) ) {
			//phpcs:disable
			$post_statuses = wp_unslash( $_POST['post_statuses'] );
			//phpcs:enable
		}
		$post_statuses      = array_keys( $post_statuses );
		$available_statuses = get_post_stati( array( 'internal' => false ) );
		$sanitized_statuses = array_intersect( $available_statuses, $post_statuses );

		$enabled_statuses = App::$options->get( 'enabled_post_statuses' );
		$statuses_changed = $this->is_status_list_changed( $enabled_statuses, $sanitized_statuses );
		if ( false === $statuses_changed ) {
			return;
		}
		App::$options->set( 'enabled_post_statuses', $sanitized_statuses );
		App::$options->save_options();

		// Set the same in legacy config manager, otherwise some of functions below will overwrite
		// enabled post statuses option.
		$config                                   = \blc_get_configuration();
		$config->options['enabled_post_statuses'] = $sanitized_statuses;

		// Do what legacy code is doing after enabled status list change.
		$overlord                        = \blcPostTypeOverlord::getInstance();
		$overlord->enabled_post_statuses = $sanitized_statuses;
		$overlord->resynch( 'wsh_status_resynch_trigger' );

		\blc_got_unsynched_items();
		\blc_cleanup_instances();
		\blc_cleanup_links();
	}

	/**
	 * Save containers.
	 */
	private function save_containers() {
		check_admin_referer( 'wdblc_settings_locations' );
		$containers   = array();
		$slugs        = $this->list_module_slugs( 'container' );
		$form_modules = array();
		if ( isset( $_POST['module'] ) ) {
			//phpcs:disable
			$form_modules = wp_unslash( $_POST['module'] );
			//phpcs:enable
		}
		if ( false === empty( $form_modules ) ) {
			foreach ( $form_modules as $key => $value ) {
				if ( in_array( $key, $slugs, true ) ) { // only allow registered module names.
					$containers[] = $key;
				}
			}
		}

		$active         = $this->merge_active_modules( 'container', $containers );
		$module_manager = \blcModuleManager::getInstance();
		$module_manager->set_active_modules( $active );
	}

	/**
	 * Save acf fields.
	 */
	private function save_acf_fields() {
		check_admin_referer( 'wdblc_settings_locations' );

		$acf_fields = array();
		if ( isset( $_POST['blc_acf_fields'] ) ) {
			$acf_fields = sanitize_text_field( wp_unslash( $_POST['blc_acf_fields'] ) );
			$acf_fields = array_filter( preg_split( '/[\r\n]+/', $acf_fields, -1, PREG_SPLIT_NO_EMPTY ) );
		}
	}

	/**
	 * Check if status lists are same.
	 *
	 * @param array $old Old list.
	 * @param array $new New list.
	 * @return boolean
	 */
	private function is_status_list_changed( $old, $new ) {
		if ( count( $old ) !== count( $new ) ) {
			return true;
		}
		$diff = array_diff( $old, $new );
		if ( count( $diff ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Module Manager works on all categories of modules in one call. One tab on
	 * Settings page only works on one type of modules. Fetch active modules
	 * from other categories and merge with what was set on a specific tab,
	 * otherwise modules from other categories would be deactivated..
	 *
	 * @param string $category parser|container|checker Category that is sent in request.
	 * @param array  $modules Module slugs for given category.
	 * @return array[string] Combined active modules.
	 */
	private function merge_active_modules( $category, $modules ) {
		$module_manager = \blcModuleManager::getInstance();
		$all            = $module_manager->get_active_by_category();

		$parser    = isset( $all['parser'] ) ? array_keys( $all['parser'] ) : array();
		$container = isset( $all['container'] ) ? array_keys( $all['container'] ) : array();
		$checker   = isset( $all['checker'] ) ? array_keys( $all['checker'] ) : array();

		// Overwrite category modules.
		$$category = $modules;

		return array_merge( $parser, $container, $checker );
	}

	/**
	 * List module slugs.
	 *
	 * @param string $category parser|container|checker Type of module.
	 */
	private function list_module_slugs( $category ) {
		$slugs          = array();
		$module_manager = \blcModuleManager::getInstance();
		$all_modules    = $module_manager->get_modules_by_category( '', true, true );

		foreach ( $all_modules[ $category ] as $module_id => $module_data ) {
			if ( $module_data['ModuleHidden'] ) {
				continue;
			}

			$slugs[] = $module_id;
		}

		return $slugs;
	}


	/**
	 * Save engine tab.
	 */
	private function save_engine_tab() {
		check_admin_referer( 'wdblc_settings_engine' );
		$automatic_engine_on = false;
		if ( isset( $_POST['automatic_engine_on'] ) ) {
			$automatic_engine_on = true;
		}
		App::$options->set( 'automatic_engine_on', $automatic_engine_on );

		$run_via_cron_frequency = '1';
		if ( isset( $_POST['run_via_cron_frequency'] ) ) {
			$run_via_cron_frequency = intval( wp_unslash( $_POST['run_via_cron_frequency'] ) );
		}
		App::$options->set( 'run_via_cron_frequency', $run_via_cron_frequency );

		$run_via_cron_period = 'hours';
		if ( isset( $_POST['run_via_cron_period'] ) ) {
			$period = sanitize_text_field( wp_unslash( $_POST['run_via_cron_period'] ) );
			if ( in_array( $period, array( 'hours', 'days', 'weeks' ), true ) ) {
				$run_via_cron_period = $period;
			}
		}
		App::$options->set( 'run_via_cron_period', $run_via_cron_period );

		$run_in_dashboard = false;
		if ( isset( $_POST['run_in_dashboard'] ) && $automatic_engine_on ) {
			$run_in_dashboard = true;
		}
		App::$options->set( 'run_in_dashboard', $run_in_dashboard );

		$check_threshold = 72;
		if ( isset( $_POST['check_threshold'] ) ) {
			$check_threshold = intval( wp_unslash( $_POST['check_threshold'] ) );
		}
		App::$options->set( 'check_threshold', $check_threshold );

		$timeout = 30;
		if ( isset( $_POST['timeout'] ) ) {
			$timeout = intval( wp_unslash( $_POST['timeout'] ) );
		}
		App::$options->set( 'timeout', $timeout );

		$max_execution_time = 30;
		if ( isset( $_POST['max_execution_time'] ) ) {
			$max_execution_time = intval( wp_unslash( $_POST['max_execution_time'] ) );
		}
		App::$options->set( 'max_execution_time', $max_execution_time );

		$user_enabled_load_limit = false;
		if ( isset( $_POST['user_enabled_load_limit'] ) ) {
			$user_enabled_load_limit = true;
		}
		App::$options->set( 'user_enabled_load_limit', $user_enabled_load_limit );

		$server_load_limit = 30;
		if ( isset( $_POST['server_load_limit'] ) ) {
			$server_load_limit = floatval( wp_unslash( $_POST['server_load_limit'] ) );
		}
		App::$options->set( 'server_load_limit', $server_load_limit );

		$run_via_cron = true;
		App::$options->set( 'run_via_cron', $run_via_cron );

		// Set the options to disable the background processes.
		if ( ! $automatic_engine_on ) {
			$run_via_cron = false;
			App::$options->set( 'run_via_cron', $run_via_cron );

			$run_in_dashboard = false;
			App::$options->set( 'run_in_dashboard', $run_in_dashboard );
		}

		App::$options->save_options();
	}


	/**
	 * Save link tweaks tab.
	 */
	private function save_link_tweaks_tab() {
		check_admin_referer( 'wdblc_settings_link_tweaks' );

		$mark_broken_links = false;
		if ( isset( $_POST['mark_broken_links'] ) ) {
			$mark_broken_links = true;
		}
		App::$options->set( 'mark_broken_links', $mark_broken_links );

		$mark_removed_links = false;
		if ( isset( $_POST['mark_removed_links'] ) ) {
			$mark_removed_links = true;
		}
		App::$options->set( 'mark_removed_links', $mark_removed_links );

		$nofollow_broken_links = false;
		if ( isset( $_POST['nofollow_broken_links'] ) ) {
			$nofollow_broken_links = true;
		}
		App::$options->set( 'nofollow_broken_links', $nofollow_broken_links );

		App::$options->save_options();
	}

	/**
	 * Save permissions tab.
	 */
	private function save_permissions_tab() {
		check_admin_referer( 'wdblc_settings_permissions' );
		$roles_inst      = wp_roles();
		$available_roles = array_keys( $roles_inst->roles );
		$enabled_roles   = array();

		foreach ( $available_roles as $role ) {
			if ( isset( $_POST[ $role ] ) ) {
				$roles[] = $role;
			}
		}

		App::$options->set( 'dashboard_widget_roles', $roles );
		App::$options->set( 'dashboard_widget_roles_converted', true );

		$widget_enabled = false;
		if ( isset( $_POST['widget_enabled'] ) ) {
			$widget_enabled = true;
		}
		App::$options->set( 'dashboard_widget_enabled', $widget_enabled );

		App::$options->save_options();
	}
}
