<?php
/**
 * Controller class used to process request, set up state and render Dashboard page in WP Admin.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page;

use BLC\Core\Controller;
use BLC\Core\App;

/**
 * Controller class used to process request, set up state and render Dashboard page in WP Admin.
 */
class Dashboard extends Controller {
	/**
	 * Menu slug.
	 *
	 * @var string
	 */
	protected $menu_slug = 'brokenlinkchecker';

	/**
	 * Returns Menu title.
	 *
	 * @return string Menu title.
	 */
	public function get_menu_title() {
		return __( 'Dashboard', 'broken-link-checker' );
	}

	/**
	 * Returns Page title.
	 *
	 * @return string Page title.
	 */
	public function get_page_title() {
		return __( 'Dashboard', 'broken-link-checker' );
	}

	/**
	 * Handle request.
	 */
	public function handle_request() {
		//phpcs:disable
		if ( isset( $_REQUEST['activate-notifications'] ) ) {
			//phpcs:enable
			$this->activate_notifications();
		}

		$page = new \BLC\Page\View\Dashboard(
			array(
				'page_url' => $this->url,
			)
		);
		$page->render();
	}

	/**
	 * Activate notifications.
	 */
	private function activate_notifications() {
		check_admin_referer( 'wdblc_dashboard' );

		if ( ! App::$options->get( 'send_email_notifications' ) ) {
			/*
			The plugin should only send notifications about links that have become broken
			since the time when email notifications were turned on. If we don't do this,
			the first email notification will be sent nigh-immediately and list *all* broken
			links that the plugin currently knows about.
			*/
			App::$options->set( 'last_notification_sent', time() );
		}

		App::$options->set( 'send_email_notifications', true );

		if ( ! App::$options->get( 'send_emails_schedule' ) ) {
			// Set email schedule to conservative twice a month schedule.
			App::$options->set( 'send_emails_schedule', 'bimonthly' );
		}

		App::$options->save_options();
	}
}
