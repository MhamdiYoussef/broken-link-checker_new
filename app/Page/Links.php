<?php
/**
 * Controller class used to process request, set up state and render Links page in WP Admin.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Page;

use BLC\Core\Controller;
use BLC\Core\App;

/**
 * Controller class used to process request, set up state and render Links page in WP Admin.
 */
class Links extends Controller {
	/**
	 * Menu slug.
	 *
	 * @var string
	 */
	protected $menu_slug = 'brokenlinkchecker-links';

	/**
	 * Returns Menu title.
	 *
	 * @return string Menu title.
	 */
	public function get_menu_title() {
		return __( 'Links', 'broken-link-checker' );
	}

	/**
	 * Returns Page title.
	 *
	 * @return string Page title.
	 */
	public function get_page_title() {
		return __( 'Broken Links', 'broken-link-checker' );
	}

	/**
	 *  Handle request.
	 */
	public function handle_request() {
		//phpcs:disable
		if ( isset( $_REQUEST['save-settings'] ) ) {
			//phpcs:enable
			$this->save_settings();
		}

		$page = new \BLC\Page\View\Links(
			array(
				'page_url' => $this->url,
			)
		);
		$page->render();
	}

	/**
	 * Saves settings set by user.
	 */
	private function save_settings() {
		//phpcs:disable
		if ( false === isset( $_REQUEST['links-tab'] ) ) {
			//phpcs:enable
			return;
		}
		//phpcs:disable
		$tab = sanitize_text_field( wp_unslash( $_REQUEST['links-tab'] ) );
		//phpcs:enable
		switch ( $tab ) {
			case 'notifications':
				$this->save_notifications();
				break;
			case 'settings':
				$this->save_settings_tab();
				break;
		}
	}

	/**
	 * Save notifications tab.
	 */
	private function save_notifications() {
		check_admin_referer( 'wdblc_links_notifications' );

		$send_email_notifications = false;
		if ( isset( $_POST['send_email_notifications'] ) ) {
			$send_email_notifications = true;
		}
		$send_authors_email_notifications = false;
		if ( isset( $_POST['send_authors_email_notifications'] ) ) {
			$send_authors_email_notifications = true;
		}

		if (
				( $send_email_notifications && ! App::$options->get( 'send_email_notifications' ) )
				|| ( $send_authors_email_notifications && ! App::$options->get( 'send_authors_email_notifications' ) )
			) {
			/*
			The plugin should only send notifications about links that have become broken
			since the time when email notifications were turned on. If we don't do this,
			the first email notification will be sent nigh-immediately and list *all* broken
			links that the plugin currently knows about.
			*/
			App::$options->set( 'last_notification_sent', time() );
		}

		App::$options->set( 'send_authors_email_notifications', $send_authors_email_notifications );
		App::$options->set( 'send_email_notifications', $send_email_notifications );

		$send_emails_schedule = '';
		if ( isset( $_POST['send_emails_schedule'] ) ) {
			$send_emails_schedule = sanitize_text_field( wp_unslash( $_POST['send_emails_schedule'] ) );
		}
		App::$options->set( 'send_emails_schedule', $send_emails_schedule );

		App::$options->save_options();
	}

	/**
	 * Save settings tab.
	 */
	private function save_settings_tab() {
		check_admin_referer( 'wdblc_links_settings' );

		$actions = array(
			'edit'                  => false,
			'delete'                => false,
			'blc-discard-action'    => false,
			'blc-dismiss-action'    => false,
			'blc-recheck-action'    => false,
			'blc-deredirect-action' => false,
		);
		if ( isset( $_POST['show_link_actions'] ) && ! empty( $_POST['show_link_actions']['edit'] ) ) {
			$actions['edit'] = true;
		}
		if ( isset( $_POST['show_link_actions'] ) && ! empty( $_POST['show_link_actions']['delete'] ) ) {
			$actions['delete'] = true;
		}
		if ( isset( $_POST['show_link_actions'] ) && ! empty( $_POST['show_link_actions']['blc-discard-action'] ) ) {
			$actions['blc-discard-action'] = true;
		}
		if ( isset( $_POST['show_link_actions'] ) && ! empty( $_POST['show_link_actions']['blc-dismiss-action'] ) ) {
			$actions['blc-dismiss-action'] = true;
		}
		if ( isset( $_POST['show_link_actions'] ) && ! empty( $_POST['show_link_actions']['blc-recheck-action'] ) ) {
			$actions['blc-recheck-action'] = true;
		}
		if ( isset( $_POST['show_link_actions'] ) && ! empty( $_POST['show_link_actions']['blc-deredirect-action'] ) ) {
			$actions['blc-deredirect-action'] = true;
		}

		App::$options->set( 'show_link_actions', $actions );

		$columns = array(
			'link_text'    => false,
			'status'       => false,
			'url'          => false,
			'redirect_url' => false,
			'source'       => false,
		);
		if ( isset( $_POST['visible_columns'] ) && ! empty( $_POST['visible_columns']['link_text'] ) ) {
			$actions['link_text'] = true;
		}
		if ( isset( $_POST['visible_columns'] ) && ! empty( $_POST['visible_columns']['status'] ) ) {
			$actions['status'] = true;
		}
		if ( isset( $_POST['visible_columns'] ) && ! empty( $_POST['visible_columns']['url'] ) ) {
			$actions['url'] = true;
		}
		if ( isset( $_POST['visible_columns'] ) && ! empty( $_POST['visible_columns']['redirect_url'] ) ) {
			$actions['redirect_url'] = true;
		}
		if ( isset( $_POST['visible_columns'] ) && ! empty( $_POST['visible_columns']['source'] ) ) {
			$actions['source'] = true;
		}

		App::$options->set( 'visible_columns', $actions );

		App::$options->save_options();
	}
}
