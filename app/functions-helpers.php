<?php
/**
 * Custom helper functions.
 *
 * Helper functions for the plugin.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC;

/**
 * Wrapper for getting option class instance.
 *
 * @since  2.0.0
 * @access public
 * @return mixed
 */
function blc_get_options() {
	// TODO this needs to be removed and replaced with call to App::get_instance()->options
	return Manager\AdminManager::$classes['Options'];
}

/**
 * Wrapper for getting plugin option
 *
 * @since  2.0.0
 * @access public
 * @param  string $option_key Option name.
 * @param  string $default Default option.
 * @return mixed
 */
function blc_get_option( $option_key, $default = null ) {
	// TODO this needs to be removed and replaced with call to App::get_instance()->options
	if ( ! $option_key ) {
		return false;
	}
	return Manager\AdminManager::$classes['Options']->get( $option_key, $default );

}

/**
 * Wrapper for setting plugin option
 *
 * @since  2.0.0
 * @access public
 * @param  string $option_key Option name.
 * @param  string $value Option value.
 * @return bool
 */
function blc_set_option( $option_key, $value ) {
	// TODO this needs to be removed and replaced with call to App::get_instance()->options
	if ( ! $option_key ) {
		return false;
	}
	Manager\AdminManager::$classes['Options']->set( $option_key, $value );
}

/**
 * Wrapper for saving plugin option
 *
 * @since  2.0.0
 * @access public
 * @return bool
 */
function blc_save_option() {
	// TODO this needs to be removed and replaced with call to App::get_instance()->options
	return Manager\AdminManager::$classes['Options']->save_options();
}

/**
 * Wrapper for loggers
 *
 * @since  2.0.0
 * @access public
 * @return object
 */
function blc_logger() {
	// TODO this needs to be removed and replaced with call to App::get_instance()->logger
	if ( blc_get_option( 'logging_enabled', false ) && ( blc_get_option( 'log_file' ) ) ) {
		$blclog = Manager\LogManager::$classes['FileLogger'];
	} else {
		$blclog = Manager\LogManager::$classes['OptionLogger'];
	}
	return $blclog;
}

/**
 * Notify the link checker that there are unsynched items
 * that might contain links (e.g. a new or edited post).
 *
 * @return void
 */
function blc_got_unsynched_items() {
	$conf = blc_get_option( 'need_resynch' );

	if ( ! $conf ) {
		$conf = true;
		blc_set_option( 'need_resynch', true );
		blc_save_option();
	}
}

/**
 * (Re)create synchronization records for all containers and mark them all as unparsed.
 *
 * @param bool $forced If true, the plugin will recreate all synch. records from scratch.
 * @return void
 */
function blc_resynch( $forced = false ) {
	global $wpdb;

	$blclog = blc_logger();
	if ( $forced ) {
		$blclog->info( '... Forced resynchronization initiated' );

		// Drop all synchronization records.
		$wpdb->query( "TRUNCATE {$wpdb->prefix}blc_synch" );//phpcs:ignore

	} else {
		$blclog->info( '... Resynchronization initiated' );
	}

	// Remove invalid DB entries.
	blc_cleanup_database();

	// (Re)create and update synch. records for all container types.
	$blclog->info( '... (Re)creating container records' );
	blcContainerHelper::resynch( $forced );

	$blclog->info( '... Setting resync. flags' );
	blc_got_unsynched_items();

	// All done.
	$blclog->info( 'Database resynchronization complete.' );
}

/**
 * Delete synch. records, instances and links that refer to missing or invalid items.
 *
 * @return void
 */
function blc_cleanup_database() {
	global $blclog;

	// Delete synch. records for container types that don't exist.
	$blclog->info( '... Deleting invalid container records' );
	blcContainerHelper::cleanup_containers();

	// Delete invalid instances.
	$blclog->info( '... Deleting invalid link instances' );
	blc_cleanup_instances();

	// Delete orphaned links.
	$blclog->info( '... Deleting orphaned links' );
	blc_cleanup_links();
}

/**
 * Remove orphaned links that have no corresponding instances.
 *
 * @param int|array $link_id (optional) Only check these links.
 * @return bool
 */
function blc_cleanup_links( $link_id = null ) {
	global $wpdb;
	$blclog = blc_logger();

	$start = microtime( true );
	$q     = "DELETE FROM {$wpdb->prefix}blc_links
			USING {$wpdb->prefix}blc_links LEFT JOIN {$wpdb->prefix}blc_instances
				ON {$wpdb->prefix}blc_instances.link_id = {$wpdb->prefix}blc_links.link_id
			WHERE
				{$wpdb->prefix}blc_instances.link_id IS NULL";

	if ( null !== $link_id ) {
		if ( ! is_array( $link_id ) ) {
			$link_id = array( intval( $link_id ) );
		}
		$q .= " AND {$wpdb->prefix}blc_links.link_id IN (" . implode( ', ', $link_id ) . ')';
	}

	$rez     = $wpdb->query( $q ); //phpcs:ignore
	$elapsed = microtime( true ) - $start;
	$blclog->log( sprintf( '... %d links deleted in %.3f seconds', $wpdb->rows_affected, $elapsed ) );

	return false !== $rez;
}

/**
 * Retrieve a list of links matching some criteria.
 *
 * The function argument should be an associative array describing the criteria.
 * The supported keys are :
 *     'offset' - Skip the first X results. Default is 0.
 *     'max_results' - The maximum number of links to return. Defaults to returning all results.
 *     'link_ids' - Retrieve only links with these IDs. This should either be a comma-separated list or an array.
 *     's_link_text' - Link text must match this keyphrase (performs a fulltext search).
 *     's_link_url' - Link URL must contain this string. You can use "*" as a wildcard.
 *     's_parser_type' - Filter links by the type of link parser that was used to find them.
 *     's_container_type' - Filter links by where they were found, e.g. 'post'.
 *     's_container_id' - Find links that belong to a container with this ID (should be used together with s_container_type).
 *     's_link_type' - Either parser type or container type must match this.
 *     's_http_code' - Filter by HTTP code. Example : 201,400-410,500
 *     's_filter' - Use a built-in filter. Available filters : 'broken', 'redirects', 'all'
 *     'where_expr' - Advanced. Lets you directly specify a part of the WHERE clause.
 *     'load_instances' - Pre-load all link instance data for each link. Default is false.
 *     'load_containers' - Pre-load container data for each instance. Default is false.
 *     'load_wrapped_objects' - Pre-load wrapped object data (e.g. posts, comments, etc) for each container. Default is false.
 *     'count_only' - Only return the number of results (int), not the whole result set. 'offset' and 'max_results' will be ignored if this is set. Default is false.
 *     'purpose' -  An optional code indicating how the links will be used.
 *     'include_invalid' - Include links that have no instances and links that only have instances that reference not-loaded containers or parsers. Defaults to false.
 *
 * All keys are optional.
 *
 * @uses blcLinkQuery::get_links();
 *
 * @param array $params
 * @return int|blcLink[] Either an array of blcLink objects, or the number of results for the query.
 */
function blc_get_links( $params = null ) {
	$instance = Classes\LinkQuery::get_instance();
	return $instance->get_links( $params );
}

