<?php
/**
 * Component class used to set up and render components.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Component;

/**
 * Component class used to set up and render components.
 */
abstract class Component {

	/**
	 * Variables used in rendering the template.
	 *
	 * @var array
	 */
	protected $vars = array();

	/**
	 * Constructor.
	 *
	 * @param array $vars Variables that will be used in rendering
	 * component template. It's best to pass all the variables to
	 * component while constructing.
	 */
	public function __construct( $vars = array() ) {
		$this->vars = $vars;
	}

	/**
	 * Modify variable value that will be used in rendering the tempalte.
	 * variables can only be added on a Component instantiation, this method
	 * only allows updating variable if needed.
	 * Useful for updating variables before the render
	 * method is called.
	 *
	 * @param string $name Existing variable name.
	 * @param mixed  $value Can be anything.
	 */
	public function set_variable( $name, $value ) {
		if ( empty( $name ) ) {
			return;
		}
		if ( false === isset( $this->vars[ $name ] ) ) {
			return;
		}
		$this->vars[ $name ] = $value;
	}

	/**
	 * Get all variables for rendering the template.
	 *
	 * @return array Template variables.
	 */
	public function get_variables() {
		return $this->vars;
	}

	/**
	 * Get variable for rendering the template.
	 *
	 * @param string $name Name of the variable.
	 * @return string Template variable.
	 */
	public function get_variable( $name ) {
		if ( empty( $name ) ) {
			return null;
		}
		if ( isset( $this->vars[ $name ] ) ) {
			return $this->vars[ $name ];
		}
		return null;
	}

	/**
	 * Returns absolute path to the template file.
	 *
	 * @param string $template Template which absolute path is required.
	 *                         This param needs to be path relative to
	 *                         'app/templates/' directory.
	 * @return string Absolute path.
	 */
	public function get_template_path( $template = '' ) {
		if ( '' !== $template ) {
			return BLC_TEMPLATE . $template . '.php';
		}
	}

	/**
	 * Render a template from templates directory. All variables stored in $this->vars
	 * will be available in the template by their key, e.g. variable added to vars
	 * with $this->set_variable( 'text', 'Some text.' ) will be available as $text.
	 * All variables that will be used in rendering need to be set up in $this->vars
	 * before rendring the template i.e. calling this method.
	 *
	 * @param string $template Tempalte path relative to app/views/.
	 */
	public function render_part( $template ) {
		$template_path = $this->get_template_path( $template );
		if ( ! file_exists( $template_path ) ) {
			echo esc_html( 'There is no template at: ' . $template_path );
			return;
		}

		// extract() but not.
		foreach ( $this->vars as $index => $var ) {
			$$index = $var;
		}

		include $template_path;
	}

	/**
	 * Render component to the page.
	 */
	abstract public function render();
}
