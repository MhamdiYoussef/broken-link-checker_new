<?php
/**
 * Tabbed Content component can have any number of tabs that it will
 * render with side navigation and content.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Component;

/**
 * Tabbed Content component can have any number of tabs that it will
 * render with side navigation and content.
 */
class TabbedContent extends Component {
	/**
	 * Constructor.
	 *
	 * @param array[BLC\Component\Tab] $tabs Tabs.
	 * @param array                    $vars Other data needed to render template.
	 */
	public function __construct( $tabs = array(), $vars = array() ) {
		$this->vars['tabs'] = $tabs;
		$this->vars         = array_merge( $this->vars, $vars );
	}

	/**
	 * Render component to the page.
	 */
	public function render() {
		$this->render_part( 'components/tabbed-content' );
	}
}
