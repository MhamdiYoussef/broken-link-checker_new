<?php
/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Component;

/**
 * Tab component that contains all the information about a tab that is
 * inner component of Tabbed Content component.
 */
abstract class Tab extends Component {
	/**
	 * Constructor.
	 *
	 * @param array $vars Other data needed to render template.
	 */
	public function __construct( $vars = array() ) {
		$this->vars = array_merge( $this->vars, $vars );
	}

	/**
	 * Returns tab title.
	 *
	 * @return string tab title.
	 */
	abstract public function get_title();

	/**
	 * Returns tab slug.
	 *
	 * @return string tab slug.
	 */
	abstract public function get_slug();

	/**
	 * Render template.
	 */
	public function render() {
		$this->render_part( $this->template );
	}
}
