<?php
/**
 * Base manager class.
 *
 * This is the base manager class.
 * Extend to create new dependent class for the application.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Abstracts;

use BLC\Core\App;

/**
 * Manager class.
 *
 * @since  2.0.0
 * @access public
 */
abstract class Manager {

	/**
	 * App instance.
	 * Since App class is responsible for binding
	 * use that class to bind the managed classes
	 *
	 * @since  2.0.0
	 * @access protected
	 * @var    App
	 */
	protected $app;

	/**
	 * Class instances.
	 * The instances of classes handled by this manager
	 *
	 * @since  2.0.0
	 * @access protected
	 * @var    App
	 */
	public static $classes;

	/**
	 * Accepts the application and sets it to the `$app` property.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  object App $app App class.
	 * @return void
	 */
	public function __construct( App $app ) {
		$this->app = $app;
	}

	/**
	 * Register the classes that are handled by the manager.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function register() {}
}
