<?php
/**
 * Base logger class.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Abstracts;

/**
 * Logger class.
 *
 * @since  2.0.0
 * @access public
 */
abstract class Logger {

	/**
	 * Debug level.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    int
	 */
	const BLC_LEVEL_DEBUG = 0;

	/**
	 * Debug level.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    int
	 */
	const BLC_LEVEL_INFO = 1;

	/**
	 * Warning level.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    int
	 */
	const BLC_LEVEL_WARNING = 2;

	/**
	 * Error level.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    int
	 */
	const BLC_LEVEL_ERROR = 3;

	/**
	 * Error level.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    int
	 */
	protected $log_level = self::BLC_LEVEL_DEBUG;

	/**
	 * Log function.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $message Message to log.
	 * @param  object $object  Data object for the logger.
	 * @param  int    $level   Level of the log message.
	 * @return void
	 */
	public function log( $message, $object = null, $level = BLC_LEVEL_INFO ) {}

	/**
	 * Log debug message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $message Message to log.
	 * @param  object $object  Data object for the logger.
	 * @return void
	 */
	public function debug( $message, $object = null ) {
		$this->log( $message, $object, self::BLC_LEVEL_DEBUG );
	}

	/**
	 * Log info message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $message Message to log.
	 * @param  object $object  Data object for the logger.
	 * @return void
	 */
	public function info( $message, $object = null ) {
		$this->log( $message, $object, BLC_LEVEL_INFO );
	}

	/**
	 * Log warning message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $message Message to log.
	 * @param  object $object  Data object for the logger.
	 * @return void
	 */
	public function warn( $message, $object = null ) {
		$this->log( $message, $object, BLC_LEVEL_WARNING );
	}

	/**
	 * Log error message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $message Message to log.
	 * @param  object $object  Data object for the logger.
	 * @return void
	 */
	public function error( $message, $object = null ) {
		$this->log( $message, $object, BLC_LEVEL_ERROR );
	}

	/**
	 * Get log message.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  int $min_level Level of the log message.
	 * @return array
	 */
	public function get_messages( $min_level = self::BLC_LEVEL_DEBUG ) {
		return array();
	}

	/**
	 * Get the whole log.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  int $min_level Level of the log message.
	 * @return array
	 */
	public function get_log( $min_level = self::BLC_LEVEL_DEBUG ) {
		return array();
	}

	/**
	 * Clear log.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function clear() {}

	/**
	 * Set log level.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  int $level The error level.
	 * @return void
	 */
	public function set_log_level( $level ) {
		$this->log_level = $level;
	}
}
