<?php
/**
 * Base class for BLC modules.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Abstracts;

/**
 * Manager class.
 *
 * @since  2.0.0
 * @access public
 */
abstract class Module {

	/**
	 * Module ID.
	 *
	 * Usually a lowercase string
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    module_id
	 */
	public $module_id;

	/**
	 * Contains header data of the module files.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    cached_header
	 */
	public $cached_header;

	/**
	 * A reference to the plugin's global configuration object.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    plugin_conf
	 */
	public $plugin_conf;

	/**
	 * A reference to the module manager.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    module_manager
	 */
	public $module_manager;

	/**
	 * Accepts the application and sets it to the `$app` property.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $module_id      Module ID.
	 * @param  array  $cached_header  Header data of module files.
	 * @param  object $plugin_conf    Plugins configuration.
	 * @param  object $module_manager Module manager reference.
	 * @return void
	 */
	public function __construct( $module_id, $cached_header, &$plugin_conf, &$module_manager ) {
		$this->module_id      = $module_id;
		$this->cached_header  = $cached_header;
		$this->plugin_conf    = &$plugin_conf;
		$this->module_manager = &$module_manager;

		$this->init();
	}


	/**
	 * Module initializer. Called when the module is first instantiated.
	 * The default implementation does nothing. Override it in a subclass to
	 * specify some sort of start-up behaviour.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function init() {}

	/**
	 * Called when the module is activated.
	 * Should be overridden in a sub-class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function activated() {}

	/**
	 * Called when the module is deactivated.
	 * Should be overridden in a sub-class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function deactivated() {}

	/**
	 * Called when BLC itself is activated.
	 * Usually this method just calls activated(), but subclasses could override it for special handling.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function plugin_activated() {
		$this->activated();
	}
}
