<?php
/**
 * Admin class.
 *
 * Handles admin pages, sets up routes and controllers.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Core;

use BLC\Classes\LinkQuery as LinkQuery;
use BLC\Core\App;
use BLC\Page;

/**
 * Handles admin pages, sets up routes and controllers.
 *
 * @since  2.0.0
 * @access public
 */
class Admin {

	/**
	 * Option name.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	private $menu_page;

	/**
	 * Initiates the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'set_up_routes' ) );
	}

	/**
	 * Activation method.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function enqueue() {
		add_filter( 'admin_body_class', array( $this, 'add_sui_body_class' ) );

		wp_enqueue_script(
			'blc',
			BLC_URL . 'assets/js/blc-admin.min.js',
			array( 'jquery' ),
			'1.2.0',
			true
		);
		// Enqueue my cloned JS here.
		wp_enqueue_script(
			'blc-admin-clone',
			BLC_URL . 'assets/js/src/blc-admin-clone.js',
			array( 'jquery' ),
			'1.2.0',
			true
		);

		// Enqueue styles.
		wp_enqueue_style(
			'blc-main',
			BLC_URL . 'assets/css/blc-admin.min.css',
			array(),
			'1.3'
		);
		wp_enqueue_style(
			'blc-style',
			BLC_URL . 'assets/css/blc-style.css',
			array(),
			'2.0.0',
			false
		);
	}

	/**
	 * Initiate admin menu.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function set_up_routes() {
		$main_menu_title = __( 'Link Checker', 'brokenlinkchecker' );
		$dashboard       = new Page\Dashboard();
		$this->menu_page = $this->add_main_menu_item( $main_menu_title, $dashboard );

		$links    = new Page\Links();
		$settings = new Page\Settings();
		$pages    = array( $dashboard, $links, $settings );

		foreach ( $pages as $page ) {
			$page_hook = add_submenu_page(
				'brokenlinkchecker',
				$page->get_menu_title(),
				$page->get_page_title(),
				$page->get_required_permission(),
				$page->get_menu_slug(),
				array( $page, 'handle_request' )
			);

			add_action( 'load-' . $page_hook, array( $this, 'enqueue' ) );
		}

	}

	/**
	 * Adds main menu item to the WP Admin menu.
	 *
	 * @param string    $title Title for the main menu item.
	 * @param Page\Page $page Main page to be shown for main menu item.
	 * @return object WordPress Menu Page.
	 */
	private function add_main_menu_item( $title, $page ) {
		return add_menu_page(
			$title,
			$title,
			$page->get_required_permission(),
			$page->get_menu_slug(),
			array( $page, 'handle_request' ),
			''
		);
	}

	/**
	 * Add SUI body class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @param  string $classes Admin body classes.
	 * @return string
	 */
	public function add_sui_body_class( $classes ) {
		$classes .= ' ' . BLC_SUI_VERSION;
		return $classes;
	}
}
