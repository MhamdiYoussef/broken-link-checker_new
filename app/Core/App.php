<?php
/**
 * App class.
 *
 * This is the main class that is initiated first which has
 * everything necessary for the plugin to boot
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Core;

/**
 * App class that contains references to instances to generally
 * used classes. Use to get access to options.
 *
 * @since  2.0.0
 * @access public
 */
class App {

	/**
	 * The current version of the plugin.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    string
	 */
	const VERSION = '2.0.0';


	/**
	 * Admin needs to be refactored into various pages and
	 * page sections.
	 *
	 * @var \BLC\Core\Admin
	 */
	public static $admin;

	/**
	 * Stores application options.
	 *
	 * @var \BLC\Classes\Options
	 */
	public static $options;

	/**
	 * App Logger, there can be only one!
	 *
	 * @var \BLC\Classes\Logger
	 */
	public static $logger;

	/**
	 * We'll see about this. Probably gonna be broken up into
	 * specific handlers.
	 *
	 * @var \BLC\Classes\Ajax
	 */
	public static $ajax;

	/**
	 * Handles email notifications.
	 *
	 * @var \BLC\Classes\Email
	 */
	public static $email;

	/**
	 * Contains single instance of this class.
	 *
	 * @var \BLC\Core\App
	 */
	private static $instance;

	/**
	 * Get the instance of the class.
	 *
	 * @return App - the single instance of the class.
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new App();
		}

		return self::$instance;
	}

	/**
	 * Initiates the class. Singleton please!
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	private function __construct() {
		$this->set_up_classes();
		add_action( 'wp_ajax_wdblc_save_onboarding', array( $this, 'save_onboarding' ) );
		add_action( 'wp_ajax_wdblc_skip_onboarding', array( $this, 'skip_onboarding' ) );
	}

	/**
	 * Sets up relevant classes for the whole application such as
	 * logger, options, etc.
	 *
	 * @return void
	 */
	private function set_up_classes() {
		$options       = new Options();
		self::$options = $options;

		if ( $options->get( 'logging_enabled', false ) && ! empty( $options->get( 'log_file' ) ) && is_writable( $options->get( 'log_file' ) ) ) {
			$log_filename = $options->get( 'log_file' );
			self::$logger = new \BLC\Classes\Loggers\FileLogger( $log_filename );
		} else {
			// TODO is this always needed?
			self::$logger = new \BLC\Classes\Loggers\OptionLogger();
		}

		self::$admin = new Admin();

		// self::$ajax  = new \BLC\Classes\Ajax();
		// TODO$this->email = new \BLC\Classes\Email();
	}

	/**
	 * Save onboarding.
	 */
	public function save_onboarding() {
		if ( false === check_ajax_referer( 'wdblc_onboarding', '_wpnonce', false ) ) {
			wp_send_json_error( array( 'error' => 'not-allowed' ) );
		}

		// Save active link types.
		$module_manager = \blcModuleManager::getInstance();
		$parsers        = array();
		$slugs          = $this->list_module_slugs( 'parser' );
		$form_modules   = array();
		if ( isset( $_POST['module'] ) ) {
			//phpcs:disable
			// disabling CS cause arrays can't be properly sanitized with wp functions.
			$form_modules = wp_unslash( $_POST['module'] );
			//phpcs:enable
		}
		if ( false === empty( $form_modules ) ) {
			foreach ( $form_modules as $key => $value ) {
				if ( in_array( $key, $slugs, true ) ) { // only allow registered module names.
					$parsers[] = $key;
				}
			}
		}

		$active = $this->merge_active_modules( 'parser', $parsers );
		$module_manager->set_active_modules( $active );

		// Save engine type.
		$automatic_engine_on = false;
		if ( false === isset( $_REQUEST['automatic_engine_on'] ) ) {
			$automatic_engine_on = true;
		}
		self::$options->set( 'automatic_engine_on', $automatic_engine_on );
		self::$options->set( 'onboarding_done', true );
		self::$options->save_options();

		wp_send_json_success();
	}

	/**
	 * List module slugs.
	 *
	 * @param string $category parser|container|checker Type of module.
	 */
	private function list_module_slugs( $category ) {
		$slugs          = array();
		$module_manager = \blcModuleManager::getInstance();
		$all_modules    = $module_manager->get_modules_by_category( '', true, true );

		foreach ( $all_modules[ $category ] as $module_id => $module_data ) {
			if ( $module_data['ModuleHidden'] ) {
				continue;
			}

			$slugs[] = $module_id;
		}

		return $slugs;
	}

	/**
	 * Module Manager works on all categories of modules in one call. One tab on
	 * Settings page only works on one type of modules. Fetch active modules
	 * from other categories and merge with what was set on a specific tab,
	 * otherwise modules from other categories would be deactivated..
	 *
	 * @param string $category parser|container|checker Category that is sent in request.
	 * @param array  $modules Module slugs for given category.
	 * @return array[string] Combined active modules.
	 */
	private function merge_active_modules( $category, $modules ) {
		$module_manager = \blcModuleManager::getInstance();
		$all            = $module_manager->get_active_by_category();

		$parser    = isset( $all['parser'] ) ? array_keys( $all['parser'] ) : array();
		$container = isset( $all['container'] ) ? array_keys( $all['container'] ) : array();
		$checker   = isset( $all['checker'] ) ? array_keys( $all['checker'] ) : array();

		// Overwrite category modules.
		$$category = $modules;

		return array_merge( $parser, $container, $checker );
	}

	/**
	 * Skip onboarding.
	 */
	public function skip_onboarding() {
		if ( false === check_ajax_referer( 'wdblc_skip_onboarding', '_wpnonce', false ) ) {
			wp_send_json_error( array( 'error' => 'not-allowed' ) );
		}

		self::$options->set( 'onboarding_done', true );
		self::$options->save_options();

		wp_send_json_success();
	}
}
