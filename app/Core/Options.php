<?php
/**
 * Options class.
 *
 * Handles options saved in DB.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Core;

/**
 * Handles options saved in DB.
 *
 * @since  2.0.0
 * @access public
 */
class Options {

	/**
	 * Database option name where all BLC options are stored..
	 *
	 * @since  2.0.0
	 * @access private
	 * @var    string
	 */
	private $db_store_key = 'wsblc_options';

	/**
	 * Plugin options.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    array
	 */
	private $options = array();

	/**
	 * Plugin default options.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    array
	 */
	private $defaults = array(
		'max_execution_time'               => 7 * 60, // (in seconds) How long the worker instance may run, at most.
		'check_threshold'                  => 72,  // (in hours) Check each link every 72 hours.
		'recheck_count'                    => 3, // How many times a broken link should be re-checked.
		'recheck_threshold'                => 30 * 60, // (in seconds) Re-check broken links after 30 minutes.
		'run_in_dashboard'                 => true, // Run the link checker algo. continuously while the Dashboard is open.
		'run_via_cron'                     => true, // Run it hourly via WordPress pseudo-cron.
		'run_via_cron_frequency'           => 1,
		'run_via_cron_period'              => 'hours',
		'mark_broken_links'                => true, // Whether to add the broken_link class to broken links in posts.
		'broken_link_css'                  => ".broken_link, a.broken_link {\n\ttext-decoration: line-through;\n}",
		'nofollow_broken_links'            => false, // Whether to add rel="nofollow" to broken links in posts.
		'mark_removed_links'               => false, // Whether to add the removed_link class when un-linking a link.
		'removed_link_css'                 => ".removed_link, a.removed_link {\n\ttext-decoration: line-through;\n}",
		'exclusion_list'                   => array(), // Links that contain a substring listed in this array won't be checked.
		'send_email_notifications'         => true, // Whether to send the admin email notifications about broken links.
		'send_authors_email_notifications' => false, // Whether to send post authors notifications about broken links in their posts.
		'notification_email_address'       => '', // If set, send email notifications to this address instead of the admin.
		'last_notification_sent'           => 0, // When the last email notification was sent (Unix timestamp).
		'suggestions_enabled'              => true, // Whether to suggest alternative URLs for broken links.
		'warnings_enabled'                 => true, // Try to automatically detect temporary problems and false positives, and report them as "Warnings" instead of broken links.
		'server_load_limit'                => null, // Stop parsing stuff & checking links if the 1-minute load average goes over this value. Only works on Linux servers. 0 = no limit.
		'enable_load_limit'                => true, // Enable/disable load monitoring. Indicates if load limit is available on server.
		'user_enabled_load_limit'          => true, // If user chose to enable load limit in the UI.
		'custom_fields'                    => array(), // List of custom fields that can contain URLs and should be checked.
		'acf_fields'                       => array(), // List of custom fields that can contain URLs and should be checked.
		'enabled_post_statuses'            => array( 'publish' ), // Only check posts that match one of these statuses.
		'autoexpand_widget'                => true, // Autoexpand the Dashboard widget if broken links are detected.
		'dashboard_widget_capability'      => 'edit_others_posts', // Only display the widget to users who have this capability.
		'show_link_count_bubble'           => true, // Display a notification bubble in the menu when broken links are found.
		'table_layout'                     => 'flexible', // The layout of the link table. Possible values : 'classic', 'flexible'.
		'table_compact'                    => true, // Compact table mode on/off.
		'table_visible_columns'            => array( 'new-url', 'status', 'used-in', 'new-link-text' ),
		'table_links_per_page'             => 30,
		'table_color_code_status'          => true, // Color-code link status text.
		'need_resynch'                     => false, // [Internal flag] True if there are unparsed items.
		'current_db_version'               => 0, // The currently set-up version of the plugin's tables.
		'timeout'                          => 30, // (in seconds) Links that take longer than this to respond will be treated as broken.
		'highlight_permanent_failures'     => false, // Highlight links that have appear to be permanently broken (in Tools -> Broken Links).
		'failure_duration_threshold'       => 3, // (days) Assume a link is permanently broken if it still hasn't recovered after this many days.
		'logging_enabled'                  => false,
		'log_file'                         => '',
		'incorrect_path'                   => false,
		'clear_log_on'                     => '',
		'custom_log_file_enabled'          => false,
		'installation_complete'            => false,
		'installation_flag_cleared_on'     => 0,
		'installation_flag_set_on'         => 0,
		'user_has_donated'                 => false, // Whether the user has donated to the plugin.
		'donation_flag_fixed'              => false,
		'show_link_actions'                => array( 'blc-deredirect-action' => false ), // Visible link actions.
		'youtube_api_key'                  => '',
		'blc_post_modified'                => '',
		'automatic_engine_on'              => true,
		'dashboard_widget_roles_converted' => false,
		'dashboard_widget_roles'           => array( 'administrator' ),
		'dashboard_widget_enabled'         => true,
	);

	/**
	 * Plugin option loaded from DB.
	 *
	 * @since  2.0.0
	 * @access public
	 * @var    bool
	 */
	private $db_option_loaded = false;

	/**
	 * Plugin option loaded from DB.
	 *
	 * @since  2.0.0
	 * @access protected
	 * @var    array
	 */
	private $loaded_values = array();

	/**
	 * Initiates the class.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		/**
		 * How often (at most) notifications will be sent.
		 * Possible values: 'daily', 'weekly'.
		 */
		$this->defaults['notification_schedule'] = apply_filters( 'blc_notification_schedule_filter', 'daily' );
		$this->options                           = $this->defaults;

		if ( ! empty( $this->db_store_key ) ) {
			$this->load_options();
		}
	}

	/**
	 * Load options.
	 *
	 * @since  2.0.0
	 * @access protected
	 * @return bool
	 */
	protected function load_options() {
		$this->db_option_loaded = false;

		$new_options = get_option( $this->db_store_key );

		// Decode JSON (if applicable).
		if ( is_string( $new_options ) && ! empty( $new_options ) ) {
			$new_options = json_decode( $new_options, true );
		}

		if ( ! is_array( $new_options ) ) {
			return false;
		} else {
			$this->loaded_values = $new_options;
			$this->options       = array_merge( $this->defaults, $this->loaded_values );
			if ( 0 === floatval( $this->options['server_load_limit'] ) ) {
				$this->options['user_enabled_load_limit'] = false;
			}
			$this->db_option_loaded = true;
			return true;
		}
	}

	/**
	 * Save options.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return bool
	 */
	public function save_options() {
		return update_option( $this->db_store_key, wp_json_encode( $this->options ) );
	}

	/**
	 * Retrieve a specific setting.
	 *
	 * @since  2.0.0
	 * @param string $key The option key.
	 * @param mixed  $default Default value.
	 * @return mixed
	 */
	public function get( $key, $default = null ) {
		if ( array_key_exists( $key, $this->options ) ) {
			return $this->options[ $key ];
		} else {
			return $default;
		}
	}

	/**
	 * Retrieve all options.
	 */
	public function get_all() {
		return $this->options;
	}

	/**
	 * Update or add a setting.
	 *
	 * @since  2.0.0
	 * @param string $key The option key.
	 * @param mixed  $value The setter option value.
	 * @return void
	 */
	public function set( $key, $value ) {
		$this->options[ $key ] = $value;
	}

}
