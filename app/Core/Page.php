<?php
/**
 * Page class used to set up and render pages in WP Admin.
 *
 * @package BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Core;

use BLC\Component\Component;
use BLC\Core\App;

/**
 * Page class used to set up and render pages in WP Admin.
 */
abstract class Page extends Component {

	/**
	 * Template file path relative to app/template and without .php file extension.
	 *
	 * @var string
	 */
	protected $template = '';

	/**
	 * Renders header.
	 */
	protected function render_header() {
		// Get total parsed links.
		$total = \blc_get_links(
			array(
				'count_only' => true,
			)
		);

		// // Get total parsed links.
		$broken = \blc_get_links(
			array(
				's_filter'   => 'broken',
				'count_only' => true,
			)
		);

		$automatic_engine_on = App::$options->get( 'automatic_engine_on', false );
		$last_checked        = App::$options->get( 'last_checked', '' );

		$vars       = array(
			'total'               => $total,
			'broken'              => $broken,
			'automatic_engine_on' => $automatic_engine_on,
			'last_checked'        => $last_checked,
		);
		$this->vars = array_merge( $this->vars, $vars );

		$this->render_part( 'header' );
	}

	/**
	 * Outputs rendered template to the page.
	 */
	public function render() {
		$this->render_part( $this->template );
	}

	/**
	 * Returns markup for a view file. Can be used for tests.
	 *
	 * @param string $path   The file to render.
	 * @param array  $params The params to send to view.
	 * @return string
	 */
	public function get_markup( $path, $params = array() ) {
		ob_start();
		$this->render( $path, $params );
		return ob_get_clean();
	}
}
