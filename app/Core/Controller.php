<?php
/**
 * Recieves request, processes the data and redirects to proper response.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
 * @link      https://premium.wpmudev.org/
 */

namespace BLC\Core;

/**
 * App class that contains references to instances to generally
 *
 * @since  2.0.0
 * @access public
 */
abstract class Controller {
	/**
	 * Whatever. We won't go so specific.
	 *
	 * @var string
	 */
	private $action;

	/**
	 * Specific wp-admin route that is handled by this Controller.
	 *
	 * @var string
	 */
	protected $route;

	/**
	 * Permission required to access this route.
	 *
	 * @var string
	 */
	protected $permission = 'manage_options';

	/**
	 * Page slug used to register page in WP Admin Menu.
	 *
	 * @var string
	 */
	protected $menu_slug = '';

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->url = admin_url(
			sprintf( 'admin.php?page=%s', $this->menu_slug )
		);
	}

	/**
	 * Returns menu slug.
	 *
	 * @return string Menu slug.
	 */
	public function get_menu_slug() {
		return $this->menu_slug;
	}

	/**
	 * Returns Menu title.
	 *
	 * @return string Menu title.
	 */
	abstract public function get_menu_title();

	/**
	 * Returns Page title.
	 *
	 * @return string Page title.
	 */
	abstract public function get_page_title();

	/**
	 * Returns required permission level to access this page.
	 * Defaults to 'manage_options', may be overridden by child classes.
	 *
	 * @return string Required permission.
	 */
	public function get_required_permission() {
		return $this->permission;
	}

	/**
	 * Handles given route. Processes data and sends proper response.
	 */
	abstract public function handle_request();
}
