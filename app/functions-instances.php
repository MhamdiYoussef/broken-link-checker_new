<?php
/**
 * Custom helper functions.
 *
 * Helper functions for the plugin.
 *
 * @package   BrokenLinkChecker
 * @author    WPMUDEV
 * @license   https://www.gnu.org/licenses/gpl-2.0.html
 * @link      https://premium.wpmudev.org/
 */

namespace BLC;

/**
 * Get all link instances associated with one or more links.
 *
 * @param array  $link_ids Array of link IDs.
 * @param string $purpose An optional code indicating how the instances will be used. Available predefined constants : BLC_FOR_DISPLAY, BLC_FOR_EDITING .
 * @param bool   $load_containers Preload containers regardless of purpose. Defaults to false.
 * @param bool   $load_wrapped_objects Preload wrapped objects regardless of purpose. Defaults to false.
 * @param bool   $include_invalid Include instances that refer to not-loaded containers or parsers. Defaults to false.
 * @return blcLinkInstance[] An array indexed by link ID. Each item of the array will be an array of blcLinkInstance objects.
 */
function blc_get_instances( $link_ids, $purpose = '', $load_containers = false, $load_wrapped_objects = false, $include_invalid = false ) {
	global $wpdb;

	if ( empty( $link_ids ) ) {
		return array();
	}

	$link_ids_in = implode( ', ', $link_ids );

	$q = "SELECT * FROM {$wpdb->prefix}blc_instances WHERE link_id IN ($link_ids_in)";

	// Skip instances that reference containers or parsers that aren't currently loaded.
	if ( ! $include_invalid ) {
		$manager           = blcModuleManager::getInstance();
		$active_containers = $manager->get_escaped_ids( 'container' );
		$active_parsers    = $manager->get_escaped_ids( 'parser' );

		$q .= " AND container_type IN ({$active_containers}) ";
		$q .= " AND parser_type IN ({$active_parsers}) ";
	}

	$results = $wpdb->get_results( $q, ARRAY_A ); //phpcs:ignore

	if ( empty( $results ) ) {
		return array();
	}

	// Also retrieve the containers, if it could be useful.
	$load_containers = $load_containers || in_array( $purpose, array( BLC_FOR_DISPLAY, BLC_FOR_EDITING ) );
	if ( $load_containers ) {
		// Collect a list of (container_type, container_id) pairs.
		$container_ids = array();

		foreach ( $results as $result ) {
			array_push(
				$container_ids,
				array( $result['container_type'], intval( $result['container_id'] ) )
			);
		}
		$containers = blcContainerHelper::get_containers( $container_ids, $purpose, '', $load_wrapped_objects );
	}

	// Create an object for each instance and group them by link ID.
	$instances = array();
	foreach ( $results as $result ) {
		$instance = new blcLinkInstance( $result );

		// Assign a container to the link instance, if available.
		if ( $load_containers && ! empty( $containers ) ) {
			$key = $instance->container_type . '|' . $instance->container_id;
			if ( isset( $containers[ $key ] ) ) {
				$instance->_container = $containers[ $key ];
			}
		}

		if ( isset( $instances[ $instance->link_id ] ) ) {
			array_push( $instances[ $instance->link_id ], $instance );
		} else {
			$instances[ $instance->link_id ] = array( $instance );
		}
	}

	return $instances;
}

/**
 * Get the number of instances that reference only currently loaded containers and parsers.
 *
 * @return int
 */
function blc_get_usable_instance_count() {
	global $wpdb;

	$q = "SELECT COUNT(instance_id) FROM {$wpdb->prefix}blc_instances WHERE 1";

	// Skip instances that reference containers or parsers that aren't currently loaded.
	$manager           = blcModuleManager::getInstance();
	$active_containers = $manager->get_escaped_ids( 'container' );
	$active_parsers    = $manager->get_escaped_ids( 'parser' );

	$q .= " AND container_type IN ({$active_containers}) ";
	$q .= " AND parser_type IN ({$active_parsers}) ";

	return $wpdb->get_var( $q ); //phpcs:ignore
}

/**
 * Remove instances that reference invalid containers or containers/parsers that are not currently loaded
 *
 * @return bool
 */
function blc_cleanup_instances() {
	global $wpdb;
	global $blclog;

	// Delete all instances that reference non-existent containers.
	$start   = microtime( true );
	$q       = "DELETE instances.*
		FROM
		{$wpdb->prefix}blc_instances AS instances LEFT JOIN {$wpdb->prefix}blc_synch AS synch
		ON instances.container_type = synch.container_type AND instances.container_id = synch.container_id
		WHERE
		synch.container_id IS NULL";
	$rez     = $wpdb->query( $q ); //phpcs:ignore
	$elapsed = microtime( true ) - $start;
	$blclog->log( sprintf( '... %d instances deleted in %.3f seconds', $wpdb->rows_affected, $elapsed ) );

	// Delete instances that reference containers and parsers that are no longer active.
	$start             = microtime( true );
	$manager           = blcModuleManager::getInstance();
	$active_containers = $manager->get_escaped_ids( 'container' );
	$active_parsers    = $manager->get_escaped_ids( 'parser' );

	$q       = "DELETE instances.*
		FROM {$wpdb->prefix}blc_instances AS instances
		WHERE
		instances.container_type NOT IN ({$active_containers}) OR
		instances.parser_type NOT IN ({$active_parsers})";
	$rez2    = $wpdb->query( $q ); //phpcs:ignore
	$elapsed = microtime( true ) - $start;
	$blclog->log( sprintf( '... %d more instances deleted in %.3f seconds', $wpdb->rows_affected, $elapsed ) );

	return ( false !== $rez ) && ( false !== $rez2 );
}