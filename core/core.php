<?php
/**
 * Simple function to replicate PHP 5 behaviour
 *
 * @link    https://wordpress.org/plugins/broken-link-checker/
 * @since   1.0.0
 * @package broken-link-checker
 */

use BLC\Core\App;

if ( ! function_exists( 'microtime_float' ) ) {
	/**
	 * Calcualate microtime_float
	 *
	 * @since 1.0.0
	 */
	function microtime_float() {
		list( $usec, $sec ) = explode( ' ', microtime() );
		return ( (float) $usec + (float) $sec );
	}
}

require BLC_DIRECTORY . '/includes/screen-options/screen-options.php';
require BLC_DIRECTORY . '/includes/screen-meta-links.php';
require BLC_DIRECTORY . '/includes/wp-mutex.php';
require BLC_DIRECTORY . '/includes/transactions-manager.php';

if ( ! class_exists( 'wsBrokenLinkChecker' ) ) {

	/**
	 * Broken Link Checker core
	 */
	class wsBrokenLinkChecker {

		/**
		 * Plugin configuration.
		 *
		 * @var object
		 */
		public $conf;

		/**
		 * Loader script path.
		 *
		 * @var string
		 */
		public $loader;

		/**
		 * DB version.
		 *
		 * @var string
		 */
		public $db_version; // The required version of the plugin's DB schema.

		/**
		 * Execution start time.
		 *
		 * @var string
		 */
		public $execution_start_time;

		/**
		 * Text domain status.
		 *
		 * @var string
		 */
		public $is_textdomain_loaded = false;

		/**
		 * Class constructor
		 *
		 * @param string                  $loader The fully
		 * qualified filename of the loader script that WP
		 * identifies as the "main" plugin file.
		 * @param blcConfigurationManager $conf An instance of the configuration manager.
		 */
		public function __construct( $loader, blcConfigurationManager $conf ) {
			$this->db_version = BLC_DATABASE_VERSION;

			$this->conf   = $conf;
			$this->loader = $loader;

			$this->load_language();

			// Unlike the activation hook, the deactivation callback *can* be registered in this file.

			// because deactivation happens after this class has already been instantiated (during the 'init' action).

			register_deactivation_hook( $loader, array( $this, 'deactivation' ) );

			add_action( 'admin_menu', array( $this, 'setup_admin_menu' ) );

			// Load jQuery on Dashboard pages (probably redundant as WP already does that).
			// add_action( 'admin_print_scripts', array( $this, 'admin_print_scripts' ) );.

			// The dashboard widget.
			add_action( 'wp_dashboard_setup', array( $this, 'hook_wp_dashboard_setup' ) );

			// AJAXy hooks.
			add_action( 'wp_ajax_blc_full_status', array( $this, 'ajax_full_status' ) );
			add_action( 'wp_ajax_blc_dashboard_status', array( $this, 'ajax_dashboard_status' ) );
			add_action( 'wp_ajax_blc_work', array( $this, 'ajax_work' ) );
			add_action( 'wp_ajax_blc_discard', array( $this, 'ajax_discard' ) );
			add_action( 'wp_ajax_blc_edit', array( $this, 'ajax_edit' ) );
			add_action( 'wp_ajax_blc_link_details', array( $this, 'ajax_link_details' ) );
			add_action( 'wp_ajax_blc_unlink', array( $this, 'ajax_unlink' ) );
			add_action( 'wp_ajax_blc_recheck', array( $this, 'ajax_recheck' ) );
			add_action( 'wp_ajax_blc_deredirect', array( $this, 'ajax_deredirect' ) );
			add_action( 'wp_ajax_blc_current_load', array( $this, 'ajax_current_load' ) );

			add_action( 'wp_ajax_blc_dismiss', array( $this, 'ajax_dismiss' ) );
			add_action( 'wp_ajax_blc_undismiss', array( $this, 'ajax_undismiss' ) );
			add_action( 'wp_ajax_blc_run_check_button', array( $this, 'ajax_run_check_button' ) );

			// Add/remove Cron events.
			$this->setup_cron_events();

			// Set hooks that listen for our Cron actions.
			add_action( 'blc_cron_email_notifications', array( $this, 'maybe_send_email_notifications' ) );
			add_action( 'blc_cron_check_links', array( $this, 'cron_check_links' ) );
			add_action( 'blc_cron_database_maintenance', array( $this, 'database_maintenance' ) );
			add_action( 'blc_corn_clear_log_file', array( $this, 'clear_log_file' ) );

			// Set the footer hook that will call the worker function via AJAX.
			add_action( 'admin_footer', array( $this, 'admin_footer' ) );

			// Add a "Screen Options" panel to the "Broken Links" page.
			add_screen_options_panel(
				'blc-screen-options',
				'',
				array( $this, 'screen_options_html' ),
				'tools_page_view-broken-links',
				array( $this, 'ajax_save_screen_options' ),
				true
			);

			// Display an explanatory note on the "Tools -> Broken Links -> Warnings" page.
			add_action( 'admin_notices', array( $this, 'show_warnings_section_notice' ) );

			// Restore post date updated with the update link.
			add_filter( 'wp_insert_post_data', array( $this, 'disable_post_date_update' ), 10, 2 );

		}


		/**
		 * Kickstarts the admin pages.
		 */
		public function setup_admin_menu() {
			require BLC_DIRECTORY . '/core/core/admin-pages.php';
			$admin_pages = new Admin_Pages( $this->conf, $this );
		}


		/**
		 * Output the script that runs the link monitor while the Dashboard is open.
		 *
		 * @return void
		 */
		public function admin_footer() {
			if ( ! $this->conf->options['run_in_dashboard'] ) {
				return;
			}
			$nonce = wp_create_nonce( 'blc_work' );

			?>
			<!-- wsblc admin footer -->
			<script type='text/javascript'>
			(function($){

				//(Re)starts the background worker thread
				function blcDoWork(){
					$.post(
						"<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>",
						{
							'action' : 'blc_work',
							'_ajax_nonce' : '<?php echo esc_js( $nonce ); ?>'
						}
					);
				}
				//Call it the first time
				blcDoWork();

				//Then call it periodically every X seconds
				setInterval(blcDoWork, <?php echo ( intval( $this->conf->options['max_execution_time'] ) + 1 ) * 1000; ?>);

			})(jQuery);
			</script>
			<!-- /wsblc admin footer -->
			<?php
		}


		/**
		 * Check if an URL matches the exclusion list.
		 *
		 * @param string $url The url to exclude.
		 * @return bool
		 */
		public function is_excluded( $url ) {
			if ( ! is_array( $this->conf->options['exclusion_list'] ) ) {
				return false;
			}
			foreach ( $this->conf->options['exclusion_list'] as $excluded_word ) {
				if ( stristr( $url, $excluded_word ) ) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Get dashboard_widget
		 */
		public function dashboard_widget() {
			?>
			<p id='wsblc_activity_box'><?php esc_html_e( 'Loading...', 'broken-link-checker' ); ?></p>
			<script type='text/javascript'>
				jQuery( function($){
					var blc_was_autoexpanded = false;

					function blcDashboardStatus(){
						$.getJSON(
							"<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>",
							{
								'action' : 'blc_dashboard_status',
								'random' : Math.random()
							},
							function (data){
								if ( data && ( typeof(data.text) != 'undefined' ) ) {
									$('#wsblc_activity_box').html(data.text);
									<?php if ( $this->conf->options['autoexpand_widget'] ) { ?>
									//Expand the widget if there are broken links.
									//Do this only once per pageload so as not to annoy the user.
									if ( !blc_was_autoexpanded && ( data.status.broken_links > 0 ) ){
										$('#blc_dashboard_widget.postbox').removeClass('closed');
										blc_was_autoexpanded = true;
									}
									<?php } ?>
								} else {
									$('#wsblc_activity_box').html('<?php esc_html_e( '[ Network error ]', 'broken-link-checker' ); ?>');
								}

								setTimeout( blcDashboardStatus, 120*1000 ); //...update every two minutes
							}
						);
					}

					blcDashboardStatus();//Call it the first time

				} );
			</script>
			<?php
		}

		/**
		 * Dashboard widget controls.
		 *
		 * @param int   $widget_id The widget ID.
		 * @param array $form_inputs The form inputs.
		 */
		public function dashboard_widget_control( $widget_id, $form_inputs = array() ) {
			if ( isset( $_POST['blc_update_widget_nonce'] ) ) :

				// Ignore sanitization field for nonce.
				$nonce = sanitize_text_field( wp_unslash( $_POST['blc_update_widget_nonce'] ) );

				if ( wp_verify_nonce( $nonce, 'blc_update_widget' ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'POST' === $_SERVER['REQUEST_METHOD'] && isset( $_POST['widget_id'] ) && 'blc_dashboard_widget' === $_POST['widget_id'] ) {

					// It appears $form_inputs isn't used in the current WP version, so lets just use $_POST.
					$this->conf->options['autoexpand_widget'] = ! empty( $_POST['blc-autoexpand'] );
					$this->conf->save_options();
				}
			endif;

			?>
			<p><label for="blc-autoexpand">
				<input id="blc-autoexpand" name="blc-autoexpand" type="checkbox" value="1"
				<?php
				if ( $this->conf->options['autoexpand_widget'] ) {
					echo 'checked="checked"';}
				?>
				/>
				<?php esc_html_e( 'Automatically expand the widget if broken links have been detected', 'broken-link-checker' ); ?>
			</label></p>
			<?php

			wp_nonce_field( 'blc_update_widget', 'blc_update_widget_nonce' );
		}


		/**
		 * A hook executed when the plugin is deactivated.
		 *
		 * @return void
		 */
		public function deactivation() {
			// Remove our Cron events.
			wp_clear_scheduled_hook( 'blc_cron_check_links' );
			wp_clear_scheduled_hook( 'blc_cron_email_notifications' );
			wp_clear_scheduled_hook( 'blc_cron_database_maintenance' );
			wp_clear_scheduled_hook( 'blc_corn_clear_log_file' );
			wp_clear_scheduled_hook( 'blc_cron_check_news' ); // Unused event.
			// Note the deactivation time for each module. This will help them
			// synch up propely if/when the plugin is reactivated.
			$moduleManager = blcModuleManager::getInstance();
			$the_time      = current_time( 'timestamp' );
			foreach ( $moduleManager->get_active_modules() as $module_id => $module ) {
				$this->conf->options['module_deactivated_when'][ $module_id ] = $the_time;
			}
			$this->conf->save_options();
		}

		/**
		 * Perform various database maintenance tasks on the plugin's tables.
		 *
		 * Removes records that reference disabled containers and parsers,
		 * deletes invalid instances and links, optimizes tables, etc.
		 *
		 * @return void
		 */
		public function database_maintenance() {
			blcContainerHelper::cleanup_containers();
			blc_cleanup_instances();
			blc_cleanup_links();

			blcUtility::optimize_database();
		}



		/**
		 * Escape all instances of the $delimiter character with a backslash (unless already escaped).
		 *
		 * @param string $pattern
		 * @param string $delimiter
		 * @return string
		 */
		private function escape_regex_delimiter( $pattern, $delimiter ) {
			if ( empty( $pattern ) ) {
				return '';
			}

			$output  = '';
			$length  = strlen( $pattern );
			$escaped = false;

			for ( $i = 0; $i < $length; $i++ ) {
				$char = $pattern[ $i ];

				if ( $escaped ) {
					$escaped = false;
				} else {
					if ( '\\' == $char ) {
						$escaped = true;
					} elseif ( $char == $delimiter ) {
						$char = '\\' . $char;
					}
				}

				$output .= $char;
			}

			return $output;
		}


		/**
		 * Show an admin notice that explains what the "Warnings" section under "Tools -> Broken Links" does.
		 * The user can hide the notice.
		 */
		public function show_warnings_section_notice() {
			$is_warnings_section = isset( $_GET['filter_id'] )
				&& ( 'warnings' === $_GET['filter_id'] )
				&& isset( $_GET['page'] )
				&& ( 'wdblc' === $_GET['page'] );

			if ( ! ( $is_warnings_section && current_user_can( 'edit_others_posts' ) ) ) {
				return;
			}

			// Let the user hide the notice.
			$conf        = blc_get_configuration();
			$notice_name = 'show_warnings_section_hint';

			if ( isset( $_GET[ $notice_name ] ) && is_numeric( $_GET[ $notice_name ] ) ) {
				$conf->set( $notice_name, (bool) $_GET[ $notice_name ] );
				$conf->save_options();
			}
			if ( ! $conf->get( $notice_name, true ) ) {
				return;
			}

			printf(
				'<div class="updated">
						<p>%1$s</p>
						<p>
							<a href="%2$s">%3$s</a> |
							<a href="%4$s">%5$s</a>
						<p>
					</div>',
				__(
					'The "Warnings" page lists problems that are probably temporary or suspected to be false positives.<br> Warnings that persist for a long time will usually be reclassified as broken links.',
					'broken-link-checker'
				),
				esc_attr( add_query_arg( $notice_name, '0' ) ),
				_x(
					'Hide notice',
					'admin notice under Tools - Broken links - Warnings',
					'broken-link-checker'
				),
				esc_attr( admin_url( 'options-general.php?page=link-checker-settings#blc_warning_settings' ) ),
				_x(
					'Change warning settings',
					'a link from the admin notice under Tools - Broken links - Warnings',
					'broken-link-checker'
				)
			);
		}

		/**
		 * Generate the HTML for the plugin's Screen Options panel.
		 *
		 * @return string
		 */
		function screen_options_html() {
			// Update the links-per-page setting when "Apply" is clicked
			if ( isset( $_POST['per_page'] ) && is_numeric( $_POST['per_page'] ) ) {
				check_admin_referer( 'screen-options-nonce', 'screenoptionnonce' );
				$per_page = intval( $_POST['per_page'] );
				if ( ( $per_page >= 1 ) && ( $per_page <= 500 ) ) {
					$this->conf->options['table_links_per_page'] = $per_page;
					$this->conf->save_options();
				}
			}

			// Let the user show/hide individual table columns
			$html = '<h5>' . __( 'Table columns', 'broken-link-checker' ) . '</h5>';

			include dirname( $this->loader ) . '/includes/admin/table-printer.php';
			$table             = new blcTablePrinter( $this );
			$available_columns = $table->get_layout_columns( $this->conf->options['table_layout'] );

			$html .= '<div id="blc-column-selector" class="metabox-prefs">';

			foreach ( $available_columns as $column_id => $data ) {
				$html .= sprintf(
					'<label><input type="checkbox" name="visible_columns[%s]"%s>%s</label>',
					esc_attr( $column_id ),
					in_array( $column_id, $this->conf->options['table_visible_columns'] ) ? ' checked="checked"' : '',
					$data['heading']
				);
			}

			$html .= '</div>';

			$html .= '<h5>' . __( 'Show on screen', 'broken-link-checker' ) . '</h5>';
			$html .= '<div class="screen-options">';
			$html .= sprintf(
				'<input type="text" name="per_page" maxlength="3" value="%d" class="screen-per-page" id="blc_links_per_page" />
				<label for="blc_links_per_page">%s</label>
				<input type="button" class="button" value="%s" id="blc-per-page-apply-button" /><br />',
				$this->conf->options['table_links_per_page'],
				__( 'links', 'broken-link-checker' ),
				__( 'Apply' )
			);
			$html .= '</div>';

			$html .= '<h5>' . __( 'Misc', 'broken-link-checker' ) . '</h5>';
			$html .= '<div class="screen-options">';
			/*
			Display a checkbox in "Screen Options" that lets the user highlight links that
			have been broken for at least X days.
			*/
			$html     .= sprintf(
				'<label><input type="checkbox" id="highlight_permanent_failures" name="highlight_permanent_failures"%s> ',
				$this->conf->options['highlight_permanent_failures'] ? ' checked="checked"' : ''
			);
			$input_box = sprintf(
				'</label><input type="text" name="failure_duration_threshold" id="failure_duration_threshold" value="%d" size="2"><label for="highlight_permanent_failures">',
				$this->conf->options['failure_duration_threshold']
			);
			$html     .= sprintf(
				__( 'Highlight links broken for at least %s days', 'broken-link-checker' ),
				$input_box
			);
			$html     .= '</label>';

			// Display a checkbox for turning colourful link status messages on/off
			$html .= sprintf(
				'<br/><label><input type="checkbox" id="table_color_code_status" name="table_color_code_status"%s> %s</label>',
				$this->conf->options['table_color_code_status'] ? ' checked="checked"' : '',
				__( 'Color-code status codes', 'broken-link-checker' )
			);

			$html .= '</div>';

			return $html;
		}

		/**
		 * AJAX callback for saving the "Screen Options" panel settings
		 *
		 * @param array $form
		 * @return void
		 */
		function ajax_save_screen_options( $form ) {
			if ( ! current_user_can( 'edit_others_posts' ) ) {
				die(
					json_encode(
						array(
							'error' => __( "You're not allowed to do that!", 'broken-link-checker' ),
						)
					)
				);
			}

			$this->conf->options['highlight_permanent_failures'] = ! empty( $form['highlight_permanent_failures'] );
			$this->conf->options['table_color_code_status']      = ! empty( $form['table_color_code_status'] );

			$failure_duration_threshold = intval( $form['failure_duration_threshold'] );
			if ( $failure_duration_threshold >= 1 ) {
				$this->conf->options['failure_duration_threshold'] = $failure_duration_threshold;
			}

			if ( isset( $form['visible_columns'] ) && is_array( $form['visible_columns'] ) ) {
				$this->conf->options['table_visible_columns'] = array_keys( $form['visible_columns'] );
			}

			$this->conf->save_options();
			die( '1' );
		}

		function start_timer() {
			$this->execution_start_time = microtime_float();
		}

		function execution_time() {
			return microtime_float() - $this->execution_start_time;
		}

		/**
		 * The main worker function that does all kinds of things.
		 *
		 * @return void
		 */
		function work() {
			global $blclog;

			// Close the session to prevent lock-ups.
			// PHP sessions are blocking. session_start() will wait until all other scripts that are using the same session
			// are finished. As a result, a long-running script that unintentionally keeps the session open can cause
			// the entire site to "lock up" for the current user/browser. WordPress itself doesn't use sessions, but some
			// plugins do, so we should explicitly close the session (if any) before starting the worker.
			if ( session_id() != '' ) {
				session_write_close();
			}

			if ( ! $this->acquire_lock() ) {
				// FB::warn("Another instance of BLC is already working. Stop.");
				$blclog->info( 'Another instance of BLC is already working. Stop.' );
				return;
			}

			if ( $this->server_too_busy() ) {
				// FB::warn("Server is too busy. Stop.");
				$blclog->warn( 'Server load is too high, stopping.' );
				return;
			}

			$this->start_timer();
			$blclog->info( 'work() starts' );

			$max_execution_time = $this->conf->options['max_execution_time'];

			/*****************************************
							Preparation
			*/
			// Check for safe mode
			if ( blcUtility::is_safe_mode() ) {
				// Do it the safe mode way - obey the existing max_execution_time setting
				$t = ini_get( 'max_execution_time' );
				if ( $t && ( $t < $max_execution_time ) ) {
					$max_execution_time = $t - 1;
				}
			} else {
				// Do it the regular way
				@set_time_limit( $max_execution_time * 2 ); // x2 should be plenty, running any longer would mean a glitch.
			}

			// Don't stop the script when the connection is closed
			ignore_user_abort( true );

			// Close the connection as per http://www.php.net/manual/en/features.connection-handling.php#71172
			// This reduces resource usage.
			// (Disable when debugging or you won't get the FirePHP output)
			if (
				! headers_sent()
				&& ( defined( 'DOING_AJAX' ) && constant( 'DOING_AJAX' ) )
				&& ( ! defined( 'BLC_DEBUG' ) || ! constant( 'BLC_DEBUG' ) )
			) {
				@ob_end_clean(); // Discard the existing buffer, if any
				header( 'Connection: close' );
				ob_start();
				echo ( 'Connection closed' ); // This could be anything
				$size = ob_get_length();
				header( "Content-Length: $size" );
				ob_end_flush(); // Strange behaviour, will not work
				flush();        // Unless both are called !
			}

			// Load modules for this context
			$moduleManager = blcModuleManager::getInstance();
			$moduleManager->load_modules( 'work' );

			$target_usage_fraction = $this->conf->get( 'target_resource_usage', 0.25 );
			// Target usage must be between 1% and 100%.
			$target_usage_fraction = max( min( $target_usage_fraction, 1 ), 0.01 );

			/*****************************************
					Parse posts and bookmarks
			*/

			$orphans_possible   = false;
			$still_need_resynch = $this->conf->options['need_resynch'];

			if ( $still_need_resynch ) {

				// FB::log("Looking for containers that need parsing...");
				$max_containers_per_query = 50;

				$start               = microtime( true );
				$containers          = blcContainerHelper::get_unsynched_containers( $max_containers_per_query );
				$get_containers_time = microtime( true ) - $start;

				while ( ! empty( $containers ) ) {
					// FB::log($containers, 'Found containers');
					$this->sleep_to_maintain_ratio( $get_containers_time, $target_usage_fraction );

					foreach ( $containers as $container ) {
						$synch_start_time = microtime( true );

						// FB::log($container, "Parsing container");
						$container->synch();

						$synch_elapsed_time = microtime( true ) - $synch_start_time;
						$blclog->info(
							sprintf(
								'Parsed container %s[%s] in %.2f ms',
								$container->container_type,
								$container->container_id,
								$synch_elapsed_time * 1000
							)
						);

						// Check if we still have some execution time left
						if ( $this->execution_time() > $max_execution_time ) {
							// FB::log('The allotted execution time has run out');
							blc_cleanup_links();
							$this->release_lock();
							return;
						}

						// Check if the server isn't overloaded
						if ( $this->server_too_busy() ) {
							// FB::log('Server overloaded, bailing out.');
							blc_cleanup_links();
							$this->release_lock();
							return;
						}

						// Intentionally slow down parsing to reduce the load on the server. Basically,
						// we work $target_usage_fraction of the time and sleep the rest of the time.
						$this->sleep_to_maintain_ratio( $synch_elapsed_time, $target_usage_fraction );
					}
					$orphans_possible = true;

					$start               = microtime( true );
					$containers          = blcContainerHelper::get_unsynched_containers( $max_containers_per_query );
					$get_containers_time = microtime( true ) - $start;
				}

				// FB::log('No unparsed items found.');
				$still_need_resynch = false;

			} else {
				// FB::log('Resynch not required.');
			}

			/******************************************
						Resynch done?
			*/
			if ( $this->conf->options['need_resynch'] && ! $still_need_resynch ) {
				$this->conf->options['need_resynch'] = $still_need_resynch;
				$this->conf->save_options();
			}

			/******************************************
						Remove orphaned links
			*/

			if ( $orphans_possible ) {
				$start = microtime( true );

				$blclog->info( 'Removing orphaned links.' );
				blc_cleanup_links();

				$get_links_time = microtime( true ) - $start;
				$this->sleep_to_maintain_ratio( $get_links_time, $target_usage_fraction );
			}

			// Check if we still have some execution time left
			if ( $this->execution_time() > $max_execution_time ) {
				// FB::log('The allotted execution time has run out');
				$blclog->info( 'The allotted execution time has run out.' );
				$this->release_lock();
				return;
			}

			if ( $this->server_too_busy() ) {
				// FB::log('Server overloaded, bailing out.');
				$blclog->info( 'Server load too high, stopping.' );
				$this->release_lock();
				return;
			}

			/*****************************************
							Check links
			*/
			$max_links_per_query = 30;

			$start          = microtime( true );
			$links          = $this->get_links_to_check( $max_links_per_query );
			$get_links_time = microtime( true ) - $start;

			while ( $links ) {
				$this->sleep_to_maintain_ratio( $get_links_time, $target_usage_fraction );

				// Some unchecked links found
				// FB::log("Checking ".count($links)." link(s)");
				$blclog->info( 'Checking ' . count( $links ) . ' link(s)' );

				// Randomizing the array reduces the chances that we'll get several links to the same domain in a row.
				shuffle( $links );

				$transactionManager = TransactionManager::getInstance();
				$transactionManager->start();

				foreach ( $links as $link ) {
					// Does this link need to be checked? Excluded links aren't checked, but their URLs are still
					// tested periodically to see if they're still on the exclusion list.
					if ( ! $this->is_excluded( $link->url ) ) {
						// Check the link.
						// FB::log($link->url, "Checking link {$link->link_id}");
						$link->check( true );
					} else {
						// FB::info("The URL {$link->url} is excluded, skipping link {$link->link_id}.");
						$link->last_check_attempt = time();
						$link->save();
					}

					// Check if we still have some execution time left
					if ( $this->execution_time() > $max_execution_time ) {
						$transactionManager->commit();
						// FB::log('The allotted execution time has run out');
						$blclog->info( 'The allotted execution time has run out.' );
						$this->release_lock();
						return;
					}

					// Check if the server isn't overloaded
					if ( $this->server_too_busy() ) {
						$transactionManager->commit();
						// FB::log('Server overloaded, bailing out.');
						$blclog->info( 'Server load too high, stopping.' );
						$this->release_lock();
						return;
					}
				}
				$transactionManager->commit();

				$start          = microtime( true );
				$links          = $this->get_links_to_check( $max_links_per_query );
				$get_links_time = microtime( true ) - $start;
			}
			// FB::log('No links need to be checked right now.');

			$this->conf->options['last_checked'] = time();
			$this->conf->save_options();

			$this->release_lock();
			$blclog->info( 'work(): All done.' );
			// FB::log('All done.');
		}

		/**
		 * Sleep long enough to maintain the required $ratio between $elapsed_time and total runtime.
		 *
		 * For example, if $ratio is 0.25 and $elapsed_time is 1 second, this method will sleep for 3 seconds.
		 * Total runtime = 1 + 3 = 4, ratio = 1 / 4 = 0.25.
		 *
		 * @param float $elapsed_time
		 * @param float $ratio
		 */
		private function sleep_to_maintain_ratio( $elapsed_time, $ratio ) {
			if ( ( $ratio <= 0 ) || ( $ratio > 1 ) ) {
				return;
			}
			$sleep_time = $elapsed_time * ( ( 1 / $ratio ) - 1 );
			if ( $sleep_time > 0.0001 ) {
				/*
				global $blclog;
				$blclog->debug(sprintf(
					'Task took %.2f ms, sleeping for %.2f ms',
					$elapsed_time * 1000,
					$sleep_time * 1000
				));*/
				usleep( $sleep_time * 1000000 );
			}
		}

		/**
		 * This function is called when the plugin's cron hook executes.
		 * Its only purpose is to invoke the worker function.
		 *
		 * @uses wsBrokenLinkChecker::work()
		 *
		 * @return void
		 */
		function cron_check_links() {
			$this->work();
		}

		/**
		 * Retrieve links that need to be checked or re-checked.
		 *
		 * @param integer $max_results The maximum number of links to return. Defaults to 0 = no limit.
		 * @param bool    $count_only If true, only the number of found links will be returned, not the links themselves.
		 * @return int|blcLink[]
		 */
		function get_links_to_check( $max_results = 0, $count_only = false ) {
			global $wpdb; /* @var wpdb $wpdb */

			$check_threshold   = date( 'Y-m-d H:i:s', strtotime( '-' . $this->conf->options['check_threshold'] . ' hours' ) );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
			$recheck_threshold = date( 'Y-m-d H:i:s', time() - $this->conf->options['recheck_threshold'] );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date

			// FB::log('Looking for links to check (threshold : '.$check_threshold.', recheck_threshold : '.$recheck_threshold.')...');

			// Select some links that haven't been checked for a long time or
			// that are broken and need to be re-checked again. Links that are
			// marked as "being checked" and have been that way for several minutes
			// can also be considered broken/buggy, so those will be selected
			// as well.

			// Only check links that have at least one valid instance (i.e. an instance exists and
			// it corresponds to one of the currently loaded container/parser types).
			$manager           = blcModuleManager::getInstance();
			$loaded_containers = $manager->get_escaped_ids( 'container' );
			$loaded_parsers    = $manager->get_escaped_ids( 'parser' );

			// Note : This is a slow query, but AFAIK there is no way to speed it up.
			// I could put an index on last_check_attempt, but that value is almost
			// certainly unique for each row so it wouldn't be much better than a full table scan.
			if ( $count_only ) {
				$q = "SELECT COUNT(DISTINCT links.link_id)\n";
			} else {
				$q = "SELECT DISTINCT links.*\n";
			}
			$q .= "FROM {$wpdb->prefix}blc_links AS links
				INNER JOIN {$wpdb->prefix}blc_instances AS instances USING(link_id)
				WHERE
					(
						( last_check_attempt < %s )
						OR
						(
							(broken = 1 OR being_checked = 1)
							AND may_recheck = 1
							AND check_count < %d
							AND last_check_attempt < %s
						)
					)

				AND
					( instances.container_type IN ({$loaded_containers}) )
					AND ( instances.parser_type IN ({$loaded_parsers}) )
				";

			if ( ! $count_only ) {
				$q .= "\nORDER BY last_check_attempt ASC\n";
				if ( ! empty( $max_results ) ) {
					$q .= 'LIMIT ' . intval( $max_results );
				}
			}

			$link_q = $wpdb->prepare(
				$q, //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
				$check_threshold,
				$this->conf->options['recheck_count'],
				$recheck_threshold
			);

			// FB::log($link_q, "Find links to check");
			// $blclog->debug("Find links to check: \n" . $link_q);

			// If we just need the number of links, retrieve it and return
			if ( $count_only ) {
				return $wpdb->get_var( $link_q );//phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
			}

			// Fetch the link data
			$link_data = $wpdb->get_results( $link_q, ARRAY_A ); //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
			if ( empty( $link_data ) ) {
				return array();
			}

			// Instantiate blcLink objects for all fetched links
			$links = array();
			foreach ( $link_data as $data ) {
				$links[] = new blcLink( $data );
			}

			return $links;
		}

		/**
		 * Output the current link checker status in JSON format.
		 * Ajax hook for the 'blc_full_status' action.
		 *
		 * @return void
		 */
		function ajax_full_status() {
			$status = $this->get_status();
			$text   = $this->status_text( $status );

			echo json_encode(
				array(
					'text'   => $text,
					'status' => $status,
				)
			);

			die();
		}

		/**
		 * Generates a status message based on the status info in $status
		 *
		 * @param array $status
		 * @return string
		 */
		function status_text( $status ) {
			$text = '';

			if ( $status['broken_links'] > 0 ) {
				$text .= sprintf(
					"<a href='%s' title='" . __( 'View broken links', 'broken-link-checker' ) . "'><strong>" .
						_n( 'Found %d broken link', 'Found %d broken links', $status['broken_links'], 'broken-link-checker' ) .
					'</strong></a>',
					esc_attr( admin_url( 'admin.php?page=wdblc' ) ),
					$status['broken_links']
				);
			} else {
				$text .= __( 'No broken links found.', 'broken-link-checker' );
			}

			$text .= '<br/>';

			if ( $status['unchecked_links'] > 0 ) {
				$text .= sprintf(
					_n( '%d URL in the work queue', '%d URLs in the work queue', $status['unchecked_links'], 'broken-link-checker' ),
					$status['unchecked_links']
				);
			} else {
				$text .= __( 'No URLs in the work queue.', 'broken-link-checker' );
			}

			$text .= '<br/>';
			if ( $status['known_links'] > 0 ) {
				$url_count  = sprintf(
					_nx( '%d unique URL', '%d unique URLs', $status['known_links'], 'for the "Detected X unique URLs in Y links" message', 'broken-link-checker' ),
					$status['known_links']
				);
				$link_count = sprintf(
					_nx( '%d link', '%d links', $status['known_instances'], 'for the "Detected X unique URLs in Y links" message', 'broken-link-checker' ),
					$status['known_instances']
				);

				if ( $this->conf->options['need_resynch'] ) {
					$text .= sprintf(
						__( 'Detected %1$s in %2$s and still searching...', 'broken-link-checker' ),
						$url_count,
						$link_count
					);
				} else {
					$text .= sprintf(
						__( 'Detected %1$s in %2$s.', 'broken-link-checker' ),
						$url_count,
						$link_count
					);
				}
			} else {
				if ( $this->conf->options['need_resynch'] ) {
					$text .= __( 'Searching your blog for links...', 'broken-link-checker' );
				} else {
					$text .= __( 'No links detected.', 'broken-link-checker' );
				}
			}

			return $text;
		}

		/**
		 * @uses wsBrokenLinkChecker::ajax_full_status()
		 *
		 * @return void
		 */
		function ajax_dashboard_status() {
			// Just display the full status.
			$this->ajax_full_status();
		}

		/**
		 * Output the current average server load (over the last one-minute period).
		 * Called via AJAX.
		 *
		 * @return void
		 */
		function ajax_current_load() {
			$load = blcUtility::get_server_load();
			if ( empty( $load ) ) {
				die( _x( 'Unknown', 'current load', 'broken-link-checker' ) );
			}

			$one_minute = reset( $load );
			printf( '%.2f', $one_minute );
			die();
		}

		/**
		 * Returns an array with various status information about the plugin. Array key reference:
		 *  check_threshold     - date/time; links checked before this threshold should be checked again.
		 *   recheck_threshold   - date/time; broken links checked before this threshold should be re-checked.
		 *   known_links         - the number of detected unique URLs (a misleading name, yes).
		 *   known_instances     - the number of detected link instances, i.e. actual link elements in posts and other places.
		 *   broken_links        - the number of detected broken links.
		 *   unchecked_links     - the number of URLs that need to be checked ASAP; based on check_threshold and recheck_threshold.
		 *
		 * @return array
		 */
		function get_status() {
			$blc_link_query = blcLinkQuery::getInstance();

			$check_threshold   = date( 'Y-m-d H:i:s', strtotime( '-' . $this->conf->options['check_threshold'] . ' hours' ) );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
			$recheck_threshold = date( 'Y-m-d H:i:s', time() - $this->conf->options['recheck_threshold'] );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date

			$known_links     = blc_get_links( array( 'count_only' => true ) );
			$known_instances = blc_get_usable_instance_count();

			$broken_links = $blc_link_query->get_filter_links( 'broken', array( 'count_only' => true ) );

			$unchecked_links = $this->get_links_to_check( 0, true );

			return array(
				'check_threshold'   => $check_threshold,
				'recheck_threshold' => $recheck_threshold,
				'known_links'       => $known_links,
				'known_instances'   => $known_instances,
				'broken_links'      => $broken_links,
				'unchecked_links'   => $unchecked_links,
			);
		}

		function ajax_work() {
			check_ajax_referer( 'blc_work' );

			// Run the worker function
			$this->work();
			die();
		}

		/**
		 * AJAX hook for the "Not broken" button. Marks a link as broken and as a likely false positive.
		 *
		 * @return void
		 */
		public function ajax_discard() {
			if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'wdblc_links', false, false ) ) {
				wp_send_json_error( array( 'error' => 'not-allowed' ) );
			}

			if ( isset( $_POST['link_id'] ) ) {
				// Load the link.
				$link = new blcLink( intval( $_POST['link_id'] ) );

				if ( ! $link->valid() ) {
					wp_send_json_error( array( 'error' => 'link-not-found' ) );
				}
				// Make it appear "not broken".
				$link->broken             = false;
				$link->warning            = false;
				$link->false_positive     = true;
				$link->last_check_attempt = time();
				$link->log                = __( 'This link was manually marked as working by the user.', 'broken-link-checker' );

				$link->isOptionLinkChanged = true;

				$transaction_manager = TransactionManager::getInstance();
				$transaction_manager->start();

				// Save the changes.
				if ( $link->save() ) {
					$transaction_manager->commit();
					die( 'OK' );
				} else {
					wp_send_json_error( array( 'error' => 'cant-modify-link' ) );
				}
			} else {
				wp_send_json_error( array( 'error' => 'missing-link-data' ) );
			}
		}

		public function ajax_dismiss() {
			$this->ajax_set_link_dismissed( true );
		}

		public function ajax_undismiss() {
			$this->ajax_set_link_dismissed( false );
		}

		private function ajax_set_link_dismissed( $dismiss ) {
			if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'wdblc_links', false, false ) ) {
				wp_send_json_error( array( 'error' => 'not-allowed' ) );
			}

			if ( isset( $_POST['link_id'] ) ) {
				// Load the link
				$link = new blcLink( intval( $_POST['link_id'] ) );

				if ( ! $link->valid() ) {
					wp_send_json_error( array( 'error' => 'link-not-found' ) );
				}

				$link->dismissed = $dismiss;

				// Save the changes
				$link->isOptionLinkChanged = true;
				$transactionManager        = TransactionManager::getInstance();
				$transactionManager->start();
				if ( $link->save() ) {
					$transactionManager->commit();
					die( 'OK' );
				} else {
					wp_send_json_error( array( 'error' => 'cant-modify-link' ) );
				}
			} else {
				wp_send_json_error( array( 'error' => 'missing-link-data' ) );
			}
		}

		/**
		 * AJAX hook for the inline link editor on Tools -> Broken Links.
		 *
		 * @return void
		 */
		function ajax_edit() {
			if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'wdblc_links', false, false ) ) {
				wp_send_json_error( array( 'error' => 'not-allowed' ) );
			}

			if ( empty( $_POST['link_id'] ) || empty( $_POST['new_url'] ) || ! is_numeric( $_POST['link_id'] ) ) {
				wp_send_json_error( array( 'error' => 'missing-link-data' ) );
			}

			// Load the link
			$link = new blcLink( intval( $_POST['link_id'] ) );

			if ( ! $link->valid() ) {
				wp_send_json_error( array( 'error' => 'link-not-found' ) );
			}

			// Validate the new URL.
			$new_url = stripslashes( $_POST['new_url'] );
			$parsed  = @parse_url( $new_url );
			if ( ! $parsed ) {
				wp_send_json_error( array( 'error' => 'url-not-valid' ) );
			}

			if ( ! current_user_can( 'unfiltered_html' ) ) {
				// Disallow potentially dangerous URLs like "javascript:...".
				$protocols         = wp_allowed_protocols();
				$good_protocol_url = wp_kses_bad_protocol( $new_url, $protocols );
				if ( $new_url != $good_protocol_url ) {
					wp_send_json_error( array( 'error' => 'url-not-valid' ) );
				}
			}

			$new_text = ( isset( $_POST['new_text'] ) && is_string( $_POST['new_text'] ) ) ? stripslashes( $_POST['new_text'] ) : null;
			if ( '' === $new_text ) {
				$new_text = null;
			}
			if ( ! empty( $new_text ) && ! current_user_can( 'unfiltered_html' ) ) {
				$new_text = stripslashes( wp_filter_post_kses( addslashes( $new_text ) ) ); // wp_filter_post_kses expects slashed data.
			}

			$rez = $link->edit( $new_url, $new_text );
			if ( false === $rez ) {
				wp_send_json_error( array( 'error' => 'unexpected-error' ) );
			} else {
				$new_link     = $rez['new_link']; /** @var blcLink $new_link */
				$new_status   = $new_link->analyse_status();
				$ui_link_text = null;
				if ( isset( $new_text ) ) {
					$instances = $new_link->get_instances();
					if ( ! empty( $instances ) ) {
						$first_instance = reset( $instances );
						$ui_link_text   = $first_instance->ui_get_link_text();
					}
				}

				$response = array(
					'new_link_id'    => $rez['new_link_id'],
					'cnt_okay'       => $rez['cnt_okay'],
					'cnt_error'      => $rez['cnt_error'],

					'status_text'    => $new_status['text'],
					'status_code'    => $new_status['code'],
					'http_code'      => empty( $new_link->http_code ) ? '' : $new_link->http_code,
					'redirect_count' => $new_link->redirect_count,

					'url'            => $new_link->url,
					'escaped_url'    => esc_url_raw( $new_link->url ),
					'final_url'      => $new_link->final_url,
					'link_text'      => isset( $new_text ) ? $new_text : null,
					'ui_link_text'   => isset( $new_text ) ? $ui_link_text : null,

					'errors'         => array(),
				);
				// url, status text, status code, link text, editable link text

				foreach ( $rez['errors'] as $error ) { /** @var $error WP_Error */
					array_push( $response['errors'], implode( ', ', $error->get_error_messages() ) );
				}
				die( json_encode( $response ) );
			}
		}

		/**
		 * AJAX hook for the "Unlink" action links in Tools -> Broken Links.
		 * Removes the specified link from all posts and other supported items.
		 *
		 * @return void
		 */
		function ajax_unlink() {
			if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'wdblc_links', false, false ) ) {
				wp_send_json_error( array( 'error' => 'not-allowed' ) );
			}

			if ( isset( $_POST['link_id'] ) ) {
				// Load the link
				$link = new blcLink( intval( $_POST['link_id'] ) );

				if ( ! $link->valid() ) {
					wp_send_json_error( array( 'error' => 'link-not-found' ) );
				}

				// Try and unlink it
				$rez = $link->unlink();

				if ( false === $rez ) {
					wp_send_json_error( array( 'error' => 'unexpected-error' ) );
				} else {
					$response = array(
						'cnt_okay'  => $rez['cnt_okay'],
						'cnt_error' => $rez['cnt_error'],
						'errors'    => array(),
					);
					foreach ( $rez['errors'] as $error ) { /** @var WP_Error $error */
						array_push( $response['errors'], implode( ', ', $error->get_error_messages() ) );
					}

					die( json_encode( $response ) );
				}
			} else {
				wp_send_json_error( array( 'error' => 'missing-link-data' ) );
			}
		}

		public function ajax_deredirect() {
			if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'blc_deredirect', false, false ) ) {
				die(
					json_encode(
						array(
							'error' => __( "You're not allowed to do that!", 'broken-link-checker' ),
						)
					)
				);
			}

			if ( ! isset( $_POST['link_id'] ) || ! is_numeric( $_POST['link_id'] ) ) {
				die(
					json_encode(
						array(
							'error' => __( 'Error : link_id not specified', 'broken-link-checker' ),
						)
					)
				);
			}

			$id   = intval( $_POST['link_id'] );
			$link = new blcLink( $id );

			if ( ! $link->valid() ) {
				die(
					json_encode(
						array(
							'error' => sprintf( __( "Oops, I can't find the link %d", 'broken-link-checker' ), $id ),
						)
					)
				);
			}

			// The actual task is simple; it's error handling that's complicated.
			$result = $link->deredirect();
			if ( is_wp_error( $result ) ) {
				die(
					json_encode(
						array(
							'error' => sprintf( '%s [%s]', $result->get_error_message(), $result->get_error_code() ),
						)
					)
				);
			}

			$link = $result['new_link']; /** @var blcLink $link */

			$status   = $link->analyse_status();
			$response = array(
				'url'            => $link->url,
				'escaped_url'    => esc_url_raw( $link->url ),
				'new_link_id'    => $result['new_link_id'],

				'status_text'    => $status['text'],
				'status_code'    => $status['code'],
				'http_code'      => empty( $link->http_code ) ? '' : $link->http_code,
				'redirect_count' => $link->redirect_count,
				'final_url'      => $link->final_url,

				'cnt_okay'       => $result['cnt_okay'],
				'cnt_error'      => $result['cnt_error'],
				'errors'         => array(),
			);

			// Convert WP_Error's to simple strings.
			if ( ! empty( $result['errors'] ) ) {
				foreach ( $result['errors'] as $error ) { /** @var WP_Error $error */
					$response['errors'][] = $error->get_error_message();
				}
			}

			die( json_encode( $response ) );
		}

		/**
		 * AJAX hook for the "Recheck" action.
		 */
		public function ajax_recheck() {
			if ( ! current_user_can( 'edit_others_posts' ) || ! check_ajax_referer( 'wdblc_links', false, false ) ) {
				wp_send_json_error( array( 'error' => 'not-allowed' ) );
			}

			if ( ! isset( $_POST['link_id'] ) || ! is_numeric( $_POST['link_id'] ) ) {
				wp_send_json_error( array( 'error' => 'missing-link-data' ) );
			}

			$id   = intval( $_POST['link_id'] );
			$link = new blcLink( $id );

			if ( ! $link->valid() ) {
				wp_send_json_error( array( 'error' => 'link-not-found' ) );
			}

			$transaction_manager = TransactionManager::getInstance();
			$transaction_manager->start();

			// In case the immediate check fails, this will ensure the link is checked during the next work() run.
			$link->last_check_attempt  = 0;
			$link->isOptionLinkChanged = true;
			$link->save();

			// Check the link and save the results.
			$link->check( true );

			$transaction_manager->commit();

			$status   = $link->analyse_status();
			$response = array(
				'status_text'    => $status['text'],
				'status_code'    => $status['code'],
				'http_code'      => empty( $link->http_code ) ? '' : $link->http_code,
				'redirect_count' => $link->redirect_count,
				'final_url'      => $link->final_url,
			);

			die( wp_json_encode( $response ) );
		}

		function ajax_link_details() {
			global $wpdb; /* @var wpdb $wpdb */

			if ( ! current_user_can( 'edit_others_posts' ) ) {
				die( __( "You don't have sufficient privileges to access this information!", 'broken-link-checker' ) );
			}

			// FB::log("Loading link details via AJAX");

			if ( isset( $_GET['link_id'] ) ) {
				// FB::info("Link ID found in GET");
				$link_id = intval( $_GET['link_id'] );
			} elseif ( isset( $_POST['link_id'] ) ) {
				// FB::info("Link ID found in POST");
				$link_id = intval( $_POST['link_id'] );
			} else {
				// FB::error('Link ID not specified, you hacking bastard.');
				die( __( 'Error : link ID not specified', 'broken-link-checker' ) );
			}

			// Load the link.
			$link = new blcLink( $link_id );

			if ( ! $link->is_new ) {
				// FB::info($link, 'Link loaded');
				if ( ! class_exists( 'blcTablePrinter' ) ) {
					require dirname( $this->loader ) . '/includes/admin/table-printer.php';
				}
				blcTablePrinter::details_row_contents( $link );
				die();
			} else {
				printf( __( 'Failed to load link details (%s)', 'broken-link-checker' ), $wpdb->last_error );
				die();
			}
		}


		/**
		 * Function for Run Check Button
		 */
		public function ajax_run_check_button(){

			if ( ! current_user_can( 'manage_options' ) || ! check_ajax_referer( 'nonce_run_check_button', false, false ) ) {
				wp_send_json_error( array( 'error' => 'not-allowed' ) );
			}

			$broken = \blc_get_links(
				array(
					's_filter'   => 'broken',
					'count_only' => true,
				)
			);

			$total = \blc_get_links(
				array(
					'count_only' => true,
				)
			);

			$last_checked = App::$options->get( 'last_checked', '' );

			/* translators: 1. last check date, 2. last check time */
			$last_checked_string = sprintf( __( '%1$s at %2$s', 'broken-link-checker' ), wp_date( 'F j, Y ', $last_checked ), wp_date( ' H:i', $last_checked ) );

			$return = array(
				'broken'       => $broken,
				'total'        => $total,
				'last_checked' => $last_checked_string,
			);

			wp_send_json_success( $return );
		}


		/**
		 * Acquire an exclusive lock.
		 * If we already hold a lock, it will be released and a new one will be acquired.
		 *
		 * @return bool
		 */
		function acquire_lock() {
			return WPMutex::acquire( 'blc_lock' );
		}

		/**
		 * Relese our exclusive lock.
		 * Does nothing if the lock has already been released.
		 *
		 * @return bool
		 */
		function release_lock() {
			return WPMutex::release( 'blc_lock' );
		}

		/**
		 * Check if server is currently too overloaded to run the link checker.
		 *
		 * @return bool
		 */
		function server_too_busy() {
			if ( ! $this->conf->options['enable_load_limit'] || ! isset( $this->conf->options['server_load_limit'] ) ) {
				return false;
			}

			$loads = blcUtility::get_server_load();
			if ( empty( $loads ) ) {
				return false;
			}
			$one_minute = floatval( reset( $loads ) );

			return $one_minute > $this->conf->options['server_load_limit'];
		}

		/**
		 * Register BLC's Dashboard widget
		 *
		 * @return void
		 */
		function hook_wp_dashboard_setup() {
			$show_widget = current_user_can( $this->conf->get( 'dashboard_widget_capability', 'edit_others_posts' ) );
			if ( function_exists( 'wp_add_dashboard_widget' ) && $show_widget ) {
				wp_add_dashboard_widget(
					'blc_dashboard_widget',
					__( 'Broken Link Checker', 'broken-link-checker' ),
					array( $this, 'dashboard_widget' ),
					array( $this, 'dashboard_widget_control' )
				);
			}
		}


		function maybe_send_email_notifications() {
			global $wpdb; /** @var wpdb $wpdb */

			// email notificaiton.
			$send_notification = apply_filters( 'blc_allow_send_email_notification', $this->conf->options['send_email_notifications'] );

			$send_authors_notifications = apply_filters( 'blc_allow_send_author_email_notification', $this->conf->options['send_authors_email_notifications'] );

			if ( ! ( $send_notification || $send_authors_notifications ) ) {
				return;
			}

			// Find links that have been detected as broken since the last sent notification.
			$last_notification = date( 'Y-m-d H:i:s', $this->conf->options['last_notification_sent'] );//phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
			$where             = $wpdb->prepare( '( first_failure >= %s )', $last_notification );

			$links = blc_get_links(
				array(
					's_filter'             => 'broken',
					'where_expr'           => $where,
					'load_instances'       => true,
					'load_containers'      => true,
					'load_wrapped_objects' => $this->conf->options['send_authors_email_notifications'],
					'max_results'          => 0,
				)
			);

			if ( empty( $links ) ) {
				return;
			}

			// Send the admin/maintainer an email notification.
			$email = $this->conf->get( 'notification_email_address' );
			if ( empty( $email ) ) {
				// Default to the admin email.
				$email = get_option( 'admin_email' );
			}
			if ( $this->conf->options['send_email_notifications'] && ! empty( $email ) ) {
				$this->send_admin_notification( $links, $email );
			}

			// Send notifications to post authors
			if ( $this->conf->options['send_authors_email_notifications'] ) {
				$this->send_authors_notifications( $links );
			}

			$this->conf->options['last_notification_sent'] = time();
			$this->conf->save_options();
		}

		function send_admin_notification( $links, $email ) {
			// Prepare email message
			$subject = sprintf(
				__( '[%s] Broken links detected', 'broken-link-checker' ),
				html_entity_decode( get_option( 'blogname' ), ENT_QUOTES )
			);

			$body  = sprintf(
				_n(
					'Broken Link Checker has detected %d new broken link on your site.',
					'Broken Link Checker has detected %d new broken links on your site.',
					count( $links ),
					'broken-link-checker'
				),
				count( $links )
			);
			$body .= '<br>';

			$instances = array();
			foreach ( $links as $link ) { /* @var blcLink $link */
				$instances = array_merge( $instances, $link->get_instances() );
			}
			$body .= $this->build_instance_list_for_email( $instances );

			if ( $this->is_textdomain_loaded && is_rtl() ) {
				$body = '<div dir="rtl">' . $body . '</div>';
			}

			$this->send_html_email( $email, $subject, $body );
		}

		function build_instance_list_for_email( $instances, $max_displayed_links = 5, $add_admin_link = true ) {
			if ( null === $max_displayed_links ) {
				$max_displayed_links = 5;
			}

			$result = '';
			if ( count( $instances ) > $max_displayed_links ) {
				$line = sprintf(
					_n(
						"Here's a list of the first %d broken links:",
						"Here's a list of the first %d broken links:",
						$max_displayed_links,
						'broken-link-checker'
					),
					$max_displayed_links
				);
			} else {
				$line = __( "Here's a list of the new broken links: ", 'broken-link-checker' );
			}

			$result .= "<p>$line</p>";

			// Show up to $max_displayed_links broken link instances right in the email.
			$displayed = 0;
			foreach ( $instances as $instance ) { /* @var blcLinkInstance $instance */
				$pieces = array(
					sprintf( __( 'Link text : %s', 'broken-link-checker' ), $instance->ui_get_link_text( 'email' ) ),
					sprintf( __( 'Link URL : <a href="%1$s">%2$s</a>', 'broken-link-checker' ), htmlentities( $instance->get_url() ), blcUtility::truncate( $instance->get_url(), 70, '' ) ),
					sprintf( __( 'Source : %s', 'broken-link-checker' ), $instance->ui_get_source( 'email' ) ),
				);

				$link_entry = implode( '<br>', $pieces );
				$result    .= "$link_entry<br><br>";

				$displayed++;
				if ( $displayed >= $max_displayed_links ) {
					break;
				}
			}

			// Add a link to the "Broken Links" tab.
			if ( $add_admin_link ) {
				$result .= __( 'You can see all broken links here:', 'broken-link-checker' ) . '<br>';
				$result .= sprintf( '<a href="%1$s">%1$s</a>', admin_url( 'admin.php?page=wdblc' ) );
			}

			return $result;
		}

		function send_html_email( $email_address, $subject, $body ) {
			// Need to override the default 'text/plain' content type to send a HTML email.
			add_filter( 'wp_mail_content_type', array( $this, 'override_mail_content_type' ) );

			// Let auto-responders and similar software know this is an auto-generated email
			// that they shouldn't respond to.
			$headers = array( 'Auto-Submitted: auto-generated' );

			$success = wp_mail( $email_address, $subject, $body, $headers );

			// Remove the override so that it doesn't interfere with other plugins that might
			// want to send normal plaintext emails.
			remove_filter( 'wp_mail_content_type', array( $this, 'override_mail_content_type' ) );

			$this->conf->options['last_email'] = array(
				'subject'   => $subject,
				'timestamp' => time(),
				'success'   => $success,
			);
			$this->conf->save_options();

			return $success;
		}

		function send_authors_notifications( $links ) {
			$authorInstances = array();
			foreach ( $links as $link ) { /* @var blcLink $link */
				foreach ( $link->get_instances() as $instance ) { /* @var blcLinkInstance $instance */
					$container = $instance->get_container(); /** @var blcContainer $container */
					if ( empty( $container ) || ! ( $container instanceof blcAnyPostContainer ) ) {
						continue;
					}
					$post = $container->get_wrapped_object(); /** @var StdClass $post */
					if ( ! array_key_exists( $post->post_author, $authorInstances ) ) {
						$authorInstances[ $post->post_author ] = array();
					}
					$authorInstances[ $post->post_author ][] = $instance;
				}
			}

			foreach ( $authorInstances as $author_id => $instances ) {
				$subject = sprintf(
					__( '[%s] Broken links detected', 'broken-link-checker' ),
					html_entity_decode( get_option( 'blogname' ), ENT_QUOTES )
				);

				$body  = sprintf(
					_n(
						'Broken Link Checker has detected %d new broken link in your posts.',
						'Broken Link Checker has detected %d new broken links in your posts.',
						count( $instances ),
						'broken-link-checker'
					),
					count( $instances )
				);
				$body .= '<br>';

				$author = get_user_by( 'id', $author_id ); /** @var WP_User $author */
				$body  .= $this->build_instance_list_for_email( $instances, null, $author->has_cap( 'edit_others_posts' ) );

				if ( $this->is_textdomain_loaded && is_rtl() ) {
					$body = '<div dir="rtl">' . $body . '</div>';
				}

				$this->send_html_email( $author->user_email, $subject, $body );
			}
		}

		function override_mail_content_type( /** @noinspection PhpUnusedParameterInspection */ $content_type ) {
			return 'text/html';
		}

		/**
		 * Adds the following cron schedules:
		 * - 10min: every 10 minutes.
		 * - weekly: once per week.
		 * - bimonthly : twice per month.
		 *
		 * @param array $schedules Existing Cron schedules.
		 * @return array
		 */
		function blc_cron_schedules( $schedules ) {

			return $schedules;

		}

		/**
		 * Install or uninstall the plugin's Cron events based on current settings.
		 *
		 * @uses wsBrokenLinkChecker::$conf Uses $conf->options to determine if events need to be (un)installed.
		 *
		 * @return void
		 */
		function setup_cron_events() {

			// Link monitor
			if ( $this->conf->options['run_via_cron'] ) {
				add_filter( 'cron_schedules', array( $this, 'blc_cron_schedules' ) );
				if ( ! wp_next_scheduled( 'blc_cron_check_links' ) ) {
					wp_schedule_event( time(), '10min', 'blc_cron_check_links' );
				}
			} else {
				wp_clear_scheduled_hook( 'blc_cron_check_links' );
				remove_filter( 'cron_schedules', 'blc_cron_schedules' );
			}

			// Email notifications about broken links
			if ( $this->conf->options['send_email_notifications'] || $this->conf->options['send_authors_email_notifications'] ) {
				if ( ! wp_next_scheduled( 'blc_cron_email_notifications' ) ) {
					wp_schedule_event( time(), $this->conf->options['notification_schedule'], 'blc_cron_email_notifications' );
				}
			} else {
				wp_clear_scheduled_hook( 'blc_cron_email_notifications' );
			}

			// Run database maintenance every two weeks or so
			if ( ! wp_next_scheduled( 'blc_cron_database_maintenance' ) ) {
				wp_schedule_event( time(), 'daily', 'blc_cron_database_maintenance' );
			}

			$clear_log = $this->conf->options['clear_log_on'];
			if ( ! wp_next_scheduled( 'blc_corn_clear_log_file' ) && ! empty( $clear_log ) ) {
				wp_schedule_event( time(), $clear_log, 'blc_corn_clear_log_file' );
			}

			if ( empty( $clear_log ) ) {
				wp_clear_scheduled_hook( 'blc_corn_clear_log_file' );
			}
		}

		/**
		 * Clear blc log file
		 *
		 * @return void
		 */
		function clear_log_file() {
			$log_file = $this->conf->options['log_file'];

			// clear log file
			if ( is_writable( $log_file ) && is_file( $log_file ) ) {
				$handle = fopen( $log_file, 'w' );
				fclose( $handle );
			}
		}

		/**
		 * Don't update the last updated date of a post
		 *
		 * @param array $data An array of slashed post data.
		 * @param array $postarr An array of sanitized, but otherwise unmodified post data.
		 * @return array $data Resulting array of slashed post data.
		 */
		public function disable_post_date_update( $data, $postarr ) {

			$last_modified = isset( $postarr['blc_post_modified'] ) ? $postarr['blc_post_modified'] : '';

			$last_modified_gmt = isset( $postarr['blc_post_modified_gmt'] ) ? $postarr['blc_post_modified_gmt'] : '';

			// if is not enabled bail!
			if ( ! $this->conf->options['blc_post_modified'] ) {
				return $data;
			}

			// only restore the post modified for BLC links
			if ( empty( $last_modified ) || empty( $last_modified_gmt ) ) {
				return $data;
			}

			// modify the post modified date.
			$data['post_modified'] = $last_modified;

			// modify the post modified gmt
			$data['post_modified_gmt'] = $last_modified_gmt;

			return $data;
		}



		/**
		 * Load the plugin's textdomain.
		 *
		 * @return void
		 */
		function load_language() {
			$this->is_textdomain_loaded = load_plugin_textdomain( 'broken-link-checker', false, basename( dirname( $this->loader ) ) . '/languages' );
		}

	}//end class

} // if class_exists...
