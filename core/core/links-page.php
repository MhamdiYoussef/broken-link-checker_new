<?php

class Links_Page {
	private $core;
	private $conf;

	public function __construct( $core, $conf ) {
		global $wpdb;
		$this->core = $core;
		$this->conf = $conf;

		$blc_link_query = blcLinkQuery::getInstance();

		// Cull invalid and missing modules so that we don't get dummy links/instances showing up.
		$module_manager = blcModuleManager::getInstance();
		$module_manager->validate_active_modules();

		if ( defined( 'BLC_DEBUG' ) && constant( 'BLC_DEBUG' ) ) {
			// Make module headers translatable. They need to be formatted corrrectly and
			// placed in a .php file to be visible to the script(s) that generate .pot files.
			$code = $module_manager->_build_header_translation_code();
			file_put_contents( dirname( $this->core->loader ) . '/includes/extra-strings.php', $code );
		}

		$action = ! empty( $_POST['action'] ) ? $_POST['action'] : '';
		if ( intval( $action ) == -1 ) {
			// Try the second bulk actions box
			$action = ! empty( $_POST['action2'] ) ? $_POST['action2'] : '';
		}

		//Get the list of link IDs selected via checkboxes
		$selected_links = array();
		if ( isset( $_POST['selected_links'] ) && is_array( $_POST['selected_links'] ) ) {
			//Convert all link IDs to integers (non-numeric entries are converted to zero)
			$selected_links = array_map( 'intval', $_POST['selected_links'] );
			//Remove all zeroes
			$selected_links = array_filter( $selected_links );
		}

		$message   = '';
		$msg_class = 'updated';

		//Run the selected bulk action, if any
		$force_delete = false;
		switch ( $action ) {
			case 'create-custom-filter':
				list($message, $msg_class) = $this->do_create_custom_filter();
				break;

			case 'delete-custom-filter':
				list($message, $msg_class) = $this->do_delete_custom_filter();
				break;

			// @noinspection PhpMissingBreakStatementInspection Deliberate fall-through.
			case 'bulk-delete-sources':
				$force_delete = true;
				//intentional fall through
			case 'bulk-trash-sources':
				list($message, $msg_class) = $this->do_bulk_delete_sources( $selected_links, $force_delete );
				break;

			case 'bulk-unlink':
				list($message, $msg_class) = $this->do_bulk_unlink( $selected_links );
				break;

			case 'bulk-deredirect':
				list($message, $msg_class) = $this->do_bulk_deredirect( $selected_links );
				break;

			case 'bulk-recheck':
				list($message, $msg_class) = $this->do_bulk_recheck( $selected_links );
				break;

			case 'bulk-not-broken':
				list($message, $msg_class) = $this->do_bulk_discard( $selected_links );
				break;

			case 'bulk-edit':
				list($message, $msg_class) = $this->do_bulk_edit( $selected_links );
				break;
		}

		if ( ! empty( $message ) ) {
			echo '<div id="message" class="' . $msg_class . ' fade"><p>' . $message . '</p></div>';
		}

		$start_time = microtime_float();

		//Load custom filters, if any
		$blc_link_query->load_custom_filters();

		//Calculate the number of links matching each filter
		$blc_link_query->count_filter_results();

		//Run the selected filter (defaults to displaying broken links)
		$selected_filter_id = isset( $_GET['filter_id'] ) ? $_GET['filter_id'] : 'broken';
		$current_filter     = $blc_link_query->exec_filter(
			$selected_filter_id,
			isset( $_GET['paged'] ) ? intval( $_GET['paged'] ) : 1,
			$this->conf->options['table_links_per_page'],
			'broken',
			isset( $_GET['orderby'] ) ? $_GET['orderby'] : '',
			isset( $_GET['order'] ) ? $_GET['order'] : ''
		);

		//exec_filter() returns an array with filter data, including the actual filter ID that was used.
		$filter_id = $current_filter['filter_id'];

		//Error?
		if ( empty( $current_filter['links'] ) && ! empty( $wpdb->last_error ) ) {
			printf( __( 'Database error : %s', 'broken-link-checker' ), $wpdb->last_error );
		}
		require BLC_DIRECTORY . '/core/core/template/links-page.php';
	}

	/**
	 * Create a custom link filter using params passed in $_POST.
	 *
	 * @uses $_POST
	 * @uses $_GET to replace the current filter ID (if any) with that of the newly created filter.
	 *
	 * @return array Message and the CSS class to apply to the message.
	 */
	function do_create_custom_filter() {
		global $wpdb;

		//Create a custom filter!
		check_admin_referer( 'create-custom-filter' );
		$msg_class = 'updated';

		//Filter name must be set
		if ( empty( $_POST['name'] ) ) {
			$message   = __( 'You must enter a filter name!', 'broken-link-checker' );
			$msg_class = 'error';
			//Filter parameters (a search query) must also be set
		} elseif ( empty( $_POST['params'] ) ) {
			$message   = __( 'Invalid search query.', 'broken-link-checker' );
			$msg_class = 'error';
		} else {
			//Save the new filter
			$name           = strip_tags( strval( $_POST['name'] ) );
			$blc_link_query = blcLinkQuery::getInstance();
			$filter_id      = $blc_link_query->create_custom_filter( $name, $_POST['params'] );

			if ( $filter_id ) {
				//Saved
				$message = sprintf( __( 'Filter "%s" created', 'broken-link-checker' ), $name );
				//A little hack to make the filter active immediately
				$_GET['filter_id'] = $filter_id;
			} else {
				//Error
				$message   = sprintf( __( 'Database error : %s', 'broken-link-checker' ), $wpdb->last_error );
				$msg_class = 'error';
			}
		}

		return array( $message, $msg_class );
	}

	/**
	 * Delete a custom link filter.
	 *
	 * @uses $_POST
	 *
	 * @return array Message and a CSS class to apply to the message.
	 */
	function do_delete_custom_filter() {
		//Delete an existing custom filter!
		check_admin_referer( 'delete-custom-filter' );
		$msg_class = 'updated';

		//Filter ID must be set
		if ( empty( $_POST['filter_id'] ) ) {
			$message   = __( 'Filter ID not specified.', 'broken-link-checker' );
			$msg_class = 'error';
		} else {
			//Try to delete the filter
			$blc_link_query = blcLinkQuery::getInstance();
			if ( $blc_link_query->delete_custom_filter( $_POST['filter_id'] ) ) {
				//Success
				$message = __( 'Filter deleted', 'broken-link-checker' );
			} else {
				//Either the ID is wrong or there was some other error
				$message   = __( 'Database error : %s', 'broken-link-checker' );
				$msg_class = 'error';
			}
		}

		return array( $message, $msg_class );
	}

		/**
		 * Delete or trash posts, bookmarks and other items that contain any of the specified links.
		 *
		 * Will prefer moving stuff to trash to permanent deletion. If it encounters an item that
		 * can't be moved to the trash, it will skip that item by default.
		 *
		 * @param array $selected_links An array of link IDs
		 * @param bool $force_delete Whether to bypass trash and force deletion. Defaults to false.
		 * @return array Confirmation message and its CSS class.
		 */
		function do_bulk_delete_sources( $selected_links, $force_delete = false ) {
			$message   = '';
			$msg_class = 'updated';

			//Delete posts, blogroll entries and any other link containers that contain any of the selected links.
			//
			//Note that once all containers containing a particular link have been deleted,
			//there is no need to explicitly delete the link record itself. The hooks attached to
			//the actions that execute when something is deleted (e.g. "post_deleted") will
			//take care of that.

			check_admin_referer( 'bulk-action' );

			if ( count( $selected_links ) > 0 ) {
				$messages = array();

				//Fetch all the selected links
				$links = blc_get_links(
					array(
						'link_ids'       => $selected_links,
						'load_instances' => true,
					)
				);

				//Make a list of all containers associated with these links, with each container
				//listed only once.
				$containers = array();
				foreach ( $links as $link ) { /* @var blcLink $link */
					$instances = $link->get_instances();
					foreach ( $instances as $instance ) { /* @var blcLinkInstance $instance */
						$key                = $instance->container_type . '|' . $instance->container_id;
						$containers[ $key ] = array( $instance->container_type, $instance->container_id );
					}
				}

				//Instantiate the containers
				$containers = blcContainerHelper::get_containers( $containers );

				//Delete/trash their associated entities
				$deleted = array();
				$skipped = array();
				foreach ( $containers as $container ) { /* @var blcContainer $container */
					if ( ! $container->current_user_can_delete() ) {
						continue;
					}

					if ( $force_delete ) {
						$rez = $container->delete_wrapped_object();
					} else {
						if ( $container->can_be_trashed() ) {
							$rez = $container->trash_wrapped_object();
						} else {
							$skipped[] = $container;
							continue;
						}
					}

					if ( is_wp_error( $rez ) ) { /* @var WP_Error $rez */
						//Record error messages for later display
						$messages[] = $rez->get_error_message();
						$msg_class  = 'error';
					} else {
						//Keep track of how many of each type were deleted.
						$container_type = $container->container_type;
						if ( isset( $deleted[ $container_type ] ) ) {
							$deleted[ $container_type ]++;
						} else {
							$deleted[ $container_type ] = 1;
						}
					}
				}

				//Generate delete confirmation messages
				foreach ( $deleted as $container_type => $number ) {
					if ( $force_delete ) {
						$messages[] = blcContainerHelper::ui_bulk_delete_message( $container_type, $number );
					} else {
						$messages[] = blcContainerHelper::ui_bulk_trash_message( $container_type, $number );
					}
				}

				//If some items couldn't be trashed, let the user know
				if ( count( $skipped ) > 0 ) {
					$message  = sprintf(
						_n(
							"%d item was skipped because it can't be moved to the Trash. You need to delete it manually.",
							"%d items were skipped because they can't be moved to the Trash. You need to delete them manually.",
							count( $skipped )
						),
						count( $skipped )
					);
					$message .= '<br><ul>';
					foreach ( $skipped as $container ) {
						$message .= sprintf(
							'<li>%s</li>',
							$container->ui_get_source( '' )
						);
					}
					$message .= '</ul>';

					$messages[] = $message;
				}

				if ( count( $messages ) > 0 ) {
					$message = implode( '<p>', $messages );
				} else {
					$message   = __( "Didn't find anything to delete!", 'broken-link-checker' );
					$msg_class = 'error';
				}
			}

			return array( $message, $msg_class );
		}

		/**
		 * Unlink multiple links.
		 *
		 * @param array $selected_links
		 * @return array Message and a CSS classname.
		 */
		function do_bulk_unlink( $selected_links ) {
			//Unlink all selected links.
			$message   = '';
			$msg_class = 'updated';

			check_admin_referer( 'bulk-action' );

			if ( count( $selected_links ) > 0 ) {

				//Fetch all the selected links
				$links = blc_get_links(
					array(
						'link_ids' => $selected_links,
						'purpose'  => BLC_FOR_EDITING,
					)
				);

				if ( count( $links ) > 0 ) {
					$processed_links = 0;
					$failed_links    = 0;

					//Unlink (delete) each one
					foreach ( $links as $link ) {
						$rez = $link->unlink();
						if ( ( false == $rez ) || is_wp_error( $rez ) ) {
							$failed_links++;
						} else {
							$processed_links++;
						}
					}

					//This message is slightly misleading - it doesn't account for the fact that
					//a link can be present in more than one post.
					$message = sprintf(
						_n(
							'%d link removed',
							'%d links removed',
							$processed_links,
							'broken-link-checker'
						),
						$processed_links
					);

					if ( $failed_links > 0 ) {
						$message  .= '<br>' . sprintf(
							_n(
								'Failed to remove %d link',
								'Failed to remove %d links',
								$failed_links,
								'broken-link-checker'
							),
							$failed_links
						);
						$msg_class = 'error';
					}
				}
			}

			return array( $message, $msg_class );
		}

		/**
		 * Modify multiple links to point to their target URLs.
		 *
		 * @param array $selected_links
		 * @return array The message to display and its CSS class.
		 */
		function do_bulk_deredirect( $selected_links ) {
			//For all selected links, replace the URL with the final URL that it redirects to.

			$message   = '';
			$msg_class = 'updated';

			check_admin_referer( 'bulk-action' );

			if ( count( $selected_links ) > 0 ) {
				//Fetch all the selected links
				$links = blc_get_links(
					array(
						'link_ids' => $selected_links,
						'purpose'  => BLC_FOR_EDITING,
					)
				);

				if ( count( $links ) > 0 ) {
					$processed_links = 0;
					$failed_links    = 0;

					//Deredirect all selected links
					foreach ( $links as $link ) {
						$rez = $link->deredirect();
						if ( ! is_wp_error( $rez ) && empty( $rez['errors'] ) ) {
							$processed_links++;
						} else {
							$failed_links++;
						}
					}

					$message = sprintf(
						_n(
							'Replaced %d redirect with a direct link',
							'Replaced %d redirects with direct links',
							$processed_links,
							'broken-link-checker'
						),
						$processed_links
					);

					if ( $failed_links > 0 ) {
						$message  .= '<br>' . sprintf(
							_n(
								'Failed to fix %d redirect',
								'Failed to fix %d redirects',
								$failed_links,
								'broken-link-checker'
							),
							$failed_links
						);
						$msg_class = 'error';
					}
				} else {
					$message = __( 'None of the selected links are redirects!', 'broken-link-checker' );
				}
			}

			return array( $message, $msg_class );
		}

		/**
		 * Mark multiple links as unchecked.
		 *
		 * @param array $selected_links An array of link IDs
		 * @return array Confirmation nessage and the CSS class to use with that message.
		 */
		function do_bulk_recheck( $selected_links ) {
			/** @var wpdb $wpdb */
			global $wpdb;

			$message   = '';
			$msg_class = 'updated';
			$total_links = count( $selected_links );
			check_admin_referer( 'bulk-action' );

			if ( $total_links > 0 ) {
				$placeholders = array_fill( 0, $total_links, '%d' );
				$format = implode( ', ', $placeholders );
				$query  = "UPDATE {$wpdb->prefix}blc_links
				SET last_check_attempt = '0000-00-00 00:00:00'
				WHERE link_id IN ( $format )";

				$changes = $wpdb->query(
					$wpdb->prepare(
						$query, //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
						$selected_links
					)
				);

				$message = sprintf(
					_n(
						'%d link scheduled for rechecking',
						'%d links scheduled for rechecking',
						$changes,
						'broken-link-checker'
					),
					$changes
				);
			}

			return array( $message, $msg_class );
		}

		/**
		 * Mark multiple links as not broken.
		 *
		 * @param array $selected_links An array of link IDs
		 * @return array Confirmation nessage and the CSS class to use with that message.
		 */
		function do_bulk_discard( $selected_links ) {
			check_admin_referer( 'bulk-action' );

			$messages        = array();
			$msg_class       = 'updated';
			$processed_links = 0;

			if ( count( $selected_links ) > 0 ) {
				$transactionManager = TransactionManager::getInstance();
				$transactionManager->start();
				foreach ( $selected_links as $link_id ) {
					//Load the link
					$link = new blcLink( intval( $link_id ) );

					//Skip links that don't actually exist
					if ( ! $link->valid() ) {
						continue;
					}

					//Skip links that weren't actually detected as broken
					if ( ! $link->broken && ! $link->warning ) {
						continue;
					}

					//Make it appear "not broken"
					$link->broken             = false;
					$link->warning            = false;
					$link->false_positive     = true;
					$link->last_check_attempt = time();
					$link->log                = __( 'This link was manually marked as working by the user.', 'broken-link-checker' );

					$link->isOptionLinkChanged = true;
					//Save the changes
					if ( $link->save() ) {
						$processed_links++;
					} else {
						$messages[] = sprintf(
							__( "Couldn't modify link %d", 'broken-link-checker' ),
							$link_id
						);
						$msg_class  = 'error';
					}
				}
			}

			if ( $processed_links > 0 ) {
				$transactionManager->commit();
				$messages[] = sprintf(
					_n(
						'%d link marked as not broken',
						'%d links marked as not broken',
						$processed_links,
						'broken-link-checker'
					),
					$processed_links
				);
			}

			return array( implode( '<br>', $messages ), $msg_class );
		}

		/**
		 * Edit multiple links in one go.
		 *
		 * @param array $selected_links
		 * @return array The message to display and its CSS class.
		 */
		function do_bulk_edit( $selected_links ) {
			$message   = '';
			$msg_class = 'updated';

			check_admin_referer( 'bulk-action' );

			$post = $_POST;
			if ( function_exists( 'wp_magic_quotes' ) ) {
				$post = stripslashes_deep( $post ); //Ceterum censeo, WP shouldn't mangle superglobals.
			}

			$search         = isset( $post['search'] ) ? esc_attr( $post['search'] ) : '';
			$replace        = isset( $post['replace'] ) ? esc_attr( $post['replace'] ) : '';
			$use_regex      = ! empty( $post['regex'] );
			$case_sensitive = ! empty( $post['case_sensitive'] );

			$delimiter = '`'; //Pick a char that's uncommon in URLs so that escaping won't usually be a problem
			if ( $use_regex ) {
				$search = $delimiter . $this->escape_regex_delimiter( $search, $delimiter ) . $delimiter;
				if ( ! $case_sensitive ) {
					$search .= 'i';
				}
			} elseif ( ! $case_sensitive ) {
				//str_ireplace() would be more appropriate for case-insensitive, non-regexp replacement,
				//but that's only available in PHP5.
				$search    = $delimiter . preg_quote( $search, $delimiter ) . $delimiter . 'i';
				$use_regex = true;
			}

			if ( count( $selected_links ) > 0 ) {
				set_time_limit( 300 ); //In case the user decides to edit hundreds of links at once

				//Fetch all the selected links
				$links = blc_get_links(
					array(
						'link_ids' => $selected_links,
						'purpose'  => BLC_FOR_EDITING,
					)
				);

				if ( count( $links ) > 0 ) {
					$processed_links = 0;
					$failed_links    = 0;
					$skipped_links   = 0;

					//Edit the links
					foreach ( $links as $link ) {
						if ( $use_regex ) {
							$new_url = preg_replace( $search, $replace, $link->url );
						} else {
							$new_url = str_replace( $search, $replace, $link->url );
						}

						if ( $new_url == $link->url ) {
							$skipped_links++;
							continue;
						}

						$rez = $link->edit( $new_url );
						if ( ! is_wp_error( $rez ) && empty( $rez['errors'] ) ) {
							$processed_links++;
						} else {
							$failed_links++;
						}
					}

					$message .= sprintf(
						_n(
							'%d link updated.',
							'%d links updated.',
							$processed_links,
							'broken-link-checker'
						),
						$processed_links
					);

					if ( $failed_links > 0 ) {
						$message  .= '<br>' . sprintf(
							_n(
								'Failed to update %d link.',
								'Failed to update %d links.',
								$failed_links,
								'broken-link-checker'
							),
							$failed_links
						);
						$msg_class = 'error';
					}
				}
			}

			return array( $message, $msg_class );
		}
}
