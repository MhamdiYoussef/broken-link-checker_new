<script type='text/javascript'>
	var blc_current_filter = '<?php echo $filter_id; ?>';
	var blc_is_broken_filter = <?php echo $current_filter['is_broken_filter'] ? 'true' : 'false'; ?>;
	var blc_current_base_filter = '<?php echo esc_js( $current_filter['base_filter'] ); ?>';
	var blc_suggestions_enabled = <?php echo $this->conf->options['suggestions_enabled'] ? 'true' : 'false'; ?>;
</script>

<div class="wrap">
		<?php
		$blc_link_query->print_filter_heading( $current_filter );
		$blc_link_query->print_filter_menu( $filter_id );

		//Display the "Search" form and associated buttons.
		//The form requires the $filter_id and $current_filter variables to be set.
		include dirname( $this->core->loader ) . '/includes/admin/search-form.php';

		//If the user has decided to switch the table to a different mode (compact/full),
		//save the new setting.
		if ( isset( $_GET['compact'] ) ) {
			$this->conf->options['table_compact'] = (bool) $_GET['compact'];
			$this->conf->save_options();
		}

		//Display the links, if any
		if ( $current_filter['links'] && ( count( $current_filter['links'] ) > 0 ) ) {

			include dirname( $this->core->loader ) . '/includes/admin/table-printer.php';
			$table = new blcTablePrinter( $this->core );
			$table->print_table(
				$current_filter,
				$this->conf->options['table_layout'],
				$this->conf->options['table_visible_columns'],
				$this->conf->options['table_compact']
			);

		};
		printf( '<!-- Total elapsed : %.4f seconds -->', microtime_float() - $start_time );

		//Load assorted JS event handlers and other shinies
		include dirname( $this->core->loader ) . '/includes/admin/links-page-js.php';

		?>
</div>
