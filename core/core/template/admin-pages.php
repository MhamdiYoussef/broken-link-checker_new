<?php
/**
 * Template for admin menu/page.
 *
 * @package core
 */

// Show a confirmation message when settings are saved.
if ( ! empty( $_GET['settings-updated'] ) ) {
	echo '<div id="message" class="updated fade"><p><strong>',esc_html__( 'Settings saved.', 'broken-link-checker' ), '</strong></p></div>';

}

// Show a thank-you message when a donation is made.
if ( ! empty( $_GET['donated'] ) ) {
	echo '<div id="message" class="updated fade"><p><strong>',esc_html__( 'Thank you for your donation!', 'broken-link-checker' ), '</strong></p></div>';
	$this->conf->set( 'user_has_donated', true );
	$this->conf->save_options();
}

// Show one when recheck is started, too.
if ( ! empty( $_GET['recheck-initiated'] ) ) {
	echo '<div id="message" class="updated fade"><p><strong>',
	esc_html__( 'Complete site recheck started.', 'broken-link-checker' ), // -- Yoda
	'</strong></p></div>';
}

// Cull invalid and missing modules
$module_manager->validate_active_modules();

$debug = $this->get_debug_info();

add_filter( 'blc-module-settings-custom_field', array( $this, 'make_custom_field_input' ), 10, 2 );
add_filter( 'blc-module-settings-acf_field', array( $this, 'make_acf_field_input' ), 10, 2 );

// Translate and markup-ify module headers for display
$modules = $module_manager->get_modules_by_category( '', true, true );

// Output the custom broken link/removed link styles for example links.
printf(
	'<style type="text/css">%s %s</style>',
	esc_html( $this->conf->options['broken_link_css'] ),
	esc_html( $this->conf->options['removed_link_css'] )
);

$section_names = array(
	'general'  => __( 'General', 'broken-link-checker' ),
	'where'    => __( 'Look For Links In', 'broken-link-checker' ),
	'which'    => __( 'Which Links To Check', 'broken-link-checker' ),
	'how'      => __( 'Protocols & APIs', 'broken-link-checker' ),
	'advanced' => __( 'Advanced', 'broken-link-checker' ),
);

?>

<!--[if lte IE 7]>
<style type="text/css">
/* Simulate inline-block in IE7 */
ul.ui-tabs-nav li {
	display: inline;
	zoom: 1;
}
</style>
<![endif]-->
<div class="wrap" id="blc-settings-wrap">
<h2><?php esc_html_e( 'Broken Link Checker Options', 'broken-link-checker' ); ?></h2>


<div id="blc-sidebar">
	<div class="metabox-holder">
		<?php require BLC_DIRECTORY . '/includes/admin/sidebar.php'; ?>
	</div>
</div>


<div id="blc-admin-content">

<form name="link_checker_options" id="link_checker_options" method="post" action="
	<?php
	echo admin_url( 'admin.php?page=wdblc-settings&noheader=1' );
	?>
">
	<?php
	wp_nonce_field( 'link-checker-options' );
	?>

<div id="blc-tabs">

<ul class="hide-if-no-js">
	<?php
	foreach ( $section_names as $section_id => $section_name ) {
		printf(
			'<li id="tab-button-%s"><a href="#section-%s" title="%s">%s</a></li>',
			esc_attr( $section_id ),
			esc_attr( $section_id ),
			esc_attr( $section_name ),
			esc_html( $section_name )
		);
	}
	?>
</ul>

<div id="section-general" class="blc-section">
<h3 class="hide-if-js"><?php echo esc_html( $section_names['general'] ); ?></h3>

<table class="form-table">

<tr valign="top">
<th scope="row">
	<?php _e( 'Status', 'broken-link-checker' ); ?>
	<br>
	<a href="javascript:void(0)" id="blc-debug-info-toggle"><?php _e( 'Show debug info', 'broken-link-checker' ); ?></a>
</th>
<td>

<div id='wsblc_full_status'>
	<br/><br/><br/>
</div>

<table id="blc-debug-info">
	<?php

	//Output the debug info in a table
	foreach ( $debug as $key => $value ) {
		printf(
			'<tr valign="top" class="blc-debug-item-%s"><th scope="row">%s</th><td>%s<div class="blc-debug-message">%s</div></td></tr>',
			$value['state'],
			$key,
			$value['value'],
			( array_key_exists( 'message', $value ) ? $value['message'] : '' )
		);
	}
	?>
</table>

</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Check each link', 'broken-link-checker' ); ?></th>
<td>

	<?php
	printf(
		__( 'Every %s hours', 'broken-link-checker' ),
		sprintf(
			'<input type="text" name="check_threshold" id="check_threshold" value="%d" size="5" maxlength="5" />',
			$this->conf->options['check_threshold']
		)
	);
	?>
<br/>
<span class="description">
	<?php _e( 'Existing links will be checked this often. New links will usually be checked ASAP.', 'broken-link-checker' ); ?>
</span>

</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'E-mail notifications', 'broken-link-checker' ); ?></th>
<td>
	<p style="margin-top: 0;">
	<label for='send_email_notifications'>
		<input type="checkbox" name="send_email_notifications" id="send_email_notifications"
		<?php
		if ( $this->conf->options['send_email_notifications'] ) {
			echo ' checked="checked"';}
		?>
		/>
		<?php _e( 'Send me e-mail notifications about newly detected broken links', 'broken-link-checker' ); ?>
	</label><br />
	</p>

	<p>
	<label for='send_authors_email_notifications'>
		<input type="checkbox" name="send_authors_email_notifications" id="send_authors_email_notifications"
		<?php
		if ( $this->conf->options['send_authors_email_notifications'] ) {
			echo ' checked="checked"';}
		?>
		/>
		<?php _e( 'Send authors e-mail notifications about broken links in their posts', 'broken-link-checker' ); ?>
	</label><br />
	</p>
</td>
</tr>

<tr valign="top">
	<th scope="row"><?php echo __( 'Notification e-mail address', 'broken-link-checker' ); ?></th>
	<td>
		<p>
		<label>
			<input
				type="text"
				name="notification_email_address"
				id="notification_email_address"
				value="<?php echo esc_attr( $this->conf->get( 'notification_email_address', '' ) ); ?>"
				class="regular-text ltr">
		</label><br>
		<span class="description">
			<?php echo __( 'Leave empty to use the e-mail address specified in Settings &rarr; General.', 'broken-link-checker' ); ?>
		</span>
		</p>
	</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Link tweaks', 'broken-link-checker' ); ?></th>
<td>
	<p style="margin-top: 0; margin-bottom: 0.5em;">
	<label for='mark_broken_links'>
		<input type="checkbox" name="mark_broken_links" id="mark_broken_links"
		<?php
		if ( $this->conf->options['mark_broken_links'] ) {
			echo ' checked="checked"';}
		?>
		/>
		<?php _e( 'Apply custom formatting to broken links', 'broken-link-checker' ); ?>
	</label>
	|
	<a id="toggle-broken-link-css-editor" href="#" class="blc-toggle-link">
	<?php
		_e( 'Edit CSS', 'broken-link-checker' );
	?>
	</a>
	</p>

	<div id="broken-link-css-wrap"
	<?php
	if ( ! blcUtility::get_cookie( 'broken-link-css-wrap', false ) ) {
		echo ' class="hidden"';
	}
	?>
	>
		<textarea name="broken_link_css" id="broken_link_css" cols='45' rows='4'>
		<?php
		if ( isset( $this->conf->options['broken_link_css'] ) ) {
			echo $this->conf->options['broken_link_css'];
		}
		?>
		</textarea>
		<p class="description">
		<?php
			printf(
				__( 'Example : Lorem ipsum <a %s>broken link</a>, dolor sit amet.', 'broken-link-checker' ),
				' href="#" class="broken_link" onclick="return false;"'
			);
							echo ' ', __( 'Click "Save Changes" to update example output.', 'broken-link-checker' );
		?>
		</p>
	</div>

	<p style="margin-bottom: 0.5em;">
	<label for='mark_removed_links'>
		<input type="checkbox" name="mark_removed_links" id="mark_removed_links"
		<?php
		if ( $this->conf->options['mark_removed_links'] ) {
			echo ' checked="checked"';}
		?>
		/>
		<?php _e( 'Apply custom formatting to removed links', 'broken-link-checker' ); ?>
	</label>
	|
	<a id="toggle-removed-link-css-editor" href="#" class="blc-toggle-link">
	<?php
		_e( 'Edit CSS', 'broken-link-checker' );
	?>
	</a>
	</p>

	<div id="removed-link-css-wrap"
	<?php
	if ( ! blcUtility::get_cookie( 'removed-link-css-wrap', false ) ) {
		echo ' class="hidden"';
	}
	?>
	>
		<textarea name="removed_link_css" id="removed_link_css" cols='45' rows='4'>
		<?php
		if ( isset( $this->conf->options['removed_link_css'] ) ) {
			echo $this->conf->options['removed_link_css'];
		}
		?>
		</textarea>

		<p class="description">
		<?php
		printf(
			__( 'Example : Lorem ipsum <span %s>removed link</span>, dolor sit amet.', 'broken-link-checker' ),
			' class="removed_link"'
		);
							echo ' ', __( 'Click "Save Changes" to update example output.', 'broken-link-checker' );
		?>

		</p>
	</div>

	<p>
	<label for='nofollow_broken_links'>
		<input type="checkbox" name="nofollow_broken_links" id="nofollow_broken_links"
		<?php
		if ( $this->conf->options['nofollow_broken_links'] ) {
			echo ' checked="checked"';}
		?>
		/>
		<?php _e( 'Stop search engines from following broken links', 'broken-link-checker' ); ?>
	</label>
	</p>

	<p class="description">
		<?php
		echo _x(
			'These settings only apply to the content of posts, not comments or custom fields.',
			'"Link tweaks" settings',
			'broken-link-checker'
		);
		?>
	</p>
</td>
</tr>

	<tr valign="top">
		<th scope="row"><?php echo _x( 'Suggestions', 'settings page', 'broken-link-checker' ); ?></th>
		<td>
			<label>
				<input type="checkbox" name="suggestions_enabled" id="suggestions_enabled"
					<?php checked( $this->conf->options['suggestions_enabled'] ); ?>/>
				<?php _e( 'Suggest alternatives to broken links', 'broken-link-checker' ); ?>
			</label>
		</td>
	</tr>

	<tr valign="top">
		<th scope="row"><?php echo _x( 'Warnings', 'settings page', 'broken-link-checker' ); ?></th>
		<td id="blc_warning_settings">
			<label>
				<input type="checkbox" name="warnings_enabled" id="warnings_enabled"
					<?php checked( $this->conf->options['warnings_enabled'] ); ?>/>
				<?php _e( 'Show uncertain or minor problems as "warnings" instead of "broken"', 'broken-link-checker' ); ?>
			</label>
			<p class="description">
			<?php
				_e( 'Turning off this option will make the plugin report all problems as broken links.', 'broken-link-checker' );
			?>
			</p>
		</td>
	</tr>

	<tr valign="top">
		<th scope="row"><?php echo __( 'YouTube API Key', 'broken-link-checker' ); ?></th>
		<td>
			<p>
			<label>
				<input
					type="text"
					name="youtube_api_key"
					id="youtube_api_key"
					value="<?php echo $this->conf->options[ 'youtube_api_key' ]; ?>"
					class="regular-text ltr">
			</label><br>
			<span class="description">
				<?php printf( __( 'Use your own %1$sapi key%2$s for checking youtube links.', 'broken-link-checker' ), '<a href="https://developers.google.com/youtube/v3/getting-started">', '</a>' ); ?>
			</span>
			</p>
		</td>
	</tr>

	<tr valign="top">
		<th scope="row"><?php echo esc_html__( 'Post Modified Date', 'broken-link-checker' ); ?></th>
		<td>
			<label>
				<input type="checkbox" name="blc_post_modified" id="blc_post_modified"
					<?php checked( $this->conf->options['blc_post_modified'] ); ?>/>
				<?php esc_html_e( 'Disable post modified date change when link is edited', 'broken-link-checker' ); ?>
			</label>
		</td>
	</tr>

</table>

</div>

<div id="section-where" class="blc-section">
<h3 class="hide-if-js"><?php echo $section_names['where']; ?></h3>

<table class="form-table">

<tr valign="top">
<th scope="row"><?php _e( 'Look for links in', 'broken-link-checker' ); ?></th>
<td>
	<?php
	if ( ! empty( $modules['container'] ) ) {
		uasort(
			$modules['container'],
			function( $a, $b ) {
				return strcasecmp( $a['Name'], $b['Name'] );
			}
		);
		$this->print_module_list( $modules['container'], $this->conf->options );
	}
	?>
</td></tr>

<tr valign="top">
<th scope="row"><?php _e( 'Post statuses', 'broken-link-checker' ); ?></th>
<td>
	<?php
	$available_statuses = get_post_stati( array( 'internal' => false ), 'objects' );

	if ( isset( $this->conf->options['enabled_post_statuses'] ) ) {
		$enabled_post_statuses = $this->conf->options['enabled_post_statuses'];
	} else {
		$enabled_post_statuses = array();
	}

	foreach ( $available_statuses as $status => $status_object ) {
		printf(
			'<p><label><input type="checkbox" name="enabled_post_statuses[]" value="%s"%s> %s</label></p>',
			esc_attr( $status ),
			in_array( $status, $enabled_post_statuses ) ? ' checked="checked"' : '',
			$status_object->label
		);
	}
	?>
</td></tr>

</table>

</div>


<div id="section-which" class="blc-section">
<h3 class="hide-if-js"><?php echo $section_names['which']; ?></h3>

<table class="form-table">

<tr valign="top">
<th scope="row"><?php _e( 'Link types', 'broken-link-checker' ); ?></th>
<td>
<?php
if ( ! empty( $modules['parser'] ) ) {
	$this->print_module_list( $modules['parser'], $this->conf->options );
} else {
	echo __( 'Error : All link parsers missing!', 'broken-link-checker' );
}
?>
</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Exclusion list', 'broken-link-checker' ); ?></th>
<td><?php _e( "Don't check links where the URL contains any of these words (one per line) :", 'broken-link-checker' ); ?><br/>
<textarea name="exclusion_list" id="exclusion_list" cols='45' rows='4'>
<?php
if ( isset( $this->conf->options['exclusion_list'] ) ) {
	echo esc_textarea( implode( "\n", $this->conf->options['exclusion_list'] ) );
}
?>
</textarea>

</td>
</tr>

</table>
</div>

<div id="section-how" class="blc-section">
<h3 class="hide-if-js"><?php echo $section_names['how']; ?></h3>

<table class="form-table">

<tr valign="top">
<th scope="row"><?php _e( 'Check links using', 'broken-link-checker' ); ?></th>
<td>
<?php
if ( ! empty( $modules['checker'] ) ) {
	$modules['checker'] = array_reverse( $modules['checker'] );
	$this->print_module_list( $modules['checker'], $this->conf->options );
}
?>
</td></tr>

</table>
</div>

<div id="section-advanced" class="blc-section">
<h3 class="hide-if-js"><?php echo $section_names['advanced']; ?></h3>

<table class="form-table">

<tr valign="top">
<th scope="row"><?php _e( 'Timeout', 'broken-link-checker' ); ?></th>
<td>

<?php

printf(
	__( '%s seconds', 'broken-link-checker' ),
	sprintf(
		'<input type="text" name="timeout" id="blc_timeout" value="%d" size="5" maxlength="3" />',
		$this->conf->options['timeout']
	)
);

?>
<br/><span class="description">
<?php _e( 'Links that take longer than this to load will be marked as broken.', 'broken-link-checker' ); ?>
</span>

</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Link monitor', 'broken-link-checker' ); ?></th>
<td>

<p>
<label for='run_in_dashboard'>

		<input type="checkbox" name="run_in_dashboard" id="run_in_dashboard"
		<?php
		if ( $this->conf->options['run_in_dashboard'] ) {
			echo ' checked="checked"';}
		?>
		/>
		<?php _e( 'Run continuously while the Dashboard is open', 'broken-link-checker' ); ?>
</label>
</p>

<p>
<label for='run_via_cron'>
		<input type="checkbox" name="run_via_cron" id="run_via_cron"
		<?php
		if ( $this->conf->options['run_via_cron'] ) {
			echo ' checked="checked"';}
		?>
		/>
		<?php _e( 'Run hourly in the background', 'broken-link-checker' ); ?>
</label>
</p>

</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Show the dashboard widget for', 'broken-link-checker' ); ?></th>
<td>

	<?php
	$widget_caps = array(
		_x( 'Administrator', 'dashboard widget visibility', 'broken-link-checker' ) => 'manage_options',
		_x( 'Editor and above', 'dashboard widget visibility', 'broken-link-checker' ) => 'edit_others_posts',
		_x( 'Nobody (disables the widget)', 'dashboard widget visibility', 'broken-link-checker' ) => 'do_not_allow',
	);

	foreach ( $widget_caps as $title => $capability ) {
		printf(
			'<p><label><input type="radio" name="dashboard_widget_capability" value="%s"%s> %s</label></p>',
			esc_attr( $capability ),
			checked( $capability, $this->conf->get( 'dashboard_widget_capability' ), false ),
			$title
		);
	}
	?>
</td>
</tr>

<tr valign="top">
<th scope="row"><?php echo _x( 'Show link actions', 'settings page', 'broken-link-checker' ); ?></th>
<td>
	<?php
	$show_link_actions = $this->conf->get( 'show_link_actions', array() );
	foreach ( $available_link_actions as $action => $text ) {
		$enabled = isset( $show_link_actions[ $action ] ) ? (bool) ( $show_link_actions[ $action ] ) : true;
		printf(
			'<p><label><input type="checkbox" name="show_link_actions[%1$s]" %3$s> %2$s</label></p>',
			$action,
			$text,
			checked( $enabled, true, false )
		);
	}
	?>
</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Max. execution time', 'broken-link-checker' ); ?></th>
<td>

<?php

printf(
	__( '%s seconds', 'broken-link-checker' ),
	sprintf(
		'<input type="text" name="max_execution_time" id="max_execution_time" value="%d" size="5" maxlength="5" />',
		$this->conf->options['max_execution_time']
	)
);

?>
<br/><span class="description">
<?php

_e( 'The plugin works by periodically launching a background job that parses your posts for links, checks the discovered URLs, and performs other time-consuming tasks. Here you can set for how long, at most, the link monitor may run each time before stopping.', 'broken-link-checker' );

?>
</span>

</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Server load limit', 'broken-link-checker' ); ?></th>
<td>
<?php

$load      = blcUtility::get_server_load();
$available = ! empty( $load );

if ( $available ) {
	$value = ! empty( $this->conf->options['server_load_limit'] ) ? sprintf( '%.2f', $this->conf->options['server_load_limit'] ) : '';
	printf(
		'<input type="text" name="server_load_limit" id="server_load_limit" value="%s" size="5" maxlength="5"/> ',
		$value
	);

	printf(
		__( 'Current load : %s', 'broken-link-checker' ),
		'<span id="wsblc_current_load">...</span>'
	);
	echo '<br/><span class="description">';
	printf(
		__(
			'Link checking will be suspended if the average <a href="%s">server load</a> rises above this number. Leave this field blank to disable load limiting.',
			'broken-link-checker'
		),
		'http://en.wikipedia.org/wiki/Load_(computing)'
	);
	echo '</span>';

} else {
	echo '<input type="text" disabled="disabled" value="', esc_attr( __( 'Not available', 'broken-link-checker' ) ), '" size="13"/><br>';
	echo '<span class="description">';
	_e( 'Load limiting only works on Linux-like systems where <code>/proc/loadavg</code> is present and accessible.', 'broken-link-checker' );
	echo '</span>';
}
?>
</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Target resource usage', 'broken-link-checker' ); ?></th>
<td>
	<?php
	$target_resource_usage = $this->conf->get( 'target_resource_usage', 0.25 );
	printf(
		'<input name="target_resource_usage" value="%d"
			type="range" min="1" max="100" id="target_resource_usage">',
		$target_resource_usage * 100
	);
	?>

	<span id="target_resource_usage_percent">
	<?php
		echo sprintf( '%.0f%%', $target_resource_usage * 100 );
	?>
	</span>
</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Logging', 'broken-link-checker' ); ?></th>
<td>
	<p>
		<label for='logging_enabled'>
			<input type="checkbox" name="logging_enabled" id="logging_enabled"
				<?php checked( $this->conf->options['logging_enabled'] ); ?>/>
			<?php _e( 'Enable logging', 'broken-link-checker' ); ?>
		</label>
	</p>
</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Log file location', 'broken-link-checker' ); ?></th>
<td>

	<div class="blc-logging-options">

	<p>
	<label>
		<input type="radio" name="custom_log_file_enabled" value=""
			<?php checked( ! $this->conf->options['custom_log_file_enabled'] ); ?>>
		<?php echo _x( 'Default', 'log file location', 'broken-link-checker' ); ?>
	</label>
	<br>
		<span class="description">
			<code>
			<?php
				echo self::get_default_log_directory(), '/', self::get_default_log_basename();
			?>
			</code>
		</span>
	</p>

	<p>
	<label>
		<input type="radio" name="custom_log_file_enabled" value="1"
			<?php checked( $this->conf->options['custom_log_file_enabled'] ); ?>>
		<?php echo _x( 'Custom', 'log file location', 'broken-link-checker' ); ?>
	</label>
	<br><input type="text" name="log_file" id="log_file" size="90"
				 value="<?php echo esc_attr( $this->conf->options['log_file'] ); ?>">
	</p>

	</div>
</td>
</tr>
<tr valign="top">
<th scope="row"><?php _e( 'Log file clear schedule', 'broken-link-checker' ); ?></th>
<td>
	<div class="blc-logging-options">
		<p>
			<?php $schedules = wp_get_schedules(); ?>
			<select name="clear_log_on">
				<option value=""> <?php esc_html_e( 'Never', 'wpmudev' ); ?></option>
				<?php
				foreach ( $schedules as $key => $schedule ) {
					$selected = selected(
						$this->conf->options['clear_log_on'],
						$key,
						false
					);
					?>
					<option <?php echo $selected; ?>value="<?php echo esc_attr( $key ); ?>"> <?php echo esc_html( $schedule['display'] ); ?></option>
					<?php
				}
				?>

			</select>
		</p>
	</div>
</td>
</tr>

<tr valign="top">
<th scope="row"><?php _e( 'Forced recheck', 'broken-link-checker' ); ?></th>
<td>
<input class="button" type="button" name="start-recheck" id="start-recheck"
		value="<?php _e( 'Re-check all pages', 'broken-link-checker' ); ?>"  />
	<input type="hidden" name="recheck" value="" id="recheck" />
<br />
	<span class="description">
<?php
	_e( 'The "Nuclear Option". Click this button to make the plugin empty its link database and recheck the entire site from scratch.', 'broken-link-checker' );

?>
</span>
</td>
</tr>

</table>
</div>

</div>

<p class="submit"><input type="submit" name="submit" class='button-primary' value="<?php _e( 'Save Changes' ); ?>" /></p>
</form>

</div> <!-- First postbox-container -->


</div>
