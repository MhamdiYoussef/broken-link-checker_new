<?php

class Admin_Pages {
	/**
	 * Configuration.
	 *
	 * @var Object
	 */
	public $conf;

	/**
	 * Core.
	 *
	 * @var Object
	 */
	public $core;

	/**
	 * Class constructor.
	 *
	 * @param object $conf - configuration.
	 * @param object $core - core.
	 */
	public function __construct( $conf, $core ) {
		$this->conf = $conf;
		$this->core = $core;

		if ( current_user_can( 'manage_options' ) ) {
			add_filter( 'plugin_action_links', array( $this, 'plugin_action_links' ), 10, 2 );
		}

		// Dashboard Main Menu.
		$page = add_menu_page(
			__( 'Broken Link Checker', 'broken-link-checker' ),
			'Link Checker',
			'manage_options',
			'wdblc',
			array( $this, 'display_links_page' )
		);

		$menu_title = __( 'Broken Links', 'broken-link-checker' );
		if ( $this->conf->options['show_link_count_bubble'] ) {
			// To make it easier to notice when broken links appear, display the current number of
			// broken links in a little bubble notification in the "Broken Links" menu.
			// (Similar to how the number of plugin updates and unmoderated comments is displayed).
			$blc_link_query = blcLinkQuery::getInstance();
			$broken_links   = $blc_link_query->get_filter_links( 'broken', array( 'count_only' => true ) );
			if ( $broken_links > 0 ) {
				// TODO: Appropriating existing CSS classes for my own purposes is hacky. Fix eventually.
				$menu_title .= sprintf(
					' <span class="update-plugins"><span class="update-count blc-menu-bubble">%d</span></span>',
					$broken_links
				);
			}
		}
		$links_page_hook   = add_submenu_page(
			'wdblc',
			__( 'View Broken Links', 'broken-link-checker' ),
			$menu_title,
			'edit_others_posts',
			'wdblc',
			array( $this, 'display_links_page' )
		);
		$options_page_hook = add_submenu_page(
			'wdblc',
			__( 'Link Checker Settings', 'broken-link-checker' ),
			__( 'Settings', 'broken-link-checker' ),
			'manage_options',
			'wdblc-settings',
			array( $this, 'options_page' )
		);

		// Add plugin-specific scripts and CSS only to the it's own pages.
		add_action( 'admin_print_styles-' . $options_page_hook, array( $this, 'options_page_css' ) );
		add_action( 'admin_print_styles-' . $links_page_hook, array( $this, 'links_page_css' ) );
		add_action( 'admin_print_scripts-' . $options_page_hook, array( $this, 'enqueue_settings_scripts' ) );
		add_action( 'admin_print_scripts-' . $links_page_hook, array( $this, 'enqueue_link_page_scripts' ) );

		// Make the Settings page link to the link list.
		add_screen_meta_link(
			'blc-links-page-link',
			__( 'Go to Broken Links', 'broken-link-checker' ),
			admin_url( 'admin.php?page=wdblc' ),
			$options_page_hook,
			array( 'style' => 'font-weight: bold;' )
		);
	}

	/**
	 * Function plugin_action_links()
	 * Handler for the 'plugin_action_links' hook. Adds a "Settings" link to this plugin's entry
	 * on the plugin list.
	 *
	 * @param array  $links Links.
	 * @param string $file  File.
	 * @return array
	 */
	public function plugin_action_links( $links, $file ) {
		$basename = plugin_basename( $this->core->loader );
		if ( $file === $basename ) {
			$links[] = "<a href='" . admin_url( 'admin.php?page=wdblc' ) . "'>" . __( 'Settings' ) . '</a>';
		}
		return $links;
	}

	/**
	 * Function to show options page
	 */
	public function options_page() {
		$module_manager = blcModuleManager::getInstance();

		// Prior to 1.5.2 (released 2012-05-27), there was a bug that would cause the donation flag to be
		// set incorrectly. So we'll unset the flag in that case.
		$reset_donation_flag = ( $this->conf->get( 'first_installation_timestamp', 0 ) < strtotime( '2012-05-27 00:00' ) ) && ! $this->conf->get( 'donation_flag_fixed', false );

		if ( $reset_donation_flag ) {
			$this->conf->set( 'user_has_donated', false );
			$this->conf->set( 'donation_flag_fixed', true );
			$this->conf->save_options();
		}

		if ( isset( $_POST['recheck'] ) && ! empty( $_POST['recheck'] ) ) {
			$this->initiate_recheck();

			// Redirect back to the settings page.
			$base_url = remove_query_arg( array( '_wpnonce', 'noheader', 'updated', 'error', 'action', 'message' ) );
			wp_redirect(
				add_query_arg(
					array(
						'recheck-initiated' => true,
					),
					$base_url
				)
			);
			die();
		}

		$available_link_actions = array(
			'edit'                  => __( 'Edit URL', 'broken-link-checker' ),
			'delete'                => __( 'Unlink', 'broken-link-checker' ),
			'blc-discard-action'    => __( 'Not broken', 'broken-link-checker' ),
			'blc-dismiss-action'    => __( 'Dismiss', 'broken-link-checker' ),
			'blc-recheck-action'    => __( 'Recheck', 'broken-link-checker' ),
			'blc-deredirect-action' => _x( 'Fix redirect', 'link action; replace one redirect with a direct link', 'broken-link-checker' ),
		);

		if ( isset( $_POST['submit'] ) ) {
			check_admin_referer( 'link-checker-options' );

			$cleanPost = $_POST;
			if ( function_exists( 'wp_magic_quotes' ) ) {
				$cleanPost = stripslashes_deep( $cleanPost ); // Ceterum censeo, WP shouldn't mangle superglobals.
			}

			// Activate/deactivate modules.
			if ( ! empty( $_POST['module'] ) ) {
				$active = array_keys( $_POST['module'] );
				$module_manager->set_active_modules( $active );
			}

			// Only post statuses that actually exist can be selected.
			if ( isset( $_POST['enabled_post_statuses'] ) && is_array( $_POST['enabled_post_statuses'] ) ) {
				$available_statuses    = get_post_stati();
				$enabled_post_statuses = array_intersect( $_POST['enabled_post_statuses'], $available_statuses );
			} else {
				$enabled_post_statuses = array();
			}
			// At least one status must be enabled; defaults to "Published".
			if ( empty( $enabled_post_statuses ) ) {
				$enabled_post_statuses = array( 'publish' );
			}

			// Did the user add/remove any post statuses?
			$same_statuses         = array_intersect( $enabled_post_statuses, $this->conf->options['enabled_post_statuses'] );
			$post_statuses_changed = ( count( $same_statuses ) != count( $enabled_post_statuses ) )
				|| ( count( $same_statuses ) !== count( $this->conf->options['enabled_post_statuses'] ) );

			$this->conf->options['enabled_post_statuses'] = $enabled_post_statuses;

			// The execution time limit must be above zero
			$new_execution_time = intval( $_POST['max_execution_time'] );
			if ( $new_execution_time > 0 ) {
				$this->conf->options['max_execution_time'] = $new_execution_time;
			}

			// The check threshold also must be > 0
			$new_check_threshold = intval( $_POST['check_threshold'] );

			if ( $new_check_threshold > 0 ) {
				$this->conf->options['check_threshold'] = $new_check_threshold;
			}

			$this->conf->options['mark_broken_links'] = ! empty( $_POST['mark_broken_links'] );
			$new_broken_link_css                      = trim( $cleanPost['broken_link_css'] );
			$this->conf->options['broken_link_css']   = $new_broken_link_css;

			$this->conf->options['mark_removed_links'] = ! empty( $_POST['mark_removed_links'] );
			$new_removed_link_css                      = trim( $cleanPost['removed_link_css'] );
			$this->conf->options['removed_link_css']   = $new_removed_link_css;

			$this->conf->options['nofollow_broken_links'] = ! empty( $_POST['nofollow_broken_links'] );

			$this->conf->options['suggestions_enabled'] = ! empty( $_POST['suggestions_enabled'] );

			$this->conf->options['exclusion_list'] = array_filter(
				preg_split(
					'/[\s\r\n]+/', // split on newlines and whitespace
					$cleanPost['exclusion_list'],
					-1,
					PREG_SPLIT_NO_EMPTY // skip empty values
				)
			);

			// Parse the custom field list
			$new_custom_fields = array_filter(
				preg_split(
					'/[\r\n]+/',
					$cleanPost['blc_custom_fields'],
					-1,
					PREG_SPLIT_NO_EMPTY
				)
			);

			// Calculate the difference between the old custom field list and the new one (used later)
			$diff1                                = array_diff( $new_custom_fields, $this->conf->options['custom_fields'] );
			$diff2                                = array_diff( $this->conf->options['custom_fields'], $new_custom_fields );
			$this->conf->options['custom_fields'] = $new_custom_fields;

			// Parse the custom field list
			$new_acf_fields = array_filter( preg_split( '/[\r\n]+/', $cleanPost['blc_acf_fields'], -1, PREG_SPLIT_NO_EMPTY ) );

			// Calculate the difference between the old custom field list and the new one (used later)
			$acf_fields_diff1                  = array_diff( $new_acf_fields, $this->conf->options['acf_fields'] );
			$acf_fields_diff2                  = array_diff( $this->conf->options['acf_fields'], $new_acf_fields );
			$this->conf->options['acf_fields'] = $new_acf_fields;

			// Turning off warnings turns existing warnings into "broken" links.
			$this->conf->options['blc_post_modified'] = ! empty( $_POST['blc_post_modified'] );

			// Turning off warnings turns existing warnings into "broken" links.
			$warnings_enabled = ! empty( $_POST['warnings_enabled'] );
			if ( $this->conf->get( 'warnings_enabled' ) && ! $warnings_enabled ) {
				$this->promote_warnings_to_broken();
			}
			$this->conf->options['warnings_enabled'] = $warnings_enabled;

			// HTTP timeout
			$new_timeout = intval( $_POST['timeout'] );
			if ( $new_timeout > 0 ) {
				$this->conf->options['timeout'] = $new_timeout;
			}

			// Server load limit
			if ( isset( $_POST['server_load_limit'] ) ) {
				$this->conf->options['server_load_limit'] = floatval( $_POST['server_load_limit'] );
				if ( $this->conf->options['server_load_limit'] < 0 ) {
					$this->conf->options['server_load_limit'] = 0;
				}
				$this->conf->options['enable_load_limit'] = $this->conf->options['server_load_limit'] > 0;
			}

			// Target resource usage (1% to 100%)
			if ( isset( $_POST['target_resource_usage'] ) ) {
				$usage                                        = floatval( $_POST['target_resource_usage'] );
				$usage                                        = max( min( $usage / 100, 1 ), 0.01 );
				$this->conf->options['target_resource_usage'] = $usage;
			}

			// When to run the checker
			$this->conf->options['run_in_dashboard'] = ! empty( $_POST['run_in_dashboard'] );
			$this->conf->options['run_via_cron']     = ! empty( $_POST['run_via_cron'] );

			// youtube api
			$this->conf->options['youtube_api_key'] = ! empty( $_POST['youtube_api_key'] ) ? $_POST['youtube_api_key'] : '';

			// Email notifications on/off
			$email_notifications              = ! empty( $_POST['send_email_notifications'] );
			$send_authors_email_notifications = ! empty( $_POST['send_authors_email_notifications'] );

			if (
				( $email_notifications && ! $this->conf->options['send_email_notifications'] )
				|| ( $send_authors_email_notifications && ! $this->conf->options['send_authors_email_notifications'] )
			) {
				/*
				The plugin should only send notifications about links that have become broken
				since the time when email notifications were turned on. If we don't do this,
				the first email notification will be sent nigh-immediately and list *all* broken
				links that the plugin currently knows about.
				*/
				$this->conf->options['last_notification_sent'] = time();
			}
			$this->conf->options['send_email_notifications']         = $email_notifications;
			$this->conf->options['send_authors_email_notifications'] = $send_authors_email_notifications;
			$this->conf->options['notification_email_address']       = strval( $_POST['notification_email_address'] );

			if ( ! filter_var( $this->conf->options['notification_email_address'], FILTER_VALIDATE_EMAIL ) ) {
				$this->conf->options['notification_email_address'] = '';
			}

			$widget_cap = strval( $_POST['dashboard_widget_capability'] );
			if ( ! empty( $widget_cap ) ) {
				$this->conf->options['dashboard_widget_capability'] = $widget_cap;
			}

			// Link actions. The user can hide some of them to reduce UI clutter.
			$show_link_actions = array();
			foreach ( array_keys( $available_link_actions ) as $action ) {
				$show_link_actions[ $action ] = isset( $_POST['show_link_actions'] ) && ! empty( $_POST['show_link_actions'][ $action ] );
			}
			$this->conf->set( 'show_link_actions', $show_link_actions );

			// Logging. The plugin can log various events and results for debugging purposes.
			$this->conf->options['logging_enabled']         = ! empty( $_POST['logging_enabled'] );
			$this->conf->options['custom_log_file_enabled'] = ! empty( $_POST['custom_log_file_enabled'] );

			if ( $this->conf->options['logging_enabled'] ) {
				if ( $this->conf->options['custom_log_file_enabled'] ) {

					$log_file = strval( $cleanPost['log_file'] );
					if ( ! file_exists( $log_file ) ) {
						if ( ! file_exists( dirname( $log_file ) ) ) {
							mkdir( dirname( $log_file ), 0750, true );
						}
						// Attempt to create the log file if not already there.
						if ( ! is_file( $log_file ) ) {
							// Add a .htaccess to hide the log file from site visitors.
							file_put_contents( dirname( $log_file ) . '/.htaccess', 'Deny from all' );
							file_put_contents( $log_file, '' );
						}
					}
					// revert to default
					if ( ! is_writable( $log_file ) || ! is_file( $log_file ) ) {
						$this->conf->options['custom_log_file_enabled'] = '';
						$log_directory                                  = self::get_default_log_directory();
						$log_file                                       = $log_directory . '/' . self::get_default_log_basename();
					}
				} else {
					// Default log file is /wp-content/uploads/broken-link-checker/blc-log.txt
					$log_directory = self::get_default_log_directory();
					$log_file      = $log_directory . '/' . self::get_default_log_basename();

					// Attempt to create the log directory.
					if ( ! is_dir( $log_directory ) ) {
						if ( mkdir( $log_directory, 0750 ) ) {
							// Add a .htaccess to hide the log file from site visitors.
							file_put_contents( $log_directory . '/.htaccess', 'Deny from all' );
						}
					}
				}

				$this->conf->options['log_file']     = $log_file;
				$this->conf->options['clear_log_on'] = strval( $cleanPost['clear_log_on'] );

				// Attempt to create the log file if not already there.
				if ( ! is_file( $log_file ) ) {
					file_put_contents( $log_file, '' );
				}

				// The log file must be writable.
				if ( ! is_writable( $log_file ) || ! is_file( $log_file ) ) {
					$this->conf->options['logging_enabled'] = false;
				}
			}

			// Make settings that affect our Cron events take effect immediately
			$this->core->setup_cron_events();
			$this->conf->save_options();

			/*
			If the list of custom fields was modified then we MUST resynchronize or
			custom fields linked with existing posts may not be detected. This is somewhat
			inefficient.
			*/
			if ( ( count( $diff1 ) > 0 ) || ( count( $diff2 ) > 0 ) ) {
				$manager = blcContainerHelper::get_manager( 'custom_field' );
				if ( ! is_null( $manager ) ) {
					$manager->resynch();
					blc_got_unsynched_items();
				}
			}

			/*
			If the list of acf fields was modified then we MUST resynchronize or
			acf fields linked with existing posts may not be detected. This is somewhat
			inefficient.
			*/
			if ( ( count( $acf_fields_diff1 ) > 0 ) || ( count( $acf_fields_diff2 ) > 0 ) ) {
				$manager = blcContainerHelper::get_manager( 'acf_field' );
				if ( ! is_null( $manager ) ) {
					$manager->resynch();
					blc_got_unsynched_items();
				}
			}

			// Resynchronize posts when the user enables or disables post statuses.
			if ( $post_statuses_changed ) {
				$overlord                        = blcPostTypeOverlord::getInstance();
				$overlord->enabled_post_statuses = $this->conf->get( 'enabled_post_statuses', array() );
				$overlord->resynch( 'wsh_status_resynch_trigger' );

				blc_got_unsynched_items();
				blc_cleanup_instances();
				blc_cleanup_links();
			}

			// Redirect back to the settings page
			$base_url = remove_query_arg( array( '_wpnonce', 'noheader', 'updated', 'error', 'action', 'message' ) );
			wp_redirect(
				add_query_arg(
					array(
						'settings-updated' => true,
					),
					$base_url
				)
			);
		}

		require BLC_DIRECTORY . '/core/core/template/admin-pages.php';

		// The various JS for this page is stored in a separate file for the purposes readability.
		include dirname( $this->core->loader ) . '/includes/admin/options-page-js.php';
	}


	/**
	 * Display the "Broken Links" page, listing links detected by the plugin and their status.
	 *
	 * @return void
	 */
	public function display_links_page() {
		require BLC_DIRECTORY . '/core/core/links-page.php';
		$links_page = new Links_Page( $this->core, $this->conf );
	}
	/**
	 * Enqueue CSS file for the plugin's Settings page.
	 *
	 * @return void
	 */
	function options_page_css() {
		wp_enqueue_style( 'blc-options-page', plugins_url( 'css/options-page.css', BLC_PLUGIN_FILE ), array(), '20141113' );
		wp_enqueue_style( 'dashboard' );
	}
	/**
	 * Enqueue CSS files for the "Broken Links" page
	 *
	 * @return void
	 */
	function links_page_css() {
		wp_enqueue_style( 'blc-links-page', plugins_url( 'css/links-page.css', $this->core->loader ), array(), '20141113-2' );
	}

	/**
	 * Enqueue settings script.
	 */
	public function enqueue_settings_scripts() {
		// jQuery UI is used on the settings page.
		wp_enqueue_script( 'jquery-ui-core' );   // Used for background color animation.
		wp_enqueue_script( 'jquery-ui-dialog' );
		wp_enqueue_script( 'jquery-ui-tabs' );
		wp_enqueue_script( 'jquery-cookie', plugins_url( 'js/jquery.cookie.js', BLC_PLUGIN_FILE ), array(), '1.0.0', false ); // Used for storing last widget states, etc.
	}

	/**
	 * Enqueue linkpage script.
	 */
	public function enqueue_link_page_scripts() {
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-dialog' ); // Used for the search form.
		wp_enqueue_script( 'jquery-color' );     // Used for background color animation.
		wp_enqueue_script( 'sprintf', plugins_url( 'js/sprintf.js', BLC_PLUGIN_FILE ), array(), '1.0.0', false ); // Used in error messages.
	}


	/**
	 * Add extra settings to the "Custom fields" entry on the plugin's config. page.
	 *
	 * Callback for the 'blc-module-settings-custom_field' filter.
	 *
	 * @param string $html Current extra HTML
	 * @param array  $current_settings The current plugin configuration.
	 * @return string New extra HTML.
	 */
	function make_custom_field_input( $html, $current_settings ) {
		$html .= '<span class="description">' .
					__(
						'Enter the names of custom fields you want to check (one per line). If a field contains HTML code, prefix its name with <code>html:</code>. For example, <code>html:field_name</code>.',
						'broken-link-checker'
					) .
				'</span>';
		$html .= '<br><textarea name="blc_custom_fields" id="blc_custom_fields" cols="45" rows="4">';
		if ( isset( $current_settings['custom_fields'] ) ) {
			$html .= esc_textarea( implode( "\n", $current_settings['custom_fields'] ) );
		}
		$html .= '</textarea>';

		return $html;
	}
	function make_acf_field_input( $html, $current_settings ) {
		$html .= '<span class="description">' . __( 'Enter the keys of acf fields you want to check (one per line). If a field contains HTML code, prefix its name with <code>html:</code>. For example, <code>html:field_586a3eaa4091b</code>.', 'broken-link-checker' ) . '</span>';
		$html .= '<br><textarea name="blc_acf_fields" id="blc_acf_fields" cols="45" rows="4">';
		if ( isset( $current_settings['acf_fields'] ) ) {
			$html .= esc_textarea( implode( "\n", $current_settings['acf_fields'] ) );
		}
		$html .= '</textarea>';

		return $html;
	}


		/**
		 * Initiate a full recheck - reparse everything and check all links anew.
		 *
		 * @return void
		 */
	public function initiate_recheck() {
		global $wpdb; // wpdb.

		// Delete all discovered instances.
		$wpdb->query( "TRUNCATE {$wpdb->prefix}blc_instances" ); //phpcs:ignore

		// Delete all discovered links.
		$wpdb->query( "TRUNCATE {$wpdb->prefix}blc_links" ); //phpcs:ignore

		// Mark all posts, custom fields and bookmarks for processing.
		blc_resynch( true );
	}

		/**
		 * Promote all links with the "warning" status to "broken".
		 */
	private function promote_warnings_to_broken() {
		global $wpdb; /** @var wpdb $wpdb */
		$wpdb->update(
			$wpdb->prefix . 'blc_links',
			array(
				'broken'  => 1,
				'warning' => 0,
			),
			array(
				'warning' => 1,
			),
			'%d'
		);
	}
	protected static function get_default_log_directory() {
		$uploads = wp_upload_dir();
		return $uploads['basedir'] . '/broken-link-checker';
	}

		/**
		 * Collect various debugging information and return it in an associative array
		 *
		 * @return array
		 */
	function get_debug_info() {
		/** @var wpdb $wpdb */
		global $wpdb;

		// Collect some information that's useful for debugging
		$debug = array();

		// PHP version. Any one is fine as long as WP supports it.
		$debug[ __( 'PHP version', 'broken-link-checker' ) ] = array(
			'state' => 'ok',
			'value' => phpversion(),
		);

		// MySQL version
		$debug[ __( 'MySQL version', 'broken-link-checker' ) ] = array(
			'state' => 'ok',
			'value' => $wpdb->db_version(),
		);

		// CURL presence and version
		if ( function_exists( 'curl_version' ) ) {
			$version = curl_version();

			if ( version_compare( $version['version'], '7.16.0', '<=' ) ) {
				$data = array(
					'state'   => 'warning',
					'value'   => $version['version'],
					'message' => __( 'You have an old version of CURL. Redirect detection may not work properly.', 'broken-link-checker' ),
				);
			} else {
				$data = array(
					'state' => 'ok',
					'value' => $version['version'],
				);
			}
		} else {
			$data = array(
				'state' => 'warning',
				'value' => __( 'Not installed', 'broken-link-checker' ),
			);
		}
		$debug[ __( 'CURL version', 'broken-link-checker' ) ] = $data;

		// Snoopy presence
		if ( class_exists( 'WP_Http' ) || file_exists( ABSPATH . WPINC . '/class-http.php' ) ) {
			$data = array(
				'state' => 'ok',
				'value' => __( 'Installed', 'broken-link-checker' ),
			);
		} else {
			// No Snoopy? This should never happen, but if it does we *must* have CURL.
			if ( function_exists( 'curl_init' ) ) {
				$data = array(
					'state' => 'ok',
					'value' => __( 'Not installed', 'broken-link-checker' ),
				);
			} else {
				$data = array(
					'state'   => 'error',
					'value'   => __( 'Not installed', 'broken-link-checker' ),
					'message' => __( 'You must have either CURL or Snoopy installed for the plugin to work!', 'broken-link-checker' ),
				);
			}
		}
		$debug['Snoopy'] = $data;

		// Safe_mode status
		if ( blcUtility::is_safe_mode() ) {
			$debug['Safe mode'] = array(
				'state'   => 'warning',
				'value'   => __( 'On', 'broken-link-checker' ),
				'message' => __( 'Redirects may be detected as broken links when safe_mode is on.', 'broken-link-checker' ),
			);
		} else {
			$debug['Safe mode'] = array(
				'state' => 'ok',
				'value' => __( 'Off', 'broken-link-checker' ),
			);
		}

		// Open_basedir status
		if ( blcUtility::is_open_basedir() ) {
			$debug['open_basedir'] = array(
				'state'   => 'warning',
				'value'   => sprintf( __( 'On ( %s )', 'broken-link-checker' ), ini_get( 'open_basedir' ) ),
				'message' => __( 'Redirects may be detected as broken links when open_basedir is on.', 'broken-link-checker' ),
			);
		} else {
			$debug['open_basedir'] = array(
				'state' => 'ok',
				'value' => __( 'Off', 'broken-link-checker' ),
			);
		}

		// Default PHP execution time limit
		$debug['Default PHP execution time limit'] = array(
			'state' => 'ok',
			'value' => sprintf( __( '%s seconds' ), ini_get( 'max_execution_time' ) ),
		);

		// Database character set. Usually it's UTF-8. Setting it to something else can cause problems
		// unless the site owner really knows what they're doing.
		$charset = $wpdb->get_charset_collate();
		$debug[ __( 'Database character set', 'broken-link-checker' ) ] = array(
			'state' => 'ok',
			'value' => ! empty( $charset ) ? $charset : '-',
		);

		// Resynch flag.
		$debug['Resynch. flag'] = array(
			'state' => 'ok',
			'value' => sprintf( '%d', $this->conf->options['need_resynch'] ? '1 (resynch. required)' : '0 (resynch. not required)' ),
		);

		// Synch records
		$synch_records = intval( $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}blc_synch" ) );
		$data          = array(
			'state' => 'ok',
			'value' => sprintf( '%d', $synch_records ),
		);
		if ( 0 === $synch_records ) {
			$data['state']   = 'warning';
			$data['message'] = __( 'If this value is zero even after several page reloads you have probably encountered a bug.', 'broken-link-checker' );
		}
		$debug['Synch. records'] = $data;

		// Total links and instances (including invalid ones)
		$all_links     = intval( $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}blc_links" ) );
		$all_instances = intval( $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}blc_instances" ) );

		// Show the number of unparsed containers. Useful for debugging. For performance,
		// this is only shown when we have no links/instances yet.
		if ( ( 0 == $all_links ) && ( 0 == $all_instances ) ) {
			$unparsed_items          = intval( $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}blc_synch WHERE synched=0" ) );
			$debug['Unparsed items'] = array(
				'state' => 'warning',
				'value' => $unparsed_items,
			);
		}

		// Links & instances
		if ( ( $all_links > 0 ) && ( $all_instances > 0 ) ) {
			$debug['Link records'] = array(
				'state' => 'ok',
				'value' => sprintf( '%d (%d)', $all_links, $all_instances ),
			);
		} else {
			$debug['Link records'] = array(
				'state' => 'warning',
				'value' => sprintf( '%d (%d)', $all_links, $all_instances ),
			);
		}

		// Email notifications.
		if ( $this->conf->options['last_notification_sent'] ) {
			$notificationDebug = array(
				'value' => date( 'Y-m-d H:i:s T', $this->conf->options['last_notification_sent'] ), //phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
				'state' => 'ok',
			);
		} else {
			$notificationDebug = array(
				'value' => 'Never',
				'state' => $this->conf->options['send_email_notifications'] ? 'ok' : 'warning',
			);
		}
		$debug['Last email notification'] = $notificationDebug;

		if ( isset( $this->conf->options['last_email'] ) ) {
			$email                    = $this->conf->options['last_email'];
			$debug['Last email sent'] = array(
				'state' => 'ok',
				'value' => sprintf(
					'"%s" on %s (%s)',
					htmlentities( $email['subject'] ),
					date( 'Y-m-d H:i:s T', $email['timestamp'] ), //phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date
					$email['success'] ? 'success' : 'failure'
				),
			);
		}

		// Installation log
		$logger           = new blcCachedOptionLogger( 'blc_installation_log' );
		$installation_log = $logger->get_messages();
		if ( ! empty( $installation_log ) ) {
			$debug['Installation log'] = array(
				'state' => $this->conf->options['installation_complete'] ? 'ok' : 'error',
				'value' => implode( "<br>\n", $installation_log ),
			);
		} else {
			$debug['Installation log'] = array(
				'state' => 'warning',
				'value' => 'No installation log found found.',
			);
		}

		return $debug;
	}
		/**
		 * Output a list of modules and their settings.
		 *
		 * Each list entry will contain a checkbox that is checked if the module is
		 * currently active.
		 *
		 * @param array $modules Array of modules to display
		 * @param array $current_settings
		 * @return void
		 */
	function print_module_list( $modules, $current_settings ) {
		$moduleManager = blcModuleManager::getInstance();

		foreach ( $modules as $module_id => $module_data ) {
			$module_id = $module_data['ModuleID'];

			$style = $module_data['ModuleHidden'] ? ' style="display:none;"' : '';

			printf(
				'<div class="module-container" id="module-container-%s"%s>',
				$module_id,
				$style
			);
			$this->print_module_checkbox( $module_id, $module_data, $moduleManager->is_active( $module_id ) );

			$extra_settings = apply_filters(
				'blc-module-settings-' . $module_id,
				'',
				$current_settings
			);

			if ( ! empty( $extra_settings ) ) {

				printf(
					' | <a class="blc-toggle-link toggle-module-settings" id="toggle-module-settings-%s" href="#">%s</a>',
					esc_attr( $module_id ),
					__( 'Configure', 'broken-link-checker' )
				);

				// The plugin remembers the last open/closed state of module configuration boxes
				$box_id = 'module-extra-settings-' . $module_id;
				$show   = blcUtility::get_cookie(
					$box_id,
					$moduleManager->is_active( $module_id )
				);

				printf(
					'<div class="module-extra-settings%s" id="%s">%s</div>',
					$show ? '' : ' hidden',
					$box_id,
					$extra_settings
				);
			}

			echo '</div>';
		}
	}
	protected static function get_default_log_basename() {
		return 'blc-log.txt';
	}


		/**
		 * Output a checkbox for a module.
		 *
		 * Generates a simple checkbox that can be used to mark a module as active/inactive.
		 * If the specified module can't be deactivated (ModuleAlwaysActive = true), the checkbox
		 * will be displayed in a disabled state and a hidden field will be created to make
		 * form submissions work correctly.
		 *
		 * @param string $module_id Module ID.
		 * @param array  $module_data Associative array of module data.
		 * @param bool   $active If true, the newly created checkbox will start out checked.
		 * @return void
		 */
	function print_module_checkbox( $module_id, $module_data, $active = false ) {
		$disabled    = false;
		$name_prefix = 'module';
		$label_class = '';
		$active      = $active || $module_data['ModuleAlwaysActive'];

		if ( $module_data['ModuleAlwaysActive'] ) {
			$disabled    = true;
			$name_prefix = 'module-always-active';
		}

		$checked = $active ? ' checked="checked"' : '';
		if ( $disabled ) {
			$checked .= ' disabled="disabled"';
		}

		printf(
			'<label class="%s">
					<input type="checkbox" name="%s[%s]" id="module-checkbox-%s"%s /> %s
				</label>',
			esc_attr( $label_class ),
			$name_prefix,
			esc_attr( $module_id ),
			esc_attr( $module_id ),
			$checked,
			$module_data['Name']
		);

		if ( $module_data['ModuleAlwaysActive'] ) {
			printf(
				'<input type="hidden" name="module[%s]" value="on">',
				esc_attr( $module_id )
			);
		}
	}
}
