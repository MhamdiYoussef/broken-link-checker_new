# Broken Link checker

## Development:
1. Clone this repo into your local server's plugins folder: `git clone git@bitbucket.org:incsub/broken-link-checker`
2. Run: `npm install` to install development dependencies.
3. Run: `php composer.phar install` to build autoloading. **Do not** submit `composer.phar` into repository.

## Compiling Assets


Use: `npm run compile-js` to compile admin JavaScript.

# Creating branches
The main development branch is the `dev` branch. All the development work should be done on this branch.

For better development flow create a new branch for every task and after completion merge into the development branch.

Naming conventions for other branches are mandatory for better sorting and management. You can name the branches according to the type of the task. Use:

*`new/some_new_feature` for new features
* fix/some_fix`bug fixes
* add/some_small_update for smaller improvements to existing features

# Tagging
For tagging after a release tag the release branches as `ReleaseVersion` eg:

`git tag -a 4.10.0 -m "Tagged release/4.10.0"`

## Packaging Instructions:
1. Update plugin version _(include beta version)_ in the following files: `broken-link-checker.php` and `package.json`.
2. Run `npm run build` to build files and zip free and pro versions.
3. Find zip files inside `/builds` folder.
4. **IMPORTANT:** Before releasing make sure to **remove beta version** and re-pack. Give a final test before releasing to make sure zip files are fine.

## To Have In Mind (Before Release):
1. Go all over steps on **Packaging Instructions** and pack beta version, for example: `1.0.1-BETA1`
2. If didn't pass, fix issues.
3. Go all over steps on **Packaging Instructions** and pack new beta `1.0.1-BETA2`
4. If passed, pack final version `1.0.1` _(notice it doesn't include beta number)_
5. Test before releasing to make sure zips are fine and work.

## Getting to know Broken Link Checker

Some notes that might help developer onboarding.

### Get list or count of links of specific status
`includes/link-query.php` contains a function on the end of file `blc_get_links`. Check function doc for more info.
Useful for getting broken links count and so on.

There is only one way to start checking or re-checking links, that is to call `work()` function in the same class.
This is the main method that goes through the links, it has two show stops, one is when server is over certain load, the other is
when process has been running and needs to sleep for a bit to maintain ratio of running as set by settings.
