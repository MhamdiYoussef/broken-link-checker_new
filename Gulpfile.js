'use strict';

// Import `src` and `dest` from gulp for use in the task.
const { src, dest } = require( 'gulp' );

// ==================================================
// Supported Packages
var fs      = require( 'fs' ),
	pump    = require( 'pump' ),
	cleaner = require( 'del' )
	;

var gulp         = require( 'gulp' ),
	sass         = require( 'gulp-sass' ),
	autoprefixer = require( 'gulp-autoprefixer' ),
	cleanCSS     = require( 'gulp-clean-css' ),
	clean        = require( 'gulp-clean' ),
	eslint       = require( 'gulp-eslint' ),
	uglify       = require( 'gulp-uglify-es' ).default,
	sourcemaps   = require( 'gulp-sourcemaps' ),
	concat       = require( 'gulp-concat' ),
	rename       = require( 'gulp-rename' ),
	replace      = require( 'gulp-replace' ),
	notify       = require( 'gulp-notify' ),
	wpPot        = require( 'gulp-wp-pot' ),
	zip          = require( 'gulp-zip' ),
	babel        = require( 'gulp-babel' )
	;

// Get package.json file
var pckg = JSON.parse( fs.readFileSync( './package.json' ) );

// ==================================================
// Variables
// List of browsers
var browserlist = [
	'last 2 version',
	'> 1%'
];

// Localize strings
var strings = [
	'broken-link-checker.php',
	'uninstall.php',
	'core/*.php',
	'includes/**/*.php',
	'includes/*.php',
    'modules/**/*.php',
    'modules/*.php',
	'idn/*.php'
];

// ==================================================
// Paths

// Main locations
var folder = {
	js:     'assets/js/',
	css:    'assets/css/',
	scss:   'assets/scss/',
	lang:   'languages/',
	builds: 'builds/',
};

// Admin styles
var admin = [
	folder.scss + 'blc-admin.scss'
];

// Admin scripts
var adminJS = [
	folder.js + '/src/*.js'
];


// BLC Package list
var blc = [
	'*',
	'**',
	'!.git',
	'!.gitattributes',
	'!.gitignore',
	'!.gitmodules',
	'!.sass-cache',
	'!DS_Store',
	'!bitbucket-pipelines.yml',
	'!composer.json',
	'!composer.lock',
	'!composer.phar',
	'!createzip.bat',
	'!createzip.sh',
	'!package.json',
	'!package-lock.json',
	'!webpack.config.js',
	'!postcss.config.js',
	'!Gulpfile.js',
	'!README.md',
	'!.vscode/*',
	'!.vscode',
	'!builds/**',
	'!builds/*',
	'!builds',
	'!node_modules/**',
	'!node_modules/*',
	'!node_modules',
	'!nbproject',
	'!nbproject/*',
	'!nbproject/**',
	'!phpcs.ruleset.xml'
];

var uglifyOptions = {
	compress: {
		drop_console: true
	}
};
// ==================================================
// Packaging Tasks

// Task: Create language files
gulp.task( 'makepot', function() {

	return gulp.src( strings )
		.pipe( wpPot({
			package: 'Broken Link Checker ' + pckg.version
		}) )
		.pipe( gulp.dest( folder.lang + 'broken-link-checker.pot' ) )
		.pipe( notify({
			message: 'Localized strings extracted',
			onLast: true
		}) )
		;
});

// Task: Compress hustle-pro
gulp.task( 'blc:package', function() {

	var name    = pckg.name,
		version = pckg.version,
		url     = pckg.homepage,
		file    = name + '-' + version + '.zip'
		;

	var rep_args = { skipBinary: true };

	// Clean up existing zip file
	cleaner( folder.builds + file, { force: true } );

	return gulp.src( blc, { base: '../' } )
		.pipe( zip( file ) )
		.pipe( gulp.dest( folder.builds ) )
		.pipe( notify({
			message: 'Broken Link Checker ' + version + ' compressed',
			onLast: true
		}) );
} );

// Task: Build admin styles
gulp.task( 'admin:styles', function( cb ) {

	gulp.src( admin )
		.pipe(
			sass({ outputStyle: 'compressed' })
			.on( 'error', sass.logError )
		)
		.pipe( autoprefixer( browserlist ) )
		.pipe( cleanCSS() )
		.pipe( rename({
			suffix: '.min'
		}) )
		.pipe( gulp.dest( folder.css ) )
		.pipe( notify({
			message: 'Admin styles are ready',
			onLast: true
		}) )
		;
    cb();
});

// Task: Build admin scripts
gulp.task( 'admin:scripts', function( cb ) {

	pump([
		gulp.src( adminJS ),
		babel({
			presets: [
				[ '@babel/env', {
					modules: false
				} ]
			]
		}),
		concat( 'blc-admin.js' ),
		uglify( uglifyOptions ),
		rename({
			suffix: '.min'
		}),
		gulp.dest( folder.js ),
	], cb, err => {
		if (err) {
			notify().write(err);
		}
		cb();
	});
});

// Task: Watch for changes across project
gulp.task( 'watch', function() {

	// Watch for admin styles changes
	//gulp.watch( admin, [ 'admin:styles' ]);

	// Watch for admin js changes
	//gulp.watch( adminJS, [ 'admin:scripts' ]);
    gulp.watch( [
        folder.scss + '*.scss',
        folder.scss + '**/*.scss'
    ], gulp.series(
        'admin:styles'
    ));

});

// Task: Build admin files
gulp.task( 'build:admin', gulp.series(
	'admin:styles',
	'admin:scripts'
));

// Task: Run development environment
gulp.task( 'dev', gulp.series( 'build:admin' ) );

// Task: Pack plugin
gulp.task( 'build', gulp.series(
	'makepot',
	'blc:package'
)
);
