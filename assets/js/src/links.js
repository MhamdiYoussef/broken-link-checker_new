( function( $ ) {
    // Toggle link page advance filter
    function toggle_filter( click_item, open_item ) {
        $( click_item ).click(function () {
            $( open_item ).toggle(200);
        });
    }

    // Check & open edit url box
    function edit_url_section_opener( selector, open_item ) {
        $( selector ).change(function () {
            if ('edit_url' == $(this).val()) {
                $( open_item ).addClass('visible');
            } else {
                $( open_item ).removeClass('visible');
            }
        });
    }

    toggle_filter( '#blc-advance-issue-filter-open', '#blc-advance-issue-filter' );
    toggle_filter( '#blc-advance-redirect-filter-open', '#blc-advance-redirect-filter' );
    toggle_filter( '#blc-advance-ignored-filter-open', '#blc-advance-ignored-filter' );
    toggle_filter( '#blc-advance-all-filter-open', '#blc-advance-all-filter' );

    edit_url_section_opener( '.blc-filter-horizontal #blc-filter-id', '.blc-bulk-url-edit-wrap' );
    edit_url_section_opener( '.blc-filter-horizontal #blc-redirect-filter-id', '.blc-bulk-url-edit-wrap' );
    edit_url_section_opener( '.blc-filter-horizontal #blc-ignored-filter-id', '.blc-bulk-url-edit-wrap' );
    edit_url_section_opener( '.blc-filter-horizontal #blc-all-filter-id', '.blc-bulk-url-edit-wrap' );

} )( jQuery );