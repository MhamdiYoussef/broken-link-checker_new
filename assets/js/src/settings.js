( function( $ ) {

    // Initiate SUI ace editor for elm
    function sui_ace_editor( id ) {
        if ( $( '#'+ id ).length ) {
            var editor = ace.edit(id);

            editor.getSession().setUseWorker( false );
            editor.setShowPrintMargin( false );

            editor.setTheme( "ace/theme/sui" );
            editor.getSession().setMode( "ace/mode/css" );

            editor.session.setTabSize( 4 );
        }
    }

    sui_ace_editor( 'sui-ace-editor' );

} )( jQuery );