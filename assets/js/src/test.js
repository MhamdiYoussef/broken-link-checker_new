jQuery( window.document ).ready(function($){
    "use strict";
    const suiTabs = {
        handleTabs: ( target ) => {
            let container = $(target).closest('.sui-wrap');
            let tabs = $('.sui-sidenav li', container );
            let tab = $(target).data('tab');
            let content, current;
            let url = window.location.href;
            let re = /module=[^&]+/;

            if ( 'undefined' === typeof tab ) {
                return;
            }
            if ( tab === window.blc_current_tab ) {
                return;
            }

            window.blc_current_tab = tab;
            content = $('.sui-box[data-tab]');
            current = $('.sui-box[data-tab="' + tab + '"]');
            if ( url.match( re ) ) {
                url = url.replace( re, 'module=' + tab );
            } else {
                url += '&module=' + tab;
            }
            window.history.pushState( { module: tab }, 'BLC', url );
            tabs.removeClass( 'current' );
            content.hide();
            $(target).parent().addClass( 'current' );
            current.show();
            return false;
        },

        handleUrl: () => {
        }
    };

    $('.sui-wrap .sui-sidenav .sui-vertical-tab a').on( 'click', function() {
        suiTabs.handleTabs( this );
    } );

    $('select').each( function() {
        console.log( SUI );
       SUI.suiSelect( this );
    } );

});

