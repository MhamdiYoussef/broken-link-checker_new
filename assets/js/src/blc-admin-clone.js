/**
 * NEW JS Functions 
 */
(function (document, $, undefined) {
  $("#wdblc-run-check-btn").on("click", function (event) {
    event.preventDefault();
    $(this).find(".wdblc-text").addClass("sui-hidden");
    $(this).find(".sui-progress-icon").removeClass("sui-hidden");
    $(this).attr("disabled", "disabled");
    $(".wdblc-last-checked-date").addClass("sui-hidden");
    $(".wdblc-last-checked-progress").removeClass("sui-hidden");
    var checkBtn = this;
    var data = {
      action: "blc_work",
      _wpnonce: BLC.work_nonce,
      reloading: false, // in case auto-reload
      delay: 3, // in seconds
    }
    jQuery.post(BLC.ajax_url, data, function (response) {
      if (response.data && response.data.error) {
        SUI.openNotice("ntc-confirm", $("#ntc-" + response.data.error).html(), {
          type: "error",
          icon: "cross-close",
        });
      } else {
        SUI.openNotice("ntc-confirm",
          $("#ntc-work-done").html(), {
            type: "info",
            icon: "info",
            autoclose: false,
          });
        $(checkBtn).find(".wdblc-text").removeClass("sui-hidden");
        $(checkBtn).find(".sui-progress-icon").addClass("sui-hidden");
        $(checkBtn).removeAttr("disabled");
        $(".wdblc-last-checked-date").removeClass("sui-hidden");
        $(".wdblc-last-checked-progress").addClass("sui-hidden");
      }
      if (data.reloading) {
        if (response == "Connection closed") {
          setTimeout(function () {
            location.reload();
          }, data.delay * 1000);
        }
      }
    });
  });

})(document, jQuery);

function reloardPage($ = jQuery) {
  location.reload();
  $("#wdblc-reload-page-btn").css("height", "30px");
  $("#wdblc-btn-text").addClass("sui-hidden");
  $(".sui-icon-undo").addClass("sui-hidden");
  $(".sui-progress-icon").removeClass("sui-hidden");
}
