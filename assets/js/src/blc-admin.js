;import '@wpmudev/shared-ui';
import '@wpmudev/shared-ui/dist/js/_src/modal-dialog';
import '@wpmudev/shared-ui/dist/js/_src/accordion';
import './links.js';
import './settings.js';

(function(document, $, undefined) {

	const focusAfterClosed = document.getElementById('wpbody'),
		focusWhenOpen    = undefined,
		hasOverlayMask   = false,
		isCloseOnEsc     = false
	;

	window.BLC = window.BLC || {};
	window.BLC.modal = function(i) {
		let modalId = $(modals[i]).attr('id');
		SUI.openModal(
			modalId,
			focusAfterClosed,
			focusWhenOpen,
			hasOverlayMask,
			isCloseOnEsc
		);
	};
	var onboardingDone = BLC.onboarding_done || false;
	if ( ! onboardingDone ) {
		let onboardingModal = $('#wdblc-onboarding-modal');
		if ( onboardingModal.size()) {
			SUI.openModal(
				'wdblc-onboarding-modal',
				$('#wpbody-content h1'),
				undefined,
				false,
				true
			);
		}
	}

	// Handle tabbed side navigation.
	let els = jQuery('.sui-vertical-tab')
	els.on('click', function(event) {
		event.preventDefault();
		$(els).removeClass('current');
		$(this).addClass('current');
		$('.blc-tab-content').hide();
		let tab = $(this).find('a').data('tab');
		$('#' + tab).show();
	});

	// Handle link actions.
	// Show edit form
	$('.wdblc-action-edit').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		par.trigger('click');
		par.next().find('.wdblc-edit-link-tab').trigger('click');
	});

	// Update link from edit form
	$('.wdblc-update-link-btn').on('click', function(event) {
		event.preventDefault();
		var linkid = $(this).data('wdblc-linkid');
		var url = $(this).parents('tr').find('.wdblc-edit-link-url-input');
		var text = $(this).parents('tr').find('.wdblc-edit-link-text-input');
		var data = {
			'action': 'blc_edit',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
			'new_url': url.val(),
			'new_text': text.val()
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-redirected').html(),
					{ type: "success", icon: "check-tick" }
				);
			}
		});
	});
	// Re-check link.
	$('.wdblc-recheck-link-btn').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		var isError = false;
		var isOk = false;
		if (par.hasClass('sui-error')) {
			isError = true;
		}
		if (par.hasClass('sui-ok')) {
			isOk = true;
		}

		if ( isOk ) {
			par.removeClass('sui-ok');
			par.addClass('sui-default');
		} else if ( isError ) {
			par.removeClass('sui-error');
			par.addClass('sui-default');
		}
		var linkid = par.find('[name=wdblc-link-id]').val();
		var btnIcon = $(this).find('.sui-icon-undo');
		btnIcon.addClass('sui-hidden');
		var loadingTag = $(this).find('.wdblc-loading');
		loadingTag.removeClass('sui-hidden');
		var data = {
			'action': 'blc_recheck',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-rechecked').html(),
					{ type: "success", icon: "check-tick" }
				);
			}
			loadingTag.addClass('sui-hidden');
			btnIcon.removeClass('sui-hidden');
		});
	});
	$('.wdblc-action-recheck').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		par.css('background-color', '#f8f8f8');

		var isError = false;
		var isOk = false;
		if (par.hasClass('sui-error')) {
			isError = true;
		}
		if (par.hasClass('sui-ok')) {
			isOk = true;
		}

		if ( isOk ) {
			par.removeClass('sui-ok');
			par.addClass('sui-default');
		} else if ( isError ) {
			par.removeClass('sui-error');
			par.addClass('sui-default');
		}

		var linkid = par.find('[name=wdblc-link-id]').val();
		var statusTag = par.find('.wdblc-status-code');
		statusTag.addClass('sui-hidden');
		var loadingTag = par.find('.wdblc-loading');
		loadingTag.removeClass('sui-hidden');
		var data = {
			'action': 'blc_recheck',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-rechecked').html(),
					{ type: "success", icon: "check-tick" }
				);
			}
			loadingTag.addClass('sui-hidden');
			statusTag.removeClass('sui-hidden');
			par.css('background-color', '');
			if ( isOk ) {
				par.addClass('sui-ok');
				par.removeClass('sui-default');
			} else if ( isError ) {
				par.addClass('sui-error');
				par.removeClass('sui-default');
			}
		});
	});
	// Ignore.
	$('.wdblc-action-ignore').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		par.css('background-color', '#f8f8f8');
		par.removeClass('sui-error');
		par.addClass('sui-default');
		var linkid = par.find('[name=wdblc-link-id]').val();
		var statusTag = par.find('.wdblc-status-code');
		statusTag.addClass('sui-hidden');
		var loadingTag = par.find('.wdblc-loading');
		loadingTag.removeClass('sui-hidden');
		var data = {
			'action': 'blc_dismiss',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-ignored').html(),
					{ type: "success", icon: "check-tick" }
				);
				par.next().remove();
				par.remove();
			}
			loadingTag.addClass('sui-hidden');
			statusTag.removeClass('sui-hidden');
			par.removeClass('sui-default');
			par.addClass('sui-error');
			par.css('background-color', '');
		});
	});
	$('.wdblc-ignore-link-btn').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		var linkid = par.find('[name=wdblc-link-id]').val();
		var btnIcon = $(this).find('.sui-icon-eye-hide');
		btnIcon.addClass('sui-hidden');
		var loadingTag = $(this).find('.wdblc-loading');
		loadingTag.removeClass('sui-hidden');
		var data = {
			'action': 'blc_dismiss',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
				btnIcon.removeClass('sui-hidden');
				loadingTag.addClass('sui-hidden');
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-ignored').html(),
					{ type: "success", icon: "check-tick" }
				);
				par.prev().remove();
				par.remove();
			}
			loadingTag.addClass('sui-hidden');
			statusTag.removeClass('sui-hidden');
			par.removeClass('sui-default');
			par.addClass('sui-error');
			par.css('background-color', '');
		});
	});
	// Unlink.
	$('.wdblc-action-unlink').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		par.css('background-color', '#f8f8f8');
		par.removeClass('sui-error');
		par.addClass('sui-default');
		var linkid = par.find('[name=wdblc-link-id]').val();
		var statusTag = par.find('.wdblc-status-code');
		statusTag.addClass('sui-hidden');
		var loadingTag = par.find('.wdblc-loading');
		loadingTag.removeClass('sui-hidden');
		var data = {
			'action': 'blc_unlink',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-unlinked').html(),
					{ type: "success", icon: "check-tick" }
				);
				par.next().remove();
				par.remove();
			}
		});
	});
	// Mark not broken.
	$('.wdblc-action-mark-not-broken').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		par.css('background-color', '#f8f8f8');
		par.removeClass('sui-error');
		par.addClass('sui-default');
		var linkid = par.find('[name=wdblc-link-id]').val();
		var statusTag = par.find('.wdblc-status-code');
		statusTag.addClass('sui-hidden');
		var loadingTag = par.find('.wdblc-loading');
		loadingTag.removeClass('sui-hidden');
		var data = {
			'action': 'blc_discard',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-marked-not-broken').html(),
					{ type: "success", icon: "check-tick" }
				);
				par.next().remove();
				par.remove();
			}
		});
	});
	// Restore ignored link.
	$('.wdblc-restore-link-btn').on('click', function(event) {
		event.preventDefault();
		var par = $(this).parents('tr');
		par.css('background-color', '#f8f8f8');
		var linkid = par.find('[name=wdblc-link-id]').val();
		var statusTag = par.find('.wdblc-status-code');
		statusTag.addClass('sui-hidden');
		var loadingTag = par.find('.wdblc-loading');
		loadingTag.removeClass('sui-hidden');
		var data = {
			'action': 'blc_undismiss',
			'_wpnonce': BLC.links_nonce,
			'link_id': linkid,
		};
		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-link-dismissed').html(),
					{ type: "success", icon: "check-tick" }
				);
				par.remove();
			}
		});
	});

	// Handle actions on Dashboard page.
	$('#wdblc-open-email-settings').on('click', function(event) {
		event.preventDefault();
		window.location = window.location.pathname + '?page=brokenlinkchecker-links&tab=notifications';
	});
	$('#wdblc-open-issues-page, #wdblc-dash-view-links-btn').on('click', function(event) {
		event.preventDefault();
		window.location = window.location.pathname + '?page=brokenlinkchecker-links';
	});


	// Show selected tab content on page load.
	const urlParams = new URLSearchParams(window.location.search);
	if ( urlParams.get('tab') ) {
		$('a[data-tab=tab-' + urlParams.get('tab') + ']' ).trigger('click');
	}

	$('#wdblc-onboarding-form').submit(function( event ) {
		event.preventDefault();
		var form = $( this ).serializeArray();
		$('#wdblc-onboarding-save').addClass('sui-button-onload-text');

		var data = {
			'action': 'wdblc_save_onboarding',
		};

		$.each( form, function( i, field ) {
			data[field.name] = field.value;
		});

		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				const newSlideId     = 'broken-link-checker--onboarding--slide-4',
					newSlideFocus    = 'element-unique-id',
					newSlideEntrance = 'next'
				;

				SUI.slideModal( newSlideId, newSlideFocus, newSlideEntrance );
			}
		});
	});

	$('#wdblc-close-onboarding').on('click', function(event) {
		event.preventDefault();
		SUI.closeModal();
		SUI.openNotice(
			'ntc-confirm',
			$('#ntc-settings-saved').html(),
			{ type: "success", icon: "check-tick" }
		);
	});

	$('.wdblc-skip-onboarding').on('click', function(event) {
		event.preventDefault();
		SUI.closeModal();
		var data = {
			'action': 'wdblc_skip_onboarding',
			'_wpnonce': BLC.skip_onboarding_nonce,
		};

		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-onboarding-skipped').html(),
					{ type: "success", icon: "check-tick" }
				);
			}
		});
	});

	$('#wdblc-run-check-btn').on('click', function(event) {
		event.preventDefault();
		$(this).find('.wdblc-text').addClass('sui-hidden');
		$(this).find('.sui-progress-icon').removeClass('sui-hidden');
		$(this).attr('disabled', 'disabled');
		$('.wdblc-last-checked-date').addClass('sui-hidden');
		$('.wdblc-last-checked-progress').removeClass('sui-hidden');
		var checkBtn = this;
		var nonceRunCheckButton = $(this).attr('data_nonce'); 
		var data = {
			'action'   : 'blc_work', 
			'action'   : 'blc_run_check_button',
			'_wpnonce' : nonceRunCheckButton,
		};

		jQuery.post(BLC.ajax_url, data, function(response) {
			if (response.data && response.data.error) {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-' + response.data.error).html(),
					{ type: "error", icon: "cross-close" }
				);
			} else {
				SUI.openNotice(
					'ntc-confirm',
					$('#ntc-work-done').html(),
					{ type: "success", icon: "check-tick" }
				);
				$('#sui-summary-large-primary').html(response.data.broken);
				$('#sui-summary-detail-primary').html(response.data.total);	
				$('#last-checked-date').html(response.data.last_checked);	
				$(checkBtn).find('.wdblc-text').removeClass('sui-hidden');
				$(checkBtn).find('.sui-progress-icon').addClass('sui-hidden');
				$(checkBtn).removeAttr('disabled');
				$('.wdblc-last-checked-date').removeClass('sui-hidden');
				$('.wdblc-last-checked-progress').addClass('sui-hidden');
			}
		});
	});

	$( '#toggle-automatic-engine-on' ).on( 'change', function (event) {
		$( '#toggle-automatic-engine-on-content' ).toggle();
	} );
})(document, jQuery);