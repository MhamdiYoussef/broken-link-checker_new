<?php
/**
 * Broken Link Checker
 *
 * @link              https://wordpress.org/plugins/broken-link-checker/
 * @since             1.0.0
 * @package           broken-link-checker
 *
 * @wordpress-plugin
 * Plugin Name: Broken Link Checker
 * Plugin URI:  https://wordpress.org/plugins/broken-link-checker/
 * Description: Checks your blog for broken links and missing images and notifies you on the dashboard if any are found.
 * Version:     1.11.14-beta-1
 * Author:      WPMU DEV
 * Author URI:  https://premium.wpmudev.org/
 * Text Domain: broken-link-checker
 * License:     GPLv2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

/*
Broken Link Checker is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Broken Link Checker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Broken Link Checker. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

// Path to this file.
if ( ! defined( 'BLC_PLUGIN_FILE' ) ) {
	define( 'BLC_PLUGIN_FILE', __FILE__ );
}

// Path to the plugin's directory.
if ( ! defined( 'BLC_DIRECTORY' ) ) {
	define( 'BLC_DIRECTORY', trailingslashit( dirname( __FILE__ ) ) );
}

// Path to the Plugin Base File.
if ( ! defined( 'BLC_BASE_FILE' ) ) {
	define( 'BLC_BASE_FILE', plugin_basename( __FILE__ ) );
}

// Plugin URL.
if ( ! defined( 'BLC_URL' ) ) {
	define( 'BLC_URL', plugin_dir_url( BLC_BASE_FILE ) );
}

// Template directory.
if ( ! defined( 'BLC_TEMPLATE' ) ) {
	define( 'BLC_TEMPLATE', BLC_DIRECTORY . 'app/templates/' );
}

// SUI Version.
if ( ! defined( 'BLC_SUI_VERSION' ) ) {
	define( 'BLC_SUI_VERSION', 'sui-2-9-5' );
}
// Load the actual plugin.
require 'core/init.php';


// THIS IS THE NEW APP!!!
require 'app/bootstrap-app.php';
